VERSION = 2.3.0

BUILD_FLAGS ?= --release

WEB_FLAGS = --no-tree-shake-icons -O1

build: .dart_tool
	dart run build_runner build --delete-conflicting-outputs

.dart_tool: 
	flutter pub get
	dart compile js web/worker.dart -o web/worker.dart.js -O4 --no-source-maps
	rm web/worker.dart.js.deps
	flutter pub run pubspec_dependency_sorter

test_handler: build
	flutter test --coverage
	lcov --remove coverage/lcov.info 'lib/*/*.g.dart' 'lib/*/*.part.dart' 'lib/data/database/*' -o coverage/lcov.info
	flutter pub run test_cov_console --line
#	genhtml coverage/lcov.info -o coverage/html

test: test_handler

appimage: build release
	flutter build linux --dart-define=APPIMAGE=true $(BUILD_FLAGS)
	cp `find /usr/lib* -name libsqlite3.so` build/linux/*64/release/bundle/lib/
	cp `find /usr/lib* -name libjsoncpp.so` build/linux/*64/release/bundle/lib/
	cp `find /usr/lib* -name libsecret-1.so` build/linux/*64/release/bundle/lib/
	mkdir -p linux/build
	cd linux && ./appimage.sh ${VERSION}

flatpak: linux
	# flatpak/finance.reckoner.Reckoner.yaml
	flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
	flatpak install --noninteractive flathub org.flatpak.Builder org.freedesktop.Platform/$(shell arch)/23.08 org.freedesktop.Sdk/$(shell arch)/23.08 org.freedesktop.Sdk.Extension.vala/$(shell arch)/23.08

	yq -i -y ".modules[0].sources[0].path = \"../../release/reckoner-${VERSION}-linux-$(shell arch).tar.gz\"" linux/flatpak/finance.reckoner.Reckoner.yaml
	cd linux/flatpak &&	flatpak-builder --repo=repo build finance.reckoner.Reckoner.yaml --force-clean
	cd linux/flatpak && flatpak build-bundle repo reckoner-${VERSION}-$(shell arch).flatpak finance.reckoner.Reckoner 
	mv linux/flatpak/*.flatpak release/
	flatpak run --command=appstream-util org.flatpak.Builder validate linux/flatpak/finance.reckoner.Reckoner.appdata.xml

appstream:
	flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
	flatpak install --noninteractive flathub org.flatpak.Builder
	flatpak run --command=appstream-util org.flatpak.Builder validate linux/flatpak/finance.reckoner.Reckoner.appdata.xml

linux: build release
	flutter build linux $(BUILD_FLAGS)
	tar -czf "release/reckoner-${VERSION}-linux-$(shell arch).tar.gz" -C build/linux/*64/release bundle

background_fetch:
#	Changes needed to be compliant with F-Droid build
	rm -f $(HOME)/.pub-cache/hosted/pub.dev/background_fetch-1.3.7/android/libs/com/transistorsoft/tsbackgroundfetch/1.0.4/tsbackgroundfetch-1.0.4.aar
	export ANDROID_HOME=$(HOME)/Library/Android/sdk && cd submodules/transistor-background-fetch/android && gradle assembleRelease && gradle publishToMavenLocal
	mv submodules/transistor-background-fetch/android/tsbackgroundfetch/build/outputs/aar/tsbackgroundfetch-release.aar $(HOME)/.pub-cache/hosted/pub.dev/background_fetch-1.3.7/android/libs/com/transistorsoft/tsbackgroundfetch/1.0.4/tsbackgroundfetch-1.0.4.aar

apk: build background_fetch release
	flutter build apk --split-per-abi --no-tree-shake-icons --no-obfuscate --target-platform android-arm,android-arm64  $(BUILD_FLAGS)
	mv build/app/outputs/apk/release/*.apk release/
	rename -v "s/-release//" release/*.apk
	rename -v "s/app-/reckoner-${VERSION}-/" release/*.apk

apk_version: apk
	touch metadata/en-US/changelogs/`${ANDROID_SDK_BUILD_TOOLS}/aapt dump badging release/reckoner-*arm64-v8a.apk | grep "versionCode" | sed -e "s/.*versionCode='//" -e "s/' .*//"`.txt

appbundle: build release
	flutter build appbundle --no-tree-shake-icons --no-obfuscate $(BUILD_FLAGS)
	mv build/app/outputs/bundle/release/app-release.aab release/reckoner-${VERSION}.aab

android: apk_version appbundle

ipa: build release
	flutter build ipa --no-tree-shake-icons --release
	mv build/ios/ipa/reckoner.ipa release/reckoner-${VERSION}-ios.ipa

macos: build release
	flutter build macos --no-tree-shake-icons --release

pkg: macos
	productbuild --component "build/macos/Build/Products/Release/Reckoner.app" /Applications "release/reckoner-${VERSION}.pkg"

dmg: macos
	rm -f release/reckoner*.dmg
	jq --arg newkey "$$(security find-identity -v -p codesigning | grep Distribution | cut -c 6-45)" '."code-sign"."signing-identity" |= $$newkey' macos/Dmg_installer/config.json > dmg-config.json
	appdmg dmg-config.json release/reckoner-${VERSION}.dmg
	rm dmg-config.json

web-tar: build release
	flutter build web $(WEB_FLAGS) $(BUILD_FLAGS)
	tar -czf "release/reckoner-${VERSION}-web.tar.gz" -C build web
	mv build/web release/

demo: build release
	flutter build web --dart-define=DEMO=true $(WEB_FLAGS) $(BUILD_FLAGS)
	mv build/web release/demo
	sed -i.backup '/start_url/d' release/demo/manifest.json
#   Removing start_url makes it not valid to be installed

release:
	mkdir -p release
	yq -i ".version = \"$(VERSION)+$$((`git rev-list --count HEAD` + 1))\"" pubspec.yaml
	yq -i ".environment.flutter = \"`flutter --version | head -n 1 | cut -d ' ' -f 2`\"" pubspec.yaml
	
clean:
	flutter clean
	rm -rf release

nuke:
	git clean -xdf

run: build
	flutter run

all_linux: clean appstream linux appimage android web-tar demo

all: clean release docker_linux android web-tar demo ipa pkg dmg

docker_linux: release
	cd linux && ./docker_image.sh
	docker run --platform linux/amd64 --rm -v .:/code:z -w /code flutter:x86 bash -c "flutter upgrade && make linux && flutter clean"
	docker run --platform linux/arm64 --rm -v .:/code:z -w /code flutter:arm bash -c "flutter upgrade && make linux && flutter clean"
	
upgrade:
	flutter pub upgrade --major-versions
	flutter pub outdated

schema:
	dart run drift_dev schema dump lib/database/reckoner_db.dart schemas/
	dart run drift_dev schema steps schemas/ lib/database/migrations/schema_versions.dart
	dart run drift_dev schema generate --data-classes --companions schemas/ test/database/generated_migrations/

server:
	flutter run -d web-server --web-port 8080 --web-hostname 0.0.0.0
