#!/bin/bash

if [[ -z "$(docker images -q -f reference='flutter')" ]]; then
    docker build --platform linux/amd64 --tag flutter:x86 .
    docker build --platform linux/arm64 --tag flutter:arm .
fi
