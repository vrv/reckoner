<?xml version="1.0" encoding="UTF-8"?>
<!-- Copyright 2024 Victor Velten -->
<component type="desktop-application">
    <id>finance.reckoner.Reckoner</id>

    <name>Reckoner</name>
    <summary>Private personal financial tracking</summary>
    <developer id="velten.dev">
        <name>Victor Velten</name>
    </developer>
    <developer_name id="velten.dev">Victor Velten</developer_name>
    <branding>
        <color type="primary" scheme_preference="light">#b388ff</color>
        <color type="primary" scheme_preference="dark">#673ab7</color>
    </branding>

    <metadata_license>CC0-1.0</metadata_license>
    <project_license>AGPL-3.0-or-later</project_license>

    <recommends>
        <display_length compare="ge">800</display_length>
    </recommends>
    <supports>
        <control>pointing</control>
        <control>keyboard</control>
        <control>touch</control>
    </supports>

    <description>
        <p>Use Reckoner to reckon with your finances and take control of your financial future!
            Reckoner is an encrypted, local first, personal financial tracker. This financial
            tracker is focused on the user. This application respect user privacy by providing an
            offline first encrypted data storage. You have the freedom to customize the application
            layout and add custom views for displaying your information at a glance. Reckoner allows
            you to create the layout you want without artificial limits.</p>

        <ul>
            <li><em>Secure and Private</em>: Reckoner encrypts data on device using AES 256 bit
                encryption. Reckoner also allows you to setup a separate passcode for the
                application so people cannot open it from your unlocked phone. By default data is
                offline only, but you can synchronize data end-to-end encrypted with AES 256 bit
                encryption to a PocketBase server you control. You can backup the database to any
                storage location you want or on a regular schedule or on demand; encrypted by
                default.</li>
            <li><em>Make it Yours</em>: With report groups, you can add any report supported and
                create an overview that you want. The UI layout can be reordered to allow for you to
                choose which screen to show on startup. Hide any unused screens you don't use or add
                more as it fits your use case. There are also app themes which can be applied if you
                want a dark theme, light theme, or AMOLED black theme.</li>
            <li><em>Custom Categorization</em>: Categories in Reckoner are flexible and allow you to
                use them the way you want. Nest categories within one another for a more intuitive
                experience. Attach budgets to categories to allow traditional budget planning on the
                cadence you want: yearly, monthly, daily or anywhere in between. You can have as
                many category groups as you want in Reckoner each of which could have or not have
                budgets as you desire. Tagging is also available for categorizing transactions.</li>
            <li><em>Designed for all Devices</em>: Reckoner has an adaptive layout which adjusts to
                your device. Whether your screen folds out or you connect to an external display,
                you will be presented with a UI optimized for the available screen size. Pocketbase
                synchronization works across all devices which run Reckoner: desktop, mobile, or
                web.</li>
        </ul>
    </description>

    <launchable type="desktop-id">finance.reckoner.Reckoner.desktop</launchable>

    <screenshots>
        <screenshot type="default">
            <caption>Desktop view light mode</caption>
            <image>https://doc.reckoner.finance/user/screens/transactions-narrow-light.png</image>
        </screenshot>
        <screenshot>
            <caption>Desktop view dark mode</caption>
            <image>https://doc.reckoner.finance/user/screens/transactions-narrow-dark.png</image>
        </screenshot>
    </screenshots>

    <url type="homepage">https://reckoner.finance</url>
    <url type="help">https://doc.reckoner.finance</url>
    <url type="bugtracker">https://codeberg.org/Reckoner/app/issues</url>
    <content_rating type="oars-1.1" />

    <releases>
        <release version="2.3.0" date="2025-03-08">
            <description>
                <p>Enhancements</p>
                <ul>
                    <li>Added inline tutorial to the app. Now the help icon takes the user through a tutorial instead of redirecting to the website.</li>
                </ul>
            </description>
        </release>
        <release version="2.2.6" date="2025-02-09">
            <description>
                <p>Fixes</p>
                <ul>
                    <li>Properly check for transaction changes in merchants and tags</li>
                    <li>Update merchant page to direct user to create a transaction to add a merchant</li>
                    <li>Update transaction edit to add icon on merchant field when adding a new merchant</li>
                </ul>
            </description>
        </release>
        <release version="2.2.5" date="2025-02-06">
            <description>
                <p>Fixes</p>
                <ul>
                    <li>Updated PB to set max length of data field to 1,000,000 characters</li>
                </ul>
            </description>
        </release>
        <release version="2.2.5" date="2025-02-02">
            <description>
                <p>Fixes</p>
                <ul>
                    <li>Fix issue with editing amounts grabbing focus (issue #13)</li>
                    <li>Fix error with validation not running when creating a multicurrency transaction (issue #15)</li>
                    <li>Fix issue with admin setup for pocketbase account sync on newer version of pocketbase (issue #16)</li>
                </ul>
            </description>
        </release>
        <release version="2.2.4" date="2025-01-26">
            <description>
                <p>Enhancements</p>
                <ul>
                    <li>Add calculator on amount fields</li>
                    <li>Add ability to save on discard popups</li>
                </ul>
                <p>Fixes</p>
                <ul>
                    <li>Properly detect if there are changes in a transaction edit so popup is not shown if no changes</li>
                    <li>Only change sync date on bulk sync to prevent sync drift</li>
                </ul>
            </description>
        </release>
        <release version="2.2.3" date="2024-12-29">
            <description>
                <p>Fixed issue #12</p>
                <ul>
                    <li>Fixed Account balance displaying custom currencies as unitless values with two decimal points</li>
                    <li>Fixed Transaction filter to not filter by USD by default</li>
                </ul>
            </description>
        </release>
        <release version="2.2.2" date="2024-12-21">
            <description>
                <p>Fixes</p>
                <ul>
                    <li>Change the report layout to only put reports on different line if out of space</li>
                    <li>Have app lock screen unlock when entering password of same length</li>
                </ul>
            </description>
        </release>
        <release version="2.2.1" date="2024-12-18">
            <description>
                <p>Enhancements</p>
                <ul>
                    <li>Simplify transaction editing by having a toggle for whether creating a expense, transfer, or income transaction</li>
                </ul>
                <p>Fixes</p>
                <ul>
                    <li>Prevent error from being logged when resizing report view to minimum width</li>
                </ul>
            </description>
        </release>
        <release version="2.2.0" date="2024-12-09">
            <description>
                <p>Enhancements</p>
                <ul>
                    <li>Add text reports which generate a summary value below the text label</li>
                    <li>Allow for other report types beside line to display data based on the selected app range</li>
                    <li>Make reports show up in cards to better distinguish where they begin and end</li>
                </ul>
                <p>Fixes</p>
                <ul>
                    <li>Fix issue where category children didn't display parent name when editing existing transactions</li>
                </ul>
            </description>
        </release>
        <release version="2.1.1" date="2024-10-15">
            <description>
                <p>Hotfix</p>
                <ul>
                    <li>Fix issue in certain platforms where secrets were not accessible</li>
                </ul>
            </description>
        </release>
        <release version="2.1.0" date="2024-10-14">
            <description>
                <p>Enhancements</p>
                <ul>
                    <li>Updated handling of item editing and UI around editable items</li>
                    <li>Delete and copy are not handled in edit screens to reduce clicks for editing items</li>
                    <li>Improved transaction table handling and bulk editing of split items</li>
                </ul>
                <p>Fixes</p>
                <ul>
                    <li>Fix tray icon menu</li>
                </ul>
            </description>
        </release>
        <release version="2.0.3" date="2024-09-28">
            <description>
                <p>Fixes</p>
                <ul>
                    <li>Fix issue where simultaneous logged errors would overwrite each other</li>
                    <li>Fix issue where merchant names would overflow available width</li>
                </ul>
            </description>
        </release>
        <release version="2.0.2" date="2024-09-12">
            <description>
                <p>Fixes</p>
                <ul>
                    <li>Re-add ability to delete transaction splits in editor</li>
                    <li>Change update date handling for settings synchronization</li>
                </ul>
            </description>
        </release>
        <release version="2.0.1" date="2024-08-25">
            <description>
                <p>Fixes</p>
                <ul>
                    <li>Hotfix for Flatpak issue</li>
                </ul>
            </description>
        </release>
        <release version="2.0.0" date="2024-08-24">
            <description>
                <p>Major Update</p>
                <ul>
                    <li>Modified handling of Merchants. They are now just a free text field on transactions instead of being their own table.</li>
                    <li>Added location handling to transactions from the merchant table</li>
                </ul>
            </description>
        </release>
        <release version="1.1.18" date="2024-08-04">
            <description>
                <p>Fixes</p>
                <ul>
                    <li>Change handling of exporting the database particularly on mobile
                        applications</li>
                </ul>
            </description>
        </release>
        <release version="1.1.17" date="2024-07-08">
            <description>
                <p>Fixes</p>
                <ul>
                    <li>Fix case where deleting a report group would prevent report groups from
                        loading</li>
                    <li>Fix issue were deleting report group or category group didn't update UI</li>
                </ul>
            </description>
        </release>
        <release version="1.1.16" date="2024-07-01">
            <description>
                <p>Fixes</p>
                <ul>
                    <li>Fix issue were budgets weren't generated</li>
                    <li>Generate budgets when going to a date range with loaded transactions</li>
                </ul>
            </description>
        </release>
        <release version="1.1.15" date="2024-06-18">
            <description>
                <p>Fixes</p>
                <ul>
                    <li>Add delay when saving sync transactions in case of foreign key dependency
                        issue</li>
                    <li>Fix issue where updates only sent on bulk sync</li>
                    <li>Fix issue where transactions weren't sending updates</li>
                </ul>
            </description>
        </release>
        <release version="1.1.14" date="2024-06-14">
            <description>
                <p>Fixes</p>
                <ul>
                    <li>Fix regression in v1.1.11 where could only export database once</li>
                </ul>
            </description>
        </release>
        <release version="1.1.13" date="2024-06-13">
            <description>
                <p>BREAKING CHANGE</p>
                <ul>
                    <li>Change sync algorithm to base 64 encode. Delete existing sync data and
                        resync from one of your devices.</li>
                </ul>
                <p>Fixes</p>
                <ul>
                    <li>Fix bulk sync with encryption on</li>
                    <li>Better error handling with bulk sync threading</li>
                </ul>
            </description>
        </release>
        <release version="1.1.12" date="2024-06-12">
            <description>
                <p>Fixes</p>
                <ul>
                    <li>Use multithreading for bulk sync operation so UI doesn't lag</li>
                    <li>Fix issue where resync would happen after first connection check</li>
                </ul>
            </description>
        </release>
        <release version="1.1.11" date="2024-06-08">
            <description>
                <p>Fixes</p>
                <ul>
                    <li>Modify database import to support versioning</li>
                    <li>Fix reset of database for non-web platforms</li>
                </ul>
            </description>
        </release>
        <release version="1.1.10" date="2024-06-07">
            <description>
                <p>Enhancements</p>
                <ul>
                    <li>Enable/disable background sync without opening up sync login</li>
                </ul>
                <p>Fixes</p>
                <ul>
                    <li>Don't have timeout throw error when checking connection</li>
                </ul>
            </description>
        </release>
        <release version="1.1.9" date="2024-06-05">
            <description>
                <p>Enhancements</p>
                <ul>
                    <li>Use toast for messages instead of snackbars</li>
                    <li>Show toast when error is logged</li>
                </ul>
            </description>
        </release>
        <release version="1.1.8" date="2024-06-03">
            <description>
                <p>Fixes</p>
                <ul>
                    <li>Handle Pocketbase 429 error with exponential backoff retries</li>
                </ul>
            </description>
        </release>
        <release version="1.1.7" date="2024-06-03">
            <description>
                <p>Fixes</p>
                <ul>
                    <li>Change Pocketbase sync to full sync of time invariant data (Accounts,
                        Merchants, etc)</li>
                </ul>
            </description>
        </release>
        <release version="1.1.4" date="2024-05-29">
            <description>
                <p>Fixes</p>
                <ul>
                    <li>Fix issue with Pocketbase sync where too many requests were made</li>
                    <li>Properly send schemaVersion in settings sync bundle</li>
                </ul>
            </description>
        </release>
        <release version="1.1.3" date="2024-05-28">
            <description>
                <p>Enhancements</p>
                <ul>
                    <li>Add Input to specify the start offset for a multi-period budget</li>
                    <li>Update edit screens to enable auto-tiling based on display size</li>
                </ul>
                <p>Fixes</p>
                <ul>
                    <li>Fix budget generation for multi-period budgets</li>
                </ul>
            </description>
        </release>
        <release version="1.1.2" date="2024-05-19">
            <description>
                <p>Load Fixes</p>
                <ul>
                    <li>Fix race condition showing category or report on load</li>
                    <li>Fix issue with report groups not saving reports</li>
                </ul>
            </description>
        </release>
        <release version="1.1.1" date="2024-05-17">
            <description>
                <p>Hot Fixes</p>
                <ul>
                    <li>Fix sync disconnect after first connect</li>
                    <li>Fix sync save regressions</li>
                    <li>Fix report group sync saving issue</li>
                </ul>
            </description>
        </release>
        <release version="1.1.0" date="2024-05-15">
            <description>
                <p>Enhancements</p>
                <ul>
                    <li>Added hide to system tray when user enables background syncing</li>
                    <li>Added help links (hidable) to page titles which link to the documentation</li>
                </ul>
                <p>Fixes</p>
                <ul>
                    <li>Make it so that only one instance of Reckoner can be created</li>
                    <li>Some refactoring and code dependency cleanup</li>
                </ul>
            </description>
        </release>
    </releases>
</component>