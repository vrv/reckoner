Fixes
* Fix case where deleting a report group would prevent report groups from loading
* Fix issue were deleting report group or category group didn't update UI