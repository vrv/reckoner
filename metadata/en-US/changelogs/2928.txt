Fixes
* Add MANAGE_EXTERNAL_STORAGE permission to allow the user to save files to locations beside Documents. To store to a custom location Reckoner will prompt the user for elevated storage permissions. Issue reported by email from Cliff, thanks Cliff!
* Fix loading screen from overflowing