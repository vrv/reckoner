Fixes
* Re-add ability to delete transaction splits in editor
* Gracefully handle mobile application pauses for sync service
* Change update date handling for settings synchronization