Fixes
* Add delay when saving sync transactions in case of foreign key dependency issue
* Fix issue where updates only sent on bulk sync
* Fix issue where transactions weren't sending updates