import 'dart:collection';
import 'dart:math';

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

import '../../model/currency.dart';
import '../../model/report.dart';
import '../../model/tz_date_time.dart';
import 'currency_input_formatter.dart';

@immutable
class TransactionReportChartData {
  final List<TransactionReportData> data = [];
  final Report report;

  TransactionReportGrouping get grouping => report.grouping;
  bool get overTime => report.loadOverTime;
  String? get currencyCode =>
      report.filterObj.currencyCodes.length == 1
          ? report.filterObj.currencyCodes.first
          : null;

  TransactionReportChartData(this.report);

  void add(final TransactionReportData dataPoint) {
    assert(
      grouping == TransactionReportGrouping.merchant
          ? dataPoint.group.runtimeType == TransactionMerchantGroup
          : grouping == TransactionReportGrouping.account
          ? dataPoint.group.runtimeType == TransactionAccountGroup
          : grouping == TransactionReportGrouping.category
          ? dataPoint.group.runtimeType == TransactionCategoryGroup
          : grouping == TransactionReportGrouping.tag
          ? dataPoint.group.runtimeType == TransactionTagGroup
          : dataPoint.group.runtimeType == TransactionCurrencyGroup,
    );
    assert(
      currencyCode == null || dataPoint.group.currencyCode == currencyCode,
    );
    data.add(dataPoint);
  }
}

@immutable
class TransactionReportData {
  final TransactionCurrencyGroup group;
  final DateTime? date;
  final int amount;

  const TransactionReportData({
    required this.group,
    required this.amount,
    this.date,
  });

  TransactionReportData operator +(final TransactionReportData other) {
    final int amount = this.amount + other.amount;
    DateTime? date;

    if (this.date == null || (other.date?.isAfter(this.date!) ?? false)) {
      date = other.date;
    } else {
      date = this.date;
    }
    return TransactionReportData(group: group, amount: amount, date: date);
  }
}

class TransactionReportOverTime {
  final Report report;
  final TZDateTimeRange? range;

  TransactionReportGrouping get grouping => report.grouping;
  Map<TransactionCurrencyGroup, Map<TZDateTime, TransactionReportData>> data =
      {};
  late Map<TransactionCurrencyGroup, int> _initialGroupTotals;

  TransactionReportOverTime({
    required this.report,
    this.range,
    final List<TransactionReportData> totals = const [],
  }) {
    TZDateTime startDate = range != null ? range!.start : tzNow();
    startDate = _stripTimeFromDate(startDate);
    _initialGroupTotals = Map.unmodifiable(
      totals.asMap().map(
        (final key, final value) => MapEntry(value.group, value.amount),
      ),
    );
  }

  TZDateTime _stripTimeFromDate(final DateTime date) =>
      TZDateTime(local, date.year, date.month, date.day, 12);

  void add({
    required final TransactionCurrencyGroup group,
    required final DateTime date,
    required final int amount,
  }) {
    final txRpt = TransactionReportData(
      amount: amount,
      group: group,
      date: date,
    );
    final convDate = _stripTimeFromDate(date);
    final existingRpt = data
        .putIfAbsent(group, () => SplayTreeMap())
        .putIfAbsent(
          convDate,
          () =>
              TransactionReportData(amount: 0, group: group, date: DateTime(0)),
        );
    data[group]![convDate] = existingRpt + txRpt;
  }

  TransactionReportChartData toChartData() {
    final chartData = TransactionReportChartData(report);

    TZDateTime? startDate;
    TZDateTime endDate = data.values.fold(
      _stripTimeFromDate(DateTime.now()),
      (final previousValue, final element) =>
          element.keys.last
                      .difference(previousValue)
                      .compareTo(const Duration(seconds: 1)) >
                  0
              ? element.keys.last
              : previousValue,
    );

    if (range != null) {
      startDate = _stripTimeFromDate(range!.start);
      endDate = _stripTimeFromDate(range!.end);
    }

    final groupSet =
        data.keys.toSet()..addAll(_initialGroupTotals.keys.toSet());

    for (final group in groupSet) {
      int amount = _initialGroupTotals[group] ?? 0;
      final groupData = data[group];
      if (groupData == null && startDate == null) continue;

      var date = startDate ?? groupData!.keys.first;

      while (date.isBefore(endDate) | date.isAtSameMomentAs(endDate)) {
        if (groupData != null) amount = (groupData[date]?.amount ?? 0) + amount;
        chartData.add(
          TransactionReportData(group: group, amount: amount, date: date),
        );
        // Use this instead of adding durations due to daylight savings...
        date = TZDateTime(local, date.year, date.month, date.day + 1, 12);
      }
    }

    return chartData;
  }
}

@immutable
class TransactionCurrencyGroup with EquatableMixin {
  final String currencyCode;
  final int currencyDigits;
  final String currencySymbol;

  TransactionCurrencyGroup({
    required this.currencyCode,
    required this.currencyDigits,
    required this.currencySymbol,
  });

  String get label => currencyCode;

  double amountToCur(final int amount) => amount / pow(10, currencyDigits);

  String amountToDisplay(final int amount) {
    return CurrencyInputFormatter(
      currency: Currency(
        symbol: currencySymbol,
        decimalDigits: currencyDigits,
        code: currencyCode,
      ),
    ).fromDB(amount);
  }

  @override
  List<Object?> get props => [currencyCode, currencyDigits, currencySymbol];
}

@immutable
class TransactionAccountGroup extends TransactionCurrencyGroup {
  final String accountName;
  final String accountId;
  final String organizationId;
  final String organizationName;

  TransactionAccountGroup({
    required super.currencyCode,
    required super.currencyDigits,
    required super.currencySymbol,
    required this.accountId,
    required this.accountName,
    required this.organizationId,
    required this.organizationName,
  });

  @override
  String get label => '$organizationName $accountName';

  @override
  List<Object?> get props =>
      super.props
        ..addAll([accountName, accountId, organizationId, organizationName]);
}

@immutable
class TransactionCategoryGroup extends TransactionCurrencyGroup {
  final String categoryId;
  final String categoryName;

  TransactionCategoryGroup({
    required super.currencyCode,
    required super.currencyDigits,
    required super.currencySymbol,
    required this.categoryId,
    required this.categoryName,
  });

  @override
  String get label => categoryName;

  @override
  List<Object?> get props => super.props..addAll([categoryId, categoryName]);
}

@immutable
class TransactionMerchantGroup extends TransactionCurrencyGroup {
  final String merchantName;

  TransactionMerchantGroup({
    required super.currencyCode,
    required super.currencyDigits,
    required super.currencySymbol,
    required this.merchantName,
  });

  @override
  String get label => merchantName;

  @override
  List<Object?> get props => super.props..addAll([merchantName]);
}

@immutable
class TransactionTagGroup extends TransactionCurrencyGroup {
  final String tagName;

  TransactionTagGroup({
    required super.currencyCode,
    required super.currencyDigits,
    required super.currencySymbol,
    required this.tagName,
  });

  @override
  String get label => tagName;

  @override
  List<Object?> get props => super.props..addAll([tagName]);
}
