import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

enum LayoutTypes { tall, square, wide, ultawide }

class LayoutConfig {
  late double screenWidth;
  late double screenHeight;
  static bool forceMobile = false;

  LayoutConfig(final BuildContext context) {
    final mediaQueryData = MediaQuery.of(context);
    screenWidth = mediaQueryData.size.width;
    screenHeight = mediaQueryData.size.height;
  }

  LayoutTypes get layout {
    if (screenWidth < 800) {
      return LayoutTypes.tall;
    } else if (screenWidth < 1200) {
      return LayoutTypes.square;
    } else if (screenWidth < 1800) {
      return LayoutTypes.wide;
    } else {
      return LayoutTypes.ultawide;
    }
  }

  bool get isMobile {
    if (screenWidth < 600 || screenHeight < 600) return true;
    return LayoutTypes.tall == layout &&
        (forceMobile ||
            [
              TargetPlatform.iOS,
              TargetPlatform.android,
            ].contains(defaultTargetPlatform));
  }

  bool get isWidescreen =>
      [LayoutTypes.wide, LayoutTypes.ultawide].contains(layout);
}
