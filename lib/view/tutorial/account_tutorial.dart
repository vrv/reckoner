import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:tutorial_coach_mark/tutorial_coach_mark.dart';

final addOrgKey = GlobalKey();
final orgDisplayKey = GlobalKey();
final addAccountButtonKey = GlobalKey();
final accountDisplayKey = GlobalKey();
final accountBalanceKey = GlobalKey();

final accountTutorial = TutorialCoachMark(
  targets: [
    TargetFocus(
      keyTarget: addOrgKey,
      alignSkip: Alignment.topRight,
      contents: [
        TargetContent(
          align: ContentAlign.top,
          builder: (final context, final controller) {
            return Center(
              child: Text(
                'This adds a new organization',
                style: TextTheme.of(
                  context,
                ).bodyLarge?.copyWith(color: Colors.white),
              ),
            );
          },
        ),
      ],
    ),
    TargetFocus(
      keyTarget: orgDisplayKey,
      alignSkip: Alignment.topRight,
      shape: ShapeLightFocus.RRect,
      contents: [
        TargetContent(
          align: ContentAlign.bottom,
          builder: (final context, final controller) {
            return Center(
              child: Text(
                'Organization name is shown here. The associated accounts are displayed below this. Used the edit icon to edit the organization information.',
                style: TextTheme.of(
                  context,
                ).bodyLarge?.copyWith(color: Colors.white),
              ),
            );
          },
        ),
      ],
    ),
    TargetFocus(
      keyTarget: addAccountButtonKey,
      alignSkip: Alignment.topRight,
      shape: ShapeLightFocus.RRect,
      contents: [
        TargetContent(
          align: ContentAlign.bottom,
          builder: (final context, final controller) {
            return Center(
              child: Text(
                'Add an account to the organization with this button',
                style: TextTheme.of(
                  context,
                ).bodyLarge?.copyWith(color: Colors.white),
              ),
            );
          },
        ),
      ],
    ),
    TargetFocus(
      paddingFocus: 0.0,
      keyTarget: accountDisplayKey,
      alignSkip: Alignment.topRight,
      shape: ShapeLightFocus.RRect,
      contents: [
        TargetContent(
          align: ContentAlign.bottom,
          builder: (final context, final controller) {
            return Center(
              child: Text(
                'Account names are shown here. You can edit the account information with the edit button. The transactions associated with the account can be viewed with the transactions button.',
                style: TextTheme.of(
                  context,
                ).bodyLarge?.copyWith(color: Colors.white),
              ),
            );
          },
        ),
      ],
    ),
    TargetFocus(
      keyTarget: accountBalanceKey,
      alignSkip: Alignment.topRight,
      shape: ShapeLightFocus.RRect,
      contents: [
        TargetContent(
          align: ContentAlign.bottom,
          builder: (final context, final controller) {
            return Center(
              child: Text(
                'The account balances are shown here.',
                style: TextTheme.of(
                  context,
                ).bodyLarge?.copyWith(color: Colors.white),
              ),
            );
          },
        ),
      ],
    ),
  ],
  colorShadow: Colors.black,
  textSkip: 'SKIP',
  paddingFocus: 10,
  opacityShadow: 0.5,
  imageFilter: ImageFilter.blur(sigmaX: 8, sigmaY: 8),
);
