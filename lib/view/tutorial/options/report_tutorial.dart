import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:tutorial_coach_mark/tutorial_coach_mark.dart';

final addReportKey = GlobalKey();
final reportTypeKey = GlobalKey();
final reportKey = GlobalKey();

final reportTutorial = TutorialCoachMark(
  targets: [
    TargetFocus(
      keyTarget: addReportKey,
      alignSkip: Alignment.topRight,
      contents: [
        TargetContent(
          align: ContentAlign.top,
          builder: (final context, final controller) {
            return Center(
              child: Text(
                'This creates a new report.',
                style: TextTheme.of(
                  context,
                ).bodyLarge?.copyWith(color: Colors.white),
              ),
            );
          },
        ),
      ],
    ),
    TargetFocus(
      keyTarget: reportTypeKey,
      alignSkip: Alignment.topRight,
      shape: ShapeLightFocus.RRect,
      contents: [
        TargetContent(
          align: ContentAlign.bottom,
          builder: (final context, final controller) {
            return Center(
              child: Text(
                'Reports are grouped together by their type. The current report types supported are Line Charts, Bar Charts, Pie Charts, Table Reports and Text Reports.',
                style: TextTheme.of(
                  context,
                ).bodyLarge?.copyWith(color: Colors.white),
              ),
            );
          },
        ),
      ],
    ),
    TargetFocus(
      keyTarget: reportKey,
      alignSkip: Alignment.topRight,
      shape: ShapeLightFocus.RRect,
      contents: [
        TargetContent(
          align: ContentAlign.bottom,
          builder: (final context, final controller) {
            return Center(
              child: Text(
                'This is the report name for a report. You can also edit the report by clicking the edit button next to the name.',
                style: TextTheme.of(
                  context,
                ).bodyLarge?.copyWith(color: Colors.white),
              ),
            );
          },
        ),
      ],
    ),
  ],
  colorShadow: Colors.black,
  textSkip: 'SKIP',
  paddingFocus: 10,
  opacityShadow: 0.5,
  imageFilter: ImageFilter.blur(sigmaX: 8, sigmaY: 8),
);
