import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:tutorial_coach_mark/tutorial_coach_mark.dart';

final addCurrencyKey = GlobalKey();
final currencyDisplayKey = GlobalKey();
final currencyDigitsKey = GlobalKey();
final currencyFavoriteKey = GlobalKey();

final currencyTutorial = TutorialCoachMark(
  targets: [
    TargetFocus(
      keyTarget: addCurrencyKey,
      alignSkip: Alignment.topRight,
      contents: [
        TargetContent(
          align: ContentAlign.top,
          builder: (final context, final controller) {
            return Center(
              child: Text(
                'This creates a new currency that is not already in Reckoner.',
                style: TextTheme.of(
                  context,
                ).bodyLarge?.copyWith(color: Colors.white),
              ),
            );
          },
        ),
      ],
    ),
    TargetFocus(
      keyTarget: currencyDisplayKey,
      alignSkip: Alignment.topRight,
      shape: ShapeLightFocus.RRect,
      contents: [
        TargetContent(
          align: ContentAlign.bottom,
          builder: (final context, final controller) {
            return Center(
              child: Text(
                'This the key currency information. Fields include currency name, currency code, and currency symbol.',
                style: TextTheme.of(
                  context,
                ).bodyLarge?.copyWith(color: Colors.white),
              ),
            );
          },
        ),
      ],
    ),
    TargetFocus(
      keyTarget: currencyDigitsKey,
      alignSkip: Alignment.topRight,
      contents: [
        TargetContent(
          align: ContentAlign.bottom,
          builder: (final context, final controller) {
            return Center(
              child: Text(
                'This is the number of decimal digits used in the currency. Decimal digits determine the lowest currency denomination.',
                style: TextTheme.of(
                  context,
                ).bodyLarge?.copyWith(color: Colors.white),
              ),
            );
          },
        ),
      ],
    ),
    TargetFocus(
      paddingFocus: 0.0,
      keyTarget: currencyFavoriteKey,
      alignSkip: Alignment.topRight,
      contents: [
        TargetContent(
          align: ContentAlign.bottom,
          builder: (final context, final controller) {
            return Center(
              child: Text(
                'This determines the default currency which should be used when creating transactions or when creating accounts.',
                style: TextTheme.of(
                  context,
                ).bodyLarge?.copyWith(color: Colors.white),
              ),
            );
          },
        ),
      ],
    ),
  ],
  colorShadow: Colors.black,
  textSkip: 'SKIP',
  paddingFocus: 10,
  opacityShadow: 0.5,
  imageFilter: ImageFilter.blur(sigmaX: 8, sigmaY: 8),
);
