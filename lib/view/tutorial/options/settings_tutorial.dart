import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:tutorial_coach_mark/tutorial_coach_mark.dart';

final syncSettingsKey = GlobalKey();
final themeKey = GlobalKey();
final securityKey = GlobalKey();
final dateKey = GlobalKey();
final aboutKey = GlobalKey();
final backupKey = GlobalKey();
final errorKey = GlobalKey();

final settingsTutorial = TutorialCoachMark(
  onClickTarget: (final targetFocus) {
    final nextKeyMap = {
      syncSettingsKey: themeKey,
      themeKey: securityKey,
      securityKey: dateKey,
      dateKey: aboutKey,
      aboutKey: backupKey,
      backupKey: errorKey,
    };
    final key = nextKeyMap[targetFocus.keyTarget];
    if (key != null) Scrollable.ensureVisible(key.currentContext!);
  },
  targets: [
    TargetFocus(
      keyTarget: syncSettingsKey,
      alignSkip: Alignment.topRight,
      shape: ShapeLightFocus.RRect,
      contents: [
        TargetContent(
          align: ContentAlign.bottom,
          builder: (final context, final controller) {
            return Center(
              child: Text(
                'This section controls the synchronization settings. Currently Reckoner needs a self-hosted PocketBase server to sync data between devices. Data is end-to-end synchronized by default, but encryption can be turned off if desired. See the online help for additional information.',
                style: TextTheme.of(
                  context,
                ).bodyLarge?.copyWith(color: Colors.white),
              ),
            );
          },
        ),
      ],
    ),
    TargetFocus(
      keyTarget: themeKey,
      alignSkip: Alignment.topRight,
      shape: ShapeLightFocus.RRect,
      contents: [
        TargetContent(
          align: ContentAlign.top,
          builder: (final context, final controller) {
            return Center(
              child: Text(
                'This section controls the application appearance and the display of the help icons.',
                style: TextTheme.of(
                  context,
                ).bodyLarge?.copyWith(color: Colors.white),
              ),
            );
          },
        ),
      ],
    ),
    TargetFocus(
      keyTarget: securityKey,
      alignSkip: Alignment.topRight,
      shape: ShapeLightFocus.RRect,
      contents: [
        TargetContent(
          align: ContentAlign.top,
          builder: (final context, final controller) {
            return Center(
              child: Text(
                'This section controls the application security on the application access and for device encryption. Depending on the device, you can set settings for database encryption, application passcode or biometrics for locking the application, and resetting the application and deleting all data.',
                style: TextTheme.of(
                  context,
                ).bodyLarge?.copyWith(color: Colors.white),
              ),
            );
          },
        ),
      ],
    ),
    TargetFocus(
      keyTarget: dateKey,
      alignSkip: Alignment.topRight,
      shape: ShapeLightFocus.RRect,
      contents: [
        TargetContent(
          align: ContentAlign.top,
          builder: (final context, final controller) {
            return Center(
              child: Text(
                'This section controls the timezone as it applies to dates. The date for transactions are displayed according to this timezone as opposed to the device timezone.',
                style: TextTheme.of(
                  context,
                ).bodyLarge?.copyWith(color: Colors.white),
              ),
            );
          },
        ),
      ],
    ),
    TargetFocus(
      keyTarget: aboutKey,
      alignSkip: Alignment.topRight,
      shape: ShapeLightFocus.RRect,
      contents: [
        TargetContent(
          align: ContentAlign.top,
          builder: (final context, final controller) {
            return Center(
              child: Text(
                'This section controls displays the version of Reckoner and a button which enables you to see the licenses for the libraries used.',
                style: TextTheme.of(
                  context,
                ).bodyLarge?.copyWith(color: Colors.white),
              ),
            );
          },
        ),
      ],
    ),
    TargetFocus(
      keyTarget: backupKey,
      alignSkip: Alignment.topRight,
      shape: ShapeLightFocus.RRect,
      contents: [
        TargetContent(
          align: ContentAlign.bottom,
          builder: (final context, final controller) {
            return Center(
              child: Text(
                'This section controls the backup and restoration of the database. You have the option to backup manually or on a regular schedule. Then you can import/restore the database from a backup.',
                style: TextTheme.of(
                  context,
                ).bodyLarge?.copyWith(color: Colors.white),
              ),
            );
          },
        ),
      ],
    ),
    TargetFocus(
      keyTarget: errorKey,
      alignSkip: Alignment.topRight,
      shape: ShapeLightFocus.RRect,
      contents: [
        TargetContent(
          align: ContentAlign.bottom,
          builder: (final context, final controller) {
            return Center(
              child: Text(
                'If you are seeing this section, I am very sorry! This is where uncaught code errors are captured. Since Reckoner doesn\'t collect any telemetry, you need to send error logs to the developer by coping the errors logged here.',
                style: TextTheme.of(
                  context,
                ).bodyLarge?.copyWith(color: Colors.white),
              ),
            );
          },
        ),
      ],
    ),
  ],
  colorShadow: Colors.black,
  textSkip: 'SKIP',
  paddingFocus: 10,
  opacityShadow: 0.5,
  imageFilter: ImageFilter.blur(sigmaX: 8, sigmaY: 8),
);
