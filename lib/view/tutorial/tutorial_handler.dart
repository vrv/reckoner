import 'package:flutter/material.dart';
import 'package:tutorial_coach_mark/tutorial_coach_mark.dart';
import 'package:watch_it/watch_it.dart';

enum TutorialHelp { none, show }

void tutorialHandler(
  final TutorialHelp helpVal,
  final BuildContext context,
  final TutorialCoachMark tutorial, {
  final List<ScrollController> controllers = const [],
}) {
  if (!context.mounted) return;
  if (helpVal != TutorialHelp.show) return;

  assert(tutorial.targets.isNotEmpty);

  for (final controller in controllers) {
    controller.animateTo(0, duration: Durations.short1, curve: Curves.easeIn);
  }

  // Show the tutorial and reset the TutorialHelp value
  Future.delayed(Durations.medium1, () {
    if (context.mounted) {
      tutorial.show(context: context);
    }
  });
  di<ValueNotifier<TutorialHelp>>().value = TutorialHelp.none;
}
