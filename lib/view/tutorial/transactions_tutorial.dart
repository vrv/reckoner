import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:tutorial_coach_mark/tutorial_coach_mark.dart';

import '../model/layout_config.dart';

final txNameKey = GlobalKey();
final txAmountKey = GlobalKey();
final txDateKey = GlobalKey();
final txSourceKey = GlobalKey();
final txDestKey = GlobalKey();
final txRowKey = GlobalKey();
final txHeaderNameKey = GlobalKey();

final transactionsTutorial = TutorialCoachMark(
  targets: [
    TargetFocus(
      keyTarget: txNameKey,
      alignSkip: Alignment.topRight,
      shape: ShapeLightFocus.RRect,
      contents: [
        TargetContent(
          align: ContentAlign.bottom,
          builder: (final context, final controller) {
            final mobile = LayoutConfig(context).isMobile;
            final text =
                'This displays the transaction name.${mobile ? "" : " Click the edit button to edit the transaction."}';
            return Center(
              child: Text(
                text,
                style: TextTheme.of(
                  context,
                ).bodyLarge?.copyWith(color: Colors.white),
              ),
            );
          },
        ),
      ],
    ),
    TargetFocus(
      keyTarget: txAmountKey,
      alignSkip: Alignment.topRight,
      shape: ShapeLightFocus.RRect,
      contents: [
        TargetContent(
          align: ContentAlign.bottom,
          builder: (final context, final controller) {
            final mobile = LayoutConfig(context).isMobile;
            final text =
                'This displays the transaction amount. Each transaction can only be expressed in a single currency. The transaction view shows the currency symbol when displaying the amount. ${mobile ? "" : "You can click the column to sort by the transaction amount. Amounts across currencies are sorted according to the lowest currency denominator."}';
            return Center(
              child: Text(
                text,
                style: TextTheme.of(
                  context,
                ).bodyLarge?.copyWith(color: Colors.white),
              ),
            );
          },
        ),
      ],
    ),
    TargetFocus(
      keyTarget: txDateKey,
      alignSkip: Alignment.topRight,
      shape: ShapeLightFocus.RRect,
      contents: [
        TargetContent(
          align: ContentAlign.bottom,
          builder: (final context, final controller) {
            return Center(
              child: Text(
                'This displays the date of the transaction.',
                style: TextTheme.of(
                  context,
                ).bodyLarge?.copyWith(color: Colors.white),
              ),
            );
          },
        ),
      ],
    ),
    TargetFocus(
      keyTarget: txSourceKey,
      alignSkip: Alignment.topRight,
      shape: ShapeLightFocus.RRect,
      contents: [
        TargetContent(
          align: ContentAlign.bottom,
          builder: (final context, final controller) {
            return Center(
              child: Text(
                'This displays the source of the transaction. For expenses and transfers, this will be a account. For income, this will be a merchant if one is specified.',
                style: TextTheme.of(
                  context,
                ).bodyLarge?.copyWith(color: Colors.white),
              ),
            );
          },
        ),
      ],
    ),
    TargetFocus(
      keyTarget: txDestKey,
      alignSkip: Alignment.topRight,
      shape: ShapeLightFocus.RRect,
      contents: [
        TargetContent(
          align: ContentAlign.bottom,
          builder: (final context, final controller) {
            return Center(
              child: Text(
                'This displays the destination of the transaction. For income and transfers, this will be a account. For expenses, this will be a merchant if one is specified.',
                style: TextTheme.of(
                  context,
                ).bodyLarge?.copyWith(color: Colors.white),
              ),
            );
          },
        ),
      ],
    ),
    TargetFocus(
      keyTarget: txRowKey,
      alignSkip: Alignment.topRight,
      shape: ShapeLightFocus.RRect,
      contents: [
        TargetContent(
          align: ContentAlign.bottom,
          builder: (final context, final controller) {
            final mobile = LayoutConfig(context).isMobile;
            final text =
                mobile
                    ? 'Additional information like splits, associated categories, and tags are displayed below the primary transaction information. Swipe left to right to edit the transaction or split. Swipe right to left to select the transaction/split to select it for bulk editing.'
                    : 'Additional information like splits and associated categories are displayed in columns. Splits are displayed in new rows. Only splits or transactions without splits are selectable for bulk editing.';
            return Center(
              child: Text(
                text,
                style: TextTheme.of(
                  context,
                ).bodyLarge?.copyWith(color: Colors.white),
              ),
            );
          },
        ),
      ],
    ),
    TargetFocus(
      keyTarget: txHeaderNameKey,
      alignSkip: Alignment.topRight,
      shape: ShapeLightFocus.RRect,
      contents: [
        TargetContent(
          align: ContentAlign.bottom,
          builder: (final context, final controller) {
            final mobile = LayoutConfig(context).isMobile;
            final text =
                'The header shows the column names and enables you to sort by different headers or rearrange the column order. Single click on a header to sort by that column. The notable exception is sorting of amounts across currencies is done according to the lowest denomination in that currency regardless of the exchange rate.${mobile ? '' : ' Double click the header to open reordering and resizing. Reorder the header by clicking and dragging the center selector (::). Resize the column by clicking and dragging either of the resize icons (<>)'}';
            return Center(
              child: Text(
                text,
                style: TextTheme.of(
                  context,
                ).bodyLarge?.copyWith(color: Colors.white),
              ),
            );
          },
        ),
      ],
    ),
  ],
  colorShadow: Colors.black,
  textSkip: 'SKIP',
  paddingFocus: 10,
  opacityShadow: 0.5,
  imageFilter: ImageFilter.blur(sigmaX: 8, sigmaY: 8),
);
