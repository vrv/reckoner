import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:tutorial_coach_mark/tutorial_coach_mark.dart';

final addCategoryKey = GlobalKey();
final categoryDisplayKey = GlobalKey();
final budgetRangeKey = GlobalKey();
final budgetAmountKey = GlobalKey();

final categoryTutorial = TutorialCoachMark(
  targets: [
    TargetFocus(
      keyTarget: addCategoryKey,
      alignSkip: Alignment.topRight,
      contents: [
        TargetContent(
          align: ContentAlign.top,
          builder: (final context, final controller) {
            return Center(
              child: Text(
                'This adds a new category',
                style: TextTheme.of(
                  context,
                ).bodyLarge?.copyWith(color: Colors.white),
              ),
            );
          },
        ),
      ],
    ),
    TargetFocus(
      keyTarget: categoryDisplayKey,
      alignSkip: Alignment.bottomRight,
      shape: ShapeLightFocus.RRect,
      contents: [
        TargetContent(
          align: ContentAlign.bottom,
          builder: (final context, final controller) {
            return Center(
              child: Text(
                'Category name is shown here. Used the edit icon to edit the category information and the transaction button to view transactions associated with this category.',
                style: TextTheme.of(
                  context,
                ).bodyLarge?.copyWith(color: Colors.white),
              ),
            );
          },
        ),
      ],
    ),
    TargetFocus(
      keyTarget: budgetRangeKey,
      alignSkip: Alignment.bottomRight,
      shape: ShapeLightFocus.RRect,
      contents: [
        TargetContent(
          align: ContentAlign.bottom,
          builder: (final context, final controller) {
            return Center(
              child: Text(
                'This is the date range for the budget associated with the category.',
                style: TextTheme.of(
                  context,
                ).bodyLarge?.copyWith(color: Colors.white),
              ),
            );
          },
        ),
      ],
    ),
    TargetFocus(
      keyTarget: budgetAmountKey,
      alignSkip: Alignment.bottomRight,
      shape: ShapeLightFocus.RRect,
      contents: [
        TargetContent(
          align: ContentAlign.bottom,
          builder: (final context, final controller) {
            return Center(
              child: Text(
                'This is the budget amount for this date period. You can edit the budget amount for this period by clicking on the budget. An edit mode will open and then any changes can be entered and saved.',
                style: TextTheme.of(
                  context,
                ).bodyLarge?.copyWith(color: Colors.white),
              ),
            );
          },
        ),
      ],
    ),
  ],
  colorShadow: Colors.black,
  textSkip: 'SKIP',
  paddingFocus: 10,
  opacityShadow: 0.5,
  imageFilter: ImageFilter.blur(sigmaX: 8, sigmaY: 8),
);
