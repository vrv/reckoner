import 'dart:async';

import 'package:timezone/timezone.dart';
import 'package:watch_it/watch_it.dart';
import 'package:worker_manager/worker_manager.dart';

import '../../config/environment_const.dart';
import '../../config/platform.dart';
import '../../database/reckoner_db.dart';
import '../../service/data_cache.dart';
import '../../service/logger.dart';
import '../../service/sync_service_manager.dart';

Future<String> databaseInit() async {
  try {
    Future<void> tryUnregister<T extends Object>() async =>
        di.isRegistered<T>() ? di.unregister<T>() : null;
    await tryUnregister<SyncServiceManager>();
    await tryUnregister<DataCache>();
    await ReckonerDb.dispose();
    if (!isWeb) {
      await workerManager.dispose();
      await workerManager.init(isolatesCount: 1);
    }

    final error = await ReckonerDb.check();
    if (error.isNotEmpty) {
      logError('Reckoner', error, null);
      return error;
    }

    final settings = await ReckonerDb.I.settingsDao.getUserSetting();
    setLocalLocation(getLocation(settings.timezone));

    if (isDemo) await ReckonerDb.generateData();

    di.registerSingleton<DataCache>(
      await DataCache.instance,
      dispose: (final cache) => cache.dispose(),
    );
    final syncService = di.registerSingleton<SyncServiceManager>(
      await SyncServiceManager.instance,
      dispose: (final service) => service.dispose(),
    );
    unawaited(syncService.start());

    return '';
  } catch (error, stack) {
    logError('Reckoner', error, stack);
    return error.toString();
  }
}
