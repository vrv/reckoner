import 'dart:async';

import 'package:flutter/material.dart' show debugPrint;
import 'package:watch_it/watch_it.dart';

import '../../database/reckoner_db.dart';
import '../../model/model.dart';
import '../../service/logger.dart';
import '../../service/sync_service_manager.dart';

export '../../database/util/generate.dart';

Future<D?> saveItem<D extends Tracked<D>>(final D item) async {
  try {
    final db = ReckonerDb.I;
    final newItem = await db.transaction(() async {
      final newItem = await db.commonDao.saveTrackedDbItem(item);
      return newItem;
    });
    di<SyncServiceManager>().sendUpdates();
    return newItem;
  } catch (err, stack) {
    logError('Reckoner', err, stack);
    debugPrint(err.toString());
    return null;
  }
}
