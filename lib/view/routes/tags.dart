import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:watch_it/watch_it.dart';

import '../../../config/routes_const.dart';
import '../../database/dao/balance_dao.dart';
import '../../database/reckoner_db.dart';
import '../../service/data_cache.dart';
import '../../service/date_range.dart';
import '../../service/logger.dart';
import '../l10n/localizations.dart';
import '../tutorial/tag_tutorial.dart';
import '../tutorial/tutorial_handler.dart';
import '../util/popup_dialogs.dart';
import '../widgets/account_balance_display.dart';
import '../widgets/display_name_edit.dart';
import '../widgets/fixed_width_container.dart';
import '../widgets/form/input_form.dart';
import '../widgets/layout_widgets.dart';
import '../widgets/no_data_placeholder.dart';

class TagDisplay extends WatchingWidget {
  const TagDisplay({super.key});

  static const style = TextStyle(fontWeight: FontWeight.bold);

  @override
  Widget build(final BuildContext context) {
    final tags = watchValue((final DataCache c) => c.tags);
    final filterRange = watchPropertyValue(
      (final DateTimeRangeLoader l) => l.getRange(),
    );
    tutorialHandler(
      watchIt<ValueNotifier<TutorialHelp>>().value,
      context,
      tagTutorial,
    );
    if (tags.isEmpty) return const NoDataPlaceholder('No transactions tagged.');

    return StreamBuilder(
      stream: ReckonerDb.I.balanceDao.loadBalances(
        type: BalanceType.tag,
        range: filterRange,
      ),
      builder: (final context, final snapshot) {
        final balanceMap = snapshot.data ?? {};
        final tiles =
            tags
                .mapIndexed(
                  (final i, final tag) => ListTile(
                    title: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        DisplayNameEdit(
                          key: i == 0 ? tagDisplayKey : null,
                          name: tag,
                          onEdit: () => _editTag(context, tag),
                          transactionPath: tagTransactionsPath(tag),
                          iconSize: 20,
                        ),
                        BalanceDisplay(
                          key: i == 0 ? tagBalanceKey : null,
                          balanceMap: balanceMap[tag] ?? {},
                        ),
                      ],
                    ),
                    visualDensity: const VisualDensity(
                      vertical: VisualDensity.minimumDensity,
                    ),
                  ),
                )
                .toList();

        if (balanceMap.containsKey(null)) {
          tiles.add(
            ListTile(
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  TextButton(
                    onPressed: () => context.push(noTagTransactionsPath),
                    child: const Text('(No Tag)'),
                  ),
                  BalanceDisplay(balanceMap: balanceMap[null]!),
                ],
              ),
              visualDensity: const VisualDensity(
                vertical: VisualDensity.minimumDensity,
              ),
            ),
          );
        }

        return ReportHeader(
          report: di<DataCache>().tagReport,
          child: FixedWidthContainer(
            child: Column(
              children: [
                const ListTile(
                  title: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Name', style: style),
                      Text('Total to Date', style: style),
                    ],
                  ),
                ),
                Expanded(child: ListView(children: tiles)),
              ],
            ),
          ),
        );
      },
    );
  }
}

void _editTag(final BuildContext context, final String tag) {
  String newTag = tag;

  showDialog<void>(
    context: context,
    builder:
        (final ctx) => InputForm(
          title: 'Edit Tag',
          onSave: () => ReckonerDb.I.transactionTagDao.updateTag(tag, newTag),
          onDelete:
              tag.isEmpty
                  ? null
                  : () => yesNoDialog(
                    context,
                    ReckonerLocalizations.of(context).deleteMsg('tag'),
                    callback: () async {
                      try {
                        await ReckonerDb.I.transactionTagDao.deleteTag(tag);
                      } catch (err, stack) {
                        logError('Reckoner', err, stack);
                      }
                    },
                  ),
          inputs: [
            TextFormField(
              textCapitalization: TextCapitalization.words,
              initialValue: newTag,
              onChanged: (final value) => newTag = value,
            ),
          ],
        ),
  );
}
