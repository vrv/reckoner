import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:watch_it/watch_it.dart';

import '../../database/reckoner_db.dart';
import '../../model/transaction.dart';
import '../../service/data_cache.dart';
import '../util/popup_dialogs.dart';
import '../widgets/amount_display.dart';
import '../widgets/form/edit_fields.dart';
import '../widgets/form/form_title.dart';
import '../widgets/form/input_form_screen.dart';
import '../widgets/transaction/transaction_edit_data.dart';
import '../widgets/transaction/transaction_edit_fields.dart';

class TransactionEntityEdit extends StatefulWidget {
  final String heroTag;
  final String? id;
  final bool copy;

  const TransactionEntityEdit({
    super.key,
    this.heroTag = '',
    this.id,
    this.copy = false,
  });

  @override
  State<StatefulWidget> createState() => _TxEntityEditState();
}

class _TxEntityEditState extends State<TransactionEntityEdit> {
  late TransactionEditData model;

  @override
  void initState() {
    super.initState();
    model = TransactionEditData(setState: setState);
    if (widget.id == null) return;
    ReckonerDb.I.transactionDao.getSingleTransactionEntity(widget.id!).then((
      final value,
    ) {
      if (value == null) return;
      model =
          widget.copy
              ? TransactionEditData(
                loadedTxEntity: value.copy(),
                setState: setState,
              )
              : TransactionEditData(loadedTxEntity: value, setState: setState);
      if (mounted) setState(() {});
    });
  }

  @override
  Widget build(final BuildContext context) {
    return InputFormScreen(
      title: titleWidget(
        context,
        model.isTransfer ? 'Transfer' : 'Transaction',
        widget.id,
      ),
      inputs: [
        if (model.isNew || !model.isTransfer)
          InputWrapper(
            size: InputWrapperSize.extraLarge,
            child: ChipEdit(
              onChanged:
                  (final v) =>
                      v == null ? null : setState(() => model.type = v),
              initialValue: model.type,
              options: [
                if (model.isNew || !model.isTransfer)
                  FormBuilderChipOption(
                    value: TransactionType.debit,
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Icon(
                          Icons.arrow_downward,
                          color: AmountDisplay.amountColor(-1),
                        ),
                        Text('Expense'),
                      ],
                    ),
                  ),
                if (model.isNew)
                  FormBuilderChipOption(
                    value: TransactionType.transfer,
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Icon(
                          Icons.sync_alt,
                          color: AmountDisplay.amountColor(0, true),
                        ),
                        Text('Transfer'),
                      ],
                    ),
                  ),
                if (model.isNew || !model.isTransfer)
                  FormBuilderChipOption(
                    value: TransactionType.credit,
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Icon(
                          Icons.arrow_upward,
                          color: AmountDisplay.amountColor(1),
                        ),
                        Text('Income'),
                      ],
                    ),
                  ),
              ],
            ),
          ),
        InputWrapper(
          size: InputWrapperSize.extraLarge,
          child: TxNameEdit(model: model.sourceTx, showLabel: true),
        ),
        Wrap(
          children: [
            ...model
                .accountWidgets(setState)
                .map((final e) => InputWrapper(child: e)),
            ...model.amountEditWidgets().map(
              (final e) => InputWrapper(child: e),
            ),
            InputWrapper(
              child: TxDateEdit(model: model.sourceTx, showLabel: true),
            ),
          ],
        ),
        ...model.sourceTx.splits.map(
          (final split) => _TransactionSplitEdit(
            txSaveModel: model.sourceTx,
            updateAmounts: model.updateAmounts,
            saveModel: split,
          ),
        ),
        if (!model.isTransfer)
          TextButton(
            onPressed: () {
              final amount = model.sourceTx.splits.last.amount ~/ 2;
              model.sourceTx.splits.last.amount -= amount;
              final split = TransactionSplit(
                uuid: model.sourceTx.uuid,
                id: model.sourceTx.splits.length,
                name: model.sourceTx.name,
                amount: amount,
              );
              setState(() => model.sourceTx.splits.add(split));
            },
            child: const Text('Add Split'),
          ),
      ],
      hasChanges: () => !model.isNew && model.entity != model.loadedTxEntity,
      heroTag: widget.heroTag,
      afterFormSave: () => model.save(),
      beforeCopy:
          model.isNew
              ? null
              : () {
                model
                  ..sourceTx = model.sourceTx.copy(true)
                  ..destTx = model.destTx.copy(true);
              },
      deleteCallback:
          widget.id == null
              ? null
              : () async {
                final entity = await ReckonerDb.I.transactionDao
                    .getSingleTransactionEntity(widget.id!);
                if (entity == null) return true;
                if (context.mounted) {
                  return deleteTxEntityPopup(context, entity);
                }
                return false;
              },
    );
  }
}

class _TransactionSplitEdit extends WatchingWidget {
  final Transaction txSaveModel;
  final TransactionSplit saveModel;
  final TransactionAmountNotifier updateAmounts;

  const _TransactionSplitEdit({
    required this.txSaveModel,
    required this.saveModel,
    required this.updateAmounts,
  });

  @override
  Widget build(final BuildContext context) {
    final bool isOnlySplit = txSaveModel.splits.length == 1;
    final List<Widget> inputList = [];
    final categoryGroupList = watchValue(
      (final DataCache c) => c.categoryGroups,
    );

    if (!isOnlySplit) {
      inputList.addAll([
        InputWrapper(
          child: TxNameEdit(
            model: saveModel,
            autovalidateMode: AutovalidateMode.disabled,
          ),
        ),
        InputWrapper(
          child: TxAmountEdit(
            transaction: txSaveModel,
            updateAmounts: updateAmounts,
            split: saveModel,
            onSaved: (final value) => saveModel.amount = value,
          ),
        ),
      ]);
    }

    for (final group in categoryGroupList) {
      if (group.categories.isNotEmpty) {
        inputList.add(
          InputWrapper(
            child: TxCategoryEdit(
              group: group,
              model: saveModel,
              showLabel: true,
            ),
          ),
        );
      }
    }

    inputList.add(
      InputWrapper(child: TxTagEdit(model: saveModel, showLabel: true)),
    );

    if (!isOnlySplit) {
      inputList.add(
        InputWrapper(
          size: InputWrapperSize.extraLarge,
          child: Row(
            children: [
              const Spacer(),
              TextButton(
                onPressed: () {
                  txSaveModel.splits.remove(saveModel);
                  for (int i = 0; i < txSaveModel.splits.length; ++i) {
                    txSaveModel.splits[i].id = i;
                  }
                  balanceTxSplits(txSaveModel);
                  updateAmounts.notify();
                },
                child: const Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [Icon(Icons.delete), Text('Delete Split')],
                ),
              ),
              const Spacer(),
            ],
          ),
        ),
      );
    }

    final column = Wrap(alignment: WrapAlignment.center, children: inputList);

    if (!isOnlySplit) {
      return InputFieldGroup(label: 'Split ${saveModel.id + 1}', child: column);
    }

    return column;
  }
}
