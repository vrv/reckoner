import 'package:flutter/material.dart';

import '../widgets/options/user_settings.dart';

class ErrorHome extends StatefulWidget {
  final String error;
  const ErrorHome(this.error, {super.key});

  @override
  State<StatefulWidget> createState() => _ErrorHomeState();
}

class _ErrorHomeState extends State<ErrorHome> {
  bool showSettings = false;

  @override
  Widget build(final BuildContext context) {
    if (showSettings) {
      return const Scaffold(body: UserSettingsEdit(fromError: true));
    }

    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Text('Error loading SQL database:\n\n${widget.error}'),
          TextButton(
            onPressed: () => setState(() => showSettings = true),
            child: Text(
              'Open User Settings',
              style: Theme.of(context).textTheme.bodyLarge?.copyWith(
                color: Colors.blue,
                fontWeight: FontWeight.bold,
                fontSize: 50,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
