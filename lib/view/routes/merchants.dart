import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:watch_it/watch_it.dart';

import '../../config/routes_const.dart';
import '../../database/dao/balance_dao.dart';
import '../../database/reckoner_db.dart';
import '../../service/data_cache.dart';
import '../../service/date_range.dart';
import '../l10n/localizations.dart';
import '../tutorial/merchant_tutorial.dart';
import '../tutorial/tutorial_handler.dart';
import '../util/popup_dialogs.dart';
import '../widgets/account_balance_display.dart';
import '../widgets/display_name_edit.dart';
import '../widgets/fixed_width_container.dart';
import '../widgets/form/form_title.dart';
import '../widgets/form/input_form.dart';
import '../widgets/layout_widgets.dart';
import '../widgets/no_data_placeholder.dart';

class MerchantDisplay extends WatchingStatefulWidget {
  const MerchantDisplay({super.key});

  @override
  State<StatefulWidget> createState() => _MerchantDisplay();
}

class _MerchantDisplay extends State<MerchantDisplay> {
  static const style = TextStyle(fontWeight: FontWeight.bold);
  late final ScrollController _controller;

  @override
  void initState() {
    super.initState();
    _controller = ScrollController();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  Future<void> renameMerchant(
    final BuildContext context,
    final String existingMerchant,
  ) => showDialog(
    context: context,
    builder: (final ctx) {
      String newName = existingMerchant;
      return InputForm(
        title: titleString(context, 'Merchant', existingMerchant),
        onSave:
            () => ReckonerDb.I.transactionMerchantDao.updateMerchant(
              existingMerchant,
              newName,
            ),
        onDelete:
            existingMerchant.isEmpty
                ? null
                : () async {
                  if (await yesNoDialog(
                    context,
                    ReckonerLocalizations.of(context).deleteMsg('merchant'),
                  )) {
                    await ReckonerDb.I.transactionMerchantDao.deleteMerchant(
                      existingMerchant,
                    );
                  }
                },
        inputs: [
          TextFormField(
            decoration: const InputDecoration(label: Text('Name')),
            initialValue: newName,
            onChanged: (final value) => newName = value,
          ),
        ],
      );
    },
  );

  @override
  Widget build(final BuildContext context) {
    final merchants = watchValue((final DataCache c) => c.merchants)
      ..sort((final a, final b) => a.compareTo(b));
    final endDate = watchPropertyValue(
      (final DateTimeRangeLoader l) => l.getRange()?.end,
    );
    tutorialHandler(
      watchIt<ValueNotifier<TutorialHelp>>().value,
      context,
      merchantTutorial,
      controllers: [_controller],
    );

    return StreamBuilder(
      stream: ReckonerDb.I.balanceDao.loadBalances(
        type: BalanceType.merchant,
        endDate: endDate,
      ),
      builder: (final context, final snapshot) {
        final balanceMap = snapshot.data ?? {};
        final tiles =
            merchants
                .mapIndexed(
                  (final i, final e) => ListTile(
                    title: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          child: DisplayNameEdit(
                            key: i == 0 ? merchantDisplayKey : null,
                            name: e,
                            onEdit: () => renameMerchant(context, e),
                            transactionPath: merchantTransactionsPath(e),
                            iconSize: 20,
                          ),
                        ),
                        fixedWidthSpacer(),
                        BalanceDisplay(
                          key: i == 0 ? merchantBalanceKey : null,
                          balanceMap: balanceMap[e] ?? {},
                        ),
                      ],
                    ),
                    visualDensity: const VisualDensity(
                      vertical: VisualDensity.minimumDensity,
                    ),
                  ),
                )
                .toList();

        if (balanceMap.containsKey(null)) {
          tiles.add(
            ListTile(
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  TextButton(
                    onPressed: () => context.push(noMerchantTransactionsPath),
                    child: const Text('(No Merchant)'),
                  ),
                  BalanceDisplay(balanceMap: balanceMap[null]!),
                ],
              ),
              visualDensity: const VisualDensity(
                vertical: VisualDensity.minimumDensity,
              ),
            ),
          );
        }

        return ReportHeader(
          report: di<DataCache>().merchantReport,
          child:
              tiles.isEmpty
                  ? const NoDataPlaceholder(
                    'No merchants created. Add a merchant by editing or creating a transaction.',
                  )
                  : FixedWidthContainer(
                    child: Column(
                      children: [
                        const ListTile(
                          title: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text('Name', style: style),
                              Text('Total to Date', style: style),
                            ],
                          ),
                        ),
                        Expanded(
                          child: ListView(
                            controller: _controller,
                            children: tiles,
                          ),
                        ),
                      ],
                    ),
                  ),
        );
      },
    );
  }
}
