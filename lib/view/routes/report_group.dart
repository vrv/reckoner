import 'dart:math';

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:watch_it/watch_it.dart';

import '../../config/routes_const.dart';
import '../../model/report.dart';
import '../../service/data_cache.dart';
import '../tutorial/report_group_tutorial.dart';
import '../tutorial/tutorial_handler.dart';
import '../widgets/layout_widgets.dart';
import '../widgets/transaction_report.dart';

const String heroTag = 'ReportEdit';

class ReportGroupDisplay extends WatchingWidget {
  const ReportGroupDisplay({super.key, required this.groupId});

  final String groupId;

  Widget _rowToDisplayList(
    final BuildContext context,
    final BoxConstraints constraints,
    final Map<String, Report> reportMap,
    final int maxSize,
    final ReportGroupRow row,
  ) {
    return SizedBox(
      width: constraints.maxWidth,
      child: Wrap(
        alignment: WrapAlignment.spaceEvenly,
        children:
            row.reports.map((final rpt) {
              return ConstrainedBox(
                constraints: BoxConstraints(
                  maxHeight: min(constraints.maxHeight / 2, maxSize.toDouble()),
                  maxWidth: min(
                    constraints.maxWidth,
                    maxSize.toDouble() * rpt.flex,
                  ),
                ),
                child: TransactionReport(
                  rpt,
                  showTitle:
                      ![ReportType.text, ReportType.table].contains(rpt.type),
                ),
              );
            }).toList(),
      ),
    );
  }

  @override
  Widget build(final BuildContext context) {
    final group = watchValue((final DataCache c) => c.reportGroupMap)[groupId];
    final reportMap = watchValue((final DataCache c) => c.reportMap);

    if (group == null) {
      return Center(
        child: Text(
          'Invalid Report Group',
          style: Theme.of(context).textTheme.titleLarge,
        ),
      );
    }
    tutorialHandler(
      watchIt<ValueNotifier<TutorialHelp>>().value,
      context,
      reportGroupTutorial,
    );
    return LayoutBuilder(
      builder: (final context, final constraints) {
        return FloatingButtonOverlay(
          SingleChildScrollView(
            child: Column(
              children:
                  group.layouts
                      .fold(
                        <ReportGroupRow>[],
                        (final prev, final layout) => prev + layout.rows,
                      )
                      .map(
                        (final row) => _rowToDisplayList(
                          context,
                          constraints,
                          reportMap,
                          group.cellSize,
                          row,
                        ),
                      )
                      .toList(),
            ),
          ),
          actionButtonKey: editReportGroupKey,
          heroTag: heroTag,
          onPressed: () => context.push(reportGroupEditPath(group.uuid)),
          icon: Icons.dashboard_customize,
          tooltip: 'Edit Reports',
        );
      },
    );
  }
}
