import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:watch_it/watch_it.dart';

import '../../database/reckoner_db.dart';
import '../../model/category.dart';
import '../../model/currency.dart';
import '../../service/data_cache.dart';
import '../model/currency_input_formatter.dart';
import '../util/relative_range_label.dart';
import '../util/save_item.dart';
import '../widgets/default_icon_button.dart';
import '../widgets/form/color_input.dart';
import '../widgets/form/edit_fields.dart';
import '../widgets/form/form_title.dart';
import '../widgets/form/input_form_screen.dart';
import '../widgets/form/list_search_input.dart';
import '../widgets/form/validators.dart';

(CategoryGroup, Category?) _findExisting(
  final String groupId,
  final String catId,
) {
  final group = di<DataCache>().categoryGroups.value.singleWhere(
    (final g) => g.uuid == groupId,
  );
  final foundCat = group.findCategory(catId);
  return (group, foundCat);
}

class CategoryEdit extends WatchingStatefulWidget {
  final String? categoryId;
  final String categoryGroupId;
  final bool copy;

  const CategoryEdit({
    super.key,
    required this.categoryGroupId,
    this.categoryId,
    this.copy = false,
  });

  @override
  State<CategoryEdit> createState() => _CategoryEdit();
}

class _CategoryEdit extends State<CategoryEdit> {
  late Category _model;
  late final Category? _existingCat;
  CurrencyMap currencyMap = {};

  @override
  void initState() {
    super.initState();

    final (group, foundCat) = _findExisting(
      widget.categoryGroupId,
      widget.categoryId ?? '',
    );
    _existingCat = foundCat;
    _existingCat?.datePeriod ??= group.datePeriod;
    _existingCat?.datePeriodInterval ??= group.datePeriodInterval;

    _model =
        foundCat?.copy(widget.copy) ?? Category()
          ..group = group;

    if (foundCat == null && group.requireLimits) {
      _model.limits.add(
        CategoryLimit(
          currencyCode: di<DataCache>().setting.value.defaultCurrencyCode ?? '',
        ),
      );
    }
  }

  List<Category> _buildParentListRecursive(
    final Category existingCat,
    final Category? filter,
  ) {
    if (existingCat == filter) return [];
    return existingCat.children.fold(
      [existingCat],
      (final list, final element) =>
          list + _buildParentListRecursive(element, filter),
    );
  }

  String _treeName(final Category data, final Iterable<Category> dataList) {
    if (data.parent != null) {
      return '${_treeName(data.parent!, dataList)} : ${data.displayName}';
    }
    return data.displayName;
  }

  List<Widget> _inputs() {
    final theme = Theme.of(context);
    final parentList = _model.group.categories
        .map((final e) => _buildParentListRecursive(e, _model))
        .fold(<Category>[], (final prev, final cur) => prev + cur);

    final List<Widget> inputs = [
      InputWrapper(child: NameEdit(model: _model)),
      InputWrapper(
        child: ListSearchFormField<Category>(
          decoration: const InputDecoration(
            suffixIcon: Icon(Icons.arrow_drop_down_rounded),
            label: Text('Category Parent'),
          ),
          onChanged: (final data) {
            _model
              ..parent = data
              ..progenitor = data?.parent;
            setState(() => _model.parent = data);
          },
          initialValue: _model.parent,
          searchItems: parentList,
          itemToDisplayString:
              (final Category? item) =>
                  item == null ? '' : _treeName(item, _model.group.categories),
        ),
      ),
    ];

    if (_model.group.requireLimits) {
      return [
        Wrap(
          alignment: WrapAlignment.spaceEvenly,
          children: [
            ...inputs,
            InputWrapper(
              size: InputWrapperSize.medium,
              child: ChipEdit<bool>(
                alignment: WrapAlignment.spaceEvenly,
                label: 'Limit Type',
                options: const [
                  FormBuilderChipOption(
                    value: true,
                    child: Text('Spending Limit'),
                  ),
                  FormBuilderChipOption(
                    value: false,
                    child: Text('Saving Limit'),
                  ),
                ],
                initialValue: _model.isSpendLimit,
                onChanged:
                    (final val) =>
                        val != null
                            ? setState(() {
                              _model.isSpendLimit = val;
                              final sign = _model.isSpendLimit ? -1 : 1;
                              _model.limits =
                                  _model.limits.map((final l) {
                                    l.amount = l.amount.abs() * sign;
                                    return l;
                                  }).toList();
                            })
                            : null,
                validator: (final value) => nonNullValidator(context, value),
              ),
            ),
            if (_model.group.enableAutoBalance)
              InputWrapper(
                size: InputWrapperSize.medium,
                child: FormBuilderSwitch(
                  name: 'autobalance',
                  title: const Text('Exclude from auto-balance'),
                  initialValue: _model.excludeFromAutoBalance,
                  onChanged:
                      (final value) =>
                          value != null
                              ? _model.excludeFromAutoBalance = value
                              : null,
                ),
              ),
          ],
        ),
        if (_model.parentUuid == null) CategoryBudgetEdit(_model),
        ExpansionTile(
          title: const Text('Limits'),
          initiallyExpanded: true,
          maintainState: true,
          children: [
            for (final limit in _model.limits)
              _singleLimitEdit(
                limit,
                DefaultIconButton(
                  onPressed: () => setState(() => _model.limits.remove(limit)),
                  icon: const Icon(Icons.delete),
                ),
              ),
            TextButton(
              onPressed:
                  () => setState(
                    () => _model.limits.add(CategoryLimit(uuid: _model.uuid)),
                  ),
              child: const Text('Add Limit'),
            ),
          ],
        ),
        ExpansionTile(
          title: const Text('Alerts'),
          initiallyExpanded: true,
          maintainState: true,
          children: [
            for (final warn in _model.alerts)
              _singleAlertEdit(
                warn,
                DefaultIconButton(
                  onPressed: () => setState(() => _model.alerts.remove(warn)),
                  icon: const Icon(Icons.delete),
                ),
              ),
            TextButton(
              onPressed:
                  () => setState(
                    () => _model.alerts.add(
                      CategoryAlert(
                        // ignore: deprecated_member_use
                        color: theme.colorScheme.onSurface.value,
                        uuid: _model.uuid,
                      ),
                    ),
                  ),
              child: const Text('Add Alert'),
            ),
          ],
        ),
      ];
    }

    return [Wrap(children: inputs)];
  }

  Widget _singleLimitEdit(
    final CategoryLimit limit,
    final Widget deleteWidget,
  ) {
    final currency = currencyMap[limit.currencyCode];
    final currencyFormatter = CurrencyInputFormatter(
      currency: currency,
      enableNegative: false,
    );
    final filterCurrencies = _model.limits
        .where((final l) => l != limit)
        .map((final l) => l.currencyCode);
    final searchCurrencies =
        currencyMap.values
            .where((final c) => !filterCurrencies.contains(c.code))
            .toList();
    return Wrap(
      alignment: WrapAlignment.center,
      crossAxisAlignment: WrapCrossAlignment.center,
      children: [
        InputWrapper(
          size: InputWrapperSize.small,
          child: TextFormField(
            decoration: const InputDecoration(labelText: 'Amount'),
            onChanged:
                (final value) =>
                    limit.amount =
                        currencyFormatter.toDB(value).abs() *
                        (_model.isSpendLimit ? -1 : 1),
            initialValue:
                limit.amount == 0
                    ? null
                    : currencyFormatter.fromDB(limit.amount.abs()),
            inputFormatters: [currencyFormatter],
            validator: (final value) => nonNullValidator(context, value),
          ),
        ),
        InputWrapper(
          size: InputWrapperSize.small,
          child: ListSearchFormField<Currency>(
            decoration: const InputDecoration(
              suffixIcon: Icon(Icons.arrow_drop_down_rounded),
              label: Text('Currency'),
            ),
            itemToDisplayString: (final item) => item.name,
            onChanged:
                (final data) =>
                    setState(() => limit.currency = data ?? limit.currency),
            initialValue: currencyMap[limit.currencyCode],
            searchItems: searchCurrencies,
            validator: (final value) => nonNullValidator(context, value),
          ),
        ),
        InputWrapper(
          size: InputWrapperSize.small,
          child: FormBuilderCheckbox(
            name: 'LimitRollover',
            title: const Text('Rollover Balance'),
            initialValue: limit.isRollover,
            onChanged:
                (final value) => limit.isRollover = value ?? limit.isRollover,
          ),
        ),
        deleteWidget,
      ],
    );
  }

  Widget _singleAlertEdit(
    final CategoryAlert alert,
    final Widget deleteWidget,
  ) {
    final theme = Theme.of(context);
    return Padding(
      padding: const EdgeInsets.all(8),
      child: Wrap(
        children: [
          ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: 500),
            child: FormBuilderSlider(
              name: 'slider',
              validator: (final value) => nonNullValidator(context, value),
              min: 0.0,
              max: 100.0,
              initialValue: alert.percentDone,
              divisions: 100,
              activeColor: Color(alert.color),
              inactiveColor: theme.disabledColor,
              decoration: const InputDecoration(labelText: 'Percent Complete'),
              onChanged: (final value) => alert.percentDone = value!,
            ),
          ),
          ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: 500),
            child: ColorInputForm(
              textLabel: 'Color',
              initialValue: Color(alert.color),
              onChanged:
                  (final color) =>
                  // ignore: deprecated_member_use
                  setState(() => alert.color = color!.value),
              validator: (final color) => nonNullValidator(context, color),
              showQuickColors: true,
            ),
          ),
          deleteWidget,
        ],
      ),
    );
  }

  @override
  Widget build(final BuildContext context) {
    currencyMap = watchValue((final DataCache c) => c.currencyMap);
    return InputFormScreen(
      title: titleWidget(context, _model.group.name, widget.categoryId),
      inputs: _inputs(),
      afterFormSave: () {
        return saveItem(_model);
      },
      hasChanges: () => _existingCat != null && _existingCat != _model,
      deleteCallback:
          _existingCat == null
              ? null
              : () async =>
                  await ReckonerDb.I.commonDao.softDeleteDbItem(_existingCat) >
                  0,
      beforeCopy: _model.isNew ? null : () => _model = _model.copy(true),
    );
  }
}

class CategoryBudgetEdit extends StatefulWidget {
  final Category model;
  const CategoryBudgetEdit(this.model, {super.key});
  @override
  State<StatefulWidget> createState() => _CategoryBudgetEdit();
}

class _CategoryBudgetEdit extends State<CategoryBudgetEdit> {
  late Category model;
  late final Category? existing;

  @override
  void initState() {
    super.initState();
    model = widget.model;
    model.datePeriod ??= model.group.datePeriod ?? CategoryLimitPeriod.monthly;
    model.datePeriodInterval ??= model.group.datePeriodInterval ?? 1;
    final (_, found) = _findExisting(model.groupUuid, model.uuid);
    existing = found;
  }

  @override
  Widget build(final BuildContext context) {
    final int sliderMax = switch (model.datePeriod!) {
      CategoryLimitPeriod.daily => 30,
      CategoryLimitPeriod.weekly => 26,
      CategoryLimitPeriod.monthly => 18,
      CategoryLimitPeriod.yearly => 5,
    };

    final showOffset =
        model.datePeriodInterval! > 1 &&
        (model.isNew ||
            (existing != null &&
                (existing!.datePeriod != model.datePeriod ||
                    existing!.datePeriodInterval != model.datePeriodInterval)));

    return Wrap(
      children: [
        InputWrapper(
          child: ChipEdit<CategoryLimitPeriod>(
            alignment: WrapAlignment.spaceEvenly,
            label: 'Limit Generation Period',
            options: [
              for (final e in CategoryLimitPeriod.values)
                FormBuilderChipOption(
                  value: e,
                  child: Text(EnumToString.convertToString(e)),
                ),
            ],
            initialValue: model.datePeriod,
            validator: (final value) => nonNullValidator(context, value),
            onChanged:
                (final value) => setState(() => model.datePeriod = value),
          ),
        ),
        InputWrapper(
          size: InputWrapperSize.small,
          child: FormBuilderSlider(
            name: 'periodPerLimit',
            decoration: const InputDecoration(labelText: 'Periods per Budget'),
            displayValues: DisplayValues.current,
            min: 1,
            max: sliderMax.toDouble(),
            divisions: sliderMax - 1,
            initialValue: model.datePeriodInterval!.toDouble(),
            onChangeEnd: (final value) {
              model.firstDateOffset = 0;
              setState(() => model.datePeriodInterval = value.round());
            },
          ),
        ),
        if (showOffset)
          InputWrapper(
            size: InputWrapperSize.small,
            child: FormBuilderSlider(
              name: 'FirstBudgetDate',
              initialValue: model.firstDateOffset.toDouble(),
              decoration: const InputDecoration(
                labelText: 'Start Date for first Budget',
              ),
              min: -model.datePeriodInterval!.toDouble() + 1,
              max: 0,
              divisions: model.datePeriodInterval! - 1,
              onChanged:
                  (final value) =>
                      model.firstDateOffset =
                          value?.toInt() ?? model.firstDateOffset,
              valueWidget:
                  (final value) => Text(
                    '${relativeRangeLabel(switch (model.datePeriod!) {
                      CategoryLimitPeriod.daily => TimeRange.day,
                      CategoryLimitPeriod.weekly => TimeRange.week,
                      CategoryLimitPeriod.monthly => TimeRange.month,
                      CategoryLimitPeriod.yearly => TimeRange.year,
                    }, model.firstDateOffset, true)}${model.firstDateOffset.abs() > 1 ? ' ago' : ''}',
                  ),
            ),
          ),
      ],
    );
  }
}
