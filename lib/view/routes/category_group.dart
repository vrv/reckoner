import 'dart:math';

import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:intl/intl.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:watch_it/watch_it.dart';

import '../../config/routes_const.dart';
import '../../database/reckoner_db.dart';
import '../../model/category.dart';
import '../../model/currency.dart';
import '../../model/transaction.dart';
import '../../model/tz_date_time.dart';
import '../../service/data_cache.dart';
import '../../service/date_range.dart';
import '../../service/sync_service_manager.dart';
import '../l10n/localizations.dart';
import '../model/currency_input_formatter.dart';
import '../model/layout_config.dart';
import '../tutorial/category_group_tutorial.dart';
import '../tutorial/tutorial_handler.dart';
import '../util/save_item.dart';
import '../widgets/default_icon_button.dart';
import '../widgets/display_name_edit.dart';
import '../widgets/fixed_width_container.dart';
import '../widgets/layout_widgets.dart';
import '../widgets/no_data_placeholder.dart';

class CategoryGroupDisplay extends WatchingStatefulWidget {
  final String groupId;

  late final String itemName;

  CategoryGroupDisplay({required this.groupId, super.key}) {
    itemName =
        di<DataCache>().categoryGroups.value
            .singleWhere((final g) => g.uuid == groupId)
            .name;
  }
  @override
  State<StatefulWidget> createState() => _CategoryGroupDisplay();
}

class _CategoryGroupDisplay extends State<CategoryGroupDisplay> {
  late final ScrollController _controller;

  @override
  void initState() {
    super.initState();
    _controller = ScrollController();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  Widget _mainWidget(
    final CategoryGroup group, [
    final TZDateTime? endDate,
    final CatBudgetAmounts amounts = const {},
  ]) => LayoutBuilder(
    builder: (final context, final constraints) {
      return ReportHeader(
        report: di<DataCache>().reportMap.value[group.reportUuid],
        child: FixedWidthContainer(
          width: min(constraints.maxWidth, 1500),
          topBottomPadding: false,
          child: DefaultTextStyle(
            style: Theme.of(context).textTheme.titleLarge!,
            child: SingleChildScrollView(
              controller: _controller,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ...group.categories.mapIndexed(
                    (final i, final e) =>
                        _CategoryDisplay(group, i == 0, e, endDate, amounts),
                  ),
                  if (group.requireLimits) _CategoryDisplay(group, false),
                ],
              ),
            ),
          ),
        ),
      );
    },
  );

  Widget _body(final CategoryGroup group, final TZDateTime? endDate) {
    if (group.categories.isEmpty) {
      return const NoDataPlaceholder(
        'No categories available. Please add a category.',
      );
    } else if (group.requireLimits) {
      return StreamBuilder(
        stream: ReckonerDb.I.categoryDao.loadBudgetAmounts(
          group.categories,
          endDate,
        ),
        builder: (final context, final snapshot) {
          return _mainWidget(group, endDate, snapshot.data ?? {});
        },
      );
    }
    return _mainWidget(group);
  }

  void _sortCategories(
    final CategoryGroup group,
    final CategoryBudgetMap budgetMap,
    final String? currencyCode,
  ) {
    int sortFunction(final Category first, final Category second) {
      if (first == second) return 0;

      if (!group.requireLimits) return first.name.compareTo(second.name);

      final durationCompare = first.getRange().duration.compareTo(
        second.getRange().duration,
      );

      if (durationCompare != 0) return durationCompare;

      final firstBudget =
          budgetMap[first.groupUuid]?[first.uuid]?[currencyCode];
      final secondBudget =
          budgetMap[second.groupUuid]?[second.uuid]?[currencyCode];

      if (firstBudget != null &&
          secondBudget != null &&
          firstBudget.endDate != secondBudget.endDate) {
        return firstBudget.endDate.compareTo(secondBudget.endDate);
      }

      final firstAmount =
          firstBudget?.amount ??
          first.limits
              .singleWhereOrNull(
                (final element) => element.currencyCode == currencyCode,
              )
              ?.amount;
      final secondAmount =
          secondBudget?.amount ??
          second.limits
              .singleWhereOrNull(
                (final element) => element.currencyCode == currencyCode,
              )
              ?.amount;

      final amountCompare =
          secondAmount?.abs().compareTo(firstAmount?.abs() ?? 0) ?? 0;

      return switch (amountCompare) {
        0 => first.name.compareTo(second.name),
        _ => amountCompare,
      };
    }

    void recursiveSort(final Category cat) {
      cat.children.sort(sortFunction);
      if (cat.children.length < 2) return;
      for (final child in cat.children) {
        recursiveSort(child);
      }
    }

    group.categories.sort(sortFunction);
    for (final cat in group.categories) {
      recursiveSort(cat);
    }
  }

  @override
  Widget build(final BuildContext context) {
    final group = watchValue(
      (final DataCache c) => c.categoryGroups,
    ).singleWhere((final g) => g.uuid == widget.groupId);
    final budgetMap = watchValue((final DataCache c) => c.categoryBudgetMap);
    _sortCategories(
      group,
      budgetMap,
      di<DataCache>().setting.value.defaultCurrencyCode,
    );
    final filterRange = watchPropertyValue(
      (final DateTimeRangeLoader l) => l.getRange(),
    );
    tutorialHandler(
      watchIt<ValueNotifier<TutorialHelp>>().value,
      context,
      categoryTutorial,
      controllers: [_controller],
    );

    return FloatingButtonOverlay(
      _body(group, filterRange?.end),
      actionButtonKey: addCategoryKey,
      heroTag: 'add',
      onPressed:
          () => context.push(
            categoryEditPath(newId),
            extra: {'groupId': widget.groupId},
          ),
      tooltip: ReckonerLocalizations.of(context).addItem(widget.itemName),
      icon: Icons.add,
    );
  }
}

class _CategoryDisplay extends StatelessWidget {
  final CategoryGroup group;
  final Category? category;
  final TZDateTime? endDate;
  final CatBudgetAmounts amounts;
  final bool first;

  const _CategoryDisplay(
    this.group,
    this.first, [
    this.category,
    this.endDate,
    this.amounts = const {},
  ]);

  @override
  Widget build(final BuildContext context) {
    String formatter(final DateTime x) => DateFormat.Md().format(x);
    final theme = Theme.of(context);

    if (category == null) {
      final range = di<DateTimeRangeLoader>().getRange();
      return ListTile(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            TextButton(
              onPressed:
                  () => context.push(uncategorizedTransactionsPath(group.uuid)),
              child: const Text(
                'Uncategorized',
                overflow: TextOverflow.ellipsis,
              ),
            ),
            fixedWidthSpacer(),
            Text(
              range != null
                  ? '${formatter(range.start)} - ${formatter(range.end)}'
                  : 'Everything',
            ),
          ],
        ),
        subtitle: _UncategorizedAmount(group),
      );
    }

    final Category parent = category!;

    final range =
        di<DataCache>()
            .categoryBudgetMap
            .value[parent.groupUuid]?[parent.uuid]
            ?.values
            .first
            .range;

    final childNodes =
        parent.children
            .map(
              (final e) => _CategoryDisplay(group, false, e, endDate, amounts),
            )
            .toList();

    DisplayNameEdit(name: '');

    final title = Row(
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        DisplayNameEdit(
          key: first ? categoryDisplayKey : null,
          name: parent.name,
          path: categoryEditPath(parent.uuid),
          onEdit:
              () => context.push(
                categoryEditPath(parent.uuid),
                extra: {'groupId': group.uuid},
              ),
          transactionPath: categoryTransactionsPath(parent.uuid),
          iconSize: 20,
        ),
        if (range != null) fixedWidthSpacer(),
        if (range != null)
          Text(
            '${formatter(range.start)} - ${formatter(range.end)}',
            key: first ? budgetRangeKey : null,
          ),
      ],
    );

    if (childNodes.isEmpty) {
      return ListTile(
        title: title,
        subtitle: _CategoryDatesEdit(
          parent,
          amounts,
          key: first ? budgetAmountKey : null,
        ),
      );
    }

    final isTall = LayoutConfig(context).layout == LayoutTypes.tall;
    return ExpansionTile(
      title: title,
      subtitle: _CategoryDatesEdit(
        parent,
        amounts,
        key: first ? budgetAmountKey : null,
      ),
      childrenPadding: EdgeInsets.only(left: isTall ? 20 : 50),
      initiallyExpanded: true,
      textColor: theme.colorScheme.onSurface,
      collapsedTextColor: theme.colorScheme.onSurface,
      iconColor: theme.colorScheme.onSurface,
      collapsedIconColor: theme.colorScheme.onSurface,
      children: childNodes,
    );
  }
}

class _CategoryDatesEdit extends WatchingStatefulWidget {
  final Category category;
  final CatBudgetAmounts amounts;

  const _CategoryDatesEdit(this.category, this.amounts, {super.key});

  @override
  State<StatefulWidget> createState() => _CategoryDatesEditState();
}

class _CategoryDatesEditState extends State<_CategoryDatesEdit> {
  bool _edit = false;
  Map<String, CurrencyToCategoryBudgetMap> groupBudgets = {};

  Color color(final double percentSpent) {
    int colorVal =
        percentSpent > 1
            ? widget.category.group.overColor
            : widget.category.group.defaultColor;
    colorVal = widget.category.alerts.fold(
      colorVal,
      (final previousValue, final element) =>
          element.percentDone <= 100 * percentSpent
              ? element.color
              : previousValue,
    );
    return Color(colorVal);
  }

  bool needsCash(final CategoryBudget budget) {
    if (budget.amount >= 0) return false;
    final amount = widget.amounts[widget.category.uuid]?[budget.currencyCode];
    if (amount == null) return false;
    return amount < budget.amount;
  }

  bool groupHasExtra(final String currencyCode) {
    final familyUuids = widget.category.familyUuids;
    for (final uuid in groupBudgets.keys) {
      if (familyUuids.contains(uuid)) continue;
      final budget = groupBudgets[uuid]![currencyCode];
      if (budget == null) continue;
      if (budget.amount - (widget.amounts[uuid]?[currencyCode] ?? 0) < 0) {
        return true;
      }
    }
    return false;
  }

  @override
  Widget build(final BuildContext context) {
    groupBudgets =
        watchValue((final DataCache c) => c.categoryBudgetMap)[widget
            .category
            .groupUuid] ??
        {};
    final dates = groupBudgets[widget.category.uuid] ?? {};

    if (dates.isEmpty) return const SizedBox.shrink();

    final budgetWidgets = dates.values.map((final date) {
      final spent = widget.amounts[date.uuid]?[date.currencyCode] ?? 0;
      final currency = date.currency;
      final currencyFormatter = CurrencyInputFormatter(
        currency: currency,
        enableNegative: false,
      );
      final sign = widget.category.isSpendLimit ? -1 : 1;

      final double percent = spent.toDouble() / date.amount;

      final displayText = currency.formatAmount(spent);

      return Row(
        children: [
          Expanded(
            child: LinearPercentIndicator(
              animation: true,
              animationDuration: 500,
              backgroundColor: Theme.of(context).highlightColor,
              barRadius: const Radius.circular(5),
              center: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    _edit
                        ? currency.formatAmount(spent)
                        : '$displayText / ${currency.formatAmount(date.amount)}',
                  ),
                  if (_edit)
                    SizedBox(
                      width: 150,
                      child: TextFormField(
                        decoration: const InputDecoration(isDense: true),
                        textAlignVertical: TextAlignVertical.center,
                        initialValue: currency.formatAmount(date.amount.abs()),
                        inputFormatters: [currencyFormatter],
                        onChanged:
                            (final value) =>
                                date
                                  ..edited = true
                                  ..amount =
                                      sign * currencyFormatter.toDB(value),
                      ),
                    ),
                ],
              ),
              lineHeight: 25,
              percent:
                  percent < 0
                      ? 0
                      : percent > 1
                      ? 1
                      : percent,
              progressColor: color(percent),
            ),
          ),
          if (!widget.category.excludeFromAutoBalance &&
              widget.category.group.enableAutoBalance &&
              needsCash(date) &&
              groupHasExtra(date.currencyCode))
            DefaultIconButton(
              onPressed: () async {
                await ReckonerDb.I.categoryDao.balanceLimitDates(
                  date,
                  widget.amounts,
                  di<DataCache>().categoryBudgetMap.value,
                );
                di<SyncServiceManager>().sendUpdates();
              },
              icon: const Icon(Icons.currency_exchange),
              tooltip: 'Autobalance',
            ),
        ],
      );
    });

    return Row(
      children: [
        Expanded(
          child: Padding(
            padding: const EdgeInsets.all(1.0),
            child:
                _edit
                    ? Column(children: budgetWidgets.toList())
                    : GestureDetector(
                      onTap: () => setState(() => _edit = true),
                      child: Column(children: budgetWidgets.toList()),
                    ),
          ),
        ),
        fixedWidthSpacer(),
        if (_edit)
          DefaultIconButton(
            onPressed: () async {
              for (final date in dates.values) {
                await saveItem(date);
              }
              setState(() => _edit = false);
            },
            icon: const Icon(Icons.save),
          ),
        if (_edit)
          DefaultIconButton(
            onPressed: () => setState(() => _edit = false),
            icon: const Icon(Icons.cancel),
          ),
      ],
    );
  }
}

class _UncategorizedAmount extends WatchingWidget {
  final CategoryGroup group;

  const _UncategorizedAmount(this.group);

  @override
  Widget build(final BuildContext context) {
    final transactions = watchValue((final DataCache c) => c.transactions);
    final filterRange = watchPropertyValue(
      (final DateTimeRangeLoader l) => l.getRange(),
    );
    final Map<Currency, int> amountMap = {};
    final range = (Category()..group = group).getRange(filterRange?.end);
    void filterTx(final Transaction? tx) {
      if (tx == null) return;
      if (tx.date.isAtSameMomentAs(range.start) ||
          (tx.date.isAfter(range.start) && tx.date.isBefore(range.end))) {
        final int amount = tx.splits
            .where((final split) => split.categories[group.uuid] == null)
            .fold(
              0,
              (final previousValue, final element) =>
                  previousValue + element.amount,
            );
        amountMap[tx.currency] = (amountMap[tx.currency] ?? 0) + amount;
      }
    }

    for (final tx in transactions) {
      filterTx(tx.transaction);
      filterTx(tx.transfer?.source);
      filterTx(tx.transfer?.destination);
    }

    return Column(
      children:
          amountMap
              .map(
                (final cur, final amount) =>
                    MapEntry(cur, Text(cur.formatAmount(amount))),
              )
              .values
              .toList(),
    );
  }
}
