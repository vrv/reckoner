import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:watch_it/watch_it.dart';

import '../../config/routes_const.dart';
import '../../database/dao/balance_dao.dart';
import '../../database/reckoner_db.dart';
import '../../model/account.dart';
import '../../model/currency.dart';
import '../../service/data_cache.dart';
import '../../service/date_range.dart';
import '../l10n/localizations.dart';
import '../tutorial/account_tutorial.dart';
import '../tutorial/tutorial_handler.dart';
import '../util/popup_dialogs.dart';
import '../util/save_item.dart';
import '../widgets/account_balance_display.dart';
import '../widgets/display_name_edit.dart';
import '../widgets/fixed_width_container.dart';
import '../widgets/form/edit_fields.dart';
import '../widgets/form/form_title.dart';
import '../widgets/form/input_form.dart';
import '../widgets/layout_widgets.dart';
import '../widgets/no_data_placeholder.dart';

enum DataChoices { account, organization }

class AccountsDisplay extends WatchingStatefulWidget {
  const AccountsDisplay({super.key});

  @override
  State<StatefulWidget> createState() => _AccountsDisplay();
}

class _AccountsDisplay extends State<AccountsDisplay> {
  late final ScrollController _controller;

  @override
  void initState() {
    super.initState();
    _controller = ScrollController();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    tutorialHandler(
      watchIt<ValueNotifier<TutorialHelp>>().value,
      context,
      accountTutorial,
      controllers: [_controller],
    );
    final currencyMap = di<DataCache>().currencyMap.value;
    final dataList = watchValue((final DataCache c) => c.organizations);
    final endDate = watchPropertyValue(
      (final DateTimeRangeLoader l) => l.getRange()?.end,
    );

    return StreamBuilder(
      stream: ReckonerDb.I.balanceDao.loadBalances(
        type: BalanceType.account,
        endDate: endDate,
      ),
      builder: (final context, final snapshot) {
        final Widget body = ReportHeader(
          report: di<DataCache>().accountReport,
          child: _body(context, dataList, currencyMap, snapshot.data ?? {}),
        );
        return FloatingButtonOverlay(
          body,
          actionButtonKey: addOrgKey,
          heroTag: 'add',
          onPressed: () => editHandler(context, null),
          tooltip: ReckonerLocalizations.of(context).addItem(itemName),
          icon: Icons.add_business,
        );
      },
    );
  }

  Widget _body(
    final BuildContext context,
    final List<Organization> dataList,
    final CurrencyMap currencyMap,
    final BalanceMap balances,
  ) {
    if (dataList.isEmpty) {
      return const NoDataPlaceholder(
        'No organizations created. Please create an organization first.',
      );
    }
    return FixedWidthContainer(
      child: ListView(
        controller: _controller,
        children:
            dataList
                .mapIndexed(
                  (final i, final orgInfo) => ListTile(
                    title: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        DisplayNameEdit(
                          key: i == 0 ? orgDisplayKey : null,
                          name: orgInfo.name,
                          onEdit: () => editHandler(context, orgInfo),
                          style: Theme.of(context).textTheme.headlineSmall,
                        ),
                        TextButton(
                          key: i == 0 ? addAccountButtonKey : null,
                          child: Text(
                            '+ Add Account',
                            style: Theme.of(context).textTheme.bodySmall,
                          ),
                          onPressed:
                              () => context.push(
                                accountEditPath(newId),
                                extra: {'orgId': orgInfo.uuid},
                              ),
                        ),
                      ],
                    ),
                    subtitle: Column(
                      children:
                          orgInfo.accounts
                              .mapIndexed(
                                (final j, final e) => _accountSummary(
                                  context,
                                  e,
                                  currencyMap,
                                  balances,
                                  i == 0 && j == 0,
                                ),
                              )
                              .toList(),
                    ),
                  ),
                )
                .toList(),
      ),
    );
  }

  List<({int color, String msg})> _checkAlerts(
    final Account account,
    final BalanceMap balanceMap,
  ) {
    final List<({int color, String msg})> warnList = [];
    final balances = balanceMap[account.uuid] ?? {};
    for (final alert in account.alerts) {
      final curCode = alert.currencyCode;
      final balance = balances[curCode] ?? 0;
      if (alert.isMax ? balance >= alert.amount : balance <= alert.amount) {
        warnList.add((color: alert.color, msg: alert.message));
      }
    }
    return warnList;
  }

  Widget _accountSummary(
    final BuildContext context,
    final Account item,
    final Map<String, Currency> currencyMap,
    final BalanceMap balanceMap,
    final bool tutorialKey,
  ) {
    return ListTile(
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Flexible(
            child: DisplayNameEdit(
              key: tutorialKey ? accountDisplayKey : null,
              name: item.name,
              path: accountEditPath(item.uuid),
              transactionPath: accountTransactionsPath(item.uuid),
              style: Theme.of(context).textTheme.titleLarge,
            ),
          ),

          //Needs to be in the title as in the trailing could cause a pixel
          //overflow. The title is the main size of the ListTile
          BalanceDisplay(
            key: tutorialKey ? accountBalanceKey : null,
            balanceMap: balanceMap[item.uuid] ?? {},
            defaultCurrency: currencyMap[item.defaultCurrencyCode ?? ''],
          ),
        ],
      ),
      subtitle: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children:
            _checkAlerts(item, balanceMap).map((final alert) {
              final color = Color(alert.color);
              final TextStyle style = Theme.of(
                context,
              ).textTheme.bodyMedium!.copyWith(color: color);
              return Text(alert.msg, style: style);
            }).toList(),
      ),
    );
  }

  Future<void> editHandler(
    final BuildContext context,
    final Organization? editItem,
  ) => showDialog(
    context: context,
    builder: (final context) {
      final model = editItem?.copy() ?? Organization();

      return InputForm(
        title: titleString(context, 'Organization', editItem),
        onSave: () => saveItem<Organization>(model),
        onDelete:
            editItem == null
                ? null
                : () => deletePopup(context, model, itemName),
        inputs: [NameEdit(model: model)],
      );
    },
  );

  String get itemName => 'Organization';
}
