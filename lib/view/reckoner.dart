import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart'
    show
        GlobalMaterialLocalizations,
        GlobalWidgetsLocalizations,
        GlobalCupertinoLocalizations;
import 'package:tray_manager/tray_manager.dart';
import 'package:watch_it/watch_it.dart';
import 'package:window_manager/window_manager.dart';

import '../config/platform.dart';
import '../database/reckoner_db.dart';
import '../service/sync_service_manager.dart';
import 'l10n/localizations.dart';
import 'router.dart';
import 'routes/_error.dart';
import 'theme.dart';
import 'util/database_init.dart';
import 'widgets/layout_widgets.dart';

class Reckoner extends StatefulWidget {
  const Reckoner({super.key});

  static void restartApp(final BuildContext context) {
    context.findAncestorStateOfType<_ReckonerState>()?.restartApp();
  }

  @override
  State<Reckoner> createState() => _ReckonerState();
}

class _ReckonerState extends State<Reckoner> {
  Key key = UniqueKey();

  void restartApp() {
    setState(() {
      key = UniqueKey();
    });
  }

  @override
  Widget build(final BuildContext context) {
    return KeyedSubtree(
      key: key,
      child: FutureBuilder<String>(
        future: databaseInit(),
        builder: (final context, final snapshot) {
          if (snapshot.hasError) {
            return ReckonerMaterialBase(snapshot.error.toString());
          } else if (snapshot.hasData) {
            return ReckonerMaterialBase(snapshot.data!);
          }
          return MaterialApp(
            theme: di<ThemeHandler>().themeData,
            home: Wrap(
              alignment: WrapAlignment.center,
              crossAxisAlignment: WrapCrossAlignment.center,
              runAlignment: WrapAlignment.center,
              children: [
                Text(
                  'Loading Database',
                  style: Theme.of(context).textTheme.displayMedium,
                  textAlign: TextAlign.center,
                ),
                fixedWidthSpacer(),
                const CircularProgressIndicator(),
              ],
            ),
          );
        },
      ),
    );
  }
}

class ReckonerMaterialBase extends WatchingStatefulWidget {
  final String error;

  const ReckonerMaterialBase(this.error, {super.key});

  static void redraw(final BuildContext context) {
    context.findAncestorStateOfType<_ReckonerMaterialBase>()?.redraw();
  }

  @override
  State<StatefulWidget> createState() => _ReckonerMaterialBase();
}

class _ReckonerMaterialBase extends State<ReckonerMaterialBase>
    with WindowListener, TrayListener, WidgetsBindingObserver {
  String statusMessage = '';
  bool isVisible = true;
  bool isMinimized = false;
  Key key = UniqueKey();

  void redraw() {
    setState(() {
      key = UniqueKey();
    });
  }

  Menu get trayMenu {
    return Menu(
      items: [
        if (statusMessage.isNotEmpty) MenuItem(label: statusMessage),
        MenuItem(
          key: 'show',
          label: 'Show',
          disabled: isVisible && !isMinimized,
        ),
        MenuItem(key: 'hide', label: 'Hide', disabled: !isVisible),
        MenuItem(key: 'close', label: 'Quit'),
      ],
    );
  }

  @override
  void initState() {
    super.initState();
    if (isDesktop) {
      windowManager.addListener(this);
      trayManager
        ..addListener(this)
        ..setContextMenu(trayMenu);
    } else if (isMobile) {
      WidgetsBinding.instance.addObserver(this);
    }
  }

  @override
  void onTrayIconMouseDown() => trayManager.popUpContextMenu();

  @override
  void dispose() {
    if (isDesktop) {
      windowManager.removeListener(this);
      trayManager.removeListener(this);
    } else if (isMobile) {
      WidgetsBinding.instance.removeObserver(this);
    }
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    final themeData = watchPropertyValue((final ThemeHandler t) => t.themeData);
    statusMessage = widget.error;
    if (statusMessage.isEmpty && di<SyncServiceManager>().enabled) {
      statusMessage = watchPropertyValue(
        (final SyncServiceManager m) => m.statusMessage,
      );
    }
    const localizationsDelegates = [
      ReckonerLocalizations.delegate,
      GlobalMaterialLocalizations.delegate,
      GlobalWidgetsLocalizations.delegate,
      GlobalCupertinoLocalizations.delegate,
    ];

    if (widget.error.isEmpty) {
      return KeyedSubtree(
        key: key,
        child: MaterialApp.router(
          debugShowCheckedModeBanner: false,
          routerConfig: AppRouter.router,
          title: 'Reckoner',
          theme: themeData,
          localizationsDelegates: localizationsDelegates,
          supportedLocales: const [
            Locale('en', ''), // English, no country code
          ],
        ),
      );
    }

    return MaterialApp(
      theme: themeData,
      localizationsDelegates: localizationsDelegates,
      home: ErrorHome(widget.error),
    );
  }

  @override
  void onWindowEvent(final String eventName) {
    switch (eventName) {
      case 'close':
        _hideApp();
        trayManager.setContextMenu(trayMenu);
        return;
      case 'hide':
        isVisible = false;
        break;
      case 'show':
        isVisible = true;
        break;
      case kWindowEventMinimize:
        isMinimized = true;
        break;
      case kWindowEventRestore:
        isMinimized = false;
        break;
    }
    trayManager.setContextMenu(trayMenu);
    super.onWindowEvent(eventName);
  }

  @override
  void onWindowFocus() => setState(() {});

  @override
  void onTrayMenuItemClick(final MenuItem menuItem) {
    switch (menuItem.key) {
      case 'show':
        windowManager.show();
        break;
      case 'hide':
        _hideApp();
        break;
      case 'close':
        ReckonerDb.dispose().then((final _) => exit(0));
    }
  }

  void _hideApp() =>
      defaultTargetPlatform == TargetPlatform.macOS
          ? windowManager.minimize()
          : windowManager.hide();

  @override
  void didChangeAppLifecycleState(final AppLifecycleState state) {
    if (!isMobile) return;
    // Handle lifecycle events here
    switch (state) {
      case AppLifecycleState.paused:
        di<SyncServiceManager>().stop();
        break;
      case AppLifecycleState.resumed:
        if (!di<SyncServiceManager>().isConnected) {
          di<SyncServiceManager>().start(force: true);
        }
        break;
      default:
        break;
    }
  }
}
