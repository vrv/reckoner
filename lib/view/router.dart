import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:watch_it/watch_it.dart';

import '../config/routes_const.dart';
import '../model/report.dart';
import '../service/app_lock.dart';
import '../service/data_cache.dart';
import 'routes/_dynamic_transactions.dart';
import 'routes/_root.dart';
import 'routes/account_edit.dart';
import 'routes/accounts.dart';
import 'routes/category_edit.dart';
import 'routes/category_group.dart';
import 'routes/login.dart';
import 'routes/merchants.dart';
import 'routes/options.dart';
import 'routes/report_edit.dart';
import 'routes/report_group.dart';
import 'routes/report_group_edit.dart';
import 'routes/tags.dart';
import 'routes/transaction_edit.dart';
import 'routes/transactions.dart';
import 'tutorial/tutorial_handler.dart';

final GlobalKey<NavigatorState> _rootNavigatorKey = GlobalKey<NavigatorState>();

class AppRouter {
  static String _lastLocation = '/';

  AppRouter._();

  static dynamic _getExtraParameter(final Object? extra, final String param) =>
      extra == null ? null : (extra as Map)[param];

  static String? _getIdPathParameter(final GoRouterState state) {
    final String? id = state.pathParameters['id'];
    if (id == newId) return null;
    return id;
  }

  static bool? _getCopyExtraParameter(final Object? extra) =>
      extra == null ? null : (extra as Map)['copy'] as bool?;

  // Should only ever be called by the AppLock class
  @protected
  static void appLockRoute() {
    final isLocked = AppLock.isLocked;
    final context = router.routerDelegate.navigatorKey.currentContext;
    final location = isLocked ? loginPath : _lastLocation;

    if (context != null) {
      if (isLocked &&
          (ModalRoute.of(context)?.settings.name?.contains('/edit/') ??
              false)) {
        return;
      } else {
        context.go(location);
      }
    } else {
      router.go(location);
    }
  }

  static final router = GoRouter(
    navigatorKey: _rootNavigatorKey,
    initialLocation: AppLock.isLocked ? loginPath : _lastLocation,
    onException: (final context, final state, final router) {
      if (state.matchedLocation == _lastLocation) {
        _lastLocation = transactionPath;
      }
      router.go(_lastLocation);
    },
    redirect: (final context, final state) {
      di<ValueNotifier<TutorialHelp>>().value = TutorialHelp.none;
      if (AppLock.isLocked && state.matchedLocation != loginPath) {
        return loginPath;
      } else {
        AppLock.userInteraction();
      }
      if (state.matchedLocation == '/') {
        return di<DataCache>().setting.value.navList.first.route;
      }

      return null;
    },
    routes: [
      GoRoute(
        parentNavigatorKey: _rootNavigatorKey,
        path: loginPath,
        redirect:
            (final context, final state) =>
                AppLock.isLocked ? null : _lastLocation,
        builder: (final context, final __) => const LoginPage(),
      ),
      GoRoute(
        parentNavigatorKey: _rootNavigatorKey,
        path: accountEditPath(':id'),
        builder: (final context, final state) {
          return AccountEdit(
            accountId: _getIdPathParameter(state),
            orgId: _getExtraParameter(state.extra, 'orgId') as String?,
            copy: _getCopyExtraParameter(state.extra) ?? false,
          );
        },
      ),
      GoRoute(
        parentNavigatorKey: _rootNavigatorKey,
        path: accountTransactionsPath(':id'),
        builder:
            (final context, final state) => TransactionScreen(
              ReportTransactionFilter(
                accountIds: [_getIdPathParameter(state)!],
              ),
            ),
      ),
      GoRoute(
        parentNavigatorKey: _rootNavigatorKey,
        path: merchantTransactionsPath(':id'),
        builder: (final context, final state) {
          final id = _getIdPathParameter(state);
          if (id == 'none') {
            return TransactionScreen(ReportTransactionFilter(noMerchant: true));
          }
          return TransactionScreen(ReportTransactionFilter(merchants: [id!]));
        },
      ),
      GoRoute(
        parentNavigatorKey: _rootNavigatorKey,
        path: categoryEditPath(':id'),
        builder:
            (final context, final state) => CategoryEdit(
              categoryGroupId: (state.extra as Map)['groupId'] as String,
              categoryId: _getIdPathParameter(state),
              copy: _getCopyExtraParameter(state.extra) ?? false,
            ),
      ),
      GoRoute(
        parentNavigatorKey: _rootNavigatorKey,
        path: categoryTransactionsPath(':id'),
        builder:
            (final context, final state) => TransactionScreen(
              ReportTransactionFilter(
                categoryIds: [_getIdPathParameter(state)!],
              ),
            ),
      ),
      GoRoute(
        parentNavigatorKey: _rootNavigatorKey,
        path: currencyTransactionsPath(':code'),
        builder:
            (final context, final state) => TransactionScreen(
              ReportTransactionFilter(
                currencyCodes: [state.pathParameters['code']!],
              ),
            ),
      ),
      GoRoute(
        parentNavigatorKey: _rootNavigatorKey,
        path: uncategorizedTransactionsPath(':id'),
        builder:
            (final context, final state) => TransactionScreen(
              ReportTransactionFilter(
                categoryGroupIds: [_getIdPathParameter(state)!],
              ),
            ),
      ),
      GoRoute(
        parentNavigatorKey: _rootNavigatorKey,
        path: reportEditPath(':id'),
        builder:
            (final context, final state) => ReportEdit(
              reportId: _getIdPathParameter(state),
              copy: _getCopyExtraParameter(state.extra) ?? false,
            ),
      ),
      GoRoute(
        parentNavigatorKey: _rootNavigatorKey,
        path: reportGroupEditPath(':id'),
        builder:
            (final context, final state) =>
                ReportGroupEdit(groupId: _getIdPathParameter(state)!),
      ),
      GoRoute(
        parentNavigatorKey: _rootNavigatorKey,
        path: tagTransactionsPath(':id'),
        builder: (final context, final state) {
          final tag = _getIdPathParameter(state);
          if (tag == '_') {
            return TransactionScreen(ReportTransactionFilter(noTag: true));
          }
          return TransactionScreen(ReportTransactionFilter(tags: [tag!]));
        },
      ),
      GoRoute(
        parentNavigatorKey: _rootNavigatorKey,
        path: transactionEditPath(':id'),
        builder:
            (final context, final state) => TransactionEntityEdit(
              id: _getIdPathParameter(state),
              copy: _getCopyExtraParameter(state.extra) ?? false,
            ),
      ),
      ShellRoute(
        builder: (
          final BuildContext context,
          final GoRouterState state,
          final Widget child,
        ) {
          final id = state.pathParameters['id'];
          _lastLocation = state.matchedLocation;
          return RootShell(route: state.matchedLocation, id: id, child: child);
        },
        routes: <RouteBase>[
          GoRoute(
            path: accountPath,
            builder:
                (final BuildContext context, final GoRouterState state) =>
                    const AccountsDisplay(),
          ),
          GoRoute(
            path: categoryGroupPath(':id'),
            builder:
                (final context, final state) =>
                    CategoryGroupDisplay(groupId: _getIdPathParameter(state)!),
          ),
          GoRoute(
            path: optionsPath,
            builder:
                (final BuildContext context, final GoRouterState state) =>
                    const OptionsDisplay(),
          ),
          GoRoute(
            path: reportGroupPath(':id'),
            builder:
                (final context, final state) =>
                    ReportGroupDisplay(groupId: _getIdPathParameter(state)!),
          ),
          GoRoute(
            path: transactionPath,
            builder:
                (final BuildContext context, final GoRouterState state) =>
                    const TransactionsDisplay(),
          ),
          GoRoute(
            path: merchantPath,
            builder:
                (final BuildContext context, final GoRouterState state) =>
                    const MerchantDisplay(),
          ),
          GoRoute(
            path: tagPath,
            builder:
                (final BuildContext context, final GoRouterState state) =>
                    const TagDisplay(),
          ),
        ],
      ),
    ],
  );
}
