import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../config/user_pref_const.dart';

final _lightScheme = ColorScheme.fromSeed(
  seedColor: Colors.indigoAccent,
  primaryFixed: const Color.fromARGB(255, 49, 77, 223),
  onPrimaryFixed: Colors.white,
  secondaryFixed: Colors.deepPurpleAccent[100],
  tertiaryFixed: Colors.deepPurpleAccent[100],
  surfaceTint: Colors.white,
);

final _darkScheme = ColorScheme.fromSeed(
  brightness: Brightness.dark,
  seedColor: Colors.blue,
  primaryFixed: Colors.indigo[800],
  onPrimaryFixed: Colors.white,
  secondaryFixed: Colors.deepPurpleAccent,
  onSecondaryFixed: Colors.white,
  tertiaryFixed: Colors.deepPurpleAccent,
  onTertiaryFixed: Colors.white,
  error: Colors.red,
  onError: Colors.white,
  surface: Colors.grey[800],
  onSurface: Colors.white,
);

final _blackScheme = ColorScheme.fromSeed(
  brightness: Brightness.dark,
  seedColor: Colors.blue,
  primaryFixed: Colors.indigo[900],
  onPrimaryFixed: Colors.white,
  secondaryFixed: Colors.deepPurple,
  onSecondaryFixed: Colors.white,
  tertiaryFixed: Colors.deepPurple,
  onTertiaryFixed: Colors.white,
  surface: Colors.black,
  onSurface: Colors.white,
  error: Colors.red,
  onError: Colors.white,
);

final lightTheme = ThemeData.from(colorScheme: _lightScheme);

final darkTheme = ThemeData.from(colorScheme: _darkScheme);

enum ThemeSelection { system, light, dark, black }

class ThemeHandler extends ChangeNotifier {
  bool _useBlackForDark = false;
  ThemeSelection _theme = ThemeSelection.system;
  late final bool _systemIsDark;
  static ThemeHandler? _instance;

  ThemeHandler._() {
    final brightness =
        MediaQueryData.fromView(
          PlatformDispatcher.instance.views.first,
        ).platformBrightness;
    _systemIsDark = (brightness == Brightness.dark);
    final origTheme = _theme;
    final origUseBlack = _useBlackForDark;
    _theme =
        EnumToString.fromString(
          ThemeSelection.values,
          getPreferences().getString(themeKey) ?? '',
        ) ??
        origTheme;
    _useBlackForDark =
        getPreferences().getBool(useBlackThemeKey) ?? origUseBlack;
  }

  static ThemeHandler get instance {
    _instance ??= ThemeHandler._();
    return _instance!;
  }

  bool get isDark =>
      _theme != ThemeSelection.light &&
      (_theme != ThemeSelection.system || _systemIsDark);

  bool get isBlack =>
      isDark &&
      (_theme == ThemeSelection.black ||
          (_theme == ThemeSelection.system && _useBlackForDark));

  ThemeData get themeData => _selectionToData(_theme);

  ThemeData _selectionToData(final ThemeSelection selection) {
    return switch (selection) {
      ThemeSelection.light => ThemeData.from(colorScheme: _lightScheme),
      ThemeSelection.dark => ThemeData.from(colorScheme: _darkScheme),
      ThemeSelection.black => ThemeData.from(colorScheme: _blackScheme),
      ThemeSelection.system =>
        _systemIsDark
            ? _useBlackForDark
                ? _selectionToData(ThemeSelection.black)
                : _selectionToData(ThemeSelection.dark)
            : _selectionToData(ThemeSelection.light),
    };
  }

  ThemeSelection get theme => _theme;
  set theme(final ThemeSelection theme) {
    _theme = theme;
    notifyListeners();
    getPreferences().setString(themeKey, EnumToString.convertToString(theme));
  }

  bool get useBlackForDark => _useBlackForDark;
  set useBlackForDark(final bool useBlack) {
    _useBlackForDark = useBlack;
    notifyListeners();
    getPreferences().setBool(useBlackThemeKey, useBlack);
  }
}
