import 'package:flutter/material.dart';
import 'package:watch_it/watch_it.dart';

import '../../config/user_pref_const.dart';
import '../../model/settings.dart';
import '../tutorial/tutorial_handler.dart';
import 'default_icon_button.dart';

class HelpIcon extends StatelessWidget {
  const HelpIcon(final NavigationEntry entry, {super.key});

  @override
  Widget build(final BuildContext context) {
    if (getPreferences().getBool(hideHelpIconKey) ?? false) {
      return const SizedBox.shrink();
    }

    return DefaultIconButton(
      icon: const Icon(Icons.help),
      onPressed:
          () => di<ValueNotifier<TutorialHelp>>().value = TutorialHelp.show,
      tooltip: 'Visit help documentation',
    );
  }
}
