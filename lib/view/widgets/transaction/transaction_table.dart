import 'dart:math';

import 'package:flutter/material.dart';
import 'package:material_table_view/material_table_view.dart';
import 'package:material_table_view/table_column_control_handles_popup_route.dart';
import 'package:material_table_view/table_view_typedefs.dart';
import 'package:watch_it/watch_it.dart';

import '../../../config/routes_const.dart';
import '../../../model/transaction.dart';
import '../../../service/data_cache.dart';
import '../../tutorial/transactions_tutorial.dart';
import '../../tutorial/tutorial_handler.dart';
import '../../util/datetime_display.dart';
import '../display_name_edit.dart';
import '../layout_widgets.dart';
import 'transaction_bulk_edit.dart';
import 'transaction_display_util.dart';
import 'transaction_table_column.dart';

typedef _TableRowData =
    ({TransactionEntity entity, bool oneSplit, TransactionSplit? split});
typedef _TableSortData = ({int index, bool desc}); // index, sortDescending

class TransactionTable extends WatchingStatefulWidget {
  final List<TransactionEntity> txEntities;

  const TransactionTable({required this.txEntities, super.key});

  @override
  State<TransactionTable> createState() => _TransactionTableState();
}

class _TransactionTableState extends State<TransactionTable>
    with SingleTickerProviderStateMixin<TransactionTable> {
  final selection = <int>{};
  List<TransactionTableColumn> columns = [];
  final List<_TableRowData> rowData = [];
  _TableSortData sortColumn = (index: 3, desc: true);
  late final TableViewController _controller;

  @override
  void initState() {
    super.initState();
    _controller = TableViewController();
    columns = getColumns();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    tutorialHandler(
      watchIt<ValueNotifier<TutorialHelp>>().value,
      context,
      transactionsTutorial,
      controllers: [
        _controller.horizontalScrollController,
        _controller.verticalScrollController,
      ],
    );

    rowData.clear();
    for (final entry in widget.txEntities) {
      if (entry.isTransfer) {
        rowData.add((
          entity: entry,
          oneSplit: true,
          split: entry.primaryTx.splits.first,
        ));
      } else if (entry.primaryTx.splits.length == 1) {
        rowData.add((
          entity: entry,
          oneSplit: true,
          split: entry.primaryTx.splits.first,
        ));
      } else {
        rowData.add((entity: entry, oneSplit: false, split: null));
        for (final split in entry.primaryTx.splits) {
          rowData.add((entity: entry, oneSplit: false, split: split));
        }
      }
    }
    final table = TableView.builder(
      controller: _controller,
      columns: columns,
      style: style,
      rowHeight: 48.0 + 4 * Theme.of(context).visualDensity.vertical,
      rowCount: rowData.length,
      rowBuilder: _rowBuilder,
      headerBuilder: _headerBuilder,
    );
    if (selection.isEmpty) return table;

    return FloatingButtonOverlay(
      table,
      onPressed: () async {
        if (await bulkTransactionEdit(
              context,
              selection
                  .where((final row) => rowData[row].split != null)
                  .map((final row) => rowData[row].split!)
                  .toList(),
            ) !=
            null) {
          setState(() => selection.clear());
        }
      },
      tooltip: 'Bulk Edit',
      icon: Icons.edit,
    );
  }

  int Function(TransactionEntity, TransactionEntity)? _columnIndexToSortFn(
    final _TableSortData sortData,
  ) {
    switch (sortData.index) {
      case 1:
        return sortData.desc
            ? (final a, final b) => b.primaryTx.name.compareTo(a.primaryTx.name)
            : (final a, final b) =>
                a.primaryTx.name.compareTo(b.primaryTx.name);
      case 2:
        return sortData.desc
            ? (final a, final b) => b.amount.compareTo(a.amount)
            : (final a, final b) => a.amount.compareTo(b.amount);
      case 3:
        return sortData.desc
            ? (final a, final b) => b.primaryTx.date.compareTo(a.primaryTx.date)
            : (final a, final b) =>
                a.primaryTx.date.compareTo(b.primaryTx.date);
      case 4:
        return sortData.desc
            ? (final a, final b) => b.sourceName.compareTo(a.sourceName)
            : (final a, final b) => a.sourceName.compareTo(b.sourceName);
      case 5:
        return sortData.desc
            ? (final a, final b) =>
                b.destinationName.compareTo(a.destinationName)
            : (final a, final b) =>
                a.destinationName.compareTo(b.destinationName);
      default:
        final index = sortData.index - 6;
        final groups = di<DataCache>().categoryGroups.value;
        if (index < 0) {
          return null;
        } else if (index < groups.length) {
          final group = groups[index];
          return (final a, final b) {
            final bCatName =
                b.primaryTx.splits.first.categories[group.uuid]?.name ?? '';
            final aCatName =
                a.primaryTx.splits.first.categories[group.uuid]?.name ?? '';
            return sortData.desc
                ? bCatName.compareTo(aCatName)
                : aCatName.compareTo(bCatName);
          };
        } else {
          return (final a, final b) {
            final bTags = b.primaryTx.splits.first.tags.join(',');
            final aTags = a.primaryTx.splits.first.tags.join(',');
            return sortData.desc
                ? bTags.compareTo(aTags)
                : aTags.compareTo(bTags);
          };
        }
    }
  }

  TableViewStyle get style {
    final theme = Theme.of(context);
    final color = theme.colorScheme.onPrimaryContainer;
    return TableViewStyle(
      dividers: TableViewDividersStyle(
        vertical: TableViewVerticalDividersStyle.symmetric(
          TableViewVerticalDividerStyle(color: color),
        ),
        horizontal: TableViewHorizontalDividersStyle.symmetric(
          TableViewHorizontalDividerStyle(color: color),
        ),
      ),
      scrollbars: const TableViewScrollbarsStyle.symmetric(
        TableViewScrollbarStyle(
          interactive: true,
          enabled: TableViewScrollbarEnabled.auto,
          thumbVisibility: WidgetStatePropertyAll(true),
          trackVisibility: WidgetStatePropertyAll(true),
        ),
      ),
    );
  }

  Widget _headerBuilder(
    final BuildContext context,
    final TableRowContentBuilder contentBuilder,
  ) => contentBuilder(context, (final context, final column) {
    final columnIndex = columns[column].index;
    final groups = di<DataCache>().categoryGroups.value;
    String indexMapper(int index) {
      index -= 6;
      if (index >= groups.length) {
        return 'Tags';
      } else if (index < 0) {
        return '';
      }
      return groups[index].name;
    }

    final color = Theme.of(context).colorScheme.onPrimaryContainer;

    switch (columnIndex) {
      case 0:
        return DecoratedBox(
          position: DecorationPosition.foreground,
          decoration: BoxDecoration(border: Border.all(color: color)),
          child: Checkbox(
            value: selection.isEmpty ? false : null,
            tristate: true,
            onChanged: (final value) {
              if (!(value ?? true)) {
                setState(() => selection.clear());
              }
            },
          ),
        );

      default:
        return Material(
          type: MaterialType.transparency,
          child: InkWell(
            key: columnIndex == 1 ? txHeaderNameKey : null,
            onTap: () {
              final newsortColumn = (
                index: columnIndex,
                desc:
                    sortColumn.index == columnIndex
                        ? !sortColumn.desc
                        : sortColumn.desc,
              );
              final sortFn = _columnIndexToSortFn(newsortColumn);
              if (sortFn == null) return;
              widget.txEntities.sort(sortFn);
              setState(() => sortColumn = newsortColumn);
            },
            onDoubleTap:
                () => Navigator.of(
                  context,
                ).push(_createColumnControlsRoute(context, column)),
            child: DecoratedBox(
              position: DecorationPosition.foreground,
              decoration: BoxDecoration(border: Border.all(color: color)),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Align(
                  alignment: Alignment.center,
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(switch (columnIndex) {
                        1 => 'Name',
                        2 => 'Amount',
                        3 => 'Date',
                        4 => 'Source',
                        5 => 'Destination',
                        _ => indexMapper(columnIndex),
                      }),
                      if (columnIndex == sortColumn.index)
                        Icon(
                          sortColumn.desc
                              ? Icons.expand_more
                              : Icons.expand_less,
                        ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
    }
  });

  ModalRoute<void> _createColumnControlsRoute(
    final BuildContext cellBuildContext,
    final int columnIndex,
  ) => TableColumnControlHandlesPopupRoute.realtime(
    controlCellBuildContext: cellBuildContext,
    columnIndex: columnIndex,
    tableViewChanged: null,
    onColumnTranslate: (final index, final newTranslation) {
      setState(
        () =>
            columns[index] = columns[index].copyWith(
              translation: newTranslation,
            ),
      );
      saveColumns(columns);
    },
    onColumnResize: (final index, final newWidth) {
      setState(() => columns[index] = columns[index].copyWith(width: newWidth));
      saveColumns(columns);
    },
    onColumnMove: (final oldIndex, final newIndex) {
      setState(() => columns.insert(newIndex, columns.removeAt(oldIndex)));
      saveColumns(columns);
    },
    leadingImmovableColumnCount: 1,
    trailingImmovableColumnCount: 1,
    popupBuilder:
        (
          final context,
          final animation,
          final secondaryAnimation,
          final columnWidth,
        ) => PreferredSize(
          preferredSize: Size(min(256, max(192, columnWidth)), 256),
          child: FadeTransition(
            opacity: animation,
            child: Material(
              type: MaterialType.transparency,
              clipBehavior: Clip.antiAlias,
              shape: RoundedRectangleBorder(
                side: Divider.createBorderSide(context),
                borderRadius: const BorderRadius.all(Radius.circular(16.0)),
              ),
            ),
          ),
        ),
  );

  Widget _wrapCell({
    final Key? key,
    required final Widget child,
    required final int columnIndex,
    final Alignment alignment = Alignment.centerLeft,
    final bool right = false,
  }) {
    final width = columns[columnIndex].width;
    final color = Theme.of(
      context,
    ).colorScheme.onPrimaryContainer.withAlpha(65);
    final borderSide = BorderSide(color: color);
    return SizedBox(
      key: key,
      width: width,
      child: DecoratedBox(
        position: DecorationPosition.foreground,
        decoration: BoxDecoration(
          border: Border(
            left: borderSide,
            right: right ? borderSide : BorderSide.none,
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: Align(alignment: alignment, child: child),
        ),
      ),
    );
  }

  Widget _wrapRow(final int index, final Widget child) {
    final color = Theme.of(
      context,
    ).colorScheme.onPrimaryContainer.withAlpha(65);
    return DecoratedBox(
      position: DecorationPosition.foreground,
      decoration: BoxDecoration(border: Border.all(color: color)),
      child: child,
    );
  }

  Widget? _rowBuilder(
    final BuildContext context,
    final int row,
    final TableRowContentBuilder contentBuilder,
  ) {
    final selected = selection.contains(row);

    var textStyle = Theme.of(context).textTheme.bodyMedium;
    if (selected) {
      textStyle = textStyle?.copyWith(
        color: Theme.of(context).colorScheme.onPrimaryContainer,
      );
    }
    final data = rowData[row];
    String source;
    String destination;

    if (!data.entity.isTransfer) {
      source = data.entity.primaryTx.sourceName;
      destination = data.entity.primaryTx.destinationName;
    } else {
      final transfer = data.entity.transfer!;
      source = transfer.source.account.displayName;
      destination = transfer.destination.account.displayName;
    }
    if (source.isEmpty) source = '';
    if (destination.isEmpty) destination = '';

    return _wrapRow(
      row,
      ColoredBox(
        color: Theme.of(
          context,
        ).colorScheme.primaryContainer.withAlpha(selected ? 255 : 0),
        child: Material(
          type: MaterialType.transparency,
          child: InkWell(
            key: row == 0 ? txRowKey : null,
            onTap:
                () =>
                    data.split == null
                        ? null
                        : setState(() {
                          selection
                            ..clear()
                            ..add(row);
                        }),
            child: contentBuilder(context, (final context, final column) {
              final columnIndex = columns[column].index;
              final groups = di<DataCache>().categoryGroups.value;
              String indexToString(int index) {
                index -= 6;
                if (index < 0) {
                  return '';
                } else if (index < groups.length) {
                  final group = groups[index];
                  return data.split?.categories[group.uuid]?.name ?? '';
                } else {
                  return data.split?.tags.join(', ') ?? '';
                }
              }

              switch (columnIndex) {
                case 0:
                  if (data.split != null) {
                    return Checkbox(
                      value: selection.contains(row),
                      onChanged:
                          (final value) => setState(
                            () =>
                                (value ?? false)
                                    ? selection.add(row)
                                    : selection.remove(row),
                          ),
                    );
                  }
                  return const SizedBox.shrink();
                case 1:
                  return _wrapCell(
                    key: row == 0 ? txNameKey : null,
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        if (!data.oneSplit && data.split != null)
                          Icon(Icons.call_split),
                        Flexible(
                          child: DisplayNameEdit(
                            name:
                                data.split?.name ?? data.entity.primaryTx.name,
                            path:
                                data.oneSplit || data.split == null
                                    ? transactionEditPath(
                                      data.entity.primaryTx.uuid,
                                    )
                                    : null,
                          ),
                        ),
                      ],
                    ),
                    columnIndex: column,
                  );
                case 2:
                  final style =
                      data.oneSplit || data.split == null
                          ? Theme.of(context).textTheme.titleSmall?.copyWith(
                            fontWeight: FontWeight.bold,
                          )
                          : null;
                  return _wrapCell(
                    key: row == 0 ? txAmountKey : null,
                    child: TransactionAmountDisplay(
                      data.entity,
                      split: data.split,
                      style: style,
                    ),
                    columnIndex: column,
                    alignment: Alignment.centerRight,
                  );
                case 3:
                  return _wrapCell(
                    key: row == 0 ? txDateKey : null,
                    child: Text(datetimeDisplay(data.entity.primaryTx.date)),
                    columnIndex: column,
                    alignment: Alignment.center,
                  );
                case 4:
                  return _wrapCell(
                    key: row == 0 ? txSourceKey : null,
                    child: Text(source, overflow: TextOverflow.ellipsis),
                    columnIndex: column,
                    alignment: Alignment.centerLeft,
                  );
                case 5:
                  return _wrapCell(
                    key: row == 0 ? txDestKey : null,
                    child: Text(destination, overflow: TextOverflow.ellipsis),
                    columnIndex: column,
                    alignment: Alignment.centerLeft,
                  );

                default:
                  return _wrapCell(
                    alignment: Alignment.centerLeft,
                    columnIndex: column,
                    right: columnIndex == columns.length,
                    child: Text(
                      indexToString(columnIndex),
                      style: textStyle,
                      overflow: TextOverflow.fade,
                      maxLines: 1,
                      softWrap: false,
                    ),
                  );
              }
            }),
          ),
        ),
      ),
    );
  }
}
