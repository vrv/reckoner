import 'package:flutter/material.dart';
import 'package:watch_it/watch_it.dart';

import '../../../database/reckoner_db.dart';
import '../../../model/category.dart';
import '../../../model/transaction.dart';
import '../../../service/data_cache.dart';
import '../../../service/sync_service_manager.dart';
import '../form/input_form.dart';
import 'transaction_edit_fields.dart';

Future<bool?> bulkTransactionEdit(
  final BuildContext context,
  final List<TransactionSplit> splits,
) {
  return showDialog(
    context: context,
    builder: (final context) {
      return TransactionBulkEdit(splits);
    },
  );
}

class TransactionBulkEdit extends StatelessWidget {
  final List<TransactionSplit> splits;

  const TransactionBulkEdit(this.splits, {super.key});

  @override
  Widget build(final BuildContext context) {
    final catGroups = di<DataCache>().categoryGroups.value;
    final Map<String, Category> catMap = {};
    String tags = '';
    return InputForm(
      title: 'Transaction Bulk Edit',
      inputs: [
        ...catGroups.map(
          (final g) => TxCategoryEdit(
            showLabel: true,
            group: g,
            model: TransactionSplit(uuid: ''),
            onChanged:
                (final c) =>
                    c == null ? catMap.remove(g.uuid) : catMap[g.uuid] = c,
            validate: false,
          ),
        ),
        TxTagEdit(
          showLabel: true,
          model: TransactionSplit(uuid: ''),
          onSaved: (final t) => tags = t ?? '',
        ),
      ],
      onSave: () async {
        final tagSet = tagStrToSet(tags);
        if (tagSet.isEmpty && catMap.isEmpty) return;

        for (final split in splits) {
          if (catMap.isNotEmpty) {
            split.categories = {...split.categories, ...catMap};
          }
          if (tagSet.isNotEmpty) split.tags = tagSet;
        }

        await ReckonerDb.I.transactionDao.updateSplitCatTag(splits);
        di<SyncServiceManager>().sendUpdates();
      },
      onDelete: () {},
      deleteLabel: 'Clear',
    );
  }
}
