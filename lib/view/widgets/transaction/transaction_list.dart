import 'dart:collection';

import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:go_router/go_router.dart';
import 'package:intl/intl.dart';
import 'package:watch_it/watch_it.dart';

import '../../../config/routes_const.dart';
import '../../../model/transaction.dart';
import '../../../model/tz_date_time.dart';
import '../../l10n/localizations.dart';
import '../../tutorial/transactions_tutorial.dart';
import '../../tutorial/tutorial_handler.dart';
import '../dashed_line.dart';
import '../layout_widgets.dart';
import 'transaction_bulk_edit.dart';
import 'transaction_display_util.dart';

class TransactionList extends WatchingStatefulWidget {
  final List<TransactionEntity> txEntities;

  const TransactionList({super.key, required this.txEntities});

  @override
  State<StatefulWidget> createState() => _TransactionListState();
}

class _TransactionListState extends State<TransactionList> {
  final selections = <TransactionSplit>{};
  late final ScrollController _controller;

  @override
  void initState() {
    super.initState();
    _controller = ScrollController();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    tutorialHandler(
      watchIt<ValueNotifier<TutorialHelp>>().value,
      context,
      transactionsTutorial,
      controllers: [_controller],
    );
    final dateMap = SplayTreeMap<TZDateTime, List<TransactionEntity>>(
      (final a, final b) => b.compareTo(a),
    );
    for (final tx in widget.txEntities) {
      final txDate = tx.primaryTx.date;
      final strippedDate = TZDateTime(
        local,
        txDate.year,
        txDate.month,
        txDate.day,
      );
      dateMap.putIfAbsent(strippedDate, () => []).add(tx);
    }

    return FloatingButtonOverlay(
      SingleChildScrollView(
        controller: _controller,
        child: Column(
          children:
              dateMap.keys
                  .mapIndexed(
                    (final i, final date) => Padding(
                      padding: EdgeInsets.only(bottom: 20),
                      child: _TxDateGroup(
                        date: date,
                        txEntities: dateMap[date]!,
                        selections: selections,
                        isFirst: i == 0,
                        selectFn: (final split) {
                          if (selections.add(split)) {
                            setState(() {});
                          } else if (selections.remove(split)) {
                            setState(() {});
                          }
                        },
                      ),
                    ),
                  )
                  .toList(),
        ),
      ),
      floatingButton:
          selections.isEmpty
              ? SizedBox.shrink()
              : ReckonerActionButton(
                child: Tooltip(
                  message: 'Bulk Edit',
                  child: InkWell(
                    onTap: () async {
                      if (await bulkTransactionEdit(
                            context,
                            selections.toList(),
                          ) !=
                          null) {
                        setState(() => selections.clear());
                      }
                    },
                    child: Icon(Icons.edit),
                  ),
                ),
              ),
    );
  }
}

class _TxDateGroup extends StatelessWidget {
  final TZDateTime date;
  final List<TransactionEntity> txEntities;
  final Set<TransactionSplit> selections;
  final void Function(TransactionSplit) selectFn;
  final bool isFirst;

  const _TxDateGroup({
    required this.date,
    required this.txEntities,
    required this.selections,
    required this.selectFn,
    this.isFirst = false,
  });

  @override
  Widget build(final BuildContext context) {
    final theme = Theme.of(context);
    final borderColor = theme.colorScheme.onPrimaryContainer.withAlpha(64);

    return Column(
      children: [
        DecoratedBox(
          key: isFirst ? txDateKey : null,
          decoration: BoxDecoration(
            border: Border(bottom: BorderSide(width: 1, color: borderColor)),
          ),
          child: SizedBox(
            width: double.infinity,
            child: Center(child: Text(DateFormat.yMMMMd().format(date))),
          ),
        ),
        ...txEntities.mapIndexed((final i, final e) {
          return Column(
            children: [
              _TxListTile(
                entity: e,
                selections: selections,
                selectFn: selectFn,
                isFirst: isFirst && i == 0,
              ),
              SizedBox(
                width: double.infinity,
                height: 1,
                child: ColoredBox(color: borderColor),
              ),
            ],
          );
        }),
      ],
    );
  }
}

class _TxSplitWrapper extends StatelessWidget {
  final Widget child;
  final TransactionSplit split;
  final Set<TransactionSplit> selections;
  final void Function(TransactionSplit) selectFn;

  const _TxSplitWrapper({
    required this.split,
    required this.selections,
    required this.selectFn,
    required this.child,
  });
  @override
  Widget build(final BuildContext context) {
    final selected = selections.contains(split);
    final colorScheme = Theme.of(context).colorScheme;
    final localization = ReckonerLocalizations.of(context);

    return Slidable(
      key: Key('${split.uuid}-${split.id}'),
      startActionPane: ActionPane(
        extentRatio: 0.2,
        motion: const ScrollMotion(),
        dismissible: DismissiblePane(
          closeOnCancel: true,
          confirmDismiss: () {
            context.push(transactionEditPath(split.uuid));
            return Future.value(false);
          },
          onDismissed: () {},
        ),
        children: [
          SlidableAction(
            backgroundColor: colorScheme.secondaryFixed,
            label: localization.edit,
            icon: Icons.edit,
            onPressed:
                (final context) =>
                    context.push(transactionEditPath(split.uuid)),
          ),
        ],
      ),
      endActionPane: ActionPane(
        extentRatio: 0.2,
        motion: const ScrollMotion(),
        dismissible: DismissiblePane(
          closeOnCancel: true,
          confirmDismiss: () {
            selectFn(split);
            return Future.value(false);
          },
          onDismissed: () {},
        ),
        children: [
          SlidableAction(
            backgroundColor: colorScheme.primaryFixed,
            label: 'Select',
            icon: selected ? Icons.check_box : Icons.check_box_outline_blank,
            onPressed: (final context) => selectFn(split),
          ),
        ],
      ),
      child: ColoredBox(
        color: colorScheme.primaryContainer.withAlpha(selected ? 255 : 0),
        child: child,
      ),
    );
  }
}

class _TxListTile extends StatelessWidget {
  final TransactionEntity entity;
  final Set<TransactionSplit> selections;
  final void Function(TransactionSplit) selectFn;
  final bool isFirst;

  const _TxListTile({
    required this.entity,
    required this.selections,
    required this.selectFn,
    this.isFirst = false,
  });

  @override
  Widget build(final BuildContext context) {
    String source;
    String destination;
    final style = Theme.of(
      context,
    ).textTheme.titleSmall?.copyWith(fontWeight: FontWeight.bold);

    if (entity.transaction != null) {
      source = entity.primaryTx.sourceName;
      destination = entity.primaryTx.destinationName;
    } else {
      final transfer = entity.transfer!;
      source = transfer.source.account.displayName;
      destination = transfer.destination.account.displayName;
    }
    if (source.isEmpty) source = 'UNKNOWN';
    if (destination.isEmpty) destination = 'UNKNOWN';

    final title = Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          key: isFirst ? txNameKey : null,
          entity.primaryTx.name,
          overflow: TextOverflow.ellipsis,
          style: style,
        ),
        TransactionAmountDisplay(
          entity,
          style: style,
          key: isFirst ? txAmountKey : null,
        ),
      ],
    );
    final destStyle = Theme.of(context).textTheme.labelSmall;
    final subtitle = Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Text(source, key: isFirst ? txSourceKey : null, style: destStyle),
        Text(' → ', style: destStyle),
        Text(destination, key: isFirst ? txDestKey : null, style: destStyle),
      ],
    );
    if (entity.primaryTx.splits.length > 1) {
      return ListTile(
        key: isFirst ? txRowKey : null,
        contentPadding: EdgeInsets.zero,
        dense: true,
        minVerticalPadding: 0,
        title: Column(
          children: [
            SizedBox(height: 6),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 16.0),
              child: title,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 16.0),
              child: subtitle,
            ),
            SizedBox(height: 4),
            ...entity.primaryTx.splits.map(
              (final split) => Column(
                children: [
                  DashedLine(),
                  _TxSplitWrapper(
                    split: split,
                    selections: selections,
                    selectFn: selectFn,
                    child: _SplitDisplay(entity, split, true),
                  ),
                ],
              ),
            ),
          ],
        ),
      );
    }
    final split = entity.primaryTx.splits.first;
    return _TxSplitWrapper(
      split: split,
      selections: selections,
      selectFn: selectFn,
      child: ListTile(
        dense: true,
        title: Column(
          children: [
            title,
            subtitle,
            _SplitInfo(entity.primaryTx.splits.first),
          ],
        ),
      ),
    );
  }
}

class _SplitDisplay extends StatelessWidget {
  final TransactionEntity entity;
  final TransactionSplit split;
  final bool showName;
  const _SplitDisplay(this.entity, this.split, this.showName);

  @override
  Widget build(final BuildContext context) {
    return ListTile(
      dense: true,
      leading: Icon(Icons.call_split),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(split.name),
          TransactionAmountDisplay(entity, split: split),
        ],
      ),
      subtitle: _SplitInfo(split),
    );
  }
}

class _SplitInfo extends StatelessWidget {
  final TransactionSplit split;
  const _SplitInfo(this.split);

  @override
  Widget build(final BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Padding(
        padding: EdgeInsets.only(left: 20, right: 20),
        child: Wrap(
          runAlignment: WrapAlignment.spaceAround,
          alignment: WrapAlignment.spaceAround,
          children: [
            ...split.categories.values.map(
              (final cat) => Text(
                '${cat.group.name}: ${cat.name}',
                style: Theme.of(context).textTheme.labelSmall,
              ),
            ),
            if (split.tags.isNotEmpty & (split.categories.length < 2))
              Text(
                'Tags: ${split.tags.fold('', (final val, final tag) => val.isEmpty ? tag : '$val, $tag')}',
                style: Theme.of(context).textTheme.labelSmall,
                overflow: TextOverflow.ellipsis,
              ),
          ],
        ),
      ),
    );
  }
}
