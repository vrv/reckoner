import 'package:collection/collection.dart';
import 'package:flutter/material.dart';

class ReckonerReoderableListView extends StatelessWidget {
  final List<Widget> children;
  final ReorderCallback onReorder;
  final Key? Function(int)? listItemDrayIconKey;
  final ScrollController? scrollController;

  const ReckonerReoderableListView({
    required this.children,
    required this.onReorder,
    super.key,
    this.listItemDrayIconKey,
    this.scrollController,
  });

  @override
  Widget build(final BuildContext context) {
    return ReorderableListView(
      buildDefaultDragHandles: false,
      padding: const EdgeInsets.symmetric(horizontal: 5),
      onReorder: onReorder,
      scrollController: scrollController,
      children:
          children
              .mapIndexed(
                (final index, final element) => _ReorderableListItem(
                  iconKey: listItemDrayIconKey?.call(index),
                  index: index,
                  key: Key('_ReorderableListItem$index'),
                  child: element,
                ),
              )
              .toList(),
    );
  }
}

class _ReorderableListItem extends StatelessWidget {
  final Widget child;
  final int index;
  final Key? iconKey;

  const _ReorderableListItem({
    required this.child,
    required this.index,
    this.iconKey,
    super.key,
  });

  @override
  Widget build(final BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Expanded(child: child),
        ReorderableDragStartListener(
          index: index,
          child: Icon(Icons.drag_handle, key: iconKey),
        ),
      ],
    );
  }
}
