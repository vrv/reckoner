import 'package:collection/collection.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:watch_it/watch_it.dart';

import '../../../model/currency.dart';
import '../../../model/settings.dart';
import '../../../service/data_cache.dart';
import '../../l10n/localizations.dart';
import '../../model/layout_config.dart';
import '../../tutorial/options/currency_tutorial.dart';
import '../../tutorial/tutorial_handler.dart';
import '../../util/popup_dialogs.dart';
import '../../util/save_item.dart';
import '../default_icon_button.dart';
import '../display_name_edit.dart';
import '../form/edit_fields.dart';
import '../form/form_title.dart';
import '../form/input_form.dart';
import '../layout_widgets.dart';
import 'util.dart';

class CurrencyEdit extends WatchingStatefulWidget {
  const CurrencyEdit({super.key});

  @override
  State<StatefulWidget> createState() => _CurrencyEdit();
}

class _CurrencyEdit extends State<CurrencyEdit> {
  late final ScrollController _controller;

  @override
  void initState() {
    super.initState();
    _controller = ScrollController();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  Future<void> editHandler(
    final BuildContext context,
    final Currency? data,
  ) => showDialog(
    context: context,
    builder: (final context) {
      final Currency model = data ?? Currency();
      final localization = ReckonerLocalizations.of(context);

      return InputForm(
        title: titleString(context, localization.currency, data),
        onSave: () => saveItem<Currency>(model),
        onDelete: () => deletePopup(context, model, localization.currency),
        inputs: [
          NameEdit(model: model),
          TextFormField(
            textCapitalization: TextCapitalization.characters,
            decoration: InputDecoration(labelText: localization.currencyCode),
            onSaved: (final value) => model.code = value!.toUpperCase(),
            maxLength: 3,
            initialValue: model.code,
            validator: (final code) {
              if (model.code.isNotEmpty && code == model.code) return null;
              if (code == null || code.isEmpty) {
                return ReckonerLocalizations.of(context).missingValue;
              }
              final codeList = di<DataCache>().currencyMap.value.values.map(
                (final e) => e.code,
              );
              if (codeList.contains(code)) return 'Code already in use';
              if (code.length != 3) return 'Code must be 3 characters long';
              return null;
            },
          ),
          TextFormField(
            decoration: InputDecoration(labelText: localization.symbol),
            onSaved: (final value) => model.symbol = value!,
            maxLength: 4,
            initialValue: model.symbol,
          ),
          FormBuilderSlider(
            name: 'decimalDigits',
            initialValue: model.decimalDigits.toDouble(),
            min: 0,
            max: 5,
            divisions: 5,
            displayValues: DisplayValues.current,
            decoration: InputDecoration(labelText: localization.currencyDigits),
            onSaved: (final value) => model.decimalDigits = value!.toInt(),
          ),
        ],
      );
    },
  );

  Widget buildMainWidget(
    final BuildContext context,
    final List<Currency> dataList,
    final UserSetting setting,
  ) {
    setIndex(context);
    final colStyle = Theme.of(context).textTheme.titleLarge;
    final localization = ReckonerLocalizations.of(context);
    final layoutCfg = LayoutConfig(context);
    final compact = !layoutCfg.isWidescreen;
    final List<DataColumn> columns = [
      DataColumn2(
        label: Text(localization.name, style: colStyle),
        size: ColumnSize.L,
      ),
    ];

    if (!compact) {
      columns.addAll([
        DataColumn2(
          label: Text(localization.currencyCode, style: colStyle),
          fixedWidth: 120,
        ),
        DataColumn2(
          label: Text(localization.symbol, style: colStyle),
          fixedWidth: 150,
        ),
      ]);
    }
    columns.addAll([
      DataColumn2(
        label: Text(
          localization.currencyDigits,
          overflow: TextOverflow.ellipsis,
          style: colStyle,
        ),
        numeric: true,
        size: ColumnSize.S,
      ),
      const DataColumn2(label: SizedBox.shrink(), size: ColumnSize.S),
    ]);

    return Container(
      padding: const EdgeInsets.only(left: 15, right: 15),
      constraints: const BoxConstraints(maxWidth: 1200),
      child: DataTable2(
        scrollController: _controller,
        columns: columns,
        rows:
            dataList.mapIndexed((final i, final data) {
              TextStyle? style;
              if (!data.isSystem) {
                style = const TextStyle(fontWeight: FontWeight.bold);
              }
              final nameText = compact ? data.summary() : data.name;
              final nameWidget = Text(
                nameText,
                style: style,
                key: i == 0 ? currencyDisplayKey : null,
              );

              final List<DataCell> cells = [
                DataCell(
                  data.isSystem
                      ? nameWidget
                      : DisplayNameEdit(
                        key: i == 0 ? currencyDisplayKey : null,
                        name: nameText,
                        onEdit: () => editHandler(context, data),
                      ),
                ),
              ];
              if (!compact) {
                cells.addAll([
                  DataCell(Text(data.code, style: style)),
                  DataCell(Text(data.symbol, style: style)),
                ]);
              }

              cells.addAll([
                DataCell(
                  Text(
                    data.decimalDigits.toString(),
                    style: style,
                    key: i == 0 ? currencyDigitsKey : null,
                  ),
                ),
                DataCell(
                  DefaultIconButton(
                    key: i == 0 ? currencyFavoriteKey : null,
                    onPressed: () {
                      setting.currency = data;
                      saveItem(setting);
                    },
                    icon: Icon(
                      setting.defaultCurrencyCode == data.code
                          ? Icons.star
                          : Icons.star_outline,
                      color:
                          setting.defaultCurrencyCode == data.code
                              ? Colors.yellow
                              : null,
                    ),
                    tooltip: 'Set as default currency',
                  ),
                ),
              ]);
              return DataRow(cells: cells);
            }).toList(),
      ),
    );
  }

  @override
  Widget build(final BuildContext context) {
    final dataList =
        watchValue((final DataCache c) => c.currencyMap).values.toList();
    final setting = watchValue((final DataCache c) => c.setting);
    tutorialHandler(
      watchIt<ValueNotifier<TutorialHelp>>().value,
      context,
      currencyTutorial,
      controllers: [_controller],
    );

    return FloatingButtonOverlay(
      buildMainWidget(context, dataList, setting),
      heroTag: 'add',
      onPressed: () => editHandler(context, null),
      tooltip: ReckonerLocalizations.of(
        context,
      ).addItem(ReckonerLocalizations.of(context).currency),
      icon: Icons.add,
      actionButtonKey: addCurrencyKey,
    );
  }
}
