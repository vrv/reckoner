import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:watch_it/watch_it.dart';

import '../../../config/environment_const.dart';
import '../../../config/platform.dart';
import '../../../service/logger.dart';
import '../../tutorial/options/settings_tutorial.dart';
import '../../tutorial/tutorial_handler.dart';
import '../form/edit_fields.dart';
import '../sync/sync_display.dart';
import 'settings/about_view.dart';
import 'settings/app_passwords.dart';
import 'settings/backup_settings.dart';
import 'settings/date_settings.dart';
import 'settings/debug_settings.dart';
import 'settings/error_log_display.dart';
import 'settings/ui_settings.dart';
import 'util.dart';

class UserSettingsEdit extends WatchingStatefulWidget {
  final bool fromError;
  const UserSettingsEdit({super.key, this.fromError = false});

  @override
  State<StatefulWidget> createState() => _UserSettingsEdit();
}

class _UserSettingsEdit extends State<UserSettingsEdit> {
  late final ScrollController _controller;

  @override
  void initState() {
    super.initState();
    _controller = ScrollController();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    setIndex(context);
    final tutorialVal = watchIt<ValueNotifier<TutorialHelp>>().value;
    return FutureBuilder(
      future: getErrorLogs(),
      builder: (final context, final snapshot) {
        tutorialHandler(
          tutorialVal,
          context,
          settingsTutorial,
          controllers: [_controller],
        );
        final hasErrorFiles = snapshot.data?.isNotEmpty ?? false;
        return SingleChildScrollView(
          child: Wrap(
            alignment: WrapAlignment.center,
            runAlignment: WrapAlignment.center,
            children: [
              if (kDebugMode && !widget.fromError)
                const _SettingGroup(
                  label: 'Debug Settings',
                  child: DebugSettings(),
                ),
              if (hasErrorFiles)
                const _SettingGroup(
                  label: 'Error Log',
                  child: ErrorLogDisplay(),
                ),
              if (!isDemo && !widget.fromError)
                _SettingGroup(
                  key: syncSettingsKey,
                  label: 'Synchronization Settings',
                  child: SyncDisplay(),
                ),
              _SettingGroup(
                key: themeKey,
                label: 'User Interface Settings',
                child: UISettings(),
              ),
              if (!isWeb)
                _SettingGroup(
                  key: backupKey,
                  label: 'Database Import/Export',
                  child: BackupSettingsDisplay(),
                ),
              _SettingGroup(
                key: securityKey,
                label: 'Database and Security',
                child: AppPasswords(),
              ),
              if (!widget.fromError)
                _SettingGroup(
                  key: dateKey,
                  label: 'Date Settings',
                  child: DateSettings(),
                ),
              _SettingGroup(key: aboutKey, label: 'About', child: AboutView()),
            ],
          ),
        );
      },
    );
  }
}

class _SettingGroup extends StatelessWidget {
  final Widget child;
  final String label;
  const _SettingGroup({required this.label, required this.child, super.key});

  @override
  Widget build(final BuildContext context) =>
      SizedBox(width: 700, child: InputFieldGroup(label: label, child: child));
}
