import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:watch_it/watch_it.dart';

import '../../../config/routes_const.dart';
import '../../../model/report.dart';
import '../../../service/data_cache.dart';
import '../../l10n/localizations.dart';
import '../../tutorial/options/report_tutorial.dart';
import '../../tutorial/tutorial_handler.dart';
import '../fixed_width_container.dart';
import '../layout_widgets.dart';
import 'util.dart';

class ReportDefinitions extends WatchingStatefulWidget {
  const ReportDefinitions({super.key});

  @override
  State<StatefulWidget> createState() => _ReportDefinitions();
}

class _ReportDefinitions extends State<ReportDefinitions> {
  late final ScrollController _controller;

  @override
  void initState() {
    super.initState();
    _controller = ScrollController();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  Widget _tile(
    final BuildContext context,
    final String title,
    final List<Report> reports,
    final bool showKey,
  ) => ExpansionTile(
    key: showKey ? reportTypeKey : null,
    leading: Icon(reports.first.icon),
    title: Text(title),
    initiallyExpanded: true,
    children:
        reports
            .mapIndexed(
              (final i, final rpt) => Padding(
                padding: const EdgeInsets.only(bottom: 5),
                child: Row(
                  key: showKey && i == 0 ? reportKey : null,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(rpt.name, overflow: TextOverflow.ellipsis),
                    SizedBox(width: 5),
                    InkWell(
                      onTap: () => context.push(reportEditPath(rpt.uuid)),
                      child: Icon(Icons.edit, size: 20),
                    ),
                  ],
                ),
              ),
            )
            .toList(),
  );

  @override
  Widget build(final BuildContext context) {
    setIndex(context);
    tutorialHandler(
      watchIt<ValueNotifier<TutorialHelp>>().value,
      context,
      reportTutorial,
      controllers: [_controller],
    );
    final reports = watchValue((final DataCache c) => c.reportMap).values;
    final Map<ReportType, List<Report>> reportMap = {};
    for (final report in reports) {
      reportMap.putIfAbsent(report.type, () => []).add(report);
    }
    bool showKey = true;
    final List<Widget> children = [];
    for (final (name, type) in [
      ('Line Charts', ReportType.line),
      ('Bar Charts', ReportType.bar),
      ('Pie Charts', ReportType.pie),
      ('Table Report', ReportType.table),
      ('Table Report', ReportType.table),
    ]) {
      if (reportMap.containsKey(type)) {
        children.add(_tile(context, name, reportMap[type]!, showKey));
        showKey = false;
      }
    }

    return FloatingButtonOverlay(
      FixedWidthContainer(
        child: ListView(controller: _controller, children: children),
      ),
      heroTag: 'add',
      onPressed: () => context.push(reportEditPath(newId)),
      tooltip: ReckonerLocalizations.of(context).addItem('Report'),
      icon: Icons.add_chart,
      actionButtonKey: addReportKey,
    );
  }
}
