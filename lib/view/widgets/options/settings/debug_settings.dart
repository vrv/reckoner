import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:watch_it/watch_it.dart';

import '../../../../config/user_pref_const.dart';
import '../../../../database/reckoner_db.dart';
import '../../../../service/data_cache.dart';
import '../../../model/layout_config.dart';
import '../../../util/csv_file_upload.dart';
import '../../../util/reset_reckoner.dart';

class DebugSettings extends WatchingWidget {
  const DebugSettings({super.key});

  @override
  Widget build(final BuildContext context) {
    final layoutCfg = LayoutConfig(context);
    final noAccounts = watchValue((final DataCache c) => c.accounts).isEmpty;

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Text('${layoutCfg.screenWidth} x ${layoutCfg.screenHeight}'),
        TextButton(
          onPressed: () => pickFileAndUpload(context),
          child: const Text('Upload CSV data'),
        ),
        if (noAccounts)
          TextButton(
            onPressed: () async {
              if (noAccounts) {
                await getPreferences().setBool(generateData, false);
              }
              if (!(getPreferences().getBool(generateData) ?? false)) {
                await ReckonerDb.generateData();
                debugPrint('Data generated');
              }
            },
            child: const Text('Generate Data'),
          ),
        TextButton(
          onPressed: () => resetReckoner(context),
          child: const Text('Reset Storage'),
        ),
        FormBuilderCheckbox(
          name: 'mobile',
          title: const Text('Force mobile layout'),
          initialValue: LayoutConfig.forceMobile,
          onChanged:
              (final val) =>
                  val != null ? LayoutConfig.forceMobile = val : null,
        ),
      ],
    );
  }
}
