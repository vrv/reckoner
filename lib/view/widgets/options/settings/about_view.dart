import 'package:flutter/material.dart';
import 'package:package_info_plus/package_info_plus.dart';

class AboutView extends StatelessWidget {
  const AboutView({super.key});

  @override
  Widget build(final BuildContext context) {
    return FutureBuilder(
      future: PackageInfo.fromPlatform(),
      builder: (final context, final snapshot) {
        if (!snapshot.hasData) return const Center(child: Text('Reckoner'));
        final info = snapshot.data!;
        return Wrap(
          alignment: WrapAlignment.spaceAround,
          crossAxisAlignment: WrapCrossAlignment.center,
          children: [
            Text('Reckoner: v${info.version}+${info.buildNumber}'),
            TextButton(
              onPressed: () => showLicensePage(context: context),
              child: Text('Show Licenses'),
            ),
          ],
        );
      },
    );
  }
}
