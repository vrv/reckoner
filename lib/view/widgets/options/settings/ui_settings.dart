import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:watch_it/watch_it.dart';

import '../../../../config/user_pref_const.dart';
import '../../../reckoner.dart';
import '../../../theme.dart';
import '../../form/edit_fields.dart';

class UISettings extends StatelessWidget {
  const UISettings({super.key});

  @override
  Widget build(final BuildContext context) {
    final ThemeHandler themeHandler = di();
    return Wrap(
      alignment: WrapAlignment.center,
      crossAxisAlignment: WrapCrossAlignment.center,
      children: [
        InputWrapper(
          size: InputWrapperSize.small,
          child: FormBuilderCheckbox(
            name: 'HideHelp',
            title: const Text('Hide Help Icon'),
            initialValue: getPreferences().getBool(hideHelpIconKey),
            onChanged: (final value) {
              getPreferences().setBool(hideHelpIconKey, value ?? false);
              ReckonerMaterialBase.redraw(context);
            },
          ),
        ),
        InputWrapper(
          size: InputWrapperSize.small,
          child: FormBuilderCheckbox(
            name: 'black',
            title: const Text('Use black for dark theme'),
            initialValue: themeHandler.useBlackForDark,
            onChanged:
                (final value) =>
                    value != null ? themeHandler.useBlackForDark = value : null,
          ),
        ),
        PopupMenuButton<ThemeSelection>(
          color: Theme.of(context).colorScheme.secondaryFixed,
          position: PopupMenuPosition.under,
          onSelected: (final theme) => themeHandler.theme = theme,
          itemBuilder:
              (final context) =>
                  ThemeSelection.values
                      .map(
                        (final e) => PopupMenuItem<ThemeSelection>(
                          value: e,
                          child: Text(e.name),
                        ),
                      )
                      .toList(),
          child: const Row(
            mainAxisSize: MainAxisSize.min,
            children: [Text('Theme'), Icon(Icons.expand_more)],
          ),
        ),
      ],
    );
  }
}
