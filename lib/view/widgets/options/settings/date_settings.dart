import 'package:flutter/material.dart';
import 'package:timezone/timezone.dart';
import 'package:watch_it/watch_it.dart';

import '../../../../service/data_cache.dart';
import '../../form/input_form.dart';
import '../../form/list_search_input.dart';
import '../../form/validators.dart';

class DateSettings extends StatelessWidget {
  const DateSettings({super.key});

  @override
  Widget build(final BuildContext context) {
    final settings = di<DataCache>().setting.value;

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        const Text('Default Time Zone:'),
        TextButton(
          onPressed:
              () => showDialog<bool>(
                context: context,
                builder: (final _) => const _DateSettingsEdit(),
              ),
          child: Text(settings.timezone),
        ),
      ],
    );
  }
}

class _DateSettingsEdit extends StatelessWidget {
  const _DateSettingsEdit();

  @override
  Widget build(final BuildContext context) {
    final settings = di<DataCache>().setting.value;
    return InputForm(
      title: 'Edit Date Settings',
      inputs: [
        ListSearchFormField(
          initialValue: settings.timezone,
          searchItems: timeZoneDatabase.locations.keys.toList(),
          itemToDisplayString: (final v) => v,
          validator: (final tz) => nonNullValidator(context, tz),
          onSaved: (final tz) => tz != null ? settings.timezone = tz : null,
        ),
      ],
    );
  }
}
