import 'package:flutter/material.dart';
import 'package:watch_it/watch_it.dart';

import '../../../../model/report.dart';
import '../../../../service/data_cache.dart';
import '../../form/input_form.dart';
import '../../form/list_search_input.dart';

void editReport(
  final BuildContext context,
  final String? initialUuid,
  final void Function(Report?) update,
  final void Function() save,
) {
  final rptMap = di<DataCache>().reportMap.value;
  showDialog<void>(
    context: context,
    builder: (final ctx) {
      return InputForm(
        title: 'Choose Report',
        inputs: [
          ListSearchFormField<Report>(
            searchItems: rptMap.values.toList(),
            initialValue: rptMap[initialUuid],
            itemToDisplayString: (final rpt) => rpt.displayName,
            onChanged: (final rpt) => update(rpt),
          ),
        ],
        onSave: save,
      );
    },
  );
}
