import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:watch_it/watch_it.dart';

import '../../../../model/category.dart';
import '../../../../model/settings.dart';
import '../../../../service/data_cache.dart';
import '../../../tutorial/options/reorder_tutorial.dart';
import '../../../util/popup_dialogs.dart';
import '../../../util/save_item.dart';
import '../../default_icon_button.dart';
import '../../display_name_edit.dart';
import 'edit_category_group.dart';
import 'edit_report.dart';
import 'edit_report_group.dart';

class HomeNavTile extends StatelessWidget {
  final NavigationEntry entry;
  late final bool showTutorial;

  HomeNavTile(this.entry, {super.key}) {
    showTutorial = (entry.type == NavigationEntryType.merchant);
  }

  @override
  Widget build(final BuildContext context) {
    final settings = di<DataCache>().setting.value;
    final style = Theme.of(context).textTheme.titleMedium;
    Widget title = Text(
      entry.name,
      style: style,
      key: showTutorial ? reorderDisplayKey : null,
    );
    void Function()? editReportCallback;
    Widget icon;
    bool hasReport = false;

    switch (entry.type) {
      case NavigationEntryType.category:
        final catGroup =
            di<DataCache>().categoryGroups.value
                .singleWhereOrNull((final g) => g.uuid == entry.uuid)
                ?.copy();
        hasReport = catGroup?.reportUuid?.isNotEmpty ?? false;
        if (catGroup != null) {
          editReportCallback =
              () => editReport(
                context,
                catGroup.reportUuid,
                (final rpt) => catGroup.reportUuid = rpt?.uuid,
                () => saveItem<CategoryGroup>(catGroup),
              );
          title = DisplayNameEdit(
            name: catGroup.name,
            onEdit: () => editCategoryGroup(context, catGroup),
          );
        }
        icon = DefaultIconButton(
          onPressed: () async {
            final iconData = await pickIcon(context, [
              Icons.category,
              Icons.calendar_month,
              Icons.sort,
              Icons.leaderboard,
              Icons.account_tree,
              Icons.schema,
            ]);
            if (iconData != null) {
              settings.setIcon(entry, iconData);
              await saveItem(settings);
            }
          },
          icon: Icon(entry.icon),
        );
        break;
      case NavigationEntryType.report:
        final reportGroup = di<DataCache>().reportGroupMap.value[entry.uuid];
        if (reportGroup != null) {
          title = DisplayNameEdit(
            name: reportGroup.name,
            onEdit: () => editReportGroup(context, reportGroup),
          );
        }
        icon = DefaultIconButton(
          onPressed: () async {
            final iconData = await pickIcon(context, [
              Icons.line_axis,
              Icons.trending_up,
              Icons.timeline,
              Icons.bar_chart,
              Icons.stacked_bar_chart,
              Icons.pie_chart,
              Icons.donut_large,
              Icons.area_chart,
              Icons.table_chart,
              Icons.dashboard,
              Icons.grid_view,
            ]);
            if (iconData != null) {
              settings.setIcon(entry, iconData);
              await saveItem(settings);
            }
          },
          icon: Icon(entry.icon),
        );
        break;
      case NavigationEntryType.transaction:
        editReportCallback =
            () => editReport(
              context,
              settings.transactionReportUuid,
              (final rpt) => settings.transactionReportUuid = rpt?.uuid,
              () => saveItem<UserSetting>(settings),
            );
        hasReport = settings.transactionReportUuid?.isNotEmpty ?? false;
        icon = Padding(
          padding: const EdgeInsets.all(5),
          child: Icon(entry.icon),
        );
      case NavigationEntryType.account:
        editReportCallback =
            () => editReport(
              context,
              settings.accountReportUuid,
              (final rpt) => settings.accountReportUuid = rpt?.uuid,
              () => saveItem<UserSetting>(settings),
            );
        hasReport = settings.accountReportUuid?.isNotEmpty ?? false;
        icon = Padding(
          padding: const EdgeInsets.all(5),
          child: Icon(entry.icon),
        );
      case NavigationEntryType.merchant:
        editReportCallback =
            () => editReport(
              context,
              settings.merchantReportUuid,
              (final rpt) => settings.merchantReportUuid = rpt?.uuid,
              () => saveItem<UserSetting>(settings),
            );
        hasReport = settings.merchantReportUuid?.isNotEmpty ?? false;
        icon = Padding(
          key: reorderIconKey,
          padding: const EdgeInsets.all(5),
          child: Icon(entry.icon),
        );
      case NavigationEntryType.tag:
        editReportCallback =
            () => editReport(
              context,
              settings.tagReportUuid,
              (final rpt) => settings.tagReportUuid = rpt?.uuid,
              () => saveItem<UserSetting>(settings),
            );
        hasReport = settings.tagReportUuid?.isNotEmpty ?? false;
        icon = Padding(
          padding: const EdgeInsets.all(5),
          child: Icon(entry.icon),
        );
      default:
        icon = Padding(
          padding: const EdgeInsets.all(5),
          child: Icon(entry.icon),
        );
        break;
    }

    if (editReportCallback != null) {
      title = Row(
        children: [
          title,
          DefaultIconButton(
            key: showTutorial ? reorderReportKey : null,
            icon: Icon(hasReport ? Icons.fact_check_outlined : Icons.add_chart),
            onPressed: editReportCallback,
          ),
        ],
      );
    }

    return ListTile(
      leading: icon,
      title: title,
      trailing:
          [
                NavigationEntryType.account,
                NavigationEntryType.options,
                NavigationEntryType.transaction,
              ].contains(entry.type)
              ? null
              : _HideIcon(
                settings,
                settings.navList.indexOf(entry),
                key: showTutorial ? reorderHideKey : null,
              ),
    );
  }
}

class _HideIcon extends StatefulWidget {
  final int index;
  final UserSetting settings;
  const _HideIcon(this.settings, this.index, {super.key});
  @override
  State<StatefulWidget> createState() => _HideIconState();
}

class _HideIconState extends State<_HideIcon> {
  late NavigationEntry entry;
  @override
  void initState() {
    super.initState();
    entry = widget.settings.navList[widget.index];
  }

  @override
  Widget build(final BuildContext context) => DefaultIconButton(
    icon: Icon(entry.hide ? Icons.visibility : Icons.visibility_off),
    onPressed: () async {
      setState(() => entry = entry.copyWith(!entry.hide));
      widget.settings.navList[widget.index] = entry;
      await saveItem<UserSetting>(widget.settings);
    },
    tooltip: entry.hide ? 'Unhide' : 'Hide',
  );
}
