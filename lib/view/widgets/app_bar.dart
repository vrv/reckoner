import 'package:flutter/material.dart';

class ReckonerAppBar extends StatelessWidget implements PreferredSizeWidget {
  final List<Widget>? actions;
  final Widget? leading;
  final Widget? title;

  const ReckonerAppBar({super.key, this.actions, this.leading, this.title});

  @override
  Widget build(final BuildContext context) => AppBar(
    centerTitle: false,
    backgroundColor: Theme.of(context).colorScheme.primaryFixed,
    foregroundColor: Theme.of(context).colorScheme.onPrimaryFixed,
    actions: actions,
    leading: leading,
    title: title,
  );

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}
