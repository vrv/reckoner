import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

import '../../model/secure_storage.dart';
import 'form/edit_fields.dart';
import 'form/password_input.dart';

class EncryptionSettingsEdit extends StatefulWidget {
  final EncryptionSettings? initialValue;
  final bool hideGenerator;
  final ValueChanged<EncryptionSettings>? onChanged;

  const EncryptionSettingsEdit({
    super.key,
    this.initialValue,
    this.hideGenerator = false,
    this.onChanged,
  });

  @override
  State<StatefulWidget> createState() => _EncryptionSettingsEditState();
}

class _EncryptionSettingsEditState extends State<EncryptionSettingsEdit> {
  late EncryptionSettings settings;

  @override
  void initState() {
    super.initState();
    settings = widget.initialValue ?? EncryptionSettings();
  }

  @override
  Widget build(final BuildContext context) {
    return Column(
      children: [
        ChipEdit(
          label: 'Encryption Type',
          initialValue: settings.type,
          options: const [
            FormBuilderChipOption(
              value: EncryptionType.none,
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [Icon(Icons.no_encryption), Text('None')],
              ),
            ),
            FormBuilderChipOption(
              value: EncryptionType.db,
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [Icon(Icons.lock), Text('Database')],
              ),
            ),
            FormBuilderChipOption(
              value: EncryptionType.custom,
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [Icon(Icons.enhanced_encryption), Text('Custom')],
              ),
            ),
          ],
          onChanged:
              (final value) => setState(() {
                settings.type = value ?? settings.type;
                widget.onChanged?.call(settings);
              }),
        ),
        if (settings.type == EncryptionType.custom)
          PasswordFormField(
            label: 'Encryption Key',
            initialValue: settings.customKey,
            onChanged: (final value) {
              settings.customKey = value;
              widget.onChanged?.call(settings);
            },
            hideGenerator: widget.hideGenerator,
          ),
      ],
    );
  }
}
