import 'package:flutter/material.dart';
import 'package:watch_it/watch_it.dart';

import '../../model/tz_date_time.dart';
import '../../service/date_range.dart';
import '../util/datetime_display.dart';
//import 'util/layout.dart';

class DateRangePicker extends WatchingWidget {
  const DateRangePicker({super.key});

  Widget iconWithText(
    final BuildContext context,
    final TZDateTimeRange? range,
  ) {
    String label = 'Everything';
    if (range != null) {
      label = '${datetimeDisplay(range.start)} - ${datetimeDisplay(range.end)}';
    }
    return Row(
      children: [Text(label), const Text(' '), const Icon(Icons.date_range)],
    );
  }

  @override
  Widget build(final BuildContext context) {
    const bold = TextStyle(
      fontStyle: FontStyle.italic,
      fontWeight: FontWeight.bold,
    );

    final range = watchPropertyValue(
      (final DateTimeRangeLoader l) => l.getRange(),
    );
    final DateTimeRangeLoader dateLoader = di();
    var swipe = Offset.zero;

    return GestureDetector(
      onPanStart: (final details) => swipe = Offset.zero,
      onPanUpdate: (final details) => swipe += details.delta,
      onPanEnd: (final details) {
        if (swipe.dx > 100) {
          dateLoader.offset += -1;
        } else if (swipe.dx < -100) {
          dateLoader.offset += 1;
        }
        swipe = Offset.zero;
      },
      // onHorizontalDragUpdate: (final details) => print(details),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          InkWell(
            onTap: () => dateLoader.offset += -1,
            child: const Icon(Icons.navigate_before),
          ),
          PopupMenuButton<DateRangeDefault>(
            tooltip: 'Filter by date range',
            position: PopupMenuPosition.under,
            color: Theme.of(context).colorScheme.secondaryFixed,
            onSelected: (final value) async {
              if (value == DateRangeDefault.custom) {
                final manualRange = await showDateRangePicker(
                  context: context,
                  firstDate: DateTime(1990),
                  lastDate: DateTime(DateTime.now().year + 1),
                  builder:
                      (final context, final child) => Column(
                        children: [
                          ConstrainedBox(
                            constraints: const BoxConstraints(maxWidth: 500.0),
                            child: child,
                          ),
                        ],
                      ),
                );
                dateLoader.setCustomRange(manualRange);
              }
              dateLoader.defaultSelection = value;
            },
            itemBuilder:
                (final context) => <PopupMenuEntry<DateRangeDefault>>[
                  PopupMenuItem<DateRangeDefault>(
                    value: DateRangeDefault.day,
                    child: Text(
                      'Today',
                      style:
                          dateLoader.defaultSelection == DateRangeDefault.day
                              ? bold
                              : null,
                    ),
                  ),
                  PopupMenuItem<DateRangeDefault>(
                    value: DateRangeDefault.week,
                    child: Text(
                      'This week',
                      style:
                          dateLoader.defaultSelection == DateRangeDefault.week
                              ? bold
                              : null,
                    ),
                  ),
                  PopupMenuItem<DateRangeDefault>(
                    value: DateRangeDefault.month,
                    child: Text(
                      'This month',
                      style:
                          dateLoader.defaultSelection == DateRangeDefault.month
                              ? bold
                              : null,
                    ),
                  ),
                  PopupMenuItem<DateRangeDefault>(
                    value: DateRangeDefault.quarter,
                    child: Text(
                      'Past three months',
                      style:
                          dateLoader.defaultSelection ==
                                  DateRangeDefault.quarter
                              ? bold
                              : null,
                    ),
                  ),
                  PopupMenuItem<DateRangeDefault>(
                    value: DateRangeDefault.year,
                    child: Text(
                      'This year',
                      style:
                          dateLoader.defaultSelection == DateRangeDefault.year
                              ? bold
                              : null,
                    ),
                  ),
                  PopupMenuItem<DateRangeDefault>(
                    value: DateRangeDefault.all,
                    child: Text(
                      'Everything',
                      style:
                          dateLoader.defaultSelection == DateRangeDefault.all
                              ? bold
                              : null,
                    ),
                  ),
                  PopupMenuItem<DateRangeDefault>(
                    value: DateRangeDefault.custom,
                    child: Text(
                      'Custom',
                      style:
                          dateLoader.defaultSelection == DateRangeDefault.custom
                              ? bold
                              : null,
                    ),
                  ),
                ],
            child: iconWithText(context, range),
          ),
          InkWell(
            onTap: () => dateLoader.offset += 1,
            child: const Icon(Icons.navigate_next),
          ),
        ],
      ),
    );
  }
}
