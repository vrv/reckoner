import 'dart:math';

import 'package:flutter/material.dart';

class ShakeWidget extends StatefulWidget {
  final Widget child;
  final double shakeOffset;
  final int shakeCount;
  final Duration shakeDuration;

  const ShakeWidget({
    super.key,
    required this.child,
    required this.shakeOffset,
    this.shakeCount = 4,
    this.shakeDuration = const Duration(milliseconds: 500),
  });

  @override
  State<StatefulWidget> createState() => _ShakeWidgetState();
}

class _ShakeWidgetState extends State<ShakeWidget>
    with SingleTickerProviderStateMixin {
  late final AnimationController animationController;

  @override
  void initState() {
    super.initState();
    animationController = AnimationController(
      duration: widget.shakeDuration,
      vsync: this,
    );
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    animationController.forward();
    return AnimatedBuilder(
      animation: animationController,
      builder: (final ctx, final child) {
        final sineVal = sin(
          widget.shakeCount * 2 * pi * animationController.value,
        );
        return Transform.translate(
          offset: Offset(sineVal * widget.shakeOffset, 0),
          child: child,
        );
      },
      child: widget.child,
    );
  }
}
