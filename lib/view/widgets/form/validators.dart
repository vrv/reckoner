import 'package:flutter/widgets.dart';

import '../../l10n/localizations.dart';

String? nonNullValidator<T>(final BuildContext context, final T? value) {
  if (value == null) return ReckonerLocalizations.of(context).missingValue;
  if (value == '') return ReckonerLocalizations.of(context).missingValue;
  return null;
}
