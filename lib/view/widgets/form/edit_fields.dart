import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:nanoid2/nanoid2.dart';

import '../../../model/model.dart';
import '../../l10n/localizations.dart';
import 'validators.dart';

class InputFieldGroup extends StatelessWidget {
  final String? label;
  final Widget child;

  const InputFieldGroup({super.key, this.label, required this.child});

  @override
  Widget build(final BuildContext context) {
    Widget? textLabel;
    if (label != null) textLabel = Text(label!);
    return Padding(
      padding: const EdgeInsets.all(10),
      child: InputDecorator(
        decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
          label: textLabel,
        ),
        child: child,
      ),
    );
  }
}

class NameEdit extends StatelessWidget {
  final NamedInterface model;
  final String? label;
  final bool validate;

  const NameEdit({
    super.key,
    required this.model,
    this.label,
    this.validate = true,
  });

  @override
  Widget build(final BuildContext context) {
    return TextFormField(
      textCapitalization: TextCapitalization.words,
      decoration: InputDecoration(
        labelText: label ?? ReckonerLocalizations.of(context).name,
      ),
      initialValue: model.name,
      onChanged: (final value) => model.name = value,
      validator:
          (final value) => validate ? nonNullValidator(context, value) : null,
    );
  }
}

class ChipEdit<T> extends StatelessWidget {
  final List<FormBuilderChipOption<T>> options;
  final T? initialValue;
  final ValueChanged<T?>? onChanged;
  final String? label;
  final WrapAlignment alignment;
  final FormFieldValidator<T>? validator;
  final FormFieldSetter<T>? onSaved;

  const ChipEdit({
    super.key,
    required this.options,
    this.initialValue,
    this.onChanged,
    this.label,
    this.alignment = WrapAlignment.spaceEvenly,
    this.validator,
    this.onSaved,
  });

  @override
  Widget build(final BuildContext context) => InputDecorator(
    decoration:
        label != null
            ? InputDecoration(border: InputBorder.none, labelText: label!)
            : const InputDecoration(border: InputBorder.none),
    child: FormBuilderChoiceChip<T>(
      alignment: alignment,
      name: label ?? nanoid(),
      selectedColor: Theme.of(context).colorScheme.secondaryFixed,
      decoration: const InputDecoration(border: InputBorder.none),
      backgroundColor: Theme.of(
        context,
      ).colorScheme.secondaryFixed.withAlpha(64),
      options: options,
      initialValue: initialValue,
      onChanged: onChanged,
      validator: validator,
      onSaved: onSaved,
    ),
  );
}

enum InputWrapperSize { small, medium, large, extraLarge }

class InputWrapper extends StatelessWidget {
  final double maxWidth;
  final EdgeInsetsGeometry padding;
  final Widget child;

  InputWrapper({
    super.key,
    final InputWrapperSize size = InputWrapperSize.large,
    this.padding = const EdgeInsets.all(8),
    required this.child,
  }) : maxWidth = switch (size) {
         InputWrapperSize.small => 250,
         InputWrapperSize.medium => 375,
         InputWrapperSize.large => 500,
         InputWrapperSize.extraLarge => 1000,
       };

  @override
  Widget build(final BuildContext context) => Container(
    constraints: BoxConstraints(maxWidth: maxWidth),
    padding: padding,
    child: child,
  );
}
