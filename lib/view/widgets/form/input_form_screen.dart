import 'package:flutter/material.dart';

import '../../util/popup_dialogs.dart';
import '../input_screen.dart';

typedef AfterSave<T> = T Function();

class InputFormScreen<T> extends StatelessWidget {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final VoidCallback? beforeCopy;
  final DeleteFunction? deleteCallback;
  final AfterSave<Future<T>> afterFormSave;
  final bool Function() hasChanges;
  final String? heroTag;
  final Widget title;
  final List<Widget> inputs;
  final bool scrollable;

  InputFormScreen({
    super.key,
    required this.title,
    required this.inputs,
    required this.afterFormSave,
    required this.hasChanges,
    this.heroTag,
    this.scrollable = true,
    this.beforeCopy,
    this.deleteCallback,
  });

  Widget body() {
    if (scrollable) return SingleChildScrollView(child: form());
    return SizedBox(height: double.infinity, child: form());
  }

  Widget form() => Form(
    key: _formKey,
    child: Center(
      child: Container(
        constraints: const BoxConstraints(minWidth: 700, maxWidth: 1200),
        padding: const EdgeInsets.all(10),
        child: Column(mainAxisSize: MainAxisSize.min, children: inputs),
      ),
    ),
  );

  @override
  Widget build(final BuildContext context) {
    Future<T?> onSave() async {
      if (!_formKey.currentState!.validate()) return null;
      _formKey.currentState!.save();
      return afterFormSave();
    }

    return InputScreen<T>(
      body: body(),
      heroTag: heroTag,
      onReturn: () {
        if (!hasChanges()) return Future.value(true);
        return editDiscardPopup(context, _formKey, onSave);
      },
      onSave: onSave,
      onCopy:
          beforeCopy == null
              ? null
              : () async {
                if (!_formKey.currentState!.validate()) return null;
                _formKey.currentState!.save();
                beforeCopy!();
                return afterFormSave();
              },
      onDelete: deleteCallback,
      title: title,
    );
  }
}
