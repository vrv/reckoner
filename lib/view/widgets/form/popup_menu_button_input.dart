import 'package:flutter/material.dart';

class PopupMenuButtonFormField<T> extends FormField<T> {
  final InputDecoration decoration;

  PopupMenuButtonFormField({
    super.key,
    final Color? color,
    final PopupMenuPosition position = PopupMenuPosition.under,
    final void Function(T)? onChanged,
    required final List<PopupMenuEntry<T>> Function(BuildContext) itemBuilder,
    required final Widget Function(T?) widgetBuilder,
    super.autovalidateMode,
    super.validator,
    super.initialValue,
    this.decoration = const InputDecoration(),
  }) : super(
         builder: (final field) {
           return InputDecorator(
             decoration: decoration.copyWith(errorText: field.errorText),
             child: PopupMenuButton<T>(
               color: color,
               position: position,
               onSelected: (final val) {
                 field.didChange(val);
                 onChanged?.call(val);
               },
               itemBuilder: itemBuilder,
               child: widgetBuilder(field.value),
             ),
           );
         },
       );
}
