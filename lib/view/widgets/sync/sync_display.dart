import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:watch_it/watch_it.dart';

import '../../../config/platform.dart';
import '../../../model/category.dart';
import '../../../model/device.dart';
import '../../../service/data_cache.dart';
import '../../../service/date_range.dart';
import '../../../service/sync_service_manager.dart';
import '../../../util/compute_date_range.dart';
import '../../l10n/localizations.dart';
import '../../model/layout_config.dart';
import '../../util/popup_dialogs.dart';
import '../../util/save_item.dart';
import '../default_icon_button.dart';
import '../form/edit_fields.dart';
import '../form/validators.dart';
import 'sync_background_enable_toggle.dart';
import 'sync_enable_toggle.dart';
import 'sync_utilities.dart';

class SyncDisplay extends WatchingWidget {
  const SyncDisplay({super.key});

  Widget _deviceToTile(
    final BuildContext context,
    final Device device,
    final Map<String, bool> deviceConnectionMap,
  ) {
    return ListTile(
      title: Text(device.name),
      subtitle: syncDateDisplay(device),
      trailing: DefaultIconButton(
        icon: const Icon(Icons.delete),
        onPressed: () => deletePopup(context, device, 'device'),
      ),
    );
  }

  @override
  Widget build(final BuildContext context) {
    final message = watchPropertyValue(
      (final SyncServiceManager m) => m.statusMessage,
    );
    final status = watchPropertyValue((final SyncServiceManager m) => m.status);
    final enabled = watchPropertyValue(
      (final SyncServiceManager m) => m.enabled,
    );
    final devices = watchValue((final DataCache c) => c.devices);
    final messageSnapshot = watchFuture(
      (final SyncServiceManager m) => m.displayInfo,
      initialValue: '',
    );
    final Map<String, bool> deviceConnectionMap = {};
    final displayText = '$message\n${messageSnapshot.data ?? ''}'.replaceAll(
      '\n',
      '\n',
    );

    final canBulkSync =
        ![
          SyncStatus.disabled,
          SyncStatus.offline,
          SyncStatus.error,
          SyncStatus.needLogin,
        ].contains(status);

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Wrap(
          alignment: WrapAlignment.center,
          runAlignment: WrapAlignment.spaceEvenly,
          children: [
            InputWrapper(
              size: InputWrapperSize.small,
              child: const SyncEnableToggle(),
            ),
            if (!isWeb)
              InputWrapper(
                size: InputWrapperSize.small,
                child: const SyncBackgroundEnableToggle(),
              ),
          ],
        ),
        Row(
          children: [
            Expanded(child: Text(displayText, textAlign: TextAlign.center)),
            if (enabled && status != SyncStatus.needLogin)
              TextButton(
                onPressed: () async {
                  if (await yesNoDialog(
                    context,
                    'Are you sure you want to disconnect? Sync status will be reset with this action',
                  )) {
                    di<SyncServiceManager>().reset().ignore();
                  }
                },
                child: const Text('Disconnect'),
              ),
          ],
        ),
        if (canBulkSync)
          TextButton(
            onPressed:
                () => showDialog<void>(
                  context: context,
                  builder: (final context) => _BulkSync(),
                ),
            child: const Text('Bulk Resync Data'),
          ),
        _ThisDevice(),
        TextButton(
          onPressed: () => editSyncSettingsCallback(context, enabled),
          child: editSyncSettingsWidget,
        ),
        ...devices.map(
          (final e) => _deviceToTile(context, e, deviceConnectionMap),
        ),
      ],
    );
  }
}

class _ThisDevice extends WatchingWidget {
  @override
  Widget build(final BuildContext context) {
    final device = watchValue((final DataCache c) => c.thisDevice);
    final width = LayoutConfig(context).screenWidth;

    return ExpansionTile(
      title: Text(device.name),
      subtitle: syncDateDisplay(device),
      trailing: const Text('This Device'),
      children: [
        Padding(
          padding: const EdgeInsets.all(5),
          child: Row(
            children: [
              Expanded(
                child: TextFormField(
                  textCapitalization: TextCapitalization.words,
                  decoration: const InputDecoration(label: Text('Device Name')),
                  initialValue: device.name,
                  onChanged: (final value) => device.name = value,
                  validator: (final value) => nonNullValidator(context, value),
                ),
              ),
              DefaultIconButton(
                onPressed: () {
                  if (device.name.isNotEmpty) saveItem(device);
                },
                icon: const Icon(Icons.save),
              ),
            ],
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const Text('Device ID: '),
            SizedBox(
              width: width < 500 ? 200 : null,
              child: Text(device.uuid, overflow: TextOverflow.ellipsis),
            ),
            DefaultIconButton(
              onPressed:
                  () => Clipboard.setData(ClipboardData(text: device.uuid)),
              icon: const Icon(Icons.copy),
            ),
          ],
        ),
      ],
    );
  }
}

class _BulkSync extends StatelessWidget {
  @override
  Widget build(final BuildContext context) {
    final localization = ReckonerLocalizations.of(context);
    final textStyle = Theme.of(context).textTheme.titleMedium?.copyWith(
      color: Theme.of(context).buttonTheme.colorScheme?.primaryFixed,
    );
    DateRangeDefault syncRange = DateRangeDefault.day;
    return AlertDialog(
      title: const Text('Bulk Sync'),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          FormBuilderChoiceChip(
            name: 'syncDate',
            initialValue: syncRange,
            decoration: const InputDecoration(border: InputBorder.none),
            onChanged: (final value) => syncRange = value ?? syncRange,
            options: const [
              FormBuilderChipOption(
                value: DateRangeDefault.day,
                child: Text('Today'),
              ),
              FormBuilderChipOption(
                value: DateRangeDefault.week,
                child: Text('This Week'),
              ),
              FormBuilderChipOption(
                value: DateRangeDefault.month,
                child: Text('This Month'),
              ),
              FormBuilderChipOption(
                value: DateRangeDefault.year,
                child: Text('This Year'),
              ),
              FormBuilderChipOption(
                value: DateRangeDefault.all,
                child: Text('Everything'),
              ),
            ],
          ),
        ],
      ),
      actions: [
        TextButton(
          onPressed: () async {
            if ([
                  DateRangeDefault.year,
                  DateRangeDefault.all,
                ].contains(syncRange) &&
                !await yesNoDialog(
                  context,
                  'This is a large time range and the full sync will take some time. Do you wish to continue?',
                )) {
              return;
            }
            if (!context.mounted) return;
            final range = switch (syncRange) {
              DateRangeDefault.day => computeDateRange(
                period: CategoryLimitPeriod.daily,
              ),
              DateRangeDefault.week => computeDateRange(
                period: CategoryLimitPeriod.weekly,
              ),
              DateRangeDefault.month => computeDateRange(
                period: CategoryLimitPeriod.monthly,
              ),
              DateRangeDefault.year => computeDateRange(
                period: CategoryLimitPeriod.yearly,
              ),
              (_) => null,
            };

            await saveItem(
              di<DataCache>().thisDevice.value..syncDate = range?.start,
            );

            di<SyncServiceManager>().bulkSync()?.ignore();

            if (context.mounted) Navigator.pop(context);
          },
          child: Text('Sync', style: textStyle),
        ),
        TextButton(
          onPressed: () => Navigator.pop(context),
          child: Text(localization.cancel, style: textStyle),
        ),
      ],
    );
  }
}
