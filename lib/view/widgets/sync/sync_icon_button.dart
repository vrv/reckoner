import 'package:flutter/material.dart';
import 'package:watch_it/watch_it.dart';

import '../../../config/platform.dart';
import '../../../model/secure_storage.dart';
import '../../../service/sync_service_manager.dart';
import 'sync_utilities.dart';

enum _SyncIconButtonAction { enableToggle, backgroundToggle, message, action }

class SyncIconButton extends WatchingStatefulWidget {
  const SyncIconButton({super.key});

  @override
  State<StatefulWidget> createState() => _SyncIconButton();
}

class _SyncIconButton extends State<SyncIconButton> {
  bool backgroundEnabled = false;

  @override
  void initState() {
    super.initState();
    SecureStorage.syncManagerSettings.then((final settings) {
      backgroundEnabled = settings.enableBackgroundSync;
      if (backgroundEnabled && mounted) setState(() {});
    });
  }

  void _toggleSync() =>
      di<SyncServiceManager>().enabled = !di<SyncServiceManager>().enabled;

  Future<void> _toggleBackground() async {
    setState(() => backgroundEnabled = !backgroundEnabled);
    final settings = await SecureStorage.syncManagerSettings;
    settings.enableBackgroundSync = backgroundEnabled;
  }

  @override
  Widget build(final BuildContext context) {
    final message = watchPropertyValue(
      (final SyncServiceManager m) => m.statusMessage,
    );
    final status = watchPropertyValue((final SyncServiceManager m) => m.status);
    final enabled = watchPropertyValue(
      (final SyncServiceManager m) => m.enabled,
    );
    final displayInfo = watchPropertyValue(
      (final SyncServiceManager m) async => m.displayInfo,
    );

    final icon = switch (status) {
      SyncStatus.disabled => Icons.cloud_off,
      SyncStatus.offline => Icons.sync_disabled,
      SyncStatus.online => Icons.sync,
      SyncStatus.error => Icons.sync_problem,
      SyncStatus.needLogin => Icons.sync_problem,
      SyncStatus.connected => Icons.cloud_done,
      SyncStatus.upload => Icons.cloud_upload,
      SyncStatus.download => Icons.cloud_download,
      SyncStatus.bulkSync => Icons.cloud_sync,
    };

    return FutureBuilder(
      future: displayInfo,
      builder: (final context, final snapshot) {
        return PopupMenuButton<_SyncIconButtonAction>(
          color: Theme.of(context).colorScheme.secondaryFixed,
          icon: Icon(icon),
          splashRadius: 25,
          onSelected:
              (final value) => switch (value) {
                _SyncIconButtonAction.action => editSyncSettingsCallback(
                  context,
                  enabled,
                ),
                _SyncIconButtonAction.message => null,
                _SyncIconButtonAction.enableToggle => _toggleSync(),
                _SyncIconButtonAction.backgroundToggle => _toggleBackground(),
              },
          position: PopupMenuPosition.under,
          itemBuilder:
              (final BuildContext context) => [
                PopupMenuItem(
                  value: _SyncIconButtonAction.enableToggle,
                  child: Center(
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Icon(enabled ? Icons.cloud_off : Icons.cloud),
                        Text(
                          enabled
                              ? 'Disable Synchronization'
                              : 'Enable Synchronization',
                        ),
                      ],
                    ),
                  ),
                ),
                if (!isWeb)
                  PopupMenuItem(
                    enabled: enabled,
                    value: _SyncIconButtonAction.backgroundToggle,
                    child: Center(
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Icon(
                            backgroundEnabled
                                ? Icons.sync_disabled
                                : Icons.sync,
                          ),
                          Text(
                            backgroundEnabled
                                ? 'Disable Background Sync'
                                : 'Enable Background Sync',
                            overflow: TextOverflow.ellipsis,
                          ),
                        ],
                      ),
                    ),
                  ),
                PopupMenuItem<_SyncIconButtonAction>(
                  value: _SyncIconButtonAction.message,
                  child: Center(
                    child: Text(
                      '$message${(snapshot.data ?? '').isNotEmpty ? '\n${snapshot.data!}' : ''}'
                          .replaceAll('\n', '\n'),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                const PopupMenuItem<_SyncIconButtonAction>(
                  value: _SyncIconButtonAction.action,
                  child: Center(child: editSyncSettingsWidget),
                ),
              ],
          tooltip: message,
        );
      },
    );
  }
}
