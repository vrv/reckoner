import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:watch_it/watch_it.dart';

import '../../../service/sync_service_manager.dart';

class SyncEnableToggle extends WatchingWidget {
  const SyncEnableToggle({super.key});

  @override
  Widget build(final BuildContext context) {
    final enabled = watchPropertyValue(
      (final SyncServiceManager m) => m.enabled,
    );
    return FormBuilderSwitch(
      initialValue: enabled,
      decoration: const InputDecoration(border: InputBorder.none),
      name: 'Enable Sync',
      title: Text(
        enabled ? 'Disable Synchronization' : 'Enable Synchronization',
      ),
      onChanged: (final value) {
        if (value == null) return;
        di<SyncServiceManager>().enabled = value;
      },
    );
  }
}
