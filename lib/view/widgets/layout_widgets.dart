import 'dart:math';

import 'package:flutter/material.dart';

import '../../model/report.dart';
import 'transaction_report.dart';

Widget fixedWidthSpacer() => const SizedBox(width: 20);

class FloatingButtonOverlay extends StatelessWidget {
  final Widget body;
  final String? heroTag;
  final Widget? floatingButton;
  final void Function()? onPressed;
  final String? tooltip;
  final IconData icon;
  final Key? actionButtonKey;

  const FloatingButtonOverlay(
    this.body, {
    super.key,
    this.actionButtonKey,
    this.floatingButton,
    this.heroTag,
    this.onPressed,
    this.icon = Icons.add,
    this.tooltip,
  });

  @protected
  Widget _floatingButton(final BuildContext context) {
    if (floatingButton != null) return floatingButton!;

    return ReckonerActionButton(
      key: actionButtonKey,
      child: Hero(
        tag: heroTag ?? '',
        child: Tooltip(
          message: tooltip,
          child: InkWell(onTap: onPressed, child: Icon(icon)),
        ),
      ),
    );
  }

  @override
  Widget build(final BuildContext context) {
    return Stack(
      children: [
        Align(alignment: Alignment.topCenter, child: body),
        Positioned(bottom: 15, right: 15, child: _floatingButton(context)),
      ],
    );
  }
}

class ReportHeader extends StatelessWidget {
  final Report? report;
  final Widget child;

  const ReportHeader({super.key, required this.report, required this.child});

  @override
  Widget build(final BuildContext context) {
    if (report == null) return child;

    return LayoutBuilder(
      builder: (final context, final constraints) {
        return Column(
          children: [
            if (report != null)
              ConstrainedBox(
                constraints: BoxConstraints(
                  maxHeight: min(250, constraints.maxHeight / 4),
                ),
                child: TransactionReport(report!),
              ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(top: 10, bottom: 10),
                child: child,
              ),
            ),
          ],
        );
      },
    );
  }
}

class ReckonerActionButton extends StatelessWidget {
  final Widget child;

  const ReckonerActionButton({super.key, this.child = const Icon(Icons.add)});

  @override
  Widget build(final BuildContext context) {
    return Container(
      width: 56,
      height: 56,
      decoration: BoxDecoration(
        color: Theme.of(context).colorScheme.secondaryFixed,
        borderRadius: BorderRadius.circular(15),
      ),
      child: child,
    );
  }
}
