import 'package:flutter/material.dart';

class DefaultTextButton extends StatelessWidget {
  final VoidCallback? onPressed;
  final Icon? icon;
  final String? tooltip;
  final Widget child;
  final ButtonStyle? style;

  const DefaultTextButton({
    super.key,
    this.icon,
    required this.child,
    required this.onPressed,
    this.tooltip,
    this.style,
  });

  @override
  Widget build(final BuildContext context) {
    final buttonStyle =
        style ??
        TextButton.styleFrom(
          backgroundColor: Theme.of(context).colorScheme.secondaryFixed,
          foregroundColor: Theme.of(context).colorScheme.onSecondaryFixed,
        );
    if (icon != null) {
      return TextButton.icon(
        style: buttonStyle,
        onPressed: onPressed,
        label: child,
        icon: icon,
      );
    }
    return TextButton(onPressed: onPressed, style: buttonStyle, child: child);
  }
}
