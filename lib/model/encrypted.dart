import 'dart:convert';
import 'dart:typed_data';

// import 'package:cryptography_plus/cryptography_plus.dart';
import 'package:json_annotation/json_annotation.dart';

part 'encrypted.g.dart';

enum EncryptionAlgorithm { none, aes }

@JsonSerializable()
class EncryptedData {
  AESEncryptedData? aes;
  String? objectJson;

  EncryptionAlgorithm get algorithm {
    if (aes != null) return EncryptionAlgorithm.aes;
    return EncryptionAlgorithm.none;
  }

  EncryptedData({this.aes, this.objectJson})
    : assert(aes != null || objectJson != null);

  factory EncryptedData.fromJson(final Map<String, dynamic> json) =>
      _$EncryptedDataFromJson(json);
  Map<String, dynamic> toJson() => _$EncryptedDataToJson(this);
}

@JsonSerializable()
class AESEncryptedData {
  String cipherText;
  String nonce; //Initialization Vector
  String? mac;

  AESEncryptedData({required this.cipherText, required this.nonce, this.mac});

  AESEncryptedData.fromBytes(final Uint8List cipher, final Uint8List iv)
    : cipherText = base64Encode(cipher),
      nonce = base64Encode(iv);

  factory AESEncryptedData.fromJson(final Map<String, dynamic> json) =>
      _$AESEncryptedDataFromJson(json);
  Map<String, dynamic> toJson() => _$AESEncryptedDataToJson(this);
}
