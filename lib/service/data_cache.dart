import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';

import '../database/reckoner_db.dart';
import '../model/account.dart';
import '../model/category.dart';
import '../model/currency.dart';
import '../model/device.dart';
import '../model/report.dart';
import '../model/settings.dart';
import '../model/transaction.dart';
import 'date_range.dart';

class DataCache {
  final organizations = ValueNotifier<List<Organization>>([]);
  final accounts = ValueNotifier<List<Account>>([]);
  final categoryGroups = ValueNotifier<List<CategoryGroup>>([]);
  final currencyMap = ValueNotifier<CurrencyMap>({});
  final reportGroupMap = ValueNotifier<ReportGroupMap>({});
  final reportMap = ValueNotifier<Map<String, Report>>({});
  final tags = ValueNotifier<List<String>>([]);
  final setting = ValueNotifier<UserSetting>(UserSetting());
  final transactions = ValueNotifier<List<TransactionEntity>>([]);
  final categoryBudgetMap = ValueNotifier<CategoryBudgetMap>({});
  final devices = ValueNotifier<List<Device>>([]);
  final thisDevice = ValueNotifier(Device());
  final merchants = ValueNotifier<List<String>>([]);
  List<StreamSubscription<Object>> _otSubscriptions = [];
  List<StreamSubscription<Object>> _tiSubscriptions = [];
  CategoryBudgetDateMap _loadedBudgets = {};
  static DataCache? _instance;

  DataCache._();

  static Future<DataCache> get instance async {
    if (_instance == null) {
      _instance = DataCache._();
      await _instance!._init();
    }
    return _instance!;
  }

  Report? get accountReport => reportMap.value[setting.value.accountReportUuid];
  Report? get transactionReport =>
      reportMap.value[setting.value.transactionReportUuid];
  Report? get merchantReport =>
      reportMap.value[setting.value.merchantReportUuid];
  Report? get tagReport => reportMap.value[setting.value.tagReportUuid];

  void _loadRangeData(final ReckonerDb db) {
    final range = DateTimeRangeLoader.instance.getRange();
    Future.wait(_otSubscriptions.map((final e) async => e.cancel())).ignore();
    _otSubscriptions = [
      db.transactionDao.loadTransactionEntities(range: range?.native).listen((
        final event,
      ) {
        transactions.value = event;
        if (event.isNotEmpty) db.categoryDao.generateDates(range?.end);
      }),
      db.categoryDao.loadCategoryBudgets(range?.native).listen((final event) {
        _loadedBudgets = event;
        _updateBudgets();
      }),
    ];
  }

  void _updateBudgets() {
    final CategoryBudgetMap newMap = {};

    void updateMap(final Category cat) {
      final budgetCurMap = _loadedBudgets[cat.groupUuid]?[cat.uuid];
      if (budgetCurMap != null) {
        newMap.putIfAbsent(cat.groupUuid, () => {})[cat.uuid] = budgetCurMap;
        for (final child in cat.children) {
          updateMap(child);
        }
      }
    }

    for (final group in categoryGroups.value) {
      for (final cat in group.categories) {
        updateMap(cat);
      }
    }
    categoryBudgetMap.value = newMap;
  }

  Future<void> _init() async {
    final db = ReckonerDb.I;

    //Need to explicitly load this data as it might be needed for the default page load
    await db.settingsDao.handleUserSettingUpdate();

    _loadRangeData(db);
    DateTimeRangeLoader.instance.addListener(() => _loadRangeData(db));
    _tiSubscriptions = [
      db.commonDao.loadAllDbItems<Report>().listen(
        (final event) =>
            reportMap.value = event.asMap().map(
              (final key, final value) => MapEntry(value.uuid, value),
            ),
      ),
      db.accountDao.loadOrganizations().listen((final orgList) {
        organizations.value = orgList;
        accounts.value = orgList.fold(
          [],
          (final accountList, final org) => accountList + org.accounts,
        );
      }),
      db.categoryDao.loadCategoryGroups().listen((final event) {
        categoryGroups.value = event;
        _updateBudgets();
      }),
      db.reportDao.loadReportGroups().listen(
        (final event) => reportGroupMap.value = event,
      ),
      db.transactionTagDao.loadTags().listen(
        (final event) => tags.value = event,
      ),
      db.currencyDao.loadCurrencyCodeMap().listen(
        (final event) => currencyMap.value = event,
      ),
      db.settingsDao.loadUserSetting().listen(
        (final val) => setting.value = val,
      ),
      db.commonDao.loadAllDbItems<Device>().listen((final event) async {
        final possibleDevices = event.where((final d) => d.thisDevice);
        if (possibleDevices.isEmpty) {
          thisDevice.value = await db.deviceDao.getCurrentDevice();
        } else if (possibleDevices.length > 1) {
          final thisDeviceList = possibleDevices.toList();
          thisDevice.value = thisDeviceList.last;
          thisDeviceList.removeLast();
          debugPrint(
            'Warning! Multiple devices claim to be current device: ${jsonEncode(possibleDevices.toList())}',
          );
          await Future.wait(
            thisDeviceList.map(
              (final e) async => db.commonDao.softDeleteDbItem(e),
            ),
          );
        } else if (thisDevice.value != possibleDevices.first) {
          thisDevice.value = possibleDevices.first;
        }
        devices.value = event.toList()..remove(thisDevice.value);
      }),
      db.transactionMerchantDao.loadMerchants().listen(
        (final event) => merchants.value = event,
      ),
    ];
  }

  Future<void> dispose() async {
    await Future.wait<void>([
      ..._otSubscriptions.map((final e) => e.cancel()),
      ..._tiSubscriptions.map((final e) => e.cancel()),
    ]);
    _instance = null;
  }
}
