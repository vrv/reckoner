import 'package:fetch_client/fetch_client.dart';
import 'package:pocketbase/pocketbase.dart';

PocketBase getPBInstance(final String url) => PocketBase(
  url,
  httpClientFactory: () => FetchClient(mode: RequestMode.cors),
);
