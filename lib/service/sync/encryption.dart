import 'dart:convert';

import '../../model/encrypted.dart';
import 'encryption/aes.dart';

Future<String> encryptObject(final String key, final Object object) async {
  final data = switch (key.isEmpty) {
    true => EncryptedData(objectJson: jsonEncode(object)),
    false => await aesEncrypt(key, object),
  };
  return jsonEncode(data);
}

Future<T?> decryptObject<T>(
  final String key,
  final T? Function(Map<String, dynamic>) fromJson,
  final String jsonString,
) async {
  try {
    final json = jsonDecode(jsonString) as Map<String, dynamic>;
    final data = EncryptedData.fromJson(json);

    return switch (data.algorithm) {
      EncryptionAlgorithm.aes => fromJson(
        (await aesDecrypt(key, data.aes!)) ?? {},
      ),
      EncryptionAlgorithm.none => fromJson(
        jsonDecode(data.objectJson!) as Map<String, dynamic>,
      ),
    };
  } on Exception {
    return null;
  }
}
