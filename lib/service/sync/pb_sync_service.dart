import 'dart:async';
import 'dart:math';

import 'package:drift/isolate.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:pocketbase/pocketbase.dart';
import 'package:worker_manager/worker_manager.dart';

import '../../config/init.dart';
import '../../database/reckoner_db.dart';
import '../../model/secure_storage.dart';
import '../../util/random_ascii_string.dart';
import '../logger.dart';
import '../sync_service_manager.dart';
import 'bundle.dart';
import 'pb/native.dart' if (dart.library.js_interop) 'pb/web.dart';
import 'sync_service.dart';

const collection = kDebugMode ? 'reckonerTestData' : 'reckonerData';

Future<String?> validatePocketBaseCredentials(
  final PocketBaseSyncSettings settings,
  final bool isAdmin,
  final SyncManagerSettings managerSettings,
) async {
  final pb = getPBInstance(settings.url.toString());
  final loginError = await PocketBaseSyncService._validateAndConnect(
    pb,
    settings.username,
    settings.password,
    isAdmin,
  );
  if (loginError?.isNotEmpty ?? false) return loginError;

  final key = await managerSettings.encryptionSettings.encryptionKey;
  final list = await pb.collection(collection).getList(page: 1, perPage: 1);

  if (list.items.isEmpty) return null;

  if (await SyncBundle.canRead(list.items.first.getStringValue('data'), key)) {
    return null;
  }
  return 'Cannot decrypt data';
}

Future<void> setPocketBaseLoginData(
  final PocketBaseSyncSettings settings,
  final bool isAdmin,
) async {
  if (isAdmin) {
    final pb = PocketBase(settings.url.toString());
    await pb
        .collection('_superusers')
        .authWithPassword(settings.username, settings.password);
    if (!pb.authStore.isValid) return;
    final device = await ReckonerDb.I.deviceDao.getCurrentDevice();
    settings
      ..username = device.uuid.replaceAll('-', '')
      ..password = randomAsciiString();
    final CollectionModel users = await pb.collections.getOne('users');
    bool hasUsername = false;
    bool hasUpdate = false;
    bool hasUsernameIndex = false;
    for (final field in users.fields) {
      if (field.name == 'email' && field.required == true) {
        field.required = false;
        hasUpdate = true;
      }
      if (field.name == 'username') {
        hasUsername = true;
        if (field.required == false) {
          field.required = true;
          hasUpdate = true;
        }
      }
    }
    for (final index in users.indexes) {
      if (index.startsWith('CREATE UNIQUE INDEX') &&
          index.contains('ON `users` (`username`)')) {
        hasUsernameIndex = true;
      }
    }
    if (!hasUsername) {
      hasUpdate = true;
      users.fields.add(
        CollectionField({'name': 'username', 'type': 'text', 'required': true}),
      );
    }
    if (!hasUsernameIndex) {
      users.indexes.add(
        'CREATE UNIQUE INDEX `idx_username__pb_users_auth_` ON `users` (`username`) WHERE `username` != \'\'',
      );
      users.passwordAuth = PasswordAuthConfig(
        enabled: true,
        identityFields: ['username'],
      );
    }
    if (hasUpdate) {
      await pb.collections.update('users', body: users.toJson());
    }
    await pb
        .collection('users')
        .create(
          body: {
            'username': settings.username,
            'password': settings.password,
            'passwordConfirm': settings.password,
          },
        );
  }
  final previousSyncSettings = await SecureStorage.pbSyncSettings;
  SecureStorage.pbSyncSettings = Future.value(settings);
  if (previousSyncSettings.url != settings.url) {
    final thisDevice = await ReckonerDb.I.deviceDao.getCurrentDevice();
    thisDevice.syncDate = null;
    await ReckonerDb.I.commonDao.saveDbItem(thisDevice);
  }
}

final class PocketBaseSyncService extends SyncService {
  PocketBase _pb;
  void Function()? _cancelBulkSync;

  PocketBaseSyncService({required super.setStatus, super.useIsolate})
    : _pb = PocketBase('');

  bool get isBulkSync => _cancelBulkSync != null;

  @override
  bool get connected => _pb.authStore.isValid;

  @override
  Future<void> disconnect() async {
    cancelBulkSync();
    await _pb.realtime.unsubscribe();
    _pb.authStore.clear();
  }

  @override
  Future<bool> login() async {
    if (connected) return true;
    final (pb, errMsg) = await _login(await SecureStorage.pbSyncSettings);
    _pb = pb;
    errorMessage = errMsg ?? '';
    return connected;
  }

  static Future<(PocketBase, String?)> _login(
    final PocketBaseSyncSettings settings,
  ) async {
    if (!settings.loggedIn) return (PocketBase(''), null);
    final pb = getPBInstance(settings.url.toString());
    final errorMsg = await _validateAndConnect(
      pb,
      settings.username,
      settings.password,
      false,
    );
    return (pb, errorMsg);
  }

  @override
  Future<void> subscribe() async {
    if (!connected || isDisposed) return;
    await _subscribe(
      _pb,
      pushStatus: pushStatus,
      popStatus: popStatus,
      resetStatus: resetStatus,
      setErrorMsg: (final val) => errorMessage = val,
      dispose: dispose,
    );
  }

  static Future<UnsubscribeFunc> _subscribe(
    final PocketBase pb, {
    final void Function(SyncStatus)? pushStatus,
    final void Function(SyncStatus)? popStatus,
    final void Function(SyncStatus)? resetStatus,
    final void Function(String)? setErrorMsg,
    final void Function()? dispose,
    final String? filter,
  }) {
    return pb.collection(collection).subscribe('*', (final e) async {
      final record = e.record;
      if (record == null) return;
      pushStatus?.call(SyncStatus.download);
      final (date, _, errMsg) = await _saveRecords(
        (await SecureStorage.syncManagerSettings).encryptionSettings,
        [record],
      );
      if (errMsg != null && errMsg.isNotEmpty) {
        setErrorMsg?.call(errMsg);
        resetStatus?.call(SyncStatus.error);
        dispose?.call();
      }
      popStatus?.call(SyncStatus.download);
    }, filter: filter);
  }

  static Future<(DateTime?, List<String> ids, String? errMsg)> _saveRecords(
    final EncryptionSettings encryptionSettings,
    final List<RecordModel> recordList, [
    final bool firstSync = false,
  ]) async {
    final bundles =
        (await Future.wait(
          recordList.map(
            (final r) => SyncBundle.fromServerData(
              encryptionSettings,
              r.getStringValue('data'),
              r.id,
            ),
          ),
        )).where((final e) => e != null).map((final e) => e!).toList();

    final syncData = SyncData(bundles);
    if (!syncData.canSave()) {
      return (
        null,
        <String>[],
        'Cannot save due to differing database versions',
      );
    }
    final saveData = await syncData.saveLocal(firstSync);
    return (saveData.date, saveData.syncIds, null);
  }

  @override
  Future<(bool, DateTime?)> bulkSync([final bool useIsolate = true]) async {
    if (isBulkSync) return (false, null);
    pushStatus(SyncStatus.bulkSync);
    final pbSettings = await SecureStorage.pbSyncSettings;
    final encryptionSettings =
        (await SecureStorage.syncManagerSettings).encryptionSettings;
    DateTime? date;
    String? errMsg;
    final token = isWeb ? null : ServicesBinding.rootIsolateToken;
    bool success = true;
    final syncDate = SyncService.syncDateWithOffset;

    if (!useIsolate || token == null) {
      _cancelBulkSync = () => success = false;
      (date, errMsg) = await _bulkSync(
        _pb,
        encryptionSettings,
        syncDate,
        () => !success,
      );
    } else {
      await encryptionSettings.cacheForIsolate();
      final connection = await ReckonerDb.I.serializableConnection();
      try {
        final cancelable = workerManager.executeGentle((
          final isCanceled,
        ) async {
          try {
            final db = ReckonerDb.isolate(await connection.connect());
            await workerManagerInit(db, token);
            if (isCanceled()) return (null, null);
            final (pb, errMsg) = await _login(pbSettings);
            if ((errMsg ?? '').isNotEmpty || !pb.authStore.isValid) {
              return (null, errMsg);
            }
            return _bulkSync(pb, encryptionSettings, syncDate, isCanceled);
          } catch (err, stack) {
            logError('Reckoner', err, stack);
            return (null, 'An error has occurred. See the error logs.');
          }
        });
        _cancelBulkSync = () {
          try {
            cancelable.cancel();
            success = false;
          } catch (_) {}
        };

        (date, errMsg) = await cancelable.future;
      } on CanceledError {
        success = false;
      }
    }
    _cancelBulkSync = null;
    if (errMsg != null) {
      errorMessage = errMsg;
      resetStatus(SyncStatus.error);
      return (false, null);
    }
    popStatus(SyncStatus.bulkSync);
    return (success, date);
  }

  static Future<(DateTime?, String?)> _bulkSync(
    final PocketBase pb,
    final EncryptionSettings encryptionSettings,
    final DateTime? lastSyncDate,
    final bool Function() isCanceled,
  ) async {
    //Need to check isCanceled before each await statement
    if (isCanceled()) return (null, null);

    Future<bool> bulkSend(final ReckonerDb db, final SyncBundle bundle) =>
        _bundleSend(pb, encryptionSettings, bundle);
    final bulkSyncStart = DateTime.now();

    final firstSync = lastSyncDate == null;
    final filter =
        lastSyncDate == null
            ? ''
            : '&& updated > "${DateFormat('yyyy-MM-dd hh:mm:ss').format(lastSyncDate.toUtc())}"';
    final tiFilter = 'overTime = false $filter';
    final otFilter = 'overTime = true $filter';

    // Get Server remote time invariant data
    if (isCanceled()) return (null, null);
    final recordList = await pb
        .collection(collection)
        .getFullList(expand: 'overTime,data', filter: tiFilter);

    if (isCanceled()) return (null, null);
    var (syncDate, savedTIids, errMsg2) = await _saveRecords(
      encryptionSettings,
      recordList,
      firstSync,
    );

    if (errMsg2 != null) return (null, errMsg2);

    //Subscribe to TI changes
    if (isCanceled()) return (null, null);
    final unsubscribe = await _subscribe(pb, filter: tiFilter);

    // Send local time invariant data merged with remote data
    // Load all data to handle any case of missing time invariant data
    if (isCanceled()) {
      await unsubscribe.call();
      return (null, null);
    }
    syncDate = chooseDate(
      syncDate,
      await (await SyncData.loadTimeInvariant(
        lastSyncDate,
      )).send(bulkSend, savedTIids),
    );

    final saveOTids = <String>[];

    // Get Server overtime data
    int page = 0;
    ResultList<RecordModel> otRecordList;

    do {
      // Check if disposed before pulling overtime data
      if (isCanceled()) {
        await unsubscribe.call();
        return (null, null);
      }
      otRecordList = await pb
          .collection(collection)
          .getList(
            page: ++page,
            perPage: 100,
            filter: otFilter,
            sort: 'updated',
            expand: 'overTime,data',
          );
      final (date, ids, errMsg) = await _saveRecords(
        encryptionSettings,
        otRecordList.items,
        firstSync,
      );
      if (errMsg != null) return (null, errMsg);
      syncDate = chooseDate(syncDate, date);
      saveOTids.addAll(ids);
    } while (page < otRecordList.totalPages);

    // Now upload category budgets and transactions
    if (isCanceled()) {
      await unsubscribe.call();
      return (null, null);
    }
    syncDate = chooseDate(
      syncDate,
      await (await SyncData.loadOverTime(
        lastSyncDate,
      )).send(bulkSend, saveOTids),
    );

    // Catch any data the user edited while the sync was in progress
    if (isCanceled()) {
      await unsubscribe.call();
      return (null, null);
    }
    syncDate = chooseDate(
      syncDate,
      await (await SyncData.loadAll(bulkSyncStart)).send(bulkSend, []),
    );
    await unsubscribe.call();
    return (syncDate, null);
  }

  @override
  Future<bool> sendUpdates() async {
    if (!connected || isBulkSync || isDisposed) return false;
    pushStatus(SyncStatus.upload);
    final SyncData data = await SyncData.loadAll(
      SyncService.syncDateWithOffset,
    );
    final sent =
        await data.send(
          (final db, final bundle) async => _bundleSend(
            _pb,
            (await SecureStorage.syncManagerSettings).encryptionSettings,
            bundle,
          ),
          [],
        ) !=
        null;
    popStatus(SyncStatus.upload);
    return sent;
  }

  static Future<bool> _bundleSend(
    final PocketBase pb,
    final EncryptionSettings encryptionSettings,
    final SyncBundle bundle, [
    final int retryCount = 0,
  ]) async {
    try {
      final body = {
        'overTime': bundle.isOverTime,
        'data': await bundle.toServerData(encryptionSettings),
      };
      RecordModel? record;
      SyncBundle? serverBundle;
      // When record isn't there, it throws an error
      try {
        record = await pb
            .collection(collection)
            .getOne(bundle.syncId, expand: 'data');
        serverBundle = await SyncBundle.fromServerData(
          encryptionSettings,
          record.getStringValue('data'),
          record.id,
        );
      } on ClientException catch (err) {
        if (err.statusCode != 404) rethrow;
      }

      if (serverBundle == null) {
        body['id'] = bundle.syncId;
        await pb.collection(collection).create(body: body);
      } else if (bundle.hasNewerData(serverBundle) ?? false) {
        await pb.collection(collection).update(bundle.syncId, body: body);
      }

      return true;
    } on ClientException catch (err) {
      //Exponentially backoff in response to 429 error (Too many requests)
      //Treat status code 0 or isAbort the same way
      //Start with 250 ms and double delay each time.
      if ([0, 429].contains(err.statusCode) || err.isAbort) {
        final delay = 250 * pow(2, retryCount).toInt() + Random().nextInt(100);
        return Future.delayed(
          Duration(milliseconds: delay),
          () async =>
              _bundleSend(pb, encryptionSettings, bundle, retryCount + 1),
        );
      } else {
        rethrow;
      }
    }
  }

  static Future<String?> _validateAndConnect(
    final PocketBase pb,
    final String username,
    final String password,
    final bool isAdmin,
  ) async {
    if (username.isEmpty) return 'Missing username';
    if (password.isEmpty) return 'Missing password';
    try {
      if (isAdmin) {
        await pb.collection('_superusers').authWithPassword(username, password);
      } else {
        await pb.collection('users').authWithPassword(username, password);
      }
    } on ClientException catch (err) {
      if (err.response['message'] is String) {
        return err.response['message'] as String;
      }
      return 'Failed to login';
    }
    if (!pb.authStore.isValid) return 'Invalid credentials';

    if (!isAdmin) return null;

    final isOverTime = CollectionField({'name': 'overTime', 'type': 'bool'});
    final data = CollectionField({
      'name': 'data',
      'type': 'text',
      'required': true,
      'min': 0,
      'max': 1_000_000,
    });
    final id = CollectionField({
      'autogeneratePattern': '[a-zA-Z0-9]{15}',
      'hidden': false,
      'max': 15,
      'min': 15,
      'name': 'id',
      'pattern': '^[a-zA-Z0-9]+\$',
      'primaryKey': true,
      'required': true,
      'system': true,
      'type': 'text',
    });
    final created = CollectionField({
      'name': 'created',
      'onCreate': true,
      'onUpdate': false,
      'type': 'autodate',
    });
    final updated = CollectionField({
      'name': 'updated',
      'onCreate': true,
      'onUpdate': true,
      'type': 'autodate',
    });
    final body = {
      'name': collection,
      'type': 'base',
      'listRule': '',
      'viewRule': '',
      'createRule': '',
      'updateRule': '',
      'deleteRule': '',
      'fields': [
        id.toJson(),
        created.toJson(),
        updated.toJson(),
        isOverTime.toJson(),
        data.toJson(),
      ],
    };

    bool hasColumns = false;
    bool hasCollection = false;
    try {
      final CollectionModel model = await pb.collections.getOne(collection);
      hasCollection = true;
      final columns = model.fields.map((final e) => e.name);
      hasColumns = columns.contains('overTime') && columns.contains('data');
    } catch (_) {}

    if (!hasCollection) {
      await pb.collections.create(body: body);
    } else if (!hasColumns) {
      await pb.collections.update(collection, body: body);
    }
    return null;
  }

  @override
  Future<void> reset() async {
    await disconnect();
    SecureStorage.pbSyncSettings = Future.value(PocketBaseSyncSettings());
  }

  static Future<bool> get isLoggedIn async =>
      (await SecureStorage.pbSyncSettings).loggedIn;

  @override
  Future<String> get displayInfo async =>
      connected ? 'Server: ${(await SecureStorage.pbSyncSettings).url}' : '';

  @override
  Future<int> get connectionStatusCode async {
    try {
      return (await _pb.health.check()).code;
    } on ClientException catch (err) {
      debugPrint(err.response.toString());
      return err.statusCode;
    }
  }

  void cancelBulkSync() {
    _cancelBulkSync?.call();
    _cancelBulkSync = null;
  }

  @override
  void dispose() {
    _cancelBulkSync?.call();
    _cancelBulkSync = null;
    super.dispose();
  }
}
