import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:local_auth/local_auth.dart';

import '../model/secure_storage.dart';
import '../view/router.dart';
import '../view/widgets/passkey_input_field.dart';

class AppLock extends ChangeNotifier {
  static bool _initialized = false;
  static bool _isLocked = false;
  static const Duration _lockDelay = Duration(minutes: 5);
  static DateTime? _lastUnlockTime;
  static bool _canUseBiometrics = false;
  static Timer? _timer;
  static bool _hasPassword = false;

  static final AppLock _instance = AppLock._();

  AppLock._();

  static bool get isLocked => _isLocked;
  static bool get canUseBiometrics => _canUseBiometrics;
  static bool get hasPassword => _hasPassword;

  static Future<String> get _appPasscode async =>
      (await SecureStorage.appLockSettings).password;

  static Future<AppLock> initialize() async {
    if (_initialized) return _instance;
    _initialized = true;

    try {
      final LocalAuthentication auth = LocalAuthentication();
      _canUseBiometrics =
          await auth.canCheckBiometrics && await auth.isDeviceSupported();
    } catch (_) {}

    final settings = await SecureStorage.appLockSettings;
    _hasPassword = settings.hasPassword;

    //Always initialize to being locked
    if (_hasPassword) await _setIsLocked(true);

    return _instance;
  }

  static Future<void> _setIsLocked(final bool value) async {
    if (value == _isLocked) return;
    if (!hasPassword && value) return;
    _isLocked = value;
    _lastUnlockTime = value ? null : DateTime.now();
    _timer?.cancel();
    if (!_isLocked) {
      final settings = await SecureStorage.appLockSettings;
      settings
        ..canUnlockTime = null
        ..unlockFailCount = 0;
      SecureStorage.appLockSettings = Future.value(settings);
      _timer = Timer(_lockDelay, () => _setIsLocked(true));
    }
    // ignore: invalid_use_of_protected_member
    AppRouter.appLockRoute();
  }

  static void userInteraction() {
    _timer?.cancel();
    if (!_isLocked) _timer = Timer(_lockDelay, () => _setIsLocked(true));
  }

  static void lock() => _setIsLocked(true);

  static Future<bool> unlock([final String lockPasscode = '']) async {
    final settings = await SecureStorage.appLockSettings;
    if ((settings.canUnlockTime?.compareTo(DateTime.now()) ?? -1) >= 0) {
      return false;
    }
    if (!_isLocked || !hasPassword) return true;

    if (settings.useBiometrics) {
      final authResult = await _callLocalAuth();
      if (authResult == null) {
        return false;
      } else if (authResult) {
        await _setIsLocked(false);
        return true;
      }
    } else if (await _appPasscode == lockPasscode) {
      await _setIsLocked(false);
      return true;
    }
    await _incrementLockFailCount();
    return false;
  }

  static Future<void> _incrementLockFailCount() async {
    final settings = await SecureStorage.appLockSettings;
    settings.unlockFailCount += 1;

    if (settings.unlockFailCount % 3 == 0) {
      final min = pow(3, (settings.unlockFailCount ~/ 3) - 1).toInt();
      settings.canUnlockTime = DateTime.now().add(Duration(minutes: min));
    }
    SecureStorage.appLockSettings = Future.value(settings);
  }

  Future<bool> setPassword(
    final String prevPasscode,
    final String newPasscode,
  ) async {
    final settings = await SecureStorage.appLockSettings;
    if (prevPasscode == settings.password) {
      settings.password = newPasscode;
      _hasPassword = settings.hasPassword;
      SecureStorage.appLockSettings = Future.value(settings);
      notifyListeners();
      return true;
    }
    return false;
  }

  Future<bool> clearPassword(final BuildContext context) async {
    if (await _forcePrompt(context) ?? false) {
      _hasPassword = false;
      SecureStorage.appLockSettings = Future.value(AppLockSettings());
      notifyListeners();
      return true;
    }
    return false;
  }

  Future<bool> enableBiometrics(final BuildContext context) async {
    if (!_canUseBiometrics) return false;
    final settings = await SecureStorage.appLockSettings;
    if (settings.useBiometrics) return false;
    if (settings.password.isNotEmpty &&
        context.mounted &&
        !await clearPassword(context)) {
      return false;
    }

    settings.useBiometrics = true;
    _hasPassword = settings.hasPassword;
    SecureStorage.appLockSettings = Future.value(settings);
    notifyListeners();
    return true;
  }

  Future<bool?> prompt(final BuildContext context) async {
    if (!context.mounted) return null;
    if (!hasPassword) return true;
    if (_lastUnlockTime != null &&
        DateTime.now()
                .difference(_lastUnlockTime!)
                .compareTo(const Duration(minutes: 1)) <
            0) {
      return true;
    }
    return _forcePrompt(context);
  }

  Future<bool?> _forcePrompt(final BuildContext context) async {
    if (_canUseBiometrics &&
        (await SecureStorage.appLockSettings).useBiometrics) {
      return _callLocalAuth();
    }

    if (context.mounted) {
      return showDialog<bool>(
        context: context,
        builder: (final ctx) {
          return AlertDialog(
            title: const Text('Enter Passkey'),
            content: PasskeyInputField(
              autofocus: true,
              textInputAction: TextInputAction.done,
              onChanged: (final value) async {
                final passcode = await _appPasscode;
                if (value.length != passcode.length) return;
                final check = (value == passcode);
                if (check) _lastUnlockTime = DateTime.now();
                if (ctx.mounted) Navigator.pop(ctx, check);
              },
            ),
          );
        },
      );
    }
    return null;
  }

  static Future<bool?> _callLocalAuth() async {
    try {
      final LocalAuthentication auth = LocalAuthentication();
      return await auth.authenticate(
        localizedReason: 'Please authenticate',
        options: const AuthenticationOptions(stickyAuth: true),
      );
    } catch (_) {}
    return null;
  }
}
