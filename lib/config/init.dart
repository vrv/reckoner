import 'dart:ui';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_timezone/flutter_timezone.dart';
import 'package:timezone/data/latest.dart';
import 'package:timezone/timezone.dart';
import 'package:watch_it/watch_it.dart';
import 'package:worker_manager/worker_manager.dart';

import '../database/reckoner_db.dart';
import '../model/report.dart';
import '../service/app_lock.dart';
import '../service/date_range.dart';
import '../service/logger.dart';
import '../view/theme.dart';
import '../view/tutorial/tutorial_handler.dart';
import 'init/native.dart' if (dart.library.js_interop) 'init/web.dart';
import 'platform.dart';
import 'user_pref_const.dart';

export 'init/native.dart'
    if (dart.library.js_interop) 'init/web.dart'
    show appOnClose;

// Normal application service initialization
Future<void> appInit() async {
  //Actually needed for GetIt
  WidgetsFlutterBinding.ensureInitialized();

  await initPreferences();

  initializeTimeZones();
  setLocalLocation(getLocation(await FlutterTimezone.getLocalTimezone()));

  //Background task setup, window properties
  await appSetup();

  //Register services in the default instance
  di
    ..registerSingleton(await AppLock.initialize())
    ..registerSingleton(DateTimeRangeLoader.instance)
    ..registerSingleton(
      ValueNotifier<ReportTransactionFilter>(ReportTransactionFilter()),
    )
    ..registerSingleton(ThemeHandler.instance)
    ..registerSingleton(ValueNotifier<TutorialHelp>(TutorialHelp.none));

  //Error logging
  await initLogger();

  //https://docs.flutter.dev/testing/errors
  FlutterError.onError = (final details) {
    FlutterError.presentError(details);
    if (details.exception is! CanceledError &&
        (details.exception is String &&
            !(details.exception as String).contains(
              'Could not navigate to initial route.',
            ))) {
      logError('Flutter', details.exception, details.stack);
    }
  };
  PlatformDispatcher.instance.onError = (final error, final stack) {
    // Worker manager throw this error when a worker is cancelled
    if (error is! CanceledError) logError('Platform', error, stack);
    return true;
  };

  await workerManager.init(isolatesCount: 1);
}

Future<void> workerManagerInit(
  final ReckonerDb db,
  final RootIsolateToken token,
) async {
  if (isWeb) {
    if (kDebugMode) throw UnsupportedError('Work manager not supported on web');
    return;
  }
  initializeTimeZones();
  final settings = await db.settingsDao.getUserSetting();
  setLocalLocation(getLocation(settings.timezone));

  // Needed since cryptography_flutter will likely spawns an isolate
  BackgroundIsolateBinaryMessenger.ensureInitialized(token);
}
