import 'dart:ffi' show DynamicLibrary;
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:tray_manager/tray_manager.dart';
import 'package:unix_single_instance/unix_single_instance.dart';
import 'package:window_manager/window_manager.dart';

import '../../model/secure_storage.dart';
import '../environment_const.dart';

Future<void> _handleMessage(final List<dynamic> args) async {
  await windowManager.show();
  await windowManager.restore();
  await windowManager.focus();
}

Future<void> desktopSetup() async {
  if (!kDebugMode && (Platform.isLinux || Platform.isMacOS)) {
    if (!await unixSingleInstance([], _handleMessage)) {
      // ignore: avoid_print
      print('Already running an instance of Reckoner!');
      exit(0);
    }
  }

  const String iconPath = 'assets/reckoner.png';

  if (Platform.isLinux && !kDebugMode && isAppImage) {
    final exePath = (Platform.resolvedExecutable.split('/')
      ..removeLast()).join('/');
    DynamicLibrary.open('$exePath/lib/libjsoncpp.so');
    DynamicLibrary.open('$exePath/lib/libsecret-1.so');
  }

  //Fix a minimum size for desktop clients
  await windowManager.ensureInitialized();
  if ((await SecureStorage.syncManagerSettings).backgroundEnabled) {
    await windowManager.setPreventClose(true);
  }

  await windowManager.waitUntilReadyToShow(
    const WindowOptions(
      minimumSize: Size(800, 600),
      title: 'Reckoner',
      windowButtonVisibility: false,
    ),
    () async {
      await windowManager.show();
      await windowManager.focus();
    },
  );

  await trayManager.setIcon(iconPath);
}
