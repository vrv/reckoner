/// /setup
const setupPath = '/setup';

/// /login
const loginPath = '/login';

/// /transactions
const transactionPath = '/transactions';

/// /accounts
const accountPath = '/accounts';

/// /options
const optionsPath = '/options';

/// /merchants
const merchantPath = '/merchants';

/// /merchants
const tagPath = '/tags';

const newId = 'new';

/// /category/$id
String categoryGroupPath(final String id) {
  assert(id.isNotEmpty);
  return '/category/group/$id';
}

/// /category/edit/$id
String categoryEditPath(final String id) {
  assert(id.isNotEmpty);
  return '/category/edit/$id';
}

/// /report/$id
String reportGroupPath(final String id) {
  assert(id.isNotEmpty);
  return '/report/group/$id';
}

/// /account/edit/$id
String accountEditPath(final String id) => '/account/edit/$id';

/// /report/edit/$id
String reportEditPath(final String id) => '/report/edit/$id';

/// /report/group/edit/$id
String reportGroupEditPath(final String id) => '/report/group/edit/$id';

/// /transaction/edit/$id
String transactionEditPath(final String id) => '/transaction/edit/$id';

/// /account/$id/transactions
String accountTransactionsPath(final String id) {
  assert(id.isNotEmpty);
  return '/account/$id/transactions';
}

/// /merchant/$id/transactions
String merchantTransactionsPath(final String id) {
  assert(id.isNotEmpty);
  return '/merchant/$id/transactions';
}

/// /merchant/none/transactions
const String noMerchantTransactionsPath = '/merchant/none/transactions';

/// /category/$id/transactions
String categoryTransactionsPath(final String id) {
  assert(id.isNotEmpty);
  return '/category/$id/transactions';
}

/// /category/group/$id/transactions
String uncategorizedTransactionsPath(final String id) {
  assert(id.isNotEmpty);
  return '/category/group/$id/transactions';
}

/// /currency/$code/transactions
String currencyTransactionsPath(final String code) {
  assert(code.isNotEmpty);
  return '/currency/$code/transactions';
}

/// /tag/$id/transactions
String tagTransactionsPath(final String id) {
  assert(id.isNotEmpty);
  return '/tag/$id/transactions';
}

/// /merchant/none/transactions
const String noTagTransactionsPath = '/tag/_/transactions';
