import 'package:drift/drift.dart';
import 'package:flutter/material.dart';
import 'package:stream_transform/stream_transform.dart';

import '../../../model/model.dart';
import '../../../model/transaction.dart';
import '../reckoner_db.dart';
import '../tables/account_tables.dart';
import '../tables/category_tables.dart';
import '../tables/currency_tables.dart';
import '../tables/transaction_tables.dart';

part 'transaction_dao.g.dart';

@DriftAccessor(
  tables: [
    Accounts,
    Categories,
    CategoryGroups,
    Currencies,
    Locations,
    Organizations,
    Transactions,
    TransactionCategories,
    TransactionSplits,
    TransactionTags,
    TransactionTransfers,
  ],
)
class TransactionDao extends DatabaseAccessor<ReckonerDb>
    with _$TransactionDaoMixin {
  TransactionDao(super.db);

  JoinedSelectStatement<HasResultSet, dynamic> _txQuery({
    final DateTimeRange? range,
    final String? accountUuid,
    final String? currencyCode,
    final Iterable<String>? uuids,
    final DateTime? syncDate,
    final String? merchantUuid,
    final int? limit,
    final int? offset,
  }) {
    final transferTx = alias(transactions, 'trTx');
    final transferCur = alias(currencies, 'trCur');
    final transferOrg = alias(organizations, 'trOrg');
    final transferAcnt = alias(accounts, 'trAcnt');
    final nullTransfer = alias(transactionTransfers, 'nullTr');
    final query = select(transactions).join([
      leftOuterJoin(
        accounts,
        accounts.uuid.equalsExp(transactions.accountUuid),
      ),
      leftOuterJoin(
        currencies,
        currencies.code.equalsExp(transactions.currencyCode),
      ),
      leftOuterJoin(
        organizations,
        organizations.uuid.equalsExp(accounts.orgUuid),
      ),
      leftOuterJoin(
        transactionTransfers,
        transactionTransfers.sourceUuid.equalsExp(transactions.uuid),
      ),
      leftOuterJoin(
        transferTx,
        transferTx.uuid.equalsExp(transactionTransfers.destinationUuid),
      ),
      leftOuterJoin(
        transferAcnt,
        transferAcnt.uuid.equalsExp(transferTx.accountUuid),
      ),
      leftOuterJoin(
        transferCur,
        transferCur.code.equalsExp(transferTx.currencyCode),
      ),
      leftOuterJoin(transferOrg, transferOrg.uuid.equalsExp(accounts.orgUuid)),
      leftOuterJoin(
        nullTransfer,
        nullTransfer.destinationUuid.equalsExp(transactions.uuid),
        useColumns: false,
      ),
      leftOuterJoin(
        locations,
        transactions.locationUuid.equalsExp(locations.uuid),
      ),
    ]);

    Expression<bool> expression = nullTransfer.destinationUuid.isNull();

    if (range != null) {
      expression = _addExpression(
        expression,
        transactions.date.isBetweenValues(range.start, range.end),
      );
    }
    if (accountUuid != null) {
      expression = _addExpression(
        expression,
        accounts.uuid.equals(accountUuid),
      );
    }
    if (currencyCode != null) {
      expression = _addExpression(
        expression,
        transactions.currencyCode.equals(currencyCode),
      );
    }
    if (uuids != null) {
      expression = _addExpression(expression, transactions.uuid.isIn(uuids));
    }
    if (merchantUuid != null) {
      expression = _addExpression(
        expression,
        transactions.merchantUuid.equals(merchantUuid),
      );
    }
    if (syncDate != null) {
      expression = _addExpression(
        expression,
        transactions.updateDate.isBiggerThanValue(syncDate),
      );
    } else {
      expression = _addExpression(expression, transactions.deleteDate.isNull());
    }

    query
      ..where(expression)
      ..orderBy([OrderingTerm.desc(transactions.date)]);

    if ((limit ?? 0) > 0) query.limit(limit!, offset: offset);

    return query;
  }

  (List<TransactionEntity>, List<String>) _mapTxQuery(
    final List<TypedResult> results,
  ) {
    final transferTx = alias(transactions, 'trTx');
    final transferCur = alias(currencies, 'trCur');
    final transferOrg = alias(organizations, 'trOrg');
    final transferAcnt = alias(accounts, 'trAcnt');
    final idList = <String>[];
    final txList =
        results.map((final row) {
          final tx = row.readTable(transactions);
          final account = row.readTableOrNull(accounts);
          if (account != null) {
            account.organization = row.readTable(organizations);
          }

          tx
            ..account = account
            ..currency = db.currencyDao.readCurrency(row, tx.currencyCode)
            ..location = row.readTableOrNull(locations);

          idList.add(tx.uuid);
          final transfer = row.readTableOrNull(transactionTransfers);
          if (transfer != null) {
            final trTx = row.readTable(transferTx);
            idList.add(trTx.uuid);
            final account = row.readTableOrNull(transferAcnt);
            if (account != null) {
              account.organization = row.readTable(transferOrg);
            }

            trTx
              ..account = account
              ..currency = db.currencyDao.readCurrency(
                row,
                trTx.currencyCode,
                transferCur,
              );

            transfer
              ..source = tx
              ..destination = trTx;
            return TransactionEntity(transfer: transfer);
          }
          return TransactionEntity(transaction: tx);
        }).toList();
    return (txList, idList);
  }

  SimpleSelectStatement<$TransactionSplitsTable, TransactionSplit>
  _txSplitQuery(final Iterable<String> idList) =>
      select(transactionSplits)..where((final tbl) => tbl.uuid.isIn(idList));

  Map<String, List<TransactionSplit>> _mapTxSplitResults(
    final List<TransactionSplit> results,
  ) => results.fold({}, (final value, final element) {
    value.putIfAbsent(element.uuid, () => []).add(element);
    return value;
  });

  JoinedSelectStatement<HasResultSet, dynamic> _txCatQuery(
    final Iterable<String> idList,
  ) {
    return select(transactionCategories).join([
      leftOuterJoin(
        categories,
        categories.uuid.equalsExp(transactionCategories.categoryUuid),
      ),
      leftOuterJoin(
        categoryGroups,
        categoryGroups.uuid.equalsExp(categories.groupUuid),
      ),
    ])..where(transactionCategories.uuid.isIn(idList));
  }

  Map<IdKey, List<TransactionCategory>> _mapTxCat(
    final List<TypedResult> rows,
  ) => rows.fold({}, (final map, final row) {
    final category = row.readTable(transactionCategories)
      ..category =
          (row.readTable(categories)!..group = row.readTable(categoryGroups));
    map.putIfAbsent(category.parentKey, () => []).add(category);
    return map;
  });

  SimpleSelectStatement<$TransactionTagsTable, TransactionTag> _tagQuery(
    final Iterable<String> idList,
  ) => select(transactionTags)..where((final tbl) => tbl.uuid.isIn(idList));

  Map<IdKey, List<String>> _mapTags(final List<TransactionTag> results) =>
      results.fold({}, (final value, final element) {
        value.putIfAbsent(element.parentKey, () => []).add(element.tag);
        return value;
      });

  void _mapTxSplitsOnEntities(
    final List<TransactionEntity> entities,
    final Map<String, List<TransactionSplit>> splitMap,
    final Map<IdKey, List<TransactionCategory>> catMap,
    final Map<IdKey, List<String>> tagMap,
  ) {
    List<TransactionSplit> mapTxSplits(final String txId) {
      return (splitMap[txId] ?? [])
          .map(
            (final split) =>
                split
                  ..categories = (catMap[split.key] ?? []).asMap().map(
                    (final key, final value) =>
                        MapEntry(value.category.groupUuid, value.category),
                  )
                  ..tags = (tagMap[split.key] ?? []).toSet(),
          )
          .toList();
    }

    for (final entity in entities) {
      if (entity.transaction != null) {
        entity.transaction!.splits = mapTxSplits(entity.transaction!.uuid);
      } else {
        entity.transfer!.source.splits = mapTxSplits(
          entity.transfer!.source.uuid,
        );
        entity.transfer!.destination.splits = mapTxSplits(
          entity.transfer!.destination.uuid,
        );
      }
    }
  }

  Stream<List<TransactionEntity>> loadTransactionEntities({
    final DateTimeRange? range,
    final String? accountUuid,
    final String? currencyCode,
    final List<String>? categoryUuidList,
    final String? tag,
  }) {
    if (categoryUuidList != null && tag != null) {
      final query =
          selectOnly(transactionCategories).join([
              leftOuterJoin(
                transactionTags,
                transactionTags.uuid.equalsExp(transactionCategories.uuid),
                useColumns: false,
              ),
            ])
            ..where(
              transactionCategories.categoryUuid.isIn(categoryUuidList) &
                  transactionTags.tag.equals(tag),
            )
            ..addColumns([transactionCategories.uuid]);
      return query.watch().switchMap((final rows) {
        final idList = rows.map(
          (final e) => e.read(transactionCategories.uuid)!,
        );
        return _loadTransactionInfo(
          accountUuid: accountUuid,
          currencyCode: currencyCode,
          range: range,
          uuids: idList,
        );
      });
    } else if (categoryUuidList != null) {
      final query =
          selectOnly(transactionCategories)
            ..where(transactionCategories.categoryUuid.isIn(categoryUuidList))
            ..addColumns([transactionCategories.uuid]);
      return query.watch().switchMap((final rows) {
        final idList = rows.map(
          (final e) => e.read(transactionCategories.uuid)!,
        );
        return _loadTransactionInfo(
          accountUuid: accountUuid,
          currencyCode: currencyCode,
          range: range,
          uuids: idList,
        );
      });
    } else if (tag != null) {
      final query =
          selectOnly(transactionTags)
            ..where(transactionTags.tag.equals(tag))
            ..addColumns([transactionTags.uuid]);
      return query.watch().switchMap((final rows) {
        final idList = rows.map((final e) => e.read(transactionTags.uuid)!);
        return _loadTransactionInfo(
          accountUuid: accountUuid,
          currencyCode: currencyCode,
          range: range,
          uuids: idList,
        );
      });
    }

    return _loadTransactionInfo(
      accountUuid: accountUuid,
      currencyCode: currencyCode,
      range: range,
    );
  }

  Stream<List<TransactionEntity>> _loadTransactionInfo({
    final DateTimeRange? range,
    final String? accountUuid,
    final String? currencyCode,
    final Iterable<String>? uuids,
  }) {
    return _txQuery(
      accountUuid: accountUuid,
      currencyCode: currencyCode,
      range: range,
      uuids: uuids,
    ).watch().switchMap((final results) {
      final (entities, ids) = _mapTxQuery(results);
      final Stream<List<Object>> stream = _txSplitQuery(ids).watch();
      return stream
          .combineLatestAll([_txCatQuery(ids).watch(), _tagQuery(ids).watch()])
          .map((final event) {
            final splitMap = _mapTxSplitResults(
              event[0] as List<TransactionSplit>,
            );
            final catMap = _mapTxCat(event[1] as List<TypedResult>);
            final tagMap = _mapTags(event[2] as List<TransactionTag>);

            _mapTxSplitsOnEntities(entities, splitMap, catMap, tagMap);

            return entities;
          });
    });
  }

  Expression<bool> _addExpression(
    final Expression<bool>? currentExpression,
    final Expression<bool> newExpression,
  ) {
    if (currentExpression == null) return newExpression;
    return currentExpression & newExpression;
  }

  Future<TransactionEntity?> getSingleTransactionEntity(final String id) async {
    final (txList, _) = _mapTxQuery(await _txQuery(uuids: [id]).get());
    final splitMap = _mapTxSplitResults(await _txSplitQuery([id]).get());
    final catMap = _mapTxCat(await _txCatQuery([id]).get());
    final tagMap = _mapTags(await _tagQuery([id]).get());
    _mapTxSplitsOnEntities(txList, splitMap, catMap, tagMap);
    return txList.length != 1 ? null : txList.first;
  }

  Future<List<TransactionEntity>> getTransactionEntitiesForSync(
    final DateTime? syncDate, {
    final int? limit,
    final int? offset,
  }) async {
    final (txList, ids) = _mapTxQuery(
      await _txQuery(syncDate: syncDate, limit: limit, offset: offset).get(),
    );
    final splitMap = _mapTxSplitResults(await _txSplitQuery(ids).get());
    final catMap = _mapTxCat(await _txCatQuery(ids).get());
    final tagMap = _mapTags(await _tagQuery(ids).get());
    _mapTxSplitsOnEntities(txList, splitMap, catMap, tagMap);
    return txList;
  }

  Future<int> softDeleteTransaction(final String uuid) async {
    final entity = await getSingleTransactionEntity(uuid);
    final txCompanion = TransactionsCompanion(
      deleteDate: Value(DateTime.now()),
    );
    if (entity == null) return 0;
    if (entity.transfer != null) {
      final source = entity.transfer!.source;
      final destination = entity.transfer!.destination;
      int count = await delete(transactionTransfers).delete(entity.transfer!);
      count += await (update(transactions)
        ..whereSamePrimaryKey(source)).write(txCompanion);
      count += await (update(transactions)
        ..whereSamePrimaryKey(destination)).write(txCompanion);
      return count;
    } else {
      return (update(transactions)
        ..whereSamePrimaryKey(entity.primaryTx)).write(txCompanion);
    }
  }

  Future<TransactionEntity> saveTransactionEntity(
    final TransactionEntity entity, [
    final bool update = true,
  ]) async {
    final newEntity = switch (entity.isTransfer) {
      true => TransactionEntity(
        transfer: await saveTransfer(entity.transfer!, update),
      ),
      false => TransactionEntity(
        transaction: await saveDbTransaction(entity.primaryTx, update),
      ),
    };
    return newEntity;
  }

  Future<TransactionTransfer> saveTransfer(
    final TransactionTransfer transfer, [
    final bool update = true,
  ]) async {
    transfer.source.amount = -transfer.source.amount.abs();
    transfer.destination
      ..amount = transfer.destination.amount.abs()
      ..name = transfer.source.name
      ..splits = transfer.source.splits.map((final e) => e.copy()).toList();
    final newTransfer = await db.commonDao.saveDbItem(
      TransactionTransfer()
        ..source = (await saveDbTransaction(transfer.source, update))
        ..destination = (await saveDbTransaction(transfer.destination, update)),
    );

    return newTransfer;
  }

  Future<Transaction> saveDbTransaction(
    final Transaction tx, [
    final bool update = true,
  ]) async {
    if (update) tx.updateDate = DateTime.now();
    final uuid = tx.uuid;

    try {
      if (tx.location != null) await db.commonDao.saveDbItem(tx.location!);
    } catch (_) {
      tx.location = null;
    }

    if (tx.splits.isEmpty) {
      tx.splits = [
        TransactionSplit(uuid: uuid, id: 0, name: tx.name, amount: tx.amount),
      ];
    } else if (tx.splits.length == 1) {
      tx.splits.first.name = tx.name;
      tx.splits.first.amount = tx.amount;
    } else {
      final splitSum = tx.splits.fold<int>(
        0,
        (final previousValue, final split) => previousValue + split.amount,
      );
      if (splitSum != tx.amount) {
        tx.splits.last.amount = tx.amount - (splitSum - tx.splits.last.amount);
      }
    }
    final newTx = await db.commonDao.saveDbItem(tx);
    await db.commonDao.deleteChildDbByUuid(db.transactionSplits, uuid);
    db.commonDao.setIdItemList(tx.splits, newTx.uuid);

    newTx.splits = await db.commonDao.saveChildList(tx.splits);

    for (final split in tx.splits) {
      final newSplit = newTx.splits.singleWhere((final s) => s.id == split.id);
      for (final category in split.categories.values) {
        await db.commonDao.saveDbItem(
          TransactionCategory(
            splitId: split.id,
            uuid: uuid,
            categoryUuid: category.uuid,
          ),
        );
        newSplit.categories[category.groupUuid] = category;
      }
      for (final tag in split.tags) {
        await db.commonDao.saveDbItem(
          TransactionTag(uuid: uuid, splitId: split.id, tag: tag),
        );
        newSplit.tags.add(tag);
      }
    }

    return newTx;
  }

  Future<void> updateSplitCatTag(final List<TransactionSplit> splits) async {
    await db.transaction(() async {
      for (final split in splits) {
        await (db.transactionCategories.delete()..where(
              (final txCat) =>
                  txCat.uuid.equals(split.uuid) &
                  txCat.splitId.equals(split.id),
            ))
            .go();
        await (db.transactionTags.delete()..where(
              (final txTag) =>
                  txTag.uuid.equals(split.uuid) &
                  txTag.splitId.equals(split.id),
            ))
            .go();
        for (final category in split.categories.values) {
          await db.commonDao.saveDbItem(
            TransactionCategory(
              splitId: split.id,
              uuid: split.uuid,
              categoryUuid: category.uuid,
            ),
          );
          // split.categories[category.groupUuid] = category;
        }
        for (final tag in split.tags) {
          await db.commonDao.saveDbItem(
            TransactionTag(uuid: split.uuid, splitId: split.id, tag: tag),
          );
        }
      }
      final uuids = splits.map((final s) => s.uuid);
      final query = db.update(transactions)
        ..where((final tx) => tx.uuid.isIn(uuids));
      await query.write(
        TransactionsCompanion(updateDate: Value(DateTime.now())),
      );
    });
  }
}
