import 'package:drift/drift.dart';

import '../../../model/tz_date_time.dart';
import '../reckoner_db.dart';
import '../tables/account_tables.dart';
import '../tables/currency_tables.dart';
import '../tables/merchant_tables.dart';
import '../tables/transaction_tables.dart';

part 'balance_dao.g.dart';

enum BalanceType { account, merchant, tag }

typedef BalanceMap = Map<String, Map<String, int>>;

@DriftAccessor(
  tables: [
    Accounts,
    AccountIdentifiers,
    AccountAlerts,
    Currencies,
    Locations,
    Merchants,
    Organizations,
    Transactions,
    TransactionSplits,
    TransactionTags,
    TransactionTransfers,
  ],
)
class BalanceDao extends DatabaseAccessor<ReckonerDb> with _$BalanceDaoMixin {
  BalanceDao(super.db);

  Stream<BalanceMap> loadBalances({
    required final BalanceType type,
    final TZDateTimeRange? range,
    final DateTime? endDate,
  }) {
    final txSum = transactionSplits.amount.sum();

    final Expression<String> uuidExp = switch (type) {
      BalanceType.account => transactions.accountUuid,
      BalanceType.merchant => transactions.merchantName,
      BalanceType.tag => transactionTags.tag,
    };

    final query =
        selectOnly(transactionSplits).join([
            leftOuterJoin(
              transactions,
              transactions.uuid.equalsExp(transactionSplits.uuid),
            ),
            if (type == BalanceType.tag)
              leftOuterJoin(
                transactionTags,
                transactionTags.uuid.equalsExp(transactionSplits.uuid) &
                    transactionTags.splitId.equalsExp(transactionSplits.id),
              ),
          ])
          ..addColumns([txSum, transactions.currencyCode, uuidExp])
          ..groupBy([transactions.currencyCode, uuidExp]);

    Expression<bool> expression = transactions.deleteDate.isNull();
    if (range != null) {
      expression =
          expression &
          transactions.date.isSmallerOrEqualValue(range.end) &
          transactions.date.isBiggerOrEqualValue(range.start);
    } else if (endDate != null) {
      expression =
          expression & transactions.date.isSmallerOrEqualValue(endDate);
    }
    if (type == BalanceType.merchant) {
      expression =
          expression &
          transactions.uuid.isNotInQuery(
            selectOnly(transactionTransfers)
              ..addColumns([transactionTransfers.sourceUuid]),
          ) &
          transactions.uuid.isNotInQuery(
            selectOnly(transactionTransfers)
              ..addColumns([transactionTransfers.destinationUuid]),
          );
    }
    query.where(expression);

    return query.watch().map((final rows) {
      final BalanceMap balanceMap = {};
      for (final result in rows) {
        final curCode = result.read(transactions.currencyCode);
        final uuid = result.read(uuidExp);
        if (curCode != null && uuid != null) {
          balanceMap.putIfAbsent(uuid, () => {})[curCode] =
              result.read(txSum) ?? 0;
        }
      }

      return balanceMap;
    });
  }
}
