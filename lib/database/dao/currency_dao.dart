import 'package:drift/drift.dart';

import '../../../model/currency.dart';
import '../reckoner_db.dart';
import '../tables/currency_tables.dart';
import '../tables/transaction_tables.dart';

part 'currency_dao.g.dart';

@DriftAccessor(tables: [Currencies, Transactions])
class CurrencyDao extends DatabaseAccessor<ReckonerDb> with _$CurrencyDaoMixin {
  CurrencyDao(super.db);

  Stream<CurrencyMap> loadCurrencyCodeMap() {
    final query = select(currencies)
      ..where((final tbl) => tbl.deleteDate.isNull());
    return query.watch().map(
      (final currencyList) => _mergeCurrencyListWithDefaults(currencyList),
    );
  }

  CurrencyMap _mergeCurrencyListWithDefaults(
    final List<Currency> currencyList,
  ) {
    final CurrencyMap map = Map.from(defaultCurrencies);
    for (final currency in currencyList) {
      map[currency.code] = currency;
    }
    return map;
  }

  Currency readCurrency(
    final TypedResult row,
    final String code, [
    final $CurrenciesTable? alias,
  ]) {
    final cur = row.readTableOrNull(alias ?? currencies);
    if (cur != null && cur.deleteDate == null) return cur;
    return defaultCurrencies[code] ?? Currency();
  }

  Future<int> deleteCurrency(final Currency currency) async {
    assert(!currency.isSystem);
    if (currency.isSystem) return 0;
    final deleteDate = DateTime.now();
    final deleteDateValue = Value(deleteDate);
    currency
      ..deleteDate = deleteDate
      ..updateDate = deleteDate;

    await update(currencies).replace(currency);
    final txQuery = update(transactions)
      ..where((final tbl) => tbl.currencyCode.equals(currency.code));
    return txQuery.write(
      TransactionsCompanion(
        updateDate: deleteDateValue,
        deleteDate: deleteDateValue,
      ),
    );
  }
}
