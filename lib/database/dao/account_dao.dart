import 'package:drift/drift.dart';
import 'package:stream_transform/stream_transform.dart';

import '../../../model/account.dart';
import '../reckoner_db.dart';
import '../tables/account_tables.dart';
import '../tables/currency_tables.dart';
import '../tables/transaction_tables.dart';

part 'account_dao.g.dart';

@DriftAccessor(
  tables: [
    Accounts,
    AccountIdentifiers,
    AccountAlerts,
    Currencies,
    Organizations,
    Transactions,
  ],
)
class AccountDao extends DatabaseAccessor<ReckonerDb> with _$AccountDaoMixin {
  AccountDao(super.db);

  Stream<List<Organization>> loadOrganizations() {
    final Stream<List<Object>> stream =
        (select(organizations)
          ..where((final tbl) => tbl.deleteDate.isNull())).watch();
    return stream
        .combineLatestAll([
          (select(accounts)
            ..where((final tbl) => tbl.deleteDate.isNull())).watch(),
          select(accountIdentifiers).watch(),
          select(accountAlerts).watch(),
        ])
        .map((final event) {
          final orgs = event[0] as List<Organization>;
          final accnts = event[1] as List<Account>;
          final identifies = event[2] as List<AccountIdentifier>;
          final alerts = event[3] as List<AccountAlert>;

          final Map<String, List<AccountIdentifier>> idMap = identifies.fold(
            {},
            (final value, final element) {
              value.putIfAbsent(element.uuid, () => []).add(element);
              return value;
            },
          );

          final Map<String, List<AccountAlert>> alertMap = alerts.fold({}, (
            final value,
            final element,
          ) {
            value.putIfAbsent(element.uuid, () => []).add(element);
            return value;
          });

          return orgs
              .map(
                (final org) =>
                    org
                      ..accounts =
                          accnts
                              .where((final a) => a.orgUuid == org.uuid)
                              .map(
                                (final account) =>
                                    account
                                      ..organization = org
                                      ..identifiers = idMap[account.uuid] ?? []
                                      ..alerts = alertMap[account.uuid] ?? [],
                              )
                              .toList(),
              )
              .toList();
        });
  }

  Future<List<Organization>> getSyncOrganizations([
    final DateTime? syncDate,
  ]) async {
    final query = select(organizations);
    if (syncDate != null) {
      query.where((final tbl) => tbl.updateDate.isBiggerThanValue(syncDate));
    }

    final orgList = await query.get();

    final orgMap = orgList.asMap().map(
      (final key, final value) => MapEntry(value.uuid, value),
    );

    final accntQuery = select(accounts)
      ..where((final tbl) => tbl.orgUuid.isIn(orgMap.keys));

    final accountList = await accntQuery.get();
    final uuidList = accountList.map((final e) => e.uuid);

    final Map<String, List<AccountAlert>> alerts = (await (select(accountAlerts)
          ..where((final tbl) => tbl.uuid.isIn(uuidList))).get())
        .fold({}, (final value, final element) {
          value.putIfAbsent(element.uuid, () => []).add(element);
          return value;
        });

    final Map<String, List<AccountIdentifier>> identifers = (await (select(
          accountIdentifiers,
        )..where((final tbl) => tbl.uuid.isIn(uuidList))).get())
        .fold({}, (final value, final element) {
          value.putIfAbsent(element.uuid, () => []).add(element);
          return value;
        });

    for (final accnt in accountList) {
      final uuid = accnt.uuid;
      accnt
        ..alerts = alerts[uuid] ?? []
        ..identifiers = identifers[uuid] ?? [];
      orgMap[accnt.orgUuid]?.accounts.add(accnt);
    }

    return orgList;
  }

  Future<List<Account>> getSyncAccounts([final DateTime? syncDate]) async {
    final query = select(accounts);
    if (syncDate != null) {
      query.where((final tbl) => tbl.updateDate.isBiggerThanValue(syncDate));
    }
    final accountList = await query.get();
    final uuidList = accountList.map((final e) => e.uuid);

    final Map<String, List<AccountAlert>> alerts = (await (select(accountAlerts)
          ..where((final tbl) => tbl.uuid.isIn(uuidList))).get())
        .fold({}, (final value, final element) {
          value.putIfAbsent(element.uuid, () => []).add(element);
          return value;
        });

    final Map<String, List<AccountIdentifier>> identifers = (await (select(
          accountIdentifiers,
        )..where((final tbl) => tbl.uuid.isIn(uuidList))).get())
        .fold({}, (final value, final element) {
          value.putIfAbsent(element.uuid, () => []).add(element);
          return value;
        });

    for (final accnt in accountList) {
      final uuid = accnt.uuid;
      accnt
        ..alerts = alerts[uuid] ?? []
        ..identifiers = identifers[uuid] ?? [];
    }

    return accountList;
  }

  Future<int> deleteAccount(final Account accnt) async {
    final deleteDate = DateTime.now();
    final deleteDateValue = Value(deleteDate);
    accnt
      ..deleteDate = deleteDate
      ..updateDate = deleteDate;
    final txQuery = update(transactions)
      ..where((final tbl) => tbl.accountUuid.equals(accnt.uuid));
    await update(accounts).replace(accnt);
    return txQuery.write(
      TransactionsCompanion(
        updateDate: deleteDateValue,
        deleteDate: deleteDateValue,
      ),
    );
  }

  Future<int> deleteOrganization(final Organization org) async {
    final deleteDate = DateTime.now();
    final deleteDateValue = Value(deleteDate);
    org
      ..deleteDate = deleteDate
      ..updateDate = deleteDate;
    await update(organizations).replace(org);
    final accntQuery = update(accounts)
      ..where((final tbl) => tbl.orgUuid.equals(org.uuid));
    await accntQuery.write(
      AccountsCompanion(
        updateDate: deleteDateValue,
        deleteDate: deleteDateValue,
      ),
    );
    final accntIds =
        selectOnly(accounts)
          ..addColumns([accounts.uuid])
          ..where(accounts.orgUuid.equals(org.uuid));
    final txQuery = update(transactions)
      ..where((final tbl) => tbl.accountUuid.isInQuery(accntIds));
    return txQuery.write(
      TransactionsCompanion(
        updateDate: deleteDateValue,
        deleteDate: deleteDateValue,
      ),
    );
  }

  Future<Account> saveDbAccount(
    final Account account,
    final bool update,
  ) async {
    final uuid = account.uuid;
    if (update) {
      final query = db.update(db.organizations)
        ..whereSamePrimaryKey(account.organization);
      await query.write(
        OrganizationsCompanion(updateDate: Value(DateTime.now())),
      );
    }
    await db.commonDao.deleteChildDbByUuid(db.accountIdentifiers, uuid);
    await db.commonDao.deleteChildDbByUuid(db.accountAlerts, uuid);

    final newAccount = await db.commonDao.saveDbItem(account);
    db.commonDao.setIdItemList(account.alerts, newAccount.uuid);
    db.commonDao.setIdItemList(account.identifiers, newAccount.uuid);
    newAccount
      ..alerts = await db.commonDao.saveChildList(account.alerts)
      ..identifiers = await db.commonDao.saveChildList(account.identifiers);

    return newAccount;
  }
}
