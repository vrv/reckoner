import 'package:drift/drift.dart';
import 'package:flutter/material.dart';
import 'package:timezone/timezone.dart' as tz;

import '../../model/category.dart';
import '../../model/model.dart';
import '../../model/report.dart';
import '../../model/settings.dart';
import '../reckoner_db.dart';
import '../tables/all_tables.dart';

part 'settings_dao.g.dart';

@DriftAccessor(tables: [UserSettings, CategoryGroups, ReportGroups])
class SettingsDao extends DatabaseAccessor<ReckonerDb> with _$SettingsDaoMixin {
  SettingsDao(super.db);

  Future<UserSetting> saveDbUserSettings(final UserSetting settings) async {
    return transaction(() async {
      final existingSettings = await select(userSettings).getSingleOrNull();
      final newSettings = await db.commonDao.saveDbItem(settings);

      if (newSettings.timezone != tz.local.name) {
        tz.setLocalLocation(tz.getLocation(newSettings.timezone));
      }

      if (existingSettings != null &&
          newSettings.uuid != existingSettings.uuid) {
        await delete(userSettings).delete(existingSettings);
      }

      return newSettings;
    });
  }

  Future<UserSetting> handleUserSettingUpdate() async {
    final settings = await select(userSettings).getSingle();
    final catGroups =
        await (select(categoryGroups)
          ..where((final tbl) => tbl.deleteDate.isNull())).get();
    final rptGroups =
        await (select(reportGroups)
          ..where((final tbl) => tbl.deleteDate.isNull())).get();
    if (_handleUpdate(settings, catGroups) ||
        _handleUpdate(settings, rptGroups)) {
      return db.commonDao.saveDbItem(settings);
    }
    return settings;
  }

  bool _handleUpdate<T extends Named<T>>(
    final UserSetting settings,
    final Iterable<T> items,
  ) {
    final navType = switch (T) {
      const (CategoryGroup) => NavigationEntryType.category,
      const (ReportGroup) => NavigationEntryType.report,
      _ => null,
    };
    if (navType == null) return false;
    bool updated = false;
    final navSubList =
        settings.navList
            .where((final element) => element.type == navType)
            .toList();
    for (final item in items) {
      final nav = _itemToDefaultEntry(item);
      final index = navSubList.indexWhere((final n) => n.uuid == nav?.uuid);
      if (nav != null && index < 0) {
        updated = true;
        settings.navList.add(nav);
      } else {
        navSubList.removeAt(index);
      }
    }
    for (final nav in navSubList) {
      updated = true;
      settings.navList.remove(nav);
    }
    return updated;
  }

  NavigationEntry? _itemToDefaultEntry<T extends Named<T>>(final T item) =>
      switch (T) {
        const (CategoryGroup) => NavigationEntry(
          type: NavigationEntryType.category,
          name: item.name,
          uuid: item.uuid,
          icon: Icons.category,
        ),
        const (ReportGroup) => NavigationEntry(
          type: NavigationEntryType.report,
          name: item.name,
          uuid: item.uuid,
          icon: Icons.stacked_bar_chart,
        ),
        _ => null,
      };

  Future<UserSetting> _handleResult(final List<UserSetting> settings) async {
    if (settings.isEmpty) {
      return into(userSettings).insertReturning(UserSetting());
    } else if (settings.length > 1) {
      final val = settings.removeLast();
      await transaction(() async {
        for (final set in settings) {
          await delete(userSettings).delete(set);
        }
      });
      return val;
    } else {
      return settings.first;
    }
  }

  Future<UserSetting> getUserSetting() async =>
      _handleResult(await select(userSettings).get());

  Stream<UserSetting> loadUserSetting() => select(
    userSettings,
  ).watch().asyncMap((final event) async => _handleResult(event));
}
