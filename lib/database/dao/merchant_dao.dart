// ignore_for_file: deprecated_member_use_from_same_package

import 'package:drift/drift.dart';

import '../../../model/merchant.dart';
import '../../model/transaction.dart';
import '../reckoner_db.dart';
import '../tables/merchant_tables.dart';
import '../tables/transaction_tables.dart';

part 'merchant_dao.g.dart';

@DriftAccessor(tables: [Merchants, Locations, Transactions])
class MerchantDao extends DatabaseAccessor<ReckonerDb> with _$MerchantDaoMixin {
  MerchantDao(super.db);

  JoinedSelectStatement<HasResultSet, dynamic> _query(final bool loadDeleted) {
    final query = select(merchants).join([
      leftOuterJoin(
        locations,
        locations.uuid.equalsExp(merchants.locationUuid),
      ),
    ]);
    if (!loadDeleted) query.where(merchants.deleteDate.isNull());
    return query;
  }

  Merchant _mapResult(final TypedResult row) =>
      row.readTable(merchants)
        ..location = row.readTableOrNull(locations) ?? Location();

  Stream<List<Merchant>> loadMerchants() => _query(false).watch().map(
    (final rows) => rows.map((final row) => _mapResult(row)).toList(),
  );

  Future<List<Merchant>> getSyncMerchants() async =>
      (await _query(true).get()).map((final row) => _mapResult(row)).toList();

  Future<Merchant?> saveDbMerchant(final Merchant? merchant) async {
    if (merchant == null) return null;
    if (merchant.name.isEmpty) return null;
    final location = merchant.location;
    if (location.hasData) {
      if (location.uuid.isEmpty) location.uuid = generateUuid();
      merchant.location = await db.commonDao.saveDbItem(location);
    }
    return db.commonDao.saveDbItem(merchant);
  }

  Future<void> setSyncId(final Iterable<String> ids, final String syncId) =>
      (update(merchants)..where(
        (final tbl) => tbl.uuid.isIn(ids),
      )).write(MerchantsCompanion(syncId: Value(syncId)));
}
