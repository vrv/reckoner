import 'package:drift/drift.dart';

import '../reckoner_db.dart';
import 'schema_versions.dart';

OnUpgrade migrator(final ReckonerDb db) {
  return (final m, final from, final to) async {
    await _upgradeSteps(m, from, to);
    // We do this on all upgrades to make sure user settings are sent to
    // the synchronization service. The update bundle for this contains
    // the database version.
    await db
        .update(db.userSettings)
        .write(UserSettingsCompanion(updateDate: Value(DateTime.now())));
  };
}

OnUpgrade _upgradeSteps = stepByStep(
  from1To2: (final m, final schema) async {
    await m.alterTable(
      TableMigration(
        schema.transactions,
        newColumns: [
          schema.transactions.merchantName,
          schema.transactions.locationUuid,
        ],
      ),
    );
    await m.database.customStatement(
      '''
UPDATE transactions
SET merchant_name = m.name, location_uuid = m.location_uuid, update_date = ?
FROM merchants AS m
WHERE m.uuid = merchant_uuid''',
      [DateTime.now().millisecondsSinceEpoch ~/ 1000],
    );
    await m.createIndex(
      Index(
        'transaction_merchant',
        'CREATE INDEX transaction_merchant ON transactions(merchant_name)',
      ),
    );
  },
);
