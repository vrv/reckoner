import 'package:drift/drift.dart';

import '../../../model/report.dart';
import 'model_tables.dart';

@UseRowClass(Report)
class Reports extends NamedTable {
  TextColumn get type => textEnum<ReportType>()();
  DateTimeColumn get startDate => dateTime().nullable()();
  DateTimeColumn get endDate => dateTime().nullable()();
  IntColumn get relativeStartOffset => integer().nullable()();
  IntColumn get relativeEndOffset => integer().nullable()();
  TextColumn get relativeRange => textEnum<ReportTimeRange>().nullable()();
  BoolColumn get loadOverTime => boolean()();
  TextColumn get grouping => textEnum<TransactionReportGrouping>()();
  BoolColumn get invert => boolean()();
  TextColumn get sorting => textEnum<ReportDataSorting>()();
  TextColumn get reportText => text().nullable()();
  TextColumn get filters => text()();
}

@UseRowClass(ReportGroup)
class ReportGroups extends NamedTable {
  IntColumn get cellSize => integer()();
}

@UseRowClass(ReportGroupLayout)
class ReportGroupLayouts extends Table implements UuidTable {
  @override
  TextColumn get uuid =>
      text().references(ReportGroups, #uuid, onDelete: KeyAction.cascade)();
  IntColumn get order => integer()();

  @override
  Set<Column>? get primaryKey => {uuid, order};
}

@UseRowClass(ReportGroupRow)
class ReportGroupRows extends Table implements UuidTable {
  @override
  TextColumn get uuid => text()();
  IntColumn get layoutOrder => integer()();
  IntColumn get order => integer()();
  @ReferenceName('primaryReportUuid')
  TextColumn get primaryReport =>
      text().nullable().references(
        Reports,
        #uuid,
        onDelete: KeyAction.setNull,
      )();
  @ReferenceName('secondaryReportUuid')
  TextColumn get secondaryReport =>
      text().nullable().references(
        Reports,
        #uuid,
        onDelete: KeyAction.setNull,
      )();
  @ReferenceName('tertiaryReportUuid')
  TextColumn get tertiaryReport =>
      text().nullable().references(
        Reports,
        #uuid,
        onDelete: KeyAction.setNull,
      )();

  @override
  Set<Column>? get primaryKey => {uuid, layoutOrder, order};
}
