import 'package:drift/drift.dart';

import '../../../model/currency.dart';
import 'model_tables.dart';

@UseRowClass(Currency)
class Currencies extends NamedTable {
  TextColumn get code => text().withLength(min: 1, max: 3)();
  TextColumn get symbol => text().withLength(min: 1, max: 4)();
  IntColumn get decimalDigits => integer().withDefault(const Constant(2))();

  @override
  Set<Column>? get primaryKey => {code};
}
