import 'package:drift/drift.dart';

import '../../../model/device.dart';
import 'model_tables.dart';

@UseRowClass(Device)
class Devices extends NamedTable {
  DateTimeColumn get syncDate => dateTime().nullable()();
  BoolColumn get thisDevice => boolean().withDefault(const Constant(false))();
}
