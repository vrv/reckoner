import 'package:drift/drift.dart';

import '../../../model/transaction.dart';
import 'account_tables.dart';
import 'category_tables.dart';
import 'model_tables.dart';

@UseRowClass(Location)
class Locations extends Table with NamedTableMixin {
  TextColumn get address => text()();
  TextColumn get city => text()();
  TextColumn get region => text()();
  TextColumn get zip => text()();
  TextColumn get uuid => text()();
  RealColumn get latitude => real().nullable()();
  RealColumn get longitude => real().nullable()();

  @override
  Set<Column>? get primaryKey => {uuid};
}

@UseRowClass(TransactionTransfer)
class TransactionTransfers extends Table {
  @ReferenceName('sourceTxUuid')
  TextColumn get sourceUuid =>
      text().references(Transactions, #uuid, onDelete: KeyAction.cascade)();
  @ReferenceName('destinationTxUuid')
  TextColumn get destinationUuid =>
      text().references(Transactions, #uuid, onDelete: KeyAction.cascade)();

  @override
  Set<Column<Object>>? get primaryKey => {sourceUuid, destinationUuid};
}

@TableIndex(name: 'transaction_date', columns: {#date})
@TableIndex(name: 'transaction_merchant', columns: {#merchantName})
@UseRowClass(Transaction)
class Transactions extends NamedTable with SyncIdTableMixin {
  DateTimeColumn get date => dateTime()();
  IntColumn get amount => integer()();
  TextColumn get accountUuid =>
      text().references(Accounts, #uuid, onDelete: KeyAction.cascade)();
  TextColumn get currencyCode => text().withLength(min: 3, max: 3)();
  TextColumn get merchantUuid => text().nullable()();
  TextColumn get merchantName => text().withLength(min: 1).nullable()();
  TextColumn get locationUuid => text().nullable()();
}

@UseRowClass(TransactionSplit)
class TransactionSplits extends Table
    with IdTableMixin, NamedTableMixin
    implements UuidTable {
  @override
  TextColumn get uuid =>
      text().references(Transactions, #uuid, onDelete: KeyAction.cascade)();
  IntColumn get amount => integer()();

  @override
  Set<Column<Object>>? get primaryKey => {uuid, id};
}

@UseRowClass(TransactionCategory)
class TransactionCategories extends Table implements UuidTable {
  @override
  TextColumn get uuid => text()();
  IntColumn get splitId => integer()();
  TextColumn get categoryUuid =>
      text().references(Categories, #uuid, onDelete: KeyAction.cascade)();

  @override
  Set<Column>? get primaryKey => {uuid, splitId, categoryUuid};

  @override
  List<String> get customConstraints => [
    'FOREIGN KEY (uuid, split_id) REFERENCES transaction_splits(uuid, id) ON DELETE CASCADE ON UPDATE CASCADE',
  ];
}

@UseRowClass(TransactionTag)
class TransactionTags extends Table implements UuidTable {
  @override
  TextColumn get uuid => text()();
  IntColumn get splitId => integer()();
  TextColumn get tag => text().withLength(min: 1)();

  @override
  Set<Column>? get primaryKey => {uuid, splitId, tag};

  @override
  List<String> get customConstraints => [
    'FOREIGN KEY (uuid, split_id) REFERENCES transaction_splits(uuid, id) ON DELETE CASCADE ON UPDATE CASCADE',
  ];
}
