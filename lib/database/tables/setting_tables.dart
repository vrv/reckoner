import 'package:drift/drift.dart';

import '../../../model/settings.dart';
import 'model_tables.dart';
import 'report_tables.dart';

@UseRowClass(UserSetting)
class UserSettings extends TrackedTable {
  TextColumn get navListJson => text()();
  TextColumn get defaultCurrencyCode =>
      text().nullable().withLength(min: 3, max: 3)();
  @ReferenceName('transactionReportUuidRef')
  TextColumn get transactionReportUuid =>
      text().nullable().references(
        Reports,
        #uuid,
        onDelete: KeyAction.setNull,
      )();
  @ReferenceName('accountReportUuidRef')
  TextColumn get accountReportUuid =>
      text().nullable().references(
        Reports,
        #uuid,
        onDelete: KeyAction.setNull,
      )();
  @ReferenceName('merchantReportUuidRef')
  TextColumn get merchantReportUuid =>
      text().nullable().references(
        Reports,
        #uuid,
        onDelete: KeyAction.setNull,
      )();
  @ReferenceName('tagReportUuidRef')
  TextColumn get tagReportUuid =>
      text().nullable().references(
        Reports,
        #uuid,
        onDelete: KeyAction.setNull,
      )();
}
