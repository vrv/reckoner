import 'package:flutter/material.dart';
import 'package:oktoast/oktoast.dart';

import 'config/init.dart';
import 'view/reckoner.dart';

void main() async {
  await appInit();
  runApp(const OKToast(child: Reckoner()));
  await appOnClose();
}
