// GENERATED CODE, DO NOT EDIT BY HAND.
// ignore_for_file: type=lint
//@dart=2.12
import 'package:drift/drift.dart';

class Organizations extends Table
    with TableInfo<Organizations, OrganizationsData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  Organizations(this.attachedDatabase, [this._alias]);
  late final GeneratedColumn<DateTime> createDate = GeneratedColumn<DateTime>(
      'create_date', aliasedName, false,
      type: DriftSqlType.dateTime,
      requiredDuringInsert: false,
      defaultValue: currentDateAndTime);
  late final GeneratedColumn<DateTime> updateDate = GeneratedColumn<DateTime>(
      'update_date', aliasedName, false,
      type: DriftSqlType.dateTime,
      requiredDuringInsert: false,
      defaultValue: currentDateAndTime);
  late final GeneratedColumn<DateTime> deleteDate = GeneratedColumn<DateTime>(
      'delete_date', aliasedName, true,
      type: DriftSqlType.dateTime, requiredDuringInsert: false);
  late final GeneratedColumn<String> uuid = GeneratedColumn<String>(
      'uuid', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  late final GeneratedColumn<String> name =
      GeneratedColumn<String>('name', aliasedName, false,
          additionalChecks: GeneratedColumn.checkTextLength(
            minTextLength: 1,
          ),
          type: DriftSqlType.string,
          requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns =>
      [createDate, updateDate, deleteDate, uuid, name];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'organizations';
  @override
  Set<GeneratedColumn> get $primaryKey => {uuid};
  @override
  OrganizationsData map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return OrganizationsData(
      createDate: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}create_date'])!,
      updateDate: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}update_date'])!,
      deleteDate: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}delete_date']),
      uuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}uuid'])!,
      name: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}name'])!,
    );
  }

  @override
  Organizations createAlias(String alias) {
    return Organizations(attachedDatabase, alias);
  }
}

class OrganizationsData extends DataClass
    implements Insertable<OrganizationsData> {
  final DateTime createDate;
  final DateTime updateDate;
  final DateTime? deleteDate;
  final String uuid;
  final String name;
  const OrganizationsData(
      {required this.createDate,
      required this.updateDate,
      this.deleteDate,
      required this.uuid,
      required this.name});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['create_date'] = Variable<DateTime>(createDate);
    map['update_date'] = Variable<DateTime>(updateDate);
    if (!nullToAbsent || deleteDate != null) {
      map['delete_date'] = Variable<DateTime>(deleteDate);
    }
    map['uuid'] = Variable<String>(uuid);
    map['name'] = Variable<String>(name);
    return map;
  }

  OrganizationsCompanion toCompanion(bool nullToAbsent) {
    return OrganizationsCompanion(
      createDate: Value(createDate),
      updateDate: Value(updateDate),
      deleteDate: deleteDate == null && nullToAbsent
          ? const Value.absent()
          : Value(deleteDate),
      uuid: Value(uuid),
      name: Value(name),
    );
  }

  factory OrganizationsData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return OrganizationsData(
      createDate: serializer.fromJson<DateTime>(json['createDate']),
      updateDate: serializer.fromJson<DateTime>(json['updateDate']),
      deleteDate: serializer.fromJson<DateTime?>(json['deleteDate']),
      uuid: serializer.fromJson<String>(json['uuid']),
      name: serializer.fromJson<String>(json['name']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'createDate': serializer.toJson<DateTime>(createDate),
      'updateDate': serializer.toJson<DateTime>(updateDate),
      'deleteDate': serializer.toJson<DateTime?>(deleteDate),
      'uuid': serializer.toJson<String>(uuid),
      'name': serializer.toJson<String>(name),
    };
  }

  OrganizationsData copyWith(
          {DateTime? createDate,
          DateTime? updateDate,
          Value<DateTime?> deleteDate = const Value.absent(),
          String? uuid,
          String? name}) =>
      OrganizationsData(
        createDate: createDate ?? this.createDate,
        updateDate: updateDate ?? this.updateDate,
        deleteDate: deleteDate.present ? deleteDate.value : this.deleteDate,
        uuid: uuid ?? this.uuid,
        name: name ?? this.name,
      );
  OrganizationsData copyWithCompanion(OrganizationsCompanion data) {
    return OrganizationsData(
      createDate:
          data.createDate.present ? data.createDate.value : this.createDate,
      updateDate:
          data.updateDate.present ? data.updateDate.value : this.updateDate,
      deleteDate:
          data.deleteDate.present ? data.deleteDate.value : this.deleteDate,
      uuid: data.uuid.present ? data.uuid.value : this.uuid,
      name: data.name.present ? data.name.value : this.name,
    );
  }

  @override
  String toString() {
    return (StringBuffer('OrganizationsData(')
          ..write('createDate: $createDate, ')
          ..write('updateDate: $updateDate, ')
          ..write('deleteDate: $deleteDate, ')
          ..write('uuid: $uuid, ')
          ..write('name: $name')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode =>
      Object.hash(createDate, updateDate, deleteDate, uuid, name);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is OrganizationsData &&
          other.createDate == this.createDate &&
          other.updateDate == this.updateDate &&
          other.deleteDate == this.deleteDate &&
          other.uuid == this.uuid &&
          other.name == this.name);
}

class OrganizationsCompanion extends UpdateCompanion<OrganizationsData> {
  final Value<DateTime> createDate;
  final Value<DateTime> updateDate;
  final Value<DateTime?> deleteDate;
  final Value<String> uuid;
  final Value<String> name;
  final Value<int> rowid;
  const OrganizationsCompanion({
    this.createDate = const Value.absent(),
    this.updateDate = const Value.absent(),
    this.deleteDate = const Value.absent(),
    this.uuid = const Value.absent(),
    this.name = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  OrganizationsCompanion.insert({
    this.createDate = const Value.absent(),
    this.updateDate = const Value.absent(),
    this.deleteDate = const Value.absent(),
    required String uuid,
    required String name,
    this.rowid = const Value.absent(),
  })  : uuid = Value(uuid),
        name = Value(name);
  static Insertable<OrganizationsData> custom({
    Expression<DateTime>? createDate,
    Expression<DateTime>? updateDate,
    Expression<DateTime>? deleteDate,
    Expression<String>? uuid,
    Expression<String>? name,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (createDate != null) 'create_date': createDate,
      if (updateDate != null) 'update_date': updateDate,
      if (deleteDate != null) 'delete_date': deleteDate,
      if (uuid != null) 'uuid': uuid,
      if (name != null) 'name': name,
      if (rowid != null) 'rowid': rowid,
    });
  }

  OrganizationsCompanion copyWith(
      {Value<DateTime>? createDate,
      Value<DateTime>? updateDate,
      Value<DateTime?>? deleteDate,
      Value<String>? uuid,
      Value<String>? name,
      Value<int>? rowid}) {
    return OrganizationsCompanion(
      createDate: createDate ?? this.createDate,
      updateDate: updateDate ?? this.updateDate,
      deleteDate: deleteDate ?? this.deleteDate,
      uuid: uuid ?? this.uuid,
      name: name ?? this.name,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (createDate.present) {
      map['create_date'] = Variable<DateTime>(createDate.value);
    }
    if (updateDate.present) {
      map['update_date'] = Variable<DateTime>(updateDate.value);
    }
    if (deleteDate.present) {
      map['delete_date'] = Variable<DateTime>(deleteDate.value);
    }
    if (uuid.present) {
      map['uuid'] = Variable<String>(uuid.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('OrganizationsCompanion(')
          ..write('createDate: $createDate, ')
          ..write('updateDate: $updateDate, ')
          ..write('deleteDate: $deleteDate, ')
          ..write('uuid: $uuid, ')
          ..write('name: $name, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class Accounts extends Table with TableInfo<Accounts, AccountsData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  Accounts(this.attachedDatabase, [this._alias]);
  late final GeneratedColumn<DateTime> createDate = GeneratedColumn<DateTime>(
      'create_date', aliasedName, false,
      type: DriftSqlType.dateTime,
      requiredDuringInsert: false,
      defaultValue: currentDateAndTime);
  late final GeneratedColumn<DateTime> updateDate = GeneratedColumn<DateTime>(
      'update_date', aliasedName, false,
      type: DriftSqlType.dateTime,
      requiredDuringInsert: false,
      defaultValue: currentDateAndTime);
  late final GeneratedColumn<DateTime> deleteDate = GeneratedColumn<DateTime>(
      'delete_date', aliasedName, true,
      type: DriftSqlType.dateTime, requiredDuringInsert: false);
  late final GeneratedColumn<String> uuid = GeneratedColumn<String>(
      'uuid', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  late final GeneratedColumn<String> name =
      GeneratedColumn<String>('name', aliasedName, false,
          additionalChecks: GeneratedColumn.checkTextLength(
            minTextLength: 1,
          ),
          type: DriftSqlType.string,
          requiredDuringInsert: true);
  late final GeneratedColumn<String> orgUuid = GeneratedColumn<String>(
      'org_uuid', aliasedName, false,
      type: DriftSqlType.string,
      requiredDuringInsert: true,
      defaultConstraints: GeneratedColumn.constraintIsAlways(
          'REFERENCES organizations (uuid) ON DELETE CASCADE'));
  late final GeneratedColumn<String> defaultCurrencyCode =
      GeneratedColumn<String>('default_currency_code', aliasedName, true,
          additionalChecks: GeneratedColumn.checkTextLength(
              minTextLength: 3, maxTextLength: 3),
          type: DriftSqlType.string,
          requiredDuringInsert: false);
  late final GeneratedColumn<bool> isMultiCurrency = GeneratedColumn<bool>(
      'is_multi_currency', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: false,
      defaultConstraints: GeneratedColumn.constraintIsAlways(
          'CHECK ("is_multi_currency" IN (0, 1))'),
      defaultValue: const Constant(false));
  @override
  List<GeneratedColumn> get $columns => [
        createDate,
        updateDate,
        deleteDate,
        uuid,
        name,
        orgUuid,
        defaultCurrencyCode,
        isMultiCurrency
      ];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'accounts';
  @override
  Set<GeneratedColumn> get $primaryKey => {uuid};
  @override
  AccountsData map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return AccountsData(
      createDate: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}create_date'])!,
      updateDate: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}update_date'])!,
      deleteDate: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}delete_date']),
      uuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}uuid'])!,
      name: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}name'])!,
      orgUuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}org_uuid'])!,
      defaultCurrencyCode: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}default_currency_code']),
      isMultiCurrency: attachedDatabase.typeMapping.read(
          DriftSqlType.bool, data['${effectivePrefix}is_multi_currency'])!,
    );
  }

  @override
  Accounts createAlias(String alias) {
    return Accounts(attachedDatabase, alias);
  }
}

class AccountsData extends DataClass implements Insertable<AccountsData> {
  final DateTime createDate;
  final DateTime updateDate;
  final DateTime? deleteDate;
  final String uuid;
  final String name;
  final String orgUuid;
  final String? defaultCurrencyCode;
  final bool isMultiCurrency;
  const AccountsData(
      {required this.createDate,
      required this.updateDate,
      this.deleteDate,
      required this.uuid,
      required this.name,
      required this.orgUuid,
      this.defaultCurrencyCode,
      required this.isMultiCurrency});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['create_date'] = Variable<DateTime>(createDate);
    map['update_date'] = Variable<DateTime>(updateDate);
    if (!nullToAbsent || deleteDate != null) {
      map['delete_date'] = Variable<DateTime>(deleteDate);
    }
    map['uuid'] = Variable<String>(uuid);
    map['name'] = Variable<String>(name);
    map['org_uuid'] = Variable<String>(orgUuid);
    if (!nullToAbsent || defaultCurrencyCode != null) {
      map['default_currency_code'] = Variable<String>(defaultCurrencyCode);
    }
    map['is_multi_currency'] = Variable<bool>(isMultiCurrency);
    return map;
  }

  AccountsCompanion toCompanion(bool nullToAbsent) {
    return AccountsCompanion(
      createDate: Value(createDate),
      updateDate: Value(updateDate),
      deleteDate: deleteDate == null && nullToAbsent
          ? const Value.absent()
          : Value(deleteDate),
      uuid: Value(uuid),
      name: Value(name),
      orgUuid: Value(orgUuid),
      defaultCurrencyCode: defaultCurrencyCode == null && nullToAbsent
          ? const Value.absent()
          : Value(defaultCurrencyCode),
      isMultiCurrency: Value(isMultiCurrency),
    );
  }

  factory AccountsData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return AccountsData(
      createDate: serializer.fromJson<DateTime>(json['createDate']),
      updateDate: serializer.fromJson<DateTime>(json['updateDate']),
      deleteDate: serializer.fromJson<DateTime?>(json['deleteDate']),
      uuid: serializer.fromJson<String>(json['uuid']),
      name: serializer.fromJson<String>(json['name']),
      orgUuid: serializer.fromJson<String>(json['orgUuid']),
      defaultCurrencyCode:
          serializer.fromJson<String?>(json['defaultCurrencyCode']),
      isMultiCurrency: serializer.fromJson<bool>(json['isMultiCurrency']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'createDate': serializer.toJson<DateTime>(createDate),
      'updateDate': serializer.toJson<DateTime>(updateDate),
      'deleteDate': serializer.toJson<DateTime?>(deleteDate),
      'uuid': serializer.toJson<String>(uuid),
      'name': serializer.toJson<String>(name),
      'orgUuid': serializer.toJson<String>(orgUuid),
      'defaultCurrencyCode': serializer.toJson<String?>(defaultCurrencyCode),
      'isMultiCurrency': serializer.toJson<bool>(isMultiCurrency),
    };
  }

  AccountsData copyWith(
          {DateTime? createDate,
          DateTime? updateDate,
          Value<DateTime?> deleteDate = const Value.absent(),
          String? uuid,
          String? name,
          String? orgUuid,
          Value<String?> defaultCurrencyCode = const Value.absent(),
          bool? isMultiCurrency}) =>
      AccountsData(
        createDate: createDate ?? this.createDate,
        updateDate: updateDate ?? this.updateDate,
        deleteDate: deleteDate.present ? deleteDate.value : this.deleteDate,
        uuid: uuid ?? this.uuid,
        name: name ?? this.name,
        orgUuid: orgUuid ?? this.orgUuid,
        defaultCurrencyCode: defaultCurrencyCode.present
            ? defaultCurrencyCode.value
            : this.defaultCurrencyCode,
        isMultiCurrency: isMultiCurrency ?? this.isMultiCurrency,
      );
  AccountsData copyWithCompanion(AccountsCompanion data) {
    return AccountsData(
      createDate:
          data.createDate.present ? data.createDate.value : this.createDate,
      updateDate:
          data.updateDate.present ? data.updateDate.value : this.updateDate,
      deleteDate:
          data.deleteDate.present ? data.deleteDate.value : this.deleteDate,
      uuid: data.uuid.present ? data.uuid.value : this.uuid,
      name: data.name.present ? data.name.value : this.name,
      orgUuid: data.orgUuid.present ? data.orgUuid.value : this.orgUuid,
      defaultCurrencyCode: data.defaultCurrencyCode.present
          ? data.defaultCurrencyCode.value
          : this.defaultCurrencyCode,
      isMultiCurrency: data.isMultiCurrency.present
          ? data.isMultiCurrency.value
          : this.isMultiCurrency,
    );
  }

  @override
  String toString() {
    return (StringBuffer('AccountsData(')
          ..write('createDate: $createDate, ')
          ..write('updateDate: $updateDate, ')
          ..write('deleteDate: $deleteDate, ')
          ..write('uuid: $uuid, ')
          ..write('name: $name, ')
          ..write('orgUuid: $orgUuid, ')
          ..write('defaultCurrencyCode: $defaultCurrencyCode, ')
          ..write('isMultiCurrency: $isMultiCurrency')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(createDate, updateDate, deleteDate, uuid,
      name, orgUuid, defaultCurrencyCode, isMultiCurrency);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is AccountsData &&
          other.createDate == this.createDate &&
          other.updateDate == this.updateDate &&
          other.deleteDate == this.deleteDate &&
          other.uuid == this.uuid &&
          other.name == this.name &&
          other.orgUuid == this.orgUuid &&
          other.defaultCurrencyCode == this.defaultCurrencyCode &&
          other.isMultiCurrency == this.isMultiCurrency);
}

class AccountsCompanion extends UpdateCompanion<AccountsData> {
  final Value<DateTime> createDate;
  final Value<DateTime> updateDate;
  final Value<DateTime?> deleteDate;
  final Value<String> uuid;
  final Value<String> name;
  final Value<String> orgUuid;
  final Value<String?> defaultCurrencyCode;
  final Value<bool> isMultiCurrency;
  final Value<int> rowid;
  const AccountsCompanion({
    this.createDate = const Value.absent(),
    this.updateDate = const Value.absent(),
    this.deleteDate = const Value.absent(),
    this.uuid = const Value.absent(),
    this.name = const Value.absent(),
    this.orgUuid = const Value.absent(),
    this.defaultCurrencyCode = const Value.absent(),
    this.isMultiCurrency = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  AccountsCompanion.insert({
    this.createDate = const Value.absent(),
    this.updateDate = const Value.absent(),
    this.deleteDate = const Value.absent(),
    required String uuid,
    required String name,
    required String orgUuid,
    this.defaultCurrencyCode = const Value.absent(),
    this.isMultiCurrency = const Value.absent(),
    this.rowid = const Value.absent(),
  })  : uuid = Value(uuid),
        name = Value(name),
        orgUuid = Value(orgUuid);
  static Insertable<AccountsData> custom({
    Expression<DateTime>? createDate,
    Expression<DateTime>? updateDate,
    Expression<DateTime>? deleteDate,
    Expression<String>? uuid,
    Expression<String>? name,
    Expression<String>? orgUuid,
    Expression<String>? defaultCurrencyCode,
    Expression<bool>? isMultiCurrency,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (createDate != null) 'create_date': createDate,
      if (updateDate != null) 'update_date': updateDate,
      if (deleteDate != null) 'delete_date': deleteDate,
      if (uuid != null) 'uuid': uuid,
      if (name != null) 'name': name,
      if (orgUuid != null) 'org_uuid': orgUuid,
      if (defaultCurrencyCode != null)
        'default_currency_code': defaultCurrencyCode,
      if (isMultiCurrency != null) 'is_multi_currency': isMultiCurrency,
      if (rowid != null) 'rowid': rowid,
    });
  }

  AccountsCompanion copyWith(
      {Value<DateTime>? createDate,
      Value<DateTime>? updateDate,
      Value<DateTime?>? deleteDate,
      Value<String>? uuid,
      Value<String>? name,
      Value<String>? orgUuid,
      Value<String?>? defaultCurrencyCode,
      Value<bool>? isMultiCurrency,
      Value<int>? rowid}) {
    return AccountsCompanion(
      createDate: createDate ?? this.createDate,
      updateDate: updateDate ?? this.updateDate,
      deleteDate: deleteDate ?? this.deleteDate,
      uuid: uuid ?? this.uuid,
      name: name ?? this.name,
      orgUuid: orgUuid ?? this.orgUuid,
      defaultCurrencyCode: defaultCurrencyCode ?? this.defaultCurrencyCode,
      isMultiCurrency: isMultiCurrency ?? this.isMultiCurrency,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (createDate.present) {
      map['create_date'] = Variable<DateTime>(createDate.value);
    }
    if (updateDate.present) {
      map['update_date'] = Variable<DateTime>(updateDate.value);
    }
    if (deleteDate.present) {
      map['delete_date'] = Variable<DateTime>(deleteDate.value);
    }
    if (uuid.present) {
      map['uuid'] = Variable<String>(uuid.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (orgUuid.present) {
      map['org_uuid'] = Variable<String>(orgUuid.value);
    }
    if (defaultCurrencyCode.present) {
      map['default_currency_code'] =
          Variable<String>(defaultCurrencyCode.value);
    }
    if (isMultiCurrency.present) {
      map['is_multi_currency'] = Variable<bool>(isMultiCurrency.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('AccountsCompanion(')
          ..write('createDate: $createDate, ')
          ..write('updateDate: $updateDate, ')
          ..write('deleteDate: $deleteDate, ')
          ..write('uuid: $uuid, ')
          ..write('name: $name, ')
          ..write('orgUuid: $orgUuid, ')
          ..write('defaultCurrencyCode: $defaultCurrencyCode, ')
          ..write('isMultiCurrency: $isMultiCurrency, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class AccountIdentifiers extends Table
    with TableInfo<AccountIdentifiers, AccountIdentifiersData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  AccountIdentifiers(this.attachedDatabase, [this._alias]);
  late final GeneratedColumn<String> uuid = GeneratedColumn<String>(
      'uuid', aliasedName, false,
      type: DriftSqlType.string,
      requiredDuringInsert: true,
      defaultConstraints: GeneratedColumn.constraintIsAlways(
          'REFERENCES accounts (uuid) ON DELETE CASCADE'));
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  late final GeneratedColumn<String> name =
      GeneratedColumn<String>('name', aliasedName, false,
          additionalChecks: GeneratedColumn.checkTextLength(
            minTextLength: 1,
          ),
          type: DriftSqlType.string,
          requiredDuringInsert: true);
  late final GeneratedColumn<String> identifier = GeneratedColumn<String>(
      'identifier', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns => [uuid, id, name, identifier];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'account_identifiers';
  @override
  Set<GeneratedColumn> get $primaryKey => {uuid, id};
  @override
  AccountIdentifiersData map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return AccountIdentifiersData(
      uuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}uuid'])!,
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      name: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}name'])!,
      identifier: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}identifier'])!,
    );
  }

  @override
  AccountIdentifiers createAlias(String alias) {
    return AccountIdentifiers(attachedDatabase, alias);
  }
}

class AccountIdentifiersData extends DataClass
    implements Insertable<AccountIdentifiersData> {
  final String uuid;
  final int id;
  final String name;
  final String identifier;
  const AccountIdentifiersData(
      {required this.uuid,
      required this.id,
      required this.name,
      required this.identifier});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['uuid'] = Variable<String>(uuid);
    map['id'] = Variable<int>(id);
    map['name'] = Variable<String>(name);
    map['identifier'] = Variable<String>(identifier);
    return map;
  }

  AccountIdentifiersCompanion toCompanion(bool nullToAbsent) {
    return AccountIdentifiersCompanion(
      uuid: Value(uuid),
      id: Value(id),
      name: Value(name),
      identifier: Value(identifier),
    );
  }

  factory AccountIdentifiersData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return AccountIdentifiersData(
      uuid: serializer.fromJson<String>(json['uuid']),
      id: serializer.fromJson<int>(json['id']),
      name: serializer.fromJson<String>(json['name']),
      identifier: serializer.fromJson<String>(json['identifier']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'uuid': serializer.toJson<String>(uuid),
      'id': serializer.toJson<int>(id),
      'name': serializer.toJson<String>(name),
      'identifier': serializer.toJson<String>(identifier),
    };
  }

  AccountIdentifiersData copyWith(
          {String? uuid, int? id, String? name, String? identifier}) =>
      AccountIdentifiersData(
        uuid: uuid ?? this.uuid,
        id: id ?? this.id,
        name: name ?? this.name,
        identifier: identifier ?? this.identifier,
      );
  AccountIdentifiersData copyWithCompanion(AccountIdentifiersCompanion data) {
    return AccountIdentifiersData(
      uuid: data.uuid.present ? data.uuid.value : this.uuid,
      id: data.id.present ? data.id.value : this.id,
      name: data.name.present ? data.name.value : this.name,
      identifier:
          data.identifier.present ? data.identifier.value : this.identifier,
    );
  }

  @override
  String toString() {
    return (StringBuffer('AccountIdentifiersData(')
          ..write('uuid: $uuid, ')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('identifier: $identifier')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(uuid, id, name, identifier);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is AccountIdentifiersData &&
          other.uuid == this.uuid &&
          other.id == this.id &&
          other.name == this.name &&
          other.identifier == this.identifier);
}

class AccountIdentifiersCompanion
    extends UpdateCompanion<AccountIdentifiersData> {
  final Value<String> uuid;
  final Value<int> id;
  final Value<String> name;
  final Value<String> identifier;
  final Value<int> rowid;
  const AccountIdentifiersCompanion({
    this.uuid = const Value.absent(),
    this.id = const Value.absent(),
    this.name = const Value.absent(),
    this.identifier = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  AccountIdentifiersCompanion.insert({
    required String uuid,
    required int id,
    required String name,
    required String identifier,
    this.rowid = const Value.absent(),
  })  : uuid = Value(uuid),
        id = Value(id),
        name = Value(name),
        identifier = Value(identifier);
  static Insertable<AccountIdentifiersData> custom({
    Expression<String>? uuid,
    Expression<int>? id,
    Expression<String>? name,
    Expression<String>? identifier,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (uuid != null) 'uuid': uuid,
      if (id != null) 'id': id,
      if (name != null) 'name': name,
      if (identifier != null) 'identifier': identifier,
      if (rowid != null) 'rowid': rowid,
    });
  }

  AccountIdentifiersCompanion copyWith(
      {Value<String>? uuid,
      Value<int>? id,
      Value<String>? name,
      Value<String>? identifier,
      Value<int>? rowid}) {
    return AccountIdentifiersCompanion(
      uuid: uuid ?? this.uuid,
      id: id ?? this.id,
      name: name ?? this.name,
      identifier: identifier ?? this.identifier,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (uuid.present) {
      map['uuid'] = Variable<String>(uuid.value);
    }
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (identifier.present) {
      map['identifier'] = Variable<String>(identifier.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('AccountIdentifiersCompanion(')
          ..write('uuid: $uuid, ')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('identifier: $identifier, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class AccountAlerts extends Table
    with TableInfo<AccountAlerts, AccountAlertsData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  AccountAlerts(this.attachedDatabase, [this._alias]);
  late final GeneratedColumn<String> uuid = GeneratedColumn<String>(
      'uuid', aliasedName, false,
      type: DriftSqlType.string,
      requiredDuringInsert: true,
      defaultConstraints: GeneratedColumn.constraintIsAlways(
          'REFERENCES accounts (uuid) ON DELETE CASCADE'));
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  late final GeneratedColumn<bool> isMax = GeneratedColumn<bool>(
      'is_max', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: true,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('CHECK ("is_max" IN (0, 1))'));
  late final GeneratedColumn<int> amount = GeneratedColumn<int>(
      'amount', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  late final GeneratedColumn<String> message = GeneratedColumn<String>(
      'message', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  late final GeneratedColumn<int> color = GeneratedColumn<int>(
      'color', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  late final GeneratedColumn<String> currencyCode = GeneratedColumn<String>(
      'currency_code', aliasedName, false,
      additionalChecks:
          GeneratedColumn.checkTextLength(minTextLength: 3, maxTextLength: 3),
      type: DriftSqlType.string,
      requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns =>
      [uuid, id, isMax, amount, message, color, currencyCode];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'account_alerts';
  @override
  Set<GeneratedColumn> get $primaryKey => {uuid, id};
  @override
  AccountAlertsData map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return AccountAlertsData(
      uuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}uuid'])!,
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      isMax: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}is_max'])!,
      amount: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}amount'])!,
      message: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}message'])!,
      color: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}color'])!,
      currencyCode: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}currency_code'])!,
    );
  }

  @override
  AccountAlerts createAlias(String alias) {
    return AccountAlerts(attachedDatabase, alias);
  }
}

class AccountAlertsData extends DataClass
    implements Insertable<AccountAlertsData> {
  final String uuid;
  final int id;
  final bool isMax;
  final int amount;
  final String message;
  final int color;
  final String currencyCode;
  const AccountAlertsData(
      {required this.uuid,
      required this.id,
      required this.isMax,
      required this.amount,
      required this.message,
      required this.color,
      required this.currencyCode});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['uuid'] = Variable<String>(uuid);
    map['id'] = Variable<int>(id);
    map['is_max'] = Variable<bool>(isMax);
    map['amount'] = Variable<int>(amount);
    map['message'] = Variable<String>(message);
    map['color'] = Variable<int>(color);
    map['currency_code'] = Variable<String>(currencyCode);
    return map;
  }

  AccountAlertsCompanion toCompanion(bool nullToAbsent) {
    return AccountAlertsCompanion(
      uuid: Value(uuid),
      id: Value(id),
      isMax: Value(isMax),
      amount: Value(amount),
      message: Value(message),
      color: Value(color),
      currencyCode: Value(currencyCode),
    );
  }

  factory AccountAlertsData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return AccountAlertsData(
      uuid: serializer.fromJson<String>(json['uuid']),
      id: serializer.fromJson<int>(json['id']),
      isMax: serializer.fromJson<bool>(json['isMax']),
      amount: serializer.fromJson<int>(json['amount']),
      message: serializer.fromJson<String>(json['message']),
      color: serializer.fromJson<int>(json['color']),
      currencyCode: serializer.fromJson<String>(json['currencyCode']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'uuid': serializer.toJson<String>(uuid),
      'id': serializer.toJson<int>(id),
      'isMax': serializer.toJson<bool>(isMax),
      'amount': serializer.toJson<int>(amount),
      'message': serializer.toJson<String>(message),
      'color': serializer.toJson<int>(color),
      'currencyCode': serializer.toJson<String>(currencyCode),
    };
  }

  AccountAlertsData copyWith(
          {String? uuid,
          int? id,
          bool? isMax,
          int? amount,
          String? message,
          int? color,
          String? currencyCode}) =>
      AccountAlertsData(
        uuid: uuid ?? this.uuid,
        id: id ?? this.id,
        isMax: isMax ?? this.isMax,
        amount: amount ?? this.amount,
        message: message ?? this.message,
        color: color ?? this.color,
        currencyCode: currencyCode ?? this.currencyCode,
      );
  AccountAlertsData copyWithCompanion(AccountAlertsCompanion data) {
    return AccountAlertsData(
      uuid: data.uuid.present ? data.uuid.value : this.uuid,
      id: data.id.present ? data.id.value : this.id,
      isMax: data.isMax.present ? data.isMax.value : this.isMax,
      amount: data.amount.present ? data.amount.value : this.amount,
      message: data.message.present ? data.message.value : this.message,
      color: data.color.present ? data.color.value : this.color,
      currencyCode: data.currencyCode.present
          ? data.currencyCode.value
          : this.currencyCode,
    );
  }

  @override
  String toString() {
    return (StringBuffer('AccountAlertsData(')
          ..write('uuid: $uuid, ')
          ..write('id: $id, ')
          ..write('isMax: $isMax, ')
          ..write('amount: $amount, ')
          ..write('message: $message, ')
          ..write('color: $color, ')
          ..write('currencyCode: $currencyCode')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode =>
      Object.hash(uuid, id, isMax, amount, message, color, currencyCode);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is AccountAlertsData &&
          other.uuid == this.uuid &&
          other.id == this.id &&
          other.isMax == this.isMax &&
          other.amount == this.amount &&
          other.message == this.message &&
          other.color == this.color &&
          other.currencyCode == this.currencyCode);
}

class AccountAlertsCompanion extends UpdateCompanion<AccountAlertsData> {
  final Value<String> uuid;
  final Value<int> id;
  final Value<bool> isMax;
  final Value<int> amount;
  final Value<String> message;
  final Value<int> color;
  final Value<String> currencyCode;
  final Value<int> rowid;
  const AccountAlertsCompanion({
    this.uuid = const Value.absent(),
    this.id = const Value.absent(),
    this.isMax = const Value.absent(),
    this.amount = const Value.absent(),
    this.message = const Value.absent(),
    this.color = const Value.absent(),
    this.currencyCode = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  AccountAlertsCompanion.insert({
    required String uuid,
    required int id,
    required bool isMax,
    required int amount,
    required String message,
    required int color,
    required String currencyCode,
    this.rowid = const Value.absent(),
  })  : uuid = Value(uuid),
        id = Value(id),
        isMax = Value(isMax),
        amount = Value(amount),
        message = Value(message),
        color = Value(color),
        currencyCode = Value(currencyCode);
  static Insertable<AccountAlertsData> custom({
    Expression<String>? uuid,
    Expression<int>? id,
    Expression<bool>? isMax,
    Expression<int>? amount,
    Expression<String>? message,
    Expression<int>? color,
    Expression<String>? currencyCode,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (uuid != null) 'uuid': uuid,
      if (id != null) 'id': id,
      if (isMax != null) 'is_max': isMax,
      if (amount != null) 'amount': amount,
      if (message != null) 'message': message,
      if (color != null) 'color': color,
      if (currencyCode != null) 'currency_code': currencyCode,
      if (rowid != null) 'rowid': rowid,
    });
  }

  AccountAlertsCompanion copyWith(
      {Value<String>? uuid,
      Value<int>? id,
      Value<bool>? isMax,
      Value<int>? amount,
      Value<String>? message,
      Value<int>? color,
      Value<String>? currencyCode,
      Value<int>? rowid}) {
    return AccountAlertsCompanion(
      uuid: uuid ?? this.uuid,
      id: id ?? this.id,
      isMax: isMax ?? this.isMax,
      amount: amount ?? this.amount,
      message: message ?? this.message,
      color: color ?? this.color,
      currencyCode: currencyCode ?? this.currencyCode,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (uuid.present) {
      map['uuid'] = Variable<String>(uuid.value);
    }
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (isMax.present) {
      map['is_max'] = Variable<bool>(isMax.value);
    }
    if (amount.present) {
      map['amount'] = Variable<int>(amount.value);
    }
    if (message.present) {
      map['message'] = Variable<String>(message.value);
    }
    if (color.present) {
      map['color'] = Variable<int>(color.value);
    }
    if (currencyCode.present) {
      map['currency_code'] = Variable<String>(currencyCode.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('AccountAlertsCompanion(')
          ..write('uuid: $uuid, ')
          ..write('id: $id, ')
          ..write('isMax: $isMax, ')
          ..write('amount: $amount, ')
          ..write('message: $message, ')
          ..write('color: $color, ')
          ..write('currencyCode: $currencyCode, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class Reports extends Table with TableInfo<Reports, ReportsData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  Reports(this.attachedDatabase, [this._alias]);
  late final GeneratedColumn<DateTime> createDate = GeneratedColumn<DateTime>(
      'create_date', aliasedName, false,
      type: DriftSqlType.dateTime,
      requiredDuringInsert: false,
      defaultValue: currentDateAndTime);
  late final GeneratedColumn<DateTime> updateDate = GeneratedColumn<DateTime>(
      'update_date', aliasedName, false,
      type: DriftSqlType.dateTime,
      requiredDuringInsert: false,
      defaultValue: currentDateAndTime);
  late final GeneratedColumn<DateTime> deleteDate = GeneratedColumn<DateTime>(
      'delete_date', aliasedName, true,
      type: DriftSqlType.dateTime, requiredDuringInsert: false);
  late final GeneratedColumn<String> uuid = GeneratedColumn<String>(
      'uuid', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  late final GeneratedColumn<String> name =
      GeneratedColumn<String>('name', aliasedName, false,
          additionalChecks: GeneratedColumn.checkTextLength(
            minTextLength: 1,
          ),
          type: DriftSqlType.string,
          requiredDuringInsert: true);
  late final GeneratedColumn<String> type = GeneratedColumn<String>(
      'type', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  late final GeneratedColumn<DateTime> startDate = GeneratedColumn<DateTime>(
      'start_date', aliasedName, true,
      type: DriftSqlType.dateTime, requiredDuringInsert: false);
  late final GeneratedColumn<DateTime> endDate = GeneratedColumn<DateTime>(
      'end_date', aliasedName, true,
      type: DriftSqlType.dateTime, requiredDuringInsert: false);
  late final GeneratedColumn<int> relativeStartOffset = GeneratedColumn<int>(
      'relative_start_offset', aliasedName, true,
      type: DriftSqlType.int, requiredDuringInsert: false);
  late final GeneratedColumn<int> relativeEndOffset = GeneratedColumn<int>(
      'relative_end_offset', aliasedName, true,
      type: DriftSqlType.int, requiredDuringInsert: false);
  late final GeneratedColumn<String> relativeRange = GeneratedColumn<String>(
      'relative_range', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  late final GeneratedColumn<bool> loadOverTime = GeneratedColumn<bool>(
      'load_over_time', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: true,
      defaultConstraints: GeneratedColumn.constraintIsAlways(
          'CHECK ("load_over_time" IN (0, 1))'));
  late final GeneratedColumn<String> grouping = GeneratedColumn<String>(
      'grouping', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  late final GeneratedColumn<bool> invert = GeneratedColumn<bool>(
      'invert', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: true,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('CHECK ("invert" IN (0, 1))'));
  late final GeneratedColumn<String> sorting = GeneratedColumn<String>(
      'sorting', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  late final GeneratedColumn<String> reportText = GeneratedColumn<String>(
      'report_text', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  late final GeneratedColumn<String> filters = GeneratedColumn<String>(
      'filters', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns => [
        createDate,
        updateDate,
        deleteDate,
        uuid,
        name,
        type,
        startDate,
        endDate,
        relativeStartOffset,
        relativeEndOffset,
        relativeRange,
        loadOverTime,
        grouping,
        invert,
        sorting,
        reportText,
        filters
      ];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'reports';
  @override
  Set<GeneratedColumn> get $primaryKey => {uuid};
  @override
  ReportsData map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return ReportsData(
      createDate: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}create_date'])!,
      updateDate: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}update_date'])!,
      deleteDate: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}delete_date']),
      uuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}uuid'])!,
      name: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}name'])!,
      type: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}type'])!,
      startDate: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}start_date']),
      endDate: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}end_date']),
      relativeStartOffset: attachedDatabase.typeMapping.read(
          DriftSqlType.int, data['${effectivePrefix}relative_start_offset']),
      relativeEndOffset: attachedDatabase.typeMapping.read(
          DriftSqlType.int, data['${effectivePrefix}relative_end_offset']),
      relativeRange: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}relative_range']),
      loadOverTime: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}load_over_time'])!,
      grouping: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}grouping'])!,
      invert: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}invert'])!,
      sorting: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}sorting'])!,
      reportText: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}report_text']),
      filters: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}filters'])!,
    );
  }

  @override
  Reports createAlias(String alias) {
    return Reports(attachedDatabase, alias);
  }
}

class ReportsData extends DataClass implements Insertable<ReportsData> {
  final DateTime createDate;
  final DateTime updateDate;
  final DateTime? deleteDate;
  final String uuid;
  final String name;
  final String type;
  final DateTime? startDate;
  final DateTime? endDate;
  final int? relativeStartOffset;
  final int? relativeEndOffset;
  final String? relativeRange;
  final bool loadOverTime;
  final String grouping;
  final bool invert;
  final String sorting;
  final String? reportText;
  final String filters;
  const ReportsData(
      {required this.createDate,
      required this.updateDate,
      this.deleteDate,
      required this.uuid,
      required this.name,
      required this.type,
      this.startDate,
      this.endDate,
      this.relativeStartOffset,
      this.relativeEndOffset,
      this.relativeRange,
      required this.loadOverTime,
      required this.grouping,
      required this.invert,
      required this.sorting,
      this.reportText,
      required this.filters});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['create_date'] = Variable<DateTime>(createDate);
    map['update_date'] = Variable<DateTime>(updateDate);
    if (!nullToAbsent || deleteDate != null) {
      map['delete_date'] = Variable<DateTime>(deleteDate);
    }
    map['uuid'] = Variable<String>(uuid);
    map['name'] = Variable<String>(name);
    map['type'] = Variable<String>(type);
    if (!nullToAbsent || startDate != null) {
      map['start_date'] = Variable<DateTime>(startDate);
    }
    if (!nullToAbsent || endDate != null) {
      map['end_date'] = Variable<DateTime>(endDate);
    }
    if (!nullToAbsent || relativeStartOffset != null) {
      map['relative_start_offset'] = Variable<int>(relativeStartOffset);
    }
    if (!nullToAbsent || relativeEndOffset != null) {
      map['relative_end_offset'] = Variable<int>(relativeEndOffset);
    }
    if (!nullToAbsent || relativeRange != null) {
      map['relative_range'] = Variable<String>(relativeRange);
    }
    map['load_over_time'] = Variable<bool>(loadOverTime);
    map['grouping'] = Variable<String>(grouping);
    map['invert'] = Variable<bool>(invert);
    map['sorting'] = Variable<String>(sorting);
    if (!nullToAbsent || reportText != null) {
      map['report_text'] = Variable<String>(reportText);
    }
    map['filters'] = Variable<String>(filters);
    return map;
  }

  ReportsCompanion toCompanion(bool nullToAbsent) {
    return ReportsCompanion(
      createDate: Value(createDate),
      updateDate: Value(updateDate),
      deleteDate: deleteDate == null && nullToAbsent
          ? const Value.absent()
          : Value(deleteDate),
      uuid: Value(uuid),
      name: Value(name),
      type: Value(type),
      startDate: startDate == null && nullToAbsent
          ? const Value.absent()
          : Value(startDate),
      endDate: endDate == null && nullToAbsent
          ? const Value.absent()
          : Value(endDate),
      relativeStartOffset: relativeStartOffset == null && nullToAbsent
          ? const Value.absent()
          : Value(relativeStartOffset),
      relativeEndOffset: relativeEndOffset == null && nullToAbsent
          ? const Value.absent()
          : Value(relativeEndOffset),
      relativeRange: relativeRange == null && nullToAbsent
          ? const Value.absent()
          : Value(relativeRange),
      loadOverTime: Value(loadOverTime),
      grouping: Value(grouping),
      invert: Value(invert),
      sorting: Value(sorting),
      reportText: reportText == null && nullToAbsent
          ? const Value.absent()
          : Value(reportText),
      filters: Value(filters),
    );
  }

  factory ReportsData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return ReportsData(
      createDate: serializer.fromJson<DateTime>(json['createDate']),
      updateDate: serializer.fromJson<DateTime>(json['updateDate']),
      deleteDate: serializer.fromJson<DateTime?>(json['deleteDate']),
      uuid: serializer.fromJson<String>(json['uuid']),
      name: serializer.fromJson<String>(json['name']),
      type: serializer.fromJson<String>(json['type']),
      startDate: serializer.fromJson<DateTime?>(json['startDate']),
      endDate: serializer.fromJson<DateTime?>(json['endDate']),
      relativeStartOffset:
          serializer.fromJson<int?>(json['relativeStartOffset']),
      relativeEndOffset: serializer.fromJson<int?>(json['relativeEndOffset']),
      relativeRange: serializer.fromJson<String?>(json['relativeRange']),
      loadOverTime: serializer.fromJson<bool>(json['loadOverTime']),
      grouping: serializer.fromJson<String>(json['grouping']),
      invert: serializer.fromJson<bool>(json['invert']),
      sorting: serializer.fromJson<String>(json['sorting']),
      reportText: serializer.fromJson<String?>(json['reportText']),
      filters: serializer.fromJson<String>(json['filters']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'createDate': serializer.toJson<DateTime>(createDate),
      'updateDate': serializer.toJson<DateTime>(updateDate),
      'deleteDate': serializer.toJson<DateTime?>(deleteDate),
      'uuid': serializer.toJson<String>(uuid),
      'name': serializer.toJson<String>(name),
      'type': serializer.toJson<String>(type),
      'startDate': serializer.toJson<DateTime?>(startDate),
      'endDate': serializer.toJson<DateTime?>(endDate),
      'relativeStartOffset': serializer.toJson<int?>(relativeStartOffset),
      'relativeEndOffset': serializer.toJson<int?>(relativeEndOffset),
      'relativeRange': serializer.toJson<String?>(relativeRange),
      'loadOverTime': serializer.toJson<bool>(loadOverTime),
      'grouping': serializer.toJson<String>(grouping),
      'invert': serializer.toJson<bool>(invert),
      'sorting': serializer.toJson<String>(sorting),
      'reportText': serializer.toJson<String?>(reportText),
      'filters': serializer.toJson<String>(filters),
    };
  }

  ReportsData copyWith(
          {DateTime? createDate,
          DateTime? updateDate,
          Value<DateTime?> deleteDate = const Value.absent(),
          String? uuid,
          String? name,
          String? type,
          Value<DateTime?> startDate = const Value.absent(),
          Value<DateTime?> endDate = const Value.absent(),
          Value<int?> relativeStartOffset = const Value.absent(),
          Value<int?> relativeEndOffset = const Value.absent(),
          Value<String?> relativeRange = const Value.absent(),
          bool? loadOverTime,
          String? grouping,
          bool? invert,
          String? sorting,
          Value<String?> reportText = const Value.absent(),
          String? filters}) =>
      ReportsData(
        createDate: createDate ?? this.createDate,
        updateDate: updateDate ?? this.updateDate,
        deleteDate: deleteDate.present ? deleteDate.value : this.deleteDate,
        uuid: uuid ?? this.uuid,
        name: name ?? this.name,
        type: type ?? this.type,
        startDate: startDate.present ? startDate.value : this.startDate,
        endDate: endDate.present ? endDate.value : this.endDate,
        relativeStartOffset: relativeStartOffset.present
            ? relativeStartOffset.value
            : this.relativeStartOffset,
        relativeEndOffset: relativeEndOffset.present
            ? relativeEndOffset.value
            : this.relativeEndOffset,
        relativeRange:
            relativeRange.present ? relativeRange.value : this.relativeRange,
        loadOverTime: loadOverTime ?? this.loadOverTime,
        grouping: grouping ?? this.grouping,
        invert: invert ?? this.invert,
        sorting: sorting ?? this.sorting,
        reportText: reportText.present ? reportText.value : this.reportText,
        filters: filters ?? this.filters,
      );
  ReportsData copyWithCompanion(ReportsCompanion data) {
    return ReportsData(
      createDate:
          data.createDate.present ? data.createDate.value : this.createDate,
      updateDate:
          data.updateDate.present ? data.updateDate.value : this.updateDate,
      deleteDate:
          data.deleteDate.present ? data.deleteDate.value : this.deleteDate,
      uuid: data.uuid.present ? data.uuid.value : this.uuid,
      name: data.name.present ? data.name.value : this.name,
      type: data.type.present ? data.type.value : this.type,
      startDate: data.startDate.present ? data.startDate.value : this.startDate,
      endDate: data.endDate.present ? data.endDate.value : this.endDate,
      relativeStartOffset: data.relativeStartOffset.present
          ? data.relativeStartOffset.value
          : this.relativeStartOffset,
      relativeEndOffset: data.relativeEndOffset.present
          ? data.relativeEndOffset.value
          : this.relativeEndOffset,
      relativeRange: data.relativeRange.present
          ? data.relativeRange.value
          : this.relativeRange,
      loadOverTime: data.loadOverTime.present
          ? data.loadOverTime.value
          : this.loadOverTime,
      grouping: data.grouping.present ? data.grouping.value : this.grouping,
      invert: data.invert.present ? data.invert.value : this.invert,
      sorting: data.sorting.present ? data.sorting.value : this.sorting,
      reportText:
          data.reportText.present ? data.reportText.value : this.reportText,
      filters: data.filters.present ? data.filters.value : this.filters,
    );
  }

  @override
  String toString() {
    return (StringBuffer('ReportsData(')
          ..write('createDate: $createDate, ')
          ..write('updateDate: $updateDate, ')
          ..write('deleteDate: $deleteDate, ')
          ..write('uuid: $uuid, ')
          ..write('name: $name, ')
          ..write('type: $type, ')
          ..write('startDate: $startDate, ')
          ..write('endDate: $endDate, ')
          ..write('relativeStartOffset: $relativeStartOffset, ')
          ..write('relativeEndOffset: $relativeEndOffset, ')
          ..write('relativeRange: $relativeRange, ')
          ..write('loadOverTime: $loadOverTime, ')
          ..write('grouping: $grouping, ')
          ..write('invert: $invert, ')
          ..write('sorting: $sorting, ')
          ..write('reportText: $reportText, ')
          ..write('filters: $filters')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(
      createDate,
      updateDate,
      deleteDate,
      uuid,
      name,
      type,
      startDate,
      endDate,
      relativeStartOffset,
      relativeEndOffset,
      relativeRange,
      loadOverTime,
      grouping,
      invert,
      sorting,
      reportText,
      filters);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is ReportsData &&
          other.createDate == this.createDate &&
          other.updateDate == this.updateDate &&
          other.deleteDate == this.deleteDate &&
          other.uuid == this.uuid &&
          other.name == this.name &&
          other.type == this.type &&
          other.startDate == this.startDate &&
          other.endDate == this.endDate &&
          other.relativeStartOffset == this.relativeStartOffset &&
          other.relativeEndOffset == this.relativeEndOffset &&
          other.relativeRange == this.relativeRange &&
          other.loadOverTime == this.loadOverTime &&
          other.grouping == this.grouping &&
          other.invert == this.invert &&
          other.sorting == this.sorting &&
          other.reportText == this.reportText &&
          other.filters == this.filters);
}

class ReportsCompanion extends UpdateCompanion<ReportsData> {
  final Value<DateTime> createDate;
  final Value<DateTime> updateDate;
  final Value<DateTime?> deleteDate;
  final Value<String> uuid;
  final Value<String> name;
  final Value<String> type;
  final Value<DateTime?> startDate;
  final Value<DateTime?> endDate;
  final Value<int?> relativeStartOffset;
  final Value<int?> relativeEndOffset;
  final Value<String?> relativeRange;
  final Value<bool> loadOverTime;
  final Value<String> grouping;
  final Value<bool> invert;
  final Value<String> sorting;
  final Value<String?> reportText;
  final Value<String> filters;
  final Value<int> rowid;
  const ReportsCompanion({
    this.createDate = const Value.absent(),
    this.updateDate = const Value.absent(),
    this.deleteDate = const Value.absent(),
    this.uuid = const Value.absent(),
    this.name = const Value.absent(),
    this.type = const Value.absent(),
    this.startDate = const Value.absent(),
    this.endDate = const Value.absent(),
    this.relativeStartOffset = const Value.absent(),
    this.relativeEndOffset = const Value.absent(),
    this.relativeRange = const Value.absent(),
    this.loadOverTime = const Value.absent(),
    this.grouping = const Value.absent(),
    this.invert = const Value.absent(),
    this.sorting = const Value.absent(),
    this.reportText = const Value.absent(),
    this.filters = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  ReportsCompanion.insert({
    this.createDate = const Value.absent(),
    this.updateDate = const Value.absent(),
    this.deleteDate = const Value.absent(),
    required String uuid,
    required String name,
    required String type,
    this.startDate = const Value.absent(),
    this.endDate = const Value.absent(),
    this.relativeStartOffset = const Value.absent(),
    this.relativeEndOffset = const Value.absent(),
    this.relativeRange = const Value.absent(),
    required bool loadOverTime,
    required String grouping,
    required bool invert,
    required String sorting,
    this.reportText = const Value.absent(),
    required String filters,
    this.rowid = const Value.absent(),
  })  : uuid = Value(uuid),
        name = Value(name),
        type = Value(type),
        loadOverTime = Value(loadOverTime),
        grouping = Value(grouping),
        invert = Value(invert),
        sorting = Value(sorting),
        filters = Value(filters);
  static Insertable<ReportsData> custom({
    Expression<DateTime>? createDate,
    Expression<DateTime>? updateDate,
    Expression<DateTime>? deleteDate,
    Expression<String>? uuid,
    Expression<String>? name,
    Expression<String>? type,
    Expression<DateTime>? startDate,
    Expression<DateTime>? endDate,
    Expression<int>? relativeStartOffset,
    Expression<int>? relativeEndOffset,
    Expression<String>? relativeRange,
    Expression<bool>? loadOverTime,
    Expression<String>? grouping,
    Expression<bool>? invert,
    Expression<String>? sorting,
    Expression<String>? reportText,
    Expression<String>? filters,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (createDate != null) 'create_date': createDate,
      if (updateDate != null) 'update_date': updateDate,
      if (deleteDate != null) 'delete_date': deleteDate,
      if (uuid != null) 'uuid': uuid,
      if (name != null) 'name': name,
      if (type != null) 'type': type,
      if (startDate != null) 'start_date': startDate,
      if (endDate != null) 'end_date': endDate,
      if (relativeStartOffset != null)
        'relative_start_offset': relativeStartOffset,
      if (relativeEndOffset != null) 'relative_end_offset': relativeEndOffset,
      if (relativeRange != null) 'relative_range': relativeRange,
      if (loadOverTime != null) 'load_over_time': loadOverTime,
      if (grouping != null) 'grouping': grouping,
      if (invert != null) 'invert': invert,
      if (sorting != null) 'sorting': sorting,
      if (reportText != null) 'report_text': reportText,
      if (filters != null) 'filters': filters,
      if (rowid != null) 'rowid': rowid,
    });
  }

  ReportsCompanion copyWith(
      {Value<DateTime>? createDate,
      Value<DateTime>? updateDate,
      Value<DateTime?>? deleteDate,
      Value<String>? uuid,
      Value<String>? name,
      Value<String>? type,
      Value<DateTime?>? startDate,
      Value<DateTime?>? endDate,
      Value<int?>? relativeStartOffset,
      Value<int?>? relativeEndOffset,
      Value<String?>? relativeRange,
      Value<bool>? loadOverTime,
      Value<String>? grouping,
      Value<bool>? invert,
      Value<String>? sorting,
      Value<String?>? reportText,
      Value<String>? filters,
      Value<int>? rowid}) {
    return ReportsCompanion(
      createDate: createDate ?? this.createDate,
      updateDate: updateDate ?? this.updateDate,
      deleteDate: deleteDate ?? this.deleteDate,
      uuid: uuid ?? this.uuid,
      name: name ?? this.name,
      type: type ?? this.type,
      startDate: startDate ?? this.startDate,
      endDate: endDate ?? this.endDate,
      relativeStartOffset: relativeStartOffset ?? this.relativeStartOffset,
      relativeEndOffset: relativeEndOffset ?? this.relativeEndOffset,
      relativeRange: relativeRange ?? this.relativeRange,
      loadOverTime: loadOverTime ?? this.loadOverTime,
      grouping: grouping ?? this.grouping,
      invert: invert ?? this.invert,
      sorting: sorting ?? this.sorting,
      reportText: reportText ?? this.reportText,
      filters: filters ?? this.filters,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (createDate.present) {
      map['create_date'] = Variable<DateTime>(createDate.value);
    }
    if (updateDate.present) {
      map['update_date'] = Variable<DateTime>(updateDate.value);
    }
    if (deleteDate.present) {
      map['delete_date'] = Variable<DateTime>(deleteDate.value);
    }
    if (uuid.present) {
      map['uuid'] = Variable<String>(uuid.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (type.present) {
      map['type'] = Variable<String>(type.value);
    }
    if (startDate.present) {
      map['start_date'] = Variable<DateTime>(startDate.value);
    }
    if (endDate.present) {
      map['end_date'] = Variable<DateTime>(endDate.value);
    }
    if (relativeStartOffset.present) {
      map['relative_start_offset'] = Variable<int>(relativeStartOffset.value);
    }
    if (relativeEndOffset.present) {
      map['relative_end_offset'] = Variable<int>(relativeEndOffset.value);
    }
    if (relativeRange.present) {
      map['relative_range'] = Variable<String>(relativeRange.value);
    }
    if (loadOverTime.present) {
      map['load_over_time'] = Variable<bool>(loadOverTime.value);
    }
    if (grouping.present) {
      map['grouping'] = Variable<String>(grouping.value);
    }
    if (invert.present) {
      map['invert'] = Variable<bool>(invert.value);
    }
    if (sorting.present) {
      map['sorting'] = Variable<String>(sorting.value);
    }
    if (reportText.present) {
      map['report_text'] = Variable<String>(reportText.value);
    }
    if (filters.present) {
      map['filters'] = Variable<String>(filters.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('ReportsCompanion(')
          ..write('createDate: $createDate, ')
          ..write('updateDate: $updateDate, ')
          ..write('deleteDate: $deleteDate, ')
          ..write('uuid: $uuid, ')
          ..write('name: $name, ')
          ..write('type: $type, ')
          ..write('startDate: $startDate, ')
          ..write('endDate: $endDate, ')
          ..write('relativeStartOffset: $relativeStartOffset, ')
          ..write('relativeEndOffset: $relativeEndOffset, ')
          ..write('relativeRange: $relativeRange, ')
          ..write('loadOverTime: $loadOverTime, ')
          ..write('grouping: $grouping, ')
          ..write('invert: $invert, ')
          ..write('sorting: $sorting, ')
          ..write('reportText: $reportText, ')
          ..write('filters: $filters, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class CategoryGroups extends Table
    with TableInfo<CategoryGroups, CategoryGroupsData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  CategoryGroups(this.attachedDatabase, [this._alias]);
  late final GeneratedColumn<DateTime> createDate = GeneratedColumn<DateTime>(
      'create_date', aliasedName, false,
      type: DriftSqlType.dateTime,
      requiredDuringInsert: false,
      defaultValue: currentDateAndTime);
  late final GeneratedColumn<DateTime> updateDate = GeneratedColumn<DateTime>(
      'update_date', aliasedName, false,
      type: DriftSqlType.dateTime,
      requiredDuringInsert: false,
      defaultValue: currentDateAndTime);
  late final GeneratedColumn<DateTime> deleteDate = GeneratedColumn<DateTime>(
      'delete_date', aliasedName, true,
      type: DriftSqlType.dateTime, requiredDuringInsert: false);
  late final GeneratedColumn<String> uuid = GeneratedColumn<String>(
      'uuid', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  late final GeneratedColumn<String> name =
      GeneratedColumn<String>('name', aliasedName, false,
          additionalChecks: GeneratedColumn.checkTextLength(
            minTextLength: 1,
          ),
          type: DriftSqlType.string,
          requiredDuringInsert: true);
  late final GeneratedColumn<bool> isRequired = GeneratedColumn<bool>(
      'is_required', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('CHECK ("is_required" IN (0, 1))'),
      defaultValue: const Constant(false));
  late final GeneratedColumn<bool> enableAutoBalance = GeneratedColumn<bool>(
      'enable_auto_balance', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: false,
      defaultConstraints: GeneratedColumn.constraintIsAlways(
          'CHECK ("enable_auto_balance" IN (0, 1))'),
      defaultValue: const Constant(false));
  late final GeneratedColumn<bool> requireLimits = GeneratedColumn<bool>(
      'require_limits', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: false,
      defaultConstraints: GeneratedColumn.constraintIsAlways(
          'CHECK ("require_limits" IN (0, 1))'),
      defaultValue: const Constant(false));
  late final GeneratedColumn<int> defaultColor = GeneratedColumn<int>(
      'default_color', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  late final GeneratedColumn<int> overColor = GeneratedColumn<int>(
      'over_color', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  late final GeneratedColumn<String> datePeriod = GeneratedColumn<String>(
      'date_period', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  late final GeneratedColumn<int> datePeriodInterval = GeneratedColumn<int>(
      'date_period_interval', aliasedName, true,
      type: DriftSqlType.int, requiredDuringInsert: false);
  late final GeneratedColumn<String> reportUuid = GeneratedColumn<String>(
      'report_uuid', aliasedName, true,
      type: DriftSqlType.string,
      requiredDuringInsert: false,
      defaultConstraints: GeneratedColumn.constraintIsAlways(
          'REFERENCES reports (uuid) ON DELETE SET NULL'));
  @override
  List<GeneratedColumn> get $columns => [
        createDate,
        updateDate,
        deleteDate,
        uuid,
        name,
        isRequired,
        enableAutoBalance,
        requireLimits,
        defaultColor,
        overColor,
        datePeriod,
        datePeriodInterval,
        reportUuid
      ];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'category_groups';
  @override
  Set<GeneratedColumn> get $primaryKey => {uuid};
  @override
  CategoryGroupsData map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return CategoryGroupsData(
      createDate: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}create_date'])!,
      updateDate: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}update_date'])!,
      deleteDate: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}delete_date']),
      uuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}uuid'])!,
      name: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}name'])!,
      isRequired: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}is_required'])!,
      enableAutoBalance: attachedDatabase.typeMapping.read(
          DriftSqlType.bool, data['${effectivePrefix}enable_auto_balance'])!,
      requireLimits: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}require_limits'])!,
      defaultColor: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}default_color'])!,
      overColor: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}over_color'])!,
      datePeriod: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}date_period']),
      datePeriodInterval: attachedDatabase.typeMapping.read(
          DriftSqlType.int, data['${effectivePrefix}date_period_interval']),
      reportUuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}report_uuid']),
    );
  }

  @override
  CategoryGroups createAlias(String alias) {
    return CategoryGroups(attachedDatabase, alias);
  }
}

class CategoryGroupsData extends DataClass
    implements Insertable<CategoryGroupsData> {
  final DateTime createDate;
  final DateTime updateDate;
  final DateTime? deleteDate;
  final String uuid;
  final String name;
  final bool isRequired;
  final bool enableAutoBalance;
  final bool requireLimits;
  final int defaultColor;
  final int overColor;
  final String? datePeriod;
  final int? datePeriodInterval;
  final String? reportUuid;
  const CategoryGroupsData(
      {required this.createDate,
      required this.updateDate,
      this.deleteDate,
      required this.uuid,
      required this.name,
      required this.isRequired,
      required this.enableAutoBalance,
      required this.requireLimits,
      required this.defaultColor,
      required this.overColor,
      this.datePeriod,
      this.datePeriodInterval,
      this.reportUuid});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['create_date'] = Variable<DateTime>(createDate);
    map['update_date'] = Variable<DateTime>(updateDate);
    if (!nullToAbsent || deleteDate != null) {
      map['delete_date'] = Variable<DateTime>(deleteDate);
    }
    map['uuid'] = Variable<String>(uuid);
    map['name'] = Variable<String>(name);
    map['is_required'] = Variable<bool>(isRequired);
    map['enable_auto_balance'] = Variable<bool>(enableAutoBalance);
    map['require_limits'] = Variable<bool>(requireLimits);
    map['default_color'] = Variable<int>(defaultColor);
    map['over_color'] = Variable<int>(overColor);
    if (!nullToAbsent || datePeriod != null) {
      map['date_period'] = Variable<String>(datePeriod);
    }
    if (!nullToAbsent || datePeriodInterval != null) {
      map['date_period_interval'] = Variable<int>(datePeriodInterval);
    }
    if (!nullToAbsent || reportUuid != null) {
      map['report_uuid'] = Variable<String>(reportUuid);
    }
    return map;
  }

  CategoryGroupsCompanion toCompanion(bool nullToAbsent) {
    return CategoryGroupsCompanion(
      createDate: Value(createDate),
      updateDate: Value(updateDate),
      deleteDate: deleteDate == null && nullToAbsent
          ? const Value.absent()
          : Value(deleteDate),
      uuid: Value(uuid),
      name: Value(name),
      isRequired: Value(isRequired),
      enableAutoBalance: Value(enableAutoBalance),
      requireLimits: Value(requireLimits),
      defaultColor: Value(defaultColor),
      overColor: Value(overColor),
      datePeriod: datePeriod == null && nullToAbsent
          ? const Value.absent()
          : Value(datePeriod),
      datePeriodInterval: datePeriodInterval == null && nullToAbsent
          ? const Value.absent()
          : Value(datePeriodInterval),
      reportUuid: reportUuid == null && nullToAbsent
          ? const Value.absent()
          : Value(reportUuid),
    );
  }

  factory CategoryGroupsData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return CategoryGroupsData(
      createDate: serializer.fromJson<DateTime>(json['createDate']),
      updateDate: serializer.fromJson<DateTime>(json['updateDate']),
      deleteDate: serializer.fromJson<DateTime?>(json['deleteDate']),
      uuid: serializer.fromJson<String>(json['uuid']),
      name: serializer.fromJson<String>(json['name']),
      isRequired: serializer.fromJson<bool>(json['isRequired']),
      enableAutoBalance: serializer.fromJson<bool>(json['enableAutoBalance']),
      requireLimits: serializer.fromJson<bool>(json['requireLimits']),
      defaultColor: serializer.fromJson<int>(json['defaultColor']),
      overColor: serializer.fromJson<int>(json['overColor']),
      datePeriod: serializer.fromJson<String?>(json['datePeriod']),
      datePeriodInterval: serializer.fromJson<int?>(json['datePeriodInterval']),
      reportUuid: serializer.fromJson<String?>(json['reportUuid']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'createDate': serializer.toJson<DateTime>(createDate),
      'updateDate': serializer.toJson<DateTime>(updateDate),
      'deleteDate': serializer.toJson<DateTime?>(deleteDate),
      'uuid': serializer.toJson<String>(uuid),
      'name': serializer.toJson<String>(name),
      'isRequired': serializer.toJson<bool>(isRequired),
      'enableAutoBalance': serializer.toJson<bool>(enableAutoBalance),
      'requireLimits': serializer.toJson<bool>(requireLimits),
      'defaultColor': serializer.toJson<int>(defaultColor),
      'overColor': serializer.toJson<int>(overColor),
      'datePeriod': serializer.toJson<String?>(datePeriod),
      'datePeriodInterval': serializer.toJson<int?>(datePeriodInterval),
      'reportUuid': serializer.toJson<String?>(reportUuid),
    };
  }

  CategoryGroupsData copyWith(
          {DateTime? createDate,
          DateTime? updateDate,
          Value<DateTime?> deleteDate = const Value.absent(),
          String? uuid,
          String? name,
          bool? isRequired,
          bool? enableAutoBalance,
          bool? requireLimits,
          int? defaultColor,
          int? overColor,
          Value<String?> datePeriod = const Value.absent(),
          Value<int?> datePeriodInterval = const Value.absent(),
          Value<String?> reportUuid = const Value.absent()}) =>
      CategoryGroupsData(
        createDate: createDate ?? this.createDate,
        updateDate: updateDate ?? this.updateDate,
        deleteDate: deleteDate.present ? deleteDate.value : this.deleteDate,
        uuid: uuid ?? this.uuid,
        name: name ?? this.name,
        isRequired: isRequired ?? this.isRequired,
        enableAutoBalance: enableAutoBalance ?? this.enableAutoBalance,
        requireLimits: requireLimits ?? this.requireLimits,
        defaultColor: defaultColor ?? this.defaultColor,
        overColor: overColor ?? this.overColor,
        datePeriod: datePeriod.present ? datePeriod.value : this.datePeriod,
        datePeriodInterval: datePeriodInterval.present
            ? datePeriodInterval.value
            : this.datePeriodInterval,
        reportUuid: reportUuid.present ? reportUuid.value : this.reportUuid,
      );
  CategoryGroupsData copyWithCompanion(CategoryGroupsCompanion data) {
    return CategoryGroupsData(
      createDate:
          data.createDate.present ? data.createDate.value : this.createDate,
      updateDate:
          data.updateDate.present ? data.updateDate.value : this.updateDate,
      deleteDate:
          data.deleteDate.present ? data.deleteDate.value : this.deleteDate,
      uuid: data.uuid.present ? data.uuid.value : this.uuid,
      name: data.name.present ? data.name.value : this.name,
      isRequired:
          data.isRequired.present ? data.isRequired.value : this.isRequired,
      enableAutoBalance: data.enableAutoBalance.present
          ? data.enableAutoBalance.value
          : this.enableAutoBalance,
      requireLimits: data.requireLimits.present
          ? data.requireLimits.value
          : this.requireLimits,
      defaultColor: data.defaultColor.present
          ? data.defaultColor.value
          : this.defaultColor,
      overColor: data.overColor.present ? data.overColor.value : this.overColor,
      datePeriod:
          data.datePeriod.present ? data.datePeriod.value : this.datePeriod,
      datePeriodInterval: data.datePeriodInterval.present
          ? data.datePeriodInterval.value
          : this.datePeriodInterval,
      reportUuid:
          data.reportUuid.present ? data.reportUuid.value : this.reportUuid,
    );
  }

  @override
  String toString() {
    return (StringBuffer('CategoryGroupsData(')
          ..write('createDate: $createDate, ')
          ..write('updateDate: $updateDate, ')
          ..write('deleteDate: $deleteDate, ')
          ..write('uuid: $uuid, ')
          ..write('name: $name, ')
          ..write('isRequired: $isRequired, ')
          ..write('enableAutoBalance: $enableAutoBalance, ')
          ..write('requireLimits: $requireLimits, ')
          ..write('defaultColor: $defaultColor, ')
          ..write('overColor: $overColor, ')
          ..write('datePeriod: $datePeriod, ')
          ..write('datePeriodInterval: $datePeriodInterval, ')
          ..write('reportUuid: $reportUuid')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(
      createDate,
      updateDate,
      deleteDate,
      uuid,
      name,
      isRequired,
      enableAutoBalance,
      requireLimits,
      defaultColor,
      overColor,
      datePeriod,
      datePeriodInterval,
      reportUuid);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is CategoryGroupsData &&
          other.createDate == this.createDate &&
          other.updateDate == this.updateDate &&
          other.deleteDate == this.deleteDate &&
          other.uuid == this.uuid &&
          other.name == this.name &&
          other.isRequired == this.isRequired &&
          other.enableAutoBalance == this.enableAutoBalance &&
          other.requireLimits == this.requireLimits &&
          other.defaultColor == this.defaultColor &&
          other.overColor == this.overColor &&
          other.datePeriod == this.datePeriod &&
          other.datePeriodInterval == this.datePeriodInterval &&
          other.reportUuid == this.reportUuid);
}

class CategoryGroupsCompanion extends UpdateCompanion<CategoryGroupsData> {
  final Value<DateTime> createDate;
  final Value<DateTime> updateDate;
  final Value<DateTime?> deleteDate;
  final Value<String> uuid;
  final Value<String> name;
  final Value<bool> isRequired;
  final Value<bool> enableAutoBalance;
  final Value<bool> requireLimits;
  final Value<int> defaultColor;
  final Value<int> overColor;
  final Value<String?> datePeriod;
  final Value<int?> datePeriodInterval;
  final Value<String?> reportUuid;
  final Value<int> rowid;
  const CategoryGroupsCompanion({
    this.createDate = const Value.absent(),
    this.updateDate = const Value.absent(),
    this.deleteDate = const Value.absent(),
    this.uuid = const Value.absent(),
    this.name = const Value.absent(),
    this.isRequired = const Value.absent(),
    this.enableAutoBalance = const Value.absent(),
    this.requireLimits = const Value.absent(),
    this.defaultColor = const Value.absent(),
    this.overColor = const Value.absent(),
    this.datePeriod = const Value.absent(),
    this.datePeriodInterval = const Value.absent(),
    this.reportUuid = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  CategoryGroupsCompanion.insert({
    this.createDate = const Value.absent(),
    this.updateDate = const Value.absent(),
    this.deleteDate = const Value.absent(),
    required String uuid,
    required String name,
    this.isRequired = const Value.absent(),
    this.enableAutoBalance = const Value.absent(),
    this.requireLimits = const Value.absent(),
    required int defaultColor,
    required int overColor,
    this.datePeriod = const Value.absent(),
    this.datePeriodInterval = const Value.absent(),
    this.reportUuid = const Value.absent(),
    this.rowid = const Value.absent(),
  })  : uuid = Value(uuid),
        name = Value(name),
        defaultColor = Value(defaultColor),
        overColor = Value(overColor);
  static Insertable<CategoryGroupsData> custom({
    Expression<DateTime>? createDate,
    Expression<DateTime>? updateDate,
    Expression<DateTime>? deleteDate,
    Expression<String>? uuid,
    Expression<String>? name,
    Expression<bool>? isRequired,
    Expression<bool>? enableAutoBalance,
    Expression<bool>? requireLimits,
    Expression<int>? defaultColor,
    Expression<int>? overColor,
    Expression<String>? datePeriod,
    Expression<int>? datePeriodInterval,
    Expression<String>? reportUuid,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (createDate != null) 'create_date': createDate,
      if (updateDate != null) 'update_date': updateDate,
      if (deleteDate != null) 'delete_date': deleteDate,
      if (uuid != null) 'uuid': uuid,
      if (name != null) 'name': name,
      if (isRequired != null) 'is_required': isRequired,
      if (enableAutoBalance != null) 'enable_auto_balance': enableAutoBalance,
      if (requireLimits != null) 'require_limits': requireLimits,
      if (defaultColor != null) 'default_color': defaultColor,
      if (overColor != null) 'over_color': overColor,
      if (datePeriod != null) 'date_period': datePeriod,
      if (datePeriodInterval != null)
        'date_period_interval': datePeriodInterval,
      if (reportUuid != null) 'report_uuid': reportUuid,
      if (rowid != null) 'rowid': rowid,
    });
  }

  CategoryGroupsCompanion copyWith(
      {Value<DateTime>? createDate,
      Value<DateTime>? updateDate,
      Value<DateTime?>? deleteDate,
      Value<String>? uuid,
      Value<String>? name,
      Value<bool>? isRequired,
      Value<bool>? enableAutoBalance,
      Value<bool>? requireLimits,
      Value<int>? defaultColor,
      Value<int>? overColor,
      Value<String?>? datePeriod,
      Value<int?>? datePeriodInterval,
      Value<String?>? reportUuid,
      Value<int>? rowid}) {
    return CategoryGroupsCompanion(
      createDate: createDate ?? this.createDate,
      updateDate: updateDate ?? this.updateDate,
      deleteDate: deleteDate ?? this.deleteDate,
      uuid: uuid ?? this.uuid,
      name: name ?? this.name,
      isRequired: isRequired ?? this.isRequired,
      enableAutoBalance: enableAutoBalance ?? this.enableAutoBalance,
      requireLimits: requireLimits ?? this.requireLimits,
      defaultColor: defaultColor ?? this.defaultColor,
      overColor: overColor ?? this.overColor,
      datePeriod: datePeriod ?? this.datePeriod,
      datePeriodInterval: datePeriodInterval ?? this.datePeriodInterval,
      reportUuid: reportUuid ?? this.reportUuid,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (createDate.present) {
      map['create_date'] = Variable<DateTime>(createDate.value);
    }
    if (updateDate.present) {
      map['update_date'] = Variable<DateTime>(updateDate.value);
    }
    if (deleteDate.present) {
      map['delete_date'] = Variable<DateTime>(deleteDate.value);
    }
    if (uuid.present) {
      map['uuid'] = Variable<String>(uuid.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (isRequired.present) {
      map['is_required'] = Variable<bool>(isRequired.value);
    }
    if (enableAutoBalance.present) {
      map['enable_auto_balance'] = Variable<bool>(enableAutoBalance.value);
    }
    if (requireLimits.present) {
      map['require_limits'] = Variable<bool>(requireLimits.value);
    }
    if (defaultColor.present) {
      map['default_color'] = Variable<int>(defaultColor.value);
    }
    if (overColor.present) {
      map['over_color'] = Variable<int>(overColor.value);
    }
    if (datePeriod.present) {
      map['date_period'] = Variable<String>(datePeriod.value);
    }
    if (datePeriodInterval.present) {
      map['date_period_interval'] = Variable<int>(datePeriodInterval.value);
    }
    if (reportUuid.present) {
      map['report_uuid'] = Variable<String>(reportUuid.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('CategoryGroupsCompanion(')
          ..write('createDate: $createDate, ')
          ..write('updateDate: $updateDate, ')
          ..write('deleteDate: $deleteDate, ')
          ..write('uuid: $uuid, ')
          ..write('name: $name, ')
          ..write('isRequired: $isRequired, ')
          ..write('enableAutoBalance: $enableAutoBalance, ')
          ..write('requireLimits: $requireLimits, ')
          ..write('defaultColor: $defaultColor, ')
          ..write('overColor: $overColor, ')
          ..write('datePeriod: $datePeriod, ')
          ..write('datePeriodInterval: $datePeriodInterval, ')
          ..write('reportUuid: $reportUuid, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class Categories extends Table with TableInfo<Categories, CategoriesData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  Categories(this.attachedDatabase, [this._alias]);
  late final GeneratedColumn<DateTime> createDate = GeneratedColumn<DateTime>(
      'create_date', aliasedName, false,
      type: DriftSqlType.dateTime,
      requiredDuringInsert: false,
      defaultValue: currentDateAndTime);
  late final GeneratedColumn<DateTime> updateDate = GeneratedColumn<DateTime>(
      'update_date', aliasedName, false,
      type: DriftSqlType.dateTime,
      requiredDuringInsert: false,
      defaultValue: currentDateAndTime);
  late final GeneratedColumn<DateTime> deleteDate = GeneratedColumn<DateTime>(
      'delete_date', aliasedName, true,
      type: DriftSqlType.dateTime, requiredDuringInsert: false);
  late final GeneratedColumn<String> uuid = GeneratedColumn<String>(
      'uuid', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  late final GeneratedColumn<String> name =
      GeneratedColumn<String>('name', aliasedName, false,
          additionalChecks: GeneratedColumn.checkTextLength(
            minTextLength: 1,
          ),
          type: DriftSqlType.string,
          requiredDuringInsert: true);
  late final GeneratedColumn<String> groupUuid = GeneratedColumn<String>(
      'group_uuid', aliasedName, false,
      type: DriftSqlType.string,
      requiredDuringInsert: true,
      defaultConstraints: GeneratedColumn.constraintIsAlways(
          'REFERENCES category_groups (uuid) ON DELETE CASCADE'));
  late final GeneratedColumn<String> parentUuid = GeneratedColumn<String>(
      'parent_uuid', aliasedName, true,
      type: DriftSqlType.string,
      requiredDuringInsert: false,
      defaultConstraints: GeneratedColumn.constraintIsAlways(
          'REFERENCES categories (uuid) ON DELETE SET NULL'));
  late final GeneratedColumn<String> progenitorUuid = GeneratedColumn<String>(
      'progenitor_uuid', aliasedName, true,
      type: DriftSqlType.string,
      requiredDuringInsert: false,
      defaultConstraints: GeneratedColumn.constraintIsAlways(
          'REFERENCES categories (uuid) ON DELETE SET NULL'));
  late final GeneratedColumn<bool> isSpendLimit = GeneratedColumn<bool>(
      'is_spend_limit', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: false,
      defaultConstraints: GeneratedColumn.constraintIsAlways(
          'CHECK ("is_spend_limit" IN (0, 1))'),
      defaultValue: const Constant(true));
  late final GeneratedColumn<bool> excludeFromAutoBalance =
      GeneratedColumn<bool>('exclude_from_auto_balance', aliasedName, false,
          type: DriftSqlType.bool,
          requiredDuringInsert: false,
          defaultConstraints: GeneratedColumn.constraintIsAlways(
              'CHECK ("exclude_from_auto_balance" IN (0, 1))'),
          defaultValue: const Constant(false));
  late final GeneratedColumn<String> datePeriod = GeneratedColumn<String>(
      'date_period', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  late final GeneratedColumn<int> datePeriodInterval = GeneratedColumn<int>(
      'date_period_interval', aliasedName, true,
      type: DriftSqlType.int, requiredDuringInsert: false);
  @override
  List<GeneratedColumn> get $columns => [
        createDate,
        updateDate,
        deleteDate,
        uuid,
        name,
        groupUuid,
        parentUuid,
        progenitorUuid,
        isSpendLimit,
        excludeFromAutoBalance,
        datePeriod,
        datePeriodInterval
      ];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'categories';
  @override
  Set<GeneratedColumn> get $primaryKey => {uuid};
  @override
  CategoriesData map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return CategoriesData(
      createDate: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}create_date'])!,
      updateDate: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}update_date'])!,
      deleteDate: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}delete_date']),
      uuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}uuid'])!,
      name: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}name'])!,
      groupUuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}group_uuid'])!,
      parentUuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}parent_uuid']),
      progenitorUuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}progenitor_uuid']),
      isSpendLimit: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}is_spend_limit'])!,
      excludeFromAutoBalance: attachedDatabase.typeMapping.read(
          DriftSqlType.bool,
          data['${effectivePrefix}exclude_from_auto_balance'])!,
      datePeriod: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}date_period']),
      datePeriodInterval: attachedDatabase.typeMapping.read(
          DriftSqlType.int, data['${effectivePrefix}date_period_interval']),
    );
  }

  @override
  Categories createAlias(String alias) {
    return Categories(attachedDatabase, alias);
  }
}

class CategoriesData extends DataClass implements Insertable<CategoriesData> {
  final DateTime createDate;
  final DateTime updateDate;
  final DateTime? deleteDate;
  final String uuid;
  final String name;
  final String groupUuid;
  final String? parentUuid;
  final String? progenitorUuid;
  final bool isSpendLimit;
  final bool excludeFromAutoBalance;
  final String? datePeriod;
  final int? datePeriodInterval;
  const CategoriesData(
      {required this.createDate,
      required this.updateDate,
      this.deleteDate,
      required this.uuid,
      required this.name,
      required this.groupUuid,
      this.parentUuid,
      this.progenitorUuid,
      required this.isSpendLimit,
      required this.excludeFromAutoBalance,
      this.datePeriod,
      this.datePeriodInterval});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['create_date'] = Variable<DateTime>(createDate);
    map['update_date'] = Variable<DateTime>(updateDate);
    if (!nullToAbsent || deleteDate != null) {
      map['delete_date'] = Variable<DateTime>(deleteDate);
    }
    map['uuid'] = Variable<String>(uuid);
    map['name'] = Variable<String>(name);
    map['group_uuid'] = Variable<String>(groupUuid);
    if (!nullToAbsent || parentUuid != null) {
      map['parent_uuid'] = Variable<String>(parentUuid);
    }
    if (!nullToAbsent || progenitorUuid != null) {
      map['progenitor_uuid'] = Variable<String>(progenitorUuid);
    }
    map['is_spend_limit'] = Variable<bool>(isSpendLimit);
    map['exclude_from_auto_balance'] = Variable<bool>(excludeFromAutoBalance);
    if (!nullToAbsent || datePeriod != null) {
      map['date_period'] = Variable<String>(datePeriod);
    }
    if (!nullToAbsent || datePeriodInterval != null) {
      map['date_period_interval'] = Variable<int>(datePeriodInterval);
    }
    return map;
  }

  CategoriesCompanion toCompanion(bool nullToAbsent) {
    return CategoriesCompanion(
      createDate: Value(createDate),
      updateDate: Value(updateDate),
      deleteDate: deleteDate == null && nullToAbsent
          ? const Value.absent()
          : Value(deleteDate),
      uuid: Value(uuid),
      name: Value(name),
      groupUuid: Value(groupUuid),
      parentUuid: parentUuid == null && nullToAbsent
          ? const Value.absent()
          : Value(parentUuid),
      progenitorUuid: progenitorUuid == null && nullToAbsent
          ? const Value.absent()
          : Value(progenitorUuid),
      isSpendLimit: Value(isSpendLimit),
      excludeFromAutoBalance: Value(excludeFromAutoBalance),
      datePeriod: datePeriod == null && nullToAbsent
          ? const Value.absent()
          : Value(datePeriod),
      datePeriodInterval: datePeriodInterval == null && nullToAbsent
          ? const Value.absent()
          : Value(datePeriodInterval),
    );
  }

  factory CategoriesData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return CategoriesData(
      createDate: serializer.fromJson<DateTime>(json['createDate']),
      updateDate: serializer.fromJson<DateTime>(json['updateDate']),
      deleteDate: serializer.fromJson<DateTime?>(json['deleteDate']),
      uuid: serializer.fromJson<String>(json['uuid']),
      name: serializer.fromJson<String>(json['name']),
      groupUuid: serializer.fromJson<String>(json['groupUuid']),
      parentUuid: serializer.fromJson<String?>(json['parentUuid']),
      progenitorUuid: serializer.fromJson<String?>(json['progenitorUuid']),
      isSpendLimit: serializer.fromJson<bool>(json['isSpendLimit']),
      excludeFromAutoBalance:
          serializer.fromJson<bool>(json['excludeFromAutoBalance']),
      datePeriod: serializer.fromJson<String?>(json['datePeriod']),
      datePeriodInterval: serializer.fromJson<int?>(json['datePeriodInterval']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'createDate': serializer.toJson<DateTime>(createDate),
      'updateDate': serializer.toJson<DateTime>(updateDate),
      'deleteDate': serializer.toJson<DateTime?>(deleteDate),
      'uuid': serializer.toJson<String>(uuid),
      'name': serializer.toJson<String>(name),
      'groupUuid': serializer.toJson<String>(groupUuid),
      'parentUuid': serializer.toJson<String?>(parentUuid),
      'progenitorUuid': serializer.toJson<String?>(progenitorUuid),
      'isSpendLimit': serializer.toJson<bool>(isSpendLimit),
      'excludeFromAutoBalance': serializer.toJson<bool>(excludeFromAutoBalance),
      'datePeriod': serializer.toJson<String?>(datePeriod),
      'datePeriodInterval': serializer.toJson<int?>(datePeriodInterval),
    };
  }

  CategoriesData copyWith(
          {DateTime? createDate,
          DateTime? updateDate,
          Value<DateTime?> deleteDate = const Value.absent(),
          String? uuid,
          String? name,
          String? groupUuid,
          Value<String?> parentUuid = const Value.absent(),
          Value<String?> progenitorUuid = const Value.absent(),
          bool? isSpendLimit,
          bool? excludeFromAutoBalance,
          Value<String?> datePeriod = const Value.absent(),
          Value<int?> datePeriodInterval = const Value.absent()}) =>
      CategoriesData(
        createDate: createDate ?? this.createDate,
        updateDate: updateDate ?? this.updateDate,
        deleteDate: deleteDate.present ? deleteDate.value : this.deleteDate,
        uuid: uuid ?? this.uuid,
        name: name ?? this.name,
        groupUuid: groupUuid ?? this.groupUuid,
        parentUuid: parentUuid.present ? parentUuid.value : this.parentUuid,
        progenitorUuid:
            progenitorUuid.present ? progenitorUuid.value : this.progenitorUuid,
        isSpendLimit: isSpendLimit ?? this.isSpendLimit,
        excludeFromAutoBalance:
            excludeFromAutoBalance ?? this.excludeFromAutoBalance,
        datePeriod: datePeriod.present ? datePeriod.value : this.datePeriod,
        datePeriodInterval: datePeriodInterval.present
            ? datePeriodInterval.value
            : this.datePeriodInterval,
      );
  CategoriesData copyWithCompanion(CategoriesCompanion data) {
    return CategoriesData(
      createDate:
          data.createDate.present ? data.createDate.value : this.createDate,
      updateDate:
          data.updateDate.present ? data.updateDate.value : this.updateDate,
      deleteDate:
          data.deleteDate.present ? data.deleteDate.value : this.deleteDate,
      uuid: data.uuid.present ? data.uuid.value : this.uuid,
      name: data.name.present ? data.name.value : this.name,
      groupUuid: data.groupUuid.present ? data.groupUuid.value : this.groupUuid,
      parentUuid:
          data.parentUuid.present ? data.parentUuid.value : this.parentUuid,
      progenitorUuid: data.progenitorUuid.present
          ? data.progenitorUuid.value
          : this.progenitorUuid,
      isSpendLimit: data.isSpendLimit.present
          ? data.isSpendLimit.value
          : this.isSpendLimit,
      excludeFromAutoBalance: data.excludeFromAutoBalance.present
          ? data.excludeFromAutoBalance.value
          : this.excludeFromAutoBalance,
      datePeriod:
          data.datePeriod.present ? data.datePeriod.value : this.datePeriod,
      datePeriodInterval: data.datePeriodInterval.present
          ? data.datePeriodInterval.value
          : this.datePeriodInterval,
    );
  }

  @override
  String toString() {
    return (StringBuffer('CategoriesData(')
          ..write('createDate: $createDate, ')
          ..write('updateDate: $updateDate, ')
          ..write('deleteDate: $deleteDate, ')
          ..write('uuid: $uuid, ')
          ..write('name: $name, ')
          ..write('groupUuid: $groupUuid, ')
          ..write('parentUuid: $parentUuid, ')
          ..write('progenitorUuid: $progenitorUuid, ')
          ..write('isSpendLimit: $isSpendLimit, ')
          ..write('excludeFromAutoBalance: $excludeFromAutoBalance, ')
          ..write('datePeriod: $datePeriod, ')
          ..write('datePeriodInterval: $datePeriodInterval')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(
      createDate,
      updateDate,
      deleteDate,
      uuid,
      name,
      groupUuid,
      parentUuid,
      progenitorUuid,
      isSpendLimit,
      excludeFromAutoBalance,
      datePeriod,
      datePeriodInterval);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is CategoriesData &&
          other.createDate == this.createDate &&
          other.updateDate == this.updateDate &&
          other.deleteDate == this.deleteDate &&
          other.uuid == this.uuid &&
          other.name == this.name &&
          other.groupUuid == this.groupUuid &&
          other.parentUuid == this.parentUuid &&
          other.progenitorUuid == this.progenitorUuid &&
          other.isSpendLimit == this.isSpendLimit &&
          other.excludeFromAutoBalance == this.excludeFromAutoBalance &&
          other.datePeriod == this.datePeriod &&
          other.datePeriodInterval == this.datePeriodInterval);
}

class CategoriesCompanion extends UpdateCompanion<CategoriesData> {
  final Value<DateTime> createDate;
  final Value<DateTime> updateDate;
  final Value<DateTime?> deleteDate;
  final Value<String> uuid;
  final Value<String> name;
  final Value<String> groupUuid;
  final Value<String?> parentUuid;
  final Value<String?> progenitorUuid;
  final Value<bool> isSpendLimit;
  final Value<bool> excludeFromAutoBalance;
  final Value<String?> datePeriod;
  final Value<int?> datePeriodInterval;
  final Value<int> rowid;
  const CategoriesCompanion({
    this.createDate = const Value.absent(),
    this.updateDate = const Value.absent(),
    this.deleteDate = const Value.absent(),
    this.uuid = const Value.absent(),
    this.name = const Value.absent(),
    this.groupUuid = const Value.absent(),
    this.parentUuid = const Value.absent(),
    this.progenitorUuid = const Value.absent(),
    this.isSpendLimit = const Value.absent(),
    this.excludeFromAutoBalance = const Value.absent(),
    this.datePeriod = const Value.absent(),
    this.datePeriodInterval = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  CategoriesCompanion.insert({
    this.createDate = const Value.absent(),
    this.updateDate = const Value.absent(),
    this.deleteDate = const Value.absent(),
    required String uuid,
    required String name,
    required String groupUuid,
    this.parentUuid = const Value.absent(),
    this.progenitorUuid = const Value.absent(),
    this.isSpendLimit = const Value.absent(),
    this.excludeFromAutoBalance = const Value.absent(),
    this.datePeriod = const Value.absent(),
    this.datePeriodInterval = const Value.absent(),
    this.rowid = const Value.absent(),
  })  : uuid = Value(uuid),
        name = Value(name),
        groupUuid = Value(groupUuid);
  static Insertable<CategoriesData> custom({
    Expression<DateTime>? createDate,
    Expression<DateTime>? updateDate,
    Expression<DateTime>? deleteDate,
    Expression<String>? uuid,
    Expression<String>? name,
    Expression<String>? groupUuid,
    Expression<String>? parentUuid,
    Expression<String>? progenitorUuid,
    Expression<bool>? isSpendLimit,
    Expression<bool>? excludeFromAutoBalance,
    Expression<String>? datePeriod,
    Expression<int>? datePeriodInterval,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (createDate != null) 'create_date': createDate,
      if (updateDate != null) 'update_date': updateDate,
      if (deleteDate != null) 'delete_date': deleteDate,
      if (uuid != null) 'uuid': uuid,
      if (name != null) 'name': name,
      if (groupUuid != null) 'group_uuid': groupUuid,
      if (parentUuid != null) 'parent_uuid': parentUuid,
      if (progenitorUuid != null) 'progenitor_uuid': progenitorUuid,
      if (isSpendLimit != null) 'is_spend_limit': isSpendLimit,
      if (excludeFromAutoBalance != null)
        'exclude_from_auto_balance': excludeFromAutoBalance,
      if (datePeriod != null) 'date_period': datePeriod,
      if (datePeriodInterval != null)
        'date_period_interval': datePeriodInterval,
      if (rowid != null) 'rowid': rowid,
    });
  }

  CategoriesCompanion copyWith(
      {Value<DateTime>? createDate,
      Value<DateTime>? updateDate,
      Value<DateTime?>? deleteDate,
      Value<String>? uuid,
      Value<String>? name,
      Value<String>? groupUuid,
      Value<String?>? parentUuid,
      Value<String?>? progenitorUuid,
      Value<bool>? isSpendLimit,
      Value<bool>? excludeFromAutoBalance,
      Value<String?>? datePeriod,
      Value<int?>? datePeriodInterval,
      Value<int>? rowid}) {
    return CategoriesCompanion(
      createDate: createDate ?? this.createDate,
      updateDate: updateDate ?? this.updateDate,
      deleteDate: deleteDate ?? this.deleteDate,
      uuid: uuid ?? this.uuid,
      name: name ?? this.name,
      groupUuid: groupUuid ?? this.groupUuid,
      parentUuid: parentUuid ?? this.parentUuid,
      progenitorUuid: progenitorUuid ?? this.progenitorUuid,
      isSpendLimit: isSpendLimit ?? this.isSpendLimit,
      excludeFromAutoBalance:
          excludeFromAutoBalance ?? this.excludeFromAutoBalance,
      datePeriod: datePeriod ?? this.datePeriod,
      datePeriodInterval: datePeriodInterval ?? this.datePeriodInterval,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (createDate.present) {
      map['create_date'] = Variable<DateTime>(createDate.value);
    }
    if (updateDate.present) {
      map['update_date'] = Variable<DateTime>(updateDate.value);
    }
    if (deleteDate.present) {
      map['delete_date'] = Variable<DateTime>(deleteDate.value);
    }
    if (uuid.present) {
      map['uuid'] = Variable<String>(uuid.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (groupUuid.present) {
      map['group_uuid'] = Variable<String>(groupUuid.value);
    }
    if (parentUuid.present) {
      map['parent_uuid'] = Variable<String>(parentUuid.value);
    }
    if (progenitorUuid.present) {
      map['progenitor_uuid'] = Variable<String>(progenitorUuid.value);
    }
    if (isSpendLimit.present) {
      map['is_spend_limit'] = Variable<bool>(isSpendLimit.value);
    }
    if (excludeFromAutoBalance.present) {
      map['exclude_from_auto_balance'] =
          Variable<bool>(excludeFromAutoBalance.value);
    }
    if (datePeriod.present) {
      map['date_period'] = Variable<String>(datePeriod.value);
    }
    if (datePeriodInterval.present) {
      map['date_period_interval'] = Variable<int>(datePeriodInterval.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('CategoriesCompanion(')
          ..write('createDate: $createDate, ')
          ..write('updateDate: $updateDate, ')
          ..write('deleteDate: $deleteDate, ')
          ..write('uuid: $uuid, ')
          ..write('name: $name, ')
          ..write('groupUuid: $groupUuid, ')
          ..write('parentUuid: $parentUuid, ')
          ..write('progenitorUuid: $progenitorUuid, ')
          ..write('isSpendLimit: $isSpendLimit, ')
          ..write('excludeFromAutoBalance: $excludeFromAutoBalance, ')
          ..write('datePeriod: $datePeriod, ')
          ..write('datePeriodInterval: $datePeriodInterval, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class CategoryAlerts extends Table
    with TableInfo<CategoryAlerts, CategoryAlertsData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  CategoryAlerts(this.attachedDatabase, [this._alias]);
  late final GeneratedColumn<String> uuid = GeneratedColumn<String>(
      'uuid', aliasedName, false,
      type: DriftSqlType.string,
      requiredDuringInsert: true,
      defaultConstraints: GeneratedColumn.constraintIsAlways(
          'REFERENCES categories (uuid) ON DELETE CASCADE'));
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  late final GeneratedColumn<int> color = GeneratedColumn<int>(
      'color', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  late final GeneratedColumn<double> percentDone = GeneratedColumn<double>(
      'percent_done', aliasedName, false,
      type: DriftSqlType.double, requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns => [uuid, id, color, percentDone];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'category_alerts';
  @override
  Set<GeneratedColumn> get $primaryKey => {uuid, id};
  @override
  CategoryAlertsData map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return CategoryAlertsData(
      uuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}uuid'])!,
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      color: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}color'])!,
      percentDone: attachedDatabase.typeMapping
          .read(DriftSqlType.double, data['${effectivePrefix}percent_done'])!,
    );
  }

  @override
  CategoryAlerts createAlias(String alias) {
    return CategoryAlerts(attachedDatabase, alias);
  }
}

class CategoryAlertsData extends DataClass
    implements Insertable<CategoryAlertsData> {
  final String uuid;
  final int id;
  final int color;
  final double percentDone;
  const CategoryAlertsData(
      {required this.uuid,
      required this.id,
      required this.color,
      required this.percentDone});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['uuid'] = Variable<String>(uuid);
    map['id'] = Variable<int>(id);
    map['color'] = Variable<int>(color);
    map['percent_done'] = Variable<double>(percentDone);
    return map;
  }

  CategoryAlertsCompanion toCompanion(bool nullToAbsent) {
    return CategoryAlertsCompanion(
      uuid: Value(uuid),
      id: Value(id),
      color: Value(color),
      percentDone: Value(percentDone),
    );
  }

  factory CategoryAlertsData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return CategoryAlertsData(
      uuid: serializer.fromJson<String>(json['uuid']),
      id: serializer.fromJson<int>(json['id']),
      color: serializer.fromJson<int>(json['color']),
      percentDone: serializer.fromJson<double>(json['percentDone']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'uuid': serializer.toJson<String>(uuid),
      'id': serializer.toJson<int>(id),
      'color': serializer.toJson<int>(color),
      'percentDone': serializer.toJson<double>(percentDone),
    };
  }

  CategoryAlertsData copyWith(
          {String? uuid, int? id, int? color, double? percentDone}) =>
      CategoryAlertsData(
        uuid: uuid ?? this.uuid,
        id: id ?? this.id,
        color: color ?? this.color,
        percentDone: percentDone ?? this.percentDone,
      );
  CategoryAlertsData copyWithCompanion(CategoryAlertsCompanion data) {
    return CategoryAlertsData(
      uuid: data.uuid.present ? data.uuid.value : this.uuid,
      id: data.id.present ? data.id.value : this.id,
      color: data.color.present ? data.color.value : this.color,
      percentDone:
          data.percentDone.present ? data.percentDone.value : this.percentDone,
    );
  }

  @override
  String toString() {
    return (StringBuffer('CategoryAlertsData(')
          ..write('uuid: $uuid, ')
          ..write('id: $id, ')
          ..write('color: $color, ')
          ..write('percentDone: $percentDone')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(uuid, id, color, percentDone);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is CategoryAlertsData &&
          other.uuid == this.uuid &&
          other.id == this.id &&
          other.color == this.color &&
          other.percentDone == this.percentDone);
}

class CategoryAlertsCompanion extends UpdateCompanion<CategoryAlertsData> {
  final Value<String> uuid;
  final Value<int> id;
  final Value<int> color;
  final Value<double> percentDone;
  final Value<int> rowid;
  const CategoryAlertsCompanion({
    this.uuid = const Value.absent(),
    this.id = const Value.absent(),
    this.color = const Value.absent(),
    this.percentDone = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  CategoryAlertsCompanion.insert({
    required String uuid,
    required int id,
    required int color,
    required double percentDone,
    this.rowid = const Value.absent(),
  })  : uuid = Value(uuid),
        id = Value(id),
        color = Value(color),
        percentDone = Value(percentDone);
  static Insertable<CategoryAlertsData> custom({
    Expression<String>? uuid,
    Expression<int>? id,
    Expression<int>? color,
    Expression<double>? percentDone,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (uuid != null) 'uuid': uuid,
      if (id != null) 'id': id,
      if (color != null) 'color': color,
      if (percentDone != null) 'percent_done': percentDone,
      if (rowid != null) 'rowid': rowid,
    });
  }

  CategoryAlertsCompanion copyWith(
      {Value<String>? uuid,
      Value<int>? id,
      Value<int>? color,
      Value<double>? percentDone,
      Value<int>? rowid}) {
    return CategoryAlertsCompanion(
      uuid: uuid ?? this.uuid,
      id: id ?? this.id,
      color: color ?? this.color,
      percentDone: percentDone ?? this.percentDone,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (uuid.present) {
      map['uuid'] = Variable<String>(uuid.value);
    }
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (color.present) {
      map['color'] = Variable<int>(color.value);
    }
    if (percentDone.present) {
      map['percent_done'] = Variable<double>(percentDone.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('CategoryAlertsCompanion(')
          ..write('uuid: $uuid, ')
          ..write('id: $id, ')
          ..write('color: $color, ')
          ..write('percentDone: $percentDone, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class CategoryLimits extends Table
    with TableInfo<CategoryLimits, CategoryLimitsData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  CategoryLimits(this.attachedDatabase, [this._alias]);
  late final GeneratedColumn<String> uuid = GeneratedColumn<String>(
      'uuid', aliasedName, false,
      type: DriftSqlType.string,
      requiredDuringInsert: true,
      defaultConstraints: GeneratedColumn.constraintIsAlways(
          'REFERENCES categories (uuid) ON DELETE CASCADE'));
  late final GeneratedColumn<int> amount = GeneratedColumn<int>(
      'amount', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  late final GeneratedColumn<bool> isRollover = GeneratedColumn<bool>(
      'is_rollover', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: true,
      defaultConstraints: GeneratedColumn.constraintIsAlways(
          'CHECK ("is_rollover" IN (0, 1))'));
  late final GeneratedColumn<String> currencyCode = GeneratedColumn<String>(
      'currency_code', aliasedName, false,
      additionalChecks:
          GeneratedColumn.checkTextLength(minTextLength: 3, maxTextLength: 3),
      type: DriftSqlType.string,
      requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns =>
      [uuid, amount, isRollover, currencyCode];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'category_limits';
  @override
  Set<GeneratedColumn> get $primaryKey => {uuid, currencyCode};
  @override
  CategoryLimitsData map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return CategoryLimitsData(
      uuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}uuid'])!,
      amount: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}amount'])!,
      isRollover: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}is_rollover'])!,
      currencyCode: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}currency_code'])!,
    );
  }

  @override
  CategoryLimits createAlias(String alias) {
    return CategoryLimits(attachedDatabase, alias);
  }
}

class CategoryLimitsData extends DataClass
    implements Insertable<CategoryLimitsData> {
  final String uuid;
  final int amount;
  final bool isRollover;
  final String currencyCode;
  const CategoryLimitsData(
      {required this.uuid,
      required this.amount,
      required this.isRollover,
      required this.currencyCode});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['uuid'] = Variable<String>(uuid);
    map['amount'] = Variable<int>(amount);
    map['is_rollover'] = Variable<bool>(isRollover);
    map['currency_code'] = Variable<String>(currencyCode);
    return map;
  }

  CategoryLimitsCompanion toCompanion(bool nullToAbsent) {
    return CategoryLimitsCompanion(
      uuid: Value(uuid),
      amount: Value(amount),
      isRollover: Value(isRollover),
      currencyCode: Value(currencyCode),
    );
  }

  factory CategoryLimitsData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return CategoryLimitsData(
      uuid: serializer.fromJson<String>(json['uuid']),
      amount: serializer.fromJson<int>(json['amount']),
      isRollover: serializer.fromJson<bool>(json['isRollover']),
      currencyCode: serializer.fromJson<String>(json['currencyCode']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'uuid': serializer.toJson<String>(uuid),
      'amount': serializer.toJson<int>(amount),
      'isRollover': serializer.toJson<bool>(isRollover),
      'currencyCode': serializer.toJson<String>(currencyCode),
    };
  }

  CategoryLimitsData copyWith(
          {String? uuid,
          int? amount,
          bool? isRollover,
          String? currencyCode}) =>
      CategoryLimitsData(
        uuid: uuid ?? this.uuid,
        amount: amount ?? this.amount,
        isRollover: isRollover ?? this.isRollover,
        currencyCode: currencyCode ?? this.currencyCode,
      );
  CategoryLimitsData copyWithCompanion(CategoryLimitsCompanion data) {
    return CategoryLimitsData(
      uuid: data.uuid.present ? data.uuid.value : this.uuid,
      amount: data.amount.present ? data.amount.value : this.amount,
      isRollover:
          data.isRollover.present ? data.isRollover.value : this.isRollover,
      currencyCode: data.currencyCode.present
          ? data.currencyCode.value
          : this.currencyCode,
    );
  }

  @override
  String toString() {
    return (StringBuffer('CategoryLimitsData(')
          ..write('uuid: $uuid, ')
          ..write('amount: $amount, ')
          ..write('isRollover: $isRollover, ')
          ..write('currencyCode: $currencyCode')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(uuid, amount, isRollover, currencyCode);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is CategoryLimitsData &&
          other.uuid == this.uuid &&
          other.amount == this.amount &&
          other.isRollover == this.isRollover &&
          other.currencyCode == this.currencyCode);
}

class CategoryLimitsCompanion extends UpdateCompanion<CategoryLimitsData> {
  final Value<String> uuid;
  final Value<int> amount;
  final Value<bool> isRollover;
  final Value<String> currencyCode;
  final Value<int> rowid;
  const CategoryLimitsCompanion({
    this.uuid = const Value.absent(),
    this.amount = const Value.absent(),
    this.isRollover = const Value.absent(),
    this.currencyCode = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  CategoryLimitsCompanion.insert({
    required String uuid,
    required int amount,
    required bool isRollover,
    required String currencyCode,
    this.rowid = const Value.absent(),
  })  : uuid = Value(uuid),
        amount = Value(amount),
        isRollover = Value(isRollover),
        currencyCode = Value(currencyCode);
  static Insertable<CategoryLimitsData> custom({
    Expression<String>? uuid,
    Expression<int>? amount,
    Expression<bool>? isRollover,
    Expression<String>? currencyCode,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (uuid != null) 'uuid': uuid,
      if (amount != null) 'amount': amount,
      if (isRollover != null) 'is_rollover': isRollover,
      if (currencyCode != null) 'currency_code': currencyCode,
      if (rowid != null) 'rowid': rowid,
    });
  }

  CategoryLimitsCompanion copyWith(
      {Value<String>? uuid,
      Value<int>? amount,
      Value<bool>? isRollover,
      Value<String>? currencyCode,
      Value<int>? rowid}) {
    return CategoryLimitsCompanion(
      uuid: uuid ?? this.uuid,
      amount: amount ?? this.amount,
      isRollover: isRollover ?? this.isRollover,
      currencyCode: currencyCode ?? this.currencyCode,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (uuid.present) {
      map['uuid'] = Variable<String>(uuid.value);
    }
    if (amount.present) {
      map['amount'] = Variable<int>(amount.value);
    }
    if (isRollover.present) {
      map['is_rollover'] = Variable<bool>(isRollover.value);
    }
    if (currencyCode.present) {
      map['currency_code'] = Variable<String>(currencyCode.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('CategoryLimitsCompanion(')
          ..write('uuid: $uuid, ')
          ..write('amount: $amount, ')
          ..write('isRollover: $isRollover, ')
          ..write('currencyCode: $currencyCode, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class CategoryBudgets extends Table
    with TableInfo<CategoryBudgets, CategoryBudgetsData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  CategoryBudgets(this.attachedDatabase, [this._alias]);
  late final GeneratedColumn<DateTime> createDate = GeneratedColumn<DateTime>(
      'create_date', aliasedName, false,
      type: DriftSqlType.dateTime,
      requiredDuringInsert: false,
      defaultValue: currentDateAndTime);
  late final GeneratedColumn<DateTime> updateDate = GeneratedColumn<DateTime>(
      'update_date', aliasedName, false,
      type: DriftSqlType.dateTime,
      requiredDuringInsert: false,
      defaultValue: currentDateAndTime);
  late final GeneratedColumn<DateTime> deleteDate = GeneratedColumn<DateTime>(
      'delete_date', aliasedName, true,
      type: DriftSqlType.dateTime, requiredDuringInsert: false);
  late final GeneratedColumn<String> uuid = GeneratedColumn<String>(
      'uuid', aliasedName, false,
      type: DriftSqlType.string,
      requiredDuringInsert: true,
      defaultConstraints: GeneratedColumn.constraintIsAlways(
          'REFERENCES categories (uuid) ON DELETE CASCADE'));
  late final GeneratedColumn<String> syncId = GeneratedColumn<String>(
      'sync_id', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  late final GeneratedColumn<String> currencyCode = GeneratedColumn<String>(
      'currency_code', aliasedName, false,
      additionalChecks:
          GeneratedColumn.checkTextLength(minTextLength: 3, maxTextLength: 3),
      type: DriftSqlType.string,
      requiredDuringInsert: true);
  late final GeneratedColumn<DateTime> startDate = GeneratedColumn<DateTime>(
      'start_date', aliasedName, false,
      type: DriftSqlType.dateTime, requiredDuringInsert: true);
  late final GeneratedColumn<DateTime> endDate = GeneratedColumn<DateTime>(
      'end_date', aliasedName, false,
      type: DriftSqlType.dateTime, requiredDuringInsert: true);
  late final GeneratedColumn<int> amount = GeneratedColumn<int>(
      'amount', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  late final GeneratedColumn<bool> edited = GeneratedColumn<bool>(
      'edited', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: true,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('CHECK ("edited" IN (0, 1))'));
  @override
  List<GeneratedColumn> get $columns => [
        createDate,
        updateDate,
        deleteDate,
        uuid,
        syncId,
        currencyCode,
        startDate,
        endDate,
        amount,
        edited
      ];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'category_budgets';
  @override
  Set<GeneratedColumn> get $primaryKey => {uuid, currencyCode, startDate};
  @override
  CategoryBudgetsData map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return CategoryBudgetsData(
      createDate: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}create_date'])!,
      updateDate: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}update_date'])!,
      deleteDate: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}delete_date']),
      uuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}uuid'])!,
      syncId: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}sync_id']),
      currencyCode: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}currency_code'])!,
      startDate: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}start_date'])!,
      endDate: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}end_date'])!,
      amount: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}amount'])!,
      edited: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}edited'])!,
    );
  }

  @override
  CategoryBudgets createAlias(String alias) {
    return CategoryBudgets(attachedDatabase, alias);
  }
}

class CategoryBudgetsData extends DataClass
    implements Insertable<CategoryBudgetsData> {
  final DateTime createDate;
  final DateTime updateDate;
  final DateTime? deleteDate;
  final String uuid;
  final String? syncId;
  final String currencyCode;
  final DateTime startDate;
  final DateTime endDate;
  final int amount;
  final bool edited;
  const CategoryBudgetsData(
      {required this.createDate,
      required this.updateDate,
      this.deleteDate,
      required this.uuid,
      this.syncId,
      required this.currencyCode,
      required this.startDate,
      required this.endDate,
      required this.amount,
      required this.edited});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['create_date'] = Variable<DateTime>(createDate);
    map['update_date'] = Variable<DateTime>(updateDate);
    if (!nullToAbsent || deleteDate != null) {
      map['delete_date'] = Variable<DateTime>(deleteDate);
    }
    map['uuid'] = Variable<String>(uuid);
    if (!nullToAbsent || syncId != null) {
      map['sync_id'] = Variable<String>(syncId);
    }
    map['currency_code'] = Variable<String>(currencyCode);
    map['start_date'] = Variable<DateTime>(startDate);
    map['end_date'] = Variable<DateTime>(endDate);
    map['amount'] = Variable<int>(amount);
    map['edited'] = Variable<bool>(edited);
    return map;
  }

  CategoryBudgetsCompanion toCompanion(bool nullToAbsent) {
    return CategoryBudgetsCompanion(
      createDate: Value(createDate),
      updateDate: Value(updateDate),
      deleteDate: deleteDate == null && nullToAbsent
          ? const Value.absent()
          : Value(deleteDate),
      uuid: Value(uuid),
      syncId:
          syncId == null && nullToAbsent ? const Value.absent() : Value(syncId),
      currencyCode: Value(currencyCode),
      startDate: Value(startDate),
      endDate: Value(endDate),
      amount: Value(amount),
      edited: Value(edited),
    );
  }

  factory CategoryBudgetsData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return CategoryBudgetsData(
      createDate: serializer.fromJson<DateTime>(json['createDate']),
      updateDate: serializer.fromJson<DateTime>(json['updateDate']),
      deleteDate: serializer.fromJson<DateTime?>(json['deleteDate']),
      uuid: serializer.fromJson<String>(json['uuid']),
      syncId: serializer.fromJson<String?>(json['syncId']),
      currencyCode: serializer.fromJson<String>(json['currencyCode']),
      startDate: serializer.fromJson<DateTime>(json['startDate']),
      endDate: serializer.fromJson<DateTime>(json['endDate']),
      amount: serializer.fromJson<int>(json['amount']),
      edited: serializer.fromJson<bool>(json['edited']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'createDate': serializer.toJson<DateTime>(createDate),
      'updateDate': serializer.toJson<DateTime>(updateDate),
      'deleteDate': serializer.toJson<DateTime?>(deleteDate),
      'uuid': serializer.toJson<String>(uuid),
      'syncId': serializer.toJson<String?>(syncId),
      'currencyCode': serializer.toJson<String>(currencyCode),
      'startDate': serializer.toJson<DateTime>(startDate),
      'endDate': serializer.toJson<DateTime>(endDate),
      'amount': serializer.toJson<int>(amount),
      'edited': serializer.toJson<bool>(edited),
    };
  }

  CategoryBudgetsData copyWith(
          {DateTime? createDate,
          DateTime? updateDate,
          Value<DateTime?> deleteDate = const Value.absent(),
          String? uuid,
          Value<String?> syncId = const Value.absent(),
          String? currencyCode,
          DateTime? startDate,
          DateTime? endDate,
          int? amount,
          bool? edited}) =>
      CategoryBudgetsData(
        createDate: createDate ?? this.createDate,
        updateDate: updateDate ?? this.updateDate,
        deleteDate: deleteDate.present ? deleteDate.value : this.deleteDate,
        uuid: uuid ?? this.uuid,
        syncId: syncId.present ? syncId.value : this.syncId,
        currencyCode: currencyCode ?? this.currencyCode,
        startDate: startDate ?? this.startDate,
        endDate: endDate ?? this.endDate,
        amount: amount ?? this.amount,
        edited: edited ?? this.edited,
      );
  CategoryBudgetsData copyWithCompanion(CategoryBudgetsCompanion data) {
    return CategoryBudgetsData(
      createDate:
          data.createDate.present ? data.createDate.value : this.createDate,
      updateDate:
          data.updateDate.present ? data.updateDate.value : this.updateDate,
      deleteDate:
          data.deleteDate.present ? data.deleteDate.value : this.deleteDate,
      uuid: data.uuid.present ? data.uuid.value : this.uuid,
      syncId: data.syncId.present ? data.syncId.value : this.syncId,
      currencyCode: data.currencyCode.present
          ? data.currencyCode.value
          : this.currencyCode,
      startDate: data.startDate.present ? data.startDate.value : this.startDate,
      endDate: data.endDate.present ? data.endDate.value : this.endDate,
      amount: data.amount.present ? data.amount.value : this.amount,
      edited: data.edited.present ? data.edited.value : this.edited,
    );
  }

  @override
  String toString() {
    return (StringBuffer('CategoryBudgetsData(')
          ..write('createDate: $createDate, ')
          ..write('updateDate: $updateDate, ')
          ..write('deleteDate: $deleteDate, ')
          ..write('uuid: $uuid, ')
          ..write('syncId: $syncId, ')
          ..write('currencyCode: $currencyCode, ')
          ..write('startDate: $startDate, ')
          ..write('endDate: $endDate, ')
          ..write('amount: $amount, ')
          ..write('edited: $edited')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(createDate, updateDate, deleteDate, uuid,
      syncId, currencyCode, startDate, endDate, amount, edited);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is CategoryBudgetsData &&
          other.createDate == this.createDate &&
          other.updateDate == this.updateDate &&
          other.deleteDate == this.deleteDate &&
          other.uuid == this.uuid &&
          other.syncId == this.syncId &&
          other.currencyCode == this.currencyCode &&
          other.startDate == this.startDate &&
          other.endDate == this.endDate &&
          other.amount == this.amount &&
          other.edited == this.edited);
}

class CategoryBudgetsCompanion extends UpdateCompanion<CategoryBudgetsData> {
  final Value<DateTime> createDate;
  final Value<DateTime> updateDate;
  final Value<DateTime?> deleteDate;
  final Value<String> uuid;
  final Value<String?> syncId;
  final Value<String> currencyCode;
  final Value<DateTime> startDate;
  final Value<DateTime> endDate;
  final Value<int> amount;
  final Value<bool> edited;
  final Value<int> rowid;
  const CategoryBudgetsCompanion({
    this.createDate = const Value.absent(),
    this.updateDate = const Value.absent(),
    this.deleteDate = const Value.absent(),
    this.uuid = const Value.absent(),
    this.syncId = const Value.absent(),
    this.currencyCode = const Value.absent(),
    this.startDate = const Value.absent(),
    this.endDate = const Value.absent(),
    this.amount = const Value.absent(),
    this.edited = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  CategoryBudgetsCompanion.insert({
    this.createDate = const Value.absent(),
    this.updateDate = const Value.absent(),
    this.deleteDate = const Value.absent(),
    required String uuid,
    this.syncId = const Value.absent(),
    required String currencyCode,
    required DateTime startDate,
    required DateTime endDate,
    required int amount,
    required bool edited,
    this.rowid = const Value.absent(),
  })  : uuid = Value(uuid),
        currencyCode = Value(currencyCode),
        startDate = Value(startDate),
        endDate = Value(endDate),
        amount = Value(amount),
        edited = Value(edited);
  static Insertable<CategoryBudgetsData> custom({
    Expression<DateTime>? createDate,
    Expression<DateTime>? updateDate,
    Expression<DateTime>? deleteDate,
    Expression<String>? uuid,
    Expression<String>? syncId,
    Expression<String>? currencyCode,
    Expression<DateTime>? startDate,
    Expression<DateTime>? endDate,
    Expression<int>? amount,
    Expression<bool>? edited,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (createDate != null) 'create_date': createDate,
      if (updateDate != null) 'update_date': updateDate,
      if (deleteDate != null) 'delete_date': deleteDate,
      if (uuid != null) 'uuid': uuid,
      if (syncId != null) 'sync_id': syncId,
      if (currencyCode != null) 'currency_code': currencyCode,
      if (startDate != null) 'start_date': startDate,
      if (endDate != null) 'end_date': endDate,
      if (amount != null) 'amount': amount,
      if (edited != null) 'edited': edited,
      if (rowid != null) 'rowid': rowid,
    });
  }

  CategoryBudgetsCompanion copyWith(
      {Value<DateTime>? createDate,
      Value<DateTime>? updateDate,
      Value<DateTime?>? deleteDate,
      Value<String>? uuid,
      Value<String?>? syncId,
      Value<String>? currencyCode,
      Value<DateTime>? startDate,
      Value<DateTime>? endDate,
      Value<int>? amount,
      Value<bool>? edited,
      Value<int>? rowid}) {
    return CategoryBudgetsCompanion(
      createDate: createDate ?? this.createDate,
      updateDate: updateDate ?? this.updateDate,
      deleteDate: deleteDate ?? this.deleteDate,
      uuid: uuid ?? this.uuid,
      syncId: syncId ?? this.syncId,
      currencyCode: currencyCode ?? this.currencyCode,
      startDate: startDate ?? this.startDate,
      endDate: endDate ?? this.endDate,
      amount: amount ?? this.amount,
      edited: edited ?? this.edited,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (createDate.present) {
      map['create_date'] = Variable<DateTime>(createDate.value);
    }
    if (updateDate.present) {
      map['update_date'] = Variable<DateTime>(updateDate.value);
    }
    if (deleteDate.present) {
      map['delete_date'] = Variable<DateTime>(deleteDate.value);
    }
    if (uuid.present) {
      map['uuid'] = Variable<String>(uuid.value);
    }
    if (syncId.present) {
      map['sync_id'] = Variable<String>(syncId.value);
    }
    if (currencyCode.present) {
      map['currency_code'] = Variable<String>(currencyCode.value);
    }
    if (startDate.present) {
      map['start_date'] = Variable<DateTime>(startDate.value);
    }
    if (endDate.present) {
      map['end_date'] = Variable<DateTime>(endDate.value);
    }
    if (amount.present) {
      map['amount'] = Variable<int>(amount.value);
    }
    if (edited.present) {
      map['edited'] = Variable<bool>(edited.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('CategoryBudgetsCompanion(')
          ..write('createDate: $createDate, ')
          ..write('updateDate: $updateDate, ')
          ..write('deleteDate: $deleteDate, ')
          ..write('uuid: $uuid, ')
          ..write('syncId: $syncId, ')
          ..write('currencyCode: $currencyCode, ')
          ..write('startDate: $startDate, ')
          ..write('endDate: $endDate, ')
          ..write('amount: $amount, ')
          ..write('edited: $edited, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class Currencies extends Table with TableInfo<Currencies, CurrenciesData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  Currencies(this.attachedDatabase, [this._alias]);
  late final GeneratedColumn<DateTime> createDate = GeneratedColumn<DateTime>(
      'create_date', aliasedName, false,
      type: DriftSqlType.dateTime,
      requiredDuringInsert: false,
      defaultValue: currentDateAndTime);
  late final GeneratedColumn<DateTime> updateDate = GeneratedColumn<DateTime>(
      'update_date', aliasedName, false,
      type: DriftSqlType.dateTime,
      requiredDuringInsert: false,
      defaultValue: currentDateAndTime);
  late final GeneratedColumn<DateTime> deleteDate = GeneratedColumn<DateTime>(
      'delete_date', aliasedName, true,
      type: DriftSqlType.dateTime, requiredDuringInsert: false);
  late final GeneratedColumn<String> uuid = GeneratedColumn<String>(
      'uuid', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  late final GeneratedColumn<String> name =
      GeneratedColumn<String>('name', aliasedName, false,
          additionalChecks: GeneratedColumn.checkTextLength(
            minTextLength: 1,
          ),
          type: DriftSqlType.string,
          requiredDuringInsert: true);
  late final GeneratedColumn<String> code = GeneratedColumn<String>(
      'code', aliasedName, false,
      additionalChecks:
          GeneratedColumn.checkTextLength(minTextLength: 1, maxTextLength: 3),
      type: DriftSqlType.string,
      requiredDuringInsert: true);
  late final GeneratedColumn<String> symbol = GeneratedColumn<String>(
      'symbol', aliasedName, false,
      additionalChecks:
          GeneratedColumn.checkTextLength(minTextLength: 1, maxTextLength: 4),
      type: DriftSqlType.string,
      requiredDuringInsert: true);
  late final GeneratedColumn<int> decimalDigits = GeneratedColumn<int>(
      'decimal_digits', aliasedName, false,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultValue: const Constant(2));
  @override
  List<GeneratedColumn> get $columns => [
        createDate,
        updateDate,
        deleteDate,
        uuid,
        name,
        code,
        symbol,
        decimalDigits
      ];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'currencies';
  @override
  Set<GeneratedColumn> get $primaryKey => {code};
  @override
  CurrenciesData map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return CurrenciesData(
      createDate: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}create_date'])!,
      updateDate: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}update_date'])!,
      deleteDate: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}delete_date']),
      uuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}uuid'])!,
      name: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}name'])!,
      code: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}code'])!,
      symbol: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}symbol'])!,
      decimalDigits: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}decimal_digits'])!,
    );
  }

  @override
  Currencies createAlias(String alias) {
    return Currencies(attachedDatabase, alias);
  }
}

class CurrenciesData extends DataClass implements Insertable<CurrenciesData> {
  final DateTime createDate;
  final DateTime updateDate;
  final DateTime? deleteDate;
  final String uuid;
  final String name;
  final String code;
  final String symbol;
  final int decimalDigits;
  const CurrenciesData(
      {required this.createDate,
      required this.updateDate,
      this.deleteDate,
      required this.uuid,
      required this.name,
      required this.code,
      required this.symbol,
      required this.decimalDigits});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['create_date'] = Variable<DateTime>(createDate);
    map['update_date'] = Variable<DateTime>(updateDate);
    if (!nullToAbsent || deleteDate != null) {
      map['delete_date'] = Variable<DateTime>(deleteDate);
    }
    map['uuid'] = Variable<String>(uuid);
    map['name'] = Variable<String>(name);
    map['code'] = Variable<String>(code);
    map['symbol'] = Variable<String>(symbol);
    map['decimal_digits'] = Variable<int>(decimalDigits);
    return map;
  }

  CurrenciesCompanion toCompanion(bool nullToAbsent) {
    return CurrenciesCompanion(
      createDate: Value(createDate),
      updateDate: Value(updateDate),
      deleteDate: deleteDate == null && nullToAbsent
          ? const Value.absent()
          : Value(deleteDate),
      uuid: Value(uuid),
      name: Value(name),
      code: Value(code),
      symbol: Value(symbol),
      decimalDigits: Value(decimalDigits),
    );
  }

  factory CurrenciesData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return CurrenciesData(
      createDate: serializer.fromJson<DateTime>(json['createDate']),
      updateDate: serializer.fromJson<DateTime>(json['updateDate']),
      deleteDate: serializer.fromJson<DateTime?>(json['deleteDate']),
      uuid: serializer.fromJson<String>(json['uuid']),
      name: serializer.fromJson<String>(json['name']),
      code: serializer.fromJson<String>(json['code']),
      symbol: serializer.fromJson<String>(json['symbol']),
      decimalDigits: serializer.fromJson<int>(json['decimalDigits']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'createDate': serializer.toJson<DateTime>(createDate),
      'updateDate': serializer.toJson<DateTime>(updateDate),
      'deleteDate': serializer.toJson<DateTime?>(deleteDate),
      'uuid': serializer.toJson<String>(uuid),
      'name': serializer.toJson<String>(name),
      'code': serializer.toJson<String>(code),
      'symbol': serializer.toJson<String>(symbol),
      'decimalDigits': serializer.toJson<int>(decimalDigits),
    };
  }

  CurrenciesData copyWith(
          {DateTime? createDate,
          DateTime? updateDate,
          Value<DateTime?> deleteDate = const Value.absent(),
          String? uuid,
          String? name,
          String? code,
          String? symbol,
          int? decimalDigits}) =>
      CurrenciesData(
        createDate: createDate ?? this.createDate,
        updateDate: updateDate ?? this.updateDate,
        deleteDate: deleteDate.present ? deleteDate.value : this.deleteDate,
        uuid: uuid ?? this.uuid,
        name: name ?? this.name,
        code: code ?? this.code,
        symbol: symbol ?? this.symbol,
        decimalDigits: decimalDigits ?? this.decimalDigits,
      );
  CurrenciesData copyWithCompanion(CurrenciesCompanion data) {
    return CurrenciesData(
      createDate:
          data.createDate.present ? data.createDate.value : this.createDate,
      updateDate:
          data.updateDate.present ? data.updateDate.value : this.updateDate,
      deleteDate:
          data.deleteDate.present ? data.deleteDate.value : this.deleteDate,
      uuid: data.uuid.present ? data.uuid.value : this.uuid,
      name: data.name.present ? data.name.value : this.name,
      code: data.code.present ? data.code.value : this.code,
      symbol: data.symbol.present ? data.symbol.value : this.symbol,
      decimalDigits: data.decimalDigits.present
          ? data.decimalDigits.value
          : this.decimalDigits,
    );
  }

  @override
  String toString() {
    return (StringBuffer('CurrenciesData(')
          ..write('createDate: $createDate, ')
          ..write('updateDate: $updateDate, ')
          ..write('deleteDate: $deleteDate, ')
          ..write('uuid: $uuid, ')
          ..write('name: $name, ')
          ..write('code: $code, ')
          ..write('symbol: $symbol, ')
          ..write('decimalDigits: $decimalDigits')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(createDate, updateDate, deleteDate, uuid,
      name, code, symbol, decimalDigits);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is CurrenciesData &&
          other.createDate == this.createDate &&
          other.updateDate == this.updateDate &&
          other.deleteDate == this.deleteDate &&
          other.uuid == this.uuid &&
          other.name == this.name &&
          other.code == this.code &&
          other.symbol == this.symbol &&
          other.decimalDigits == this.decimalDigits);
}

class CurrenciesCompanion extends UpdateCompanion<CurrenciesData> {
  final Value<DateTime> createDate;
  final Value<DateTime> updateDate;
  final Value<DateTime?> deleteDate;
  final Value<String> uuid;
  final Value<String> name;
  final Value<String> code;
  final Value<String> symbol;
  final Value<int> decimalDigits;
  final Value<int> rowid;
  const CurrenciesCompanion({
    this.createDate = const Value.absent(),
    this.updateDate = const Value.absent(),
    this.deleteDate = const Value.absent(),
    this.uuid = const Value.absent(),
    this.name = const Value.absent(),
    this.code = const Value.absent(),
    this.symbol = const Value.absent(),
    this.decimalDigits = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  CurrenciesCompanion.insert({
    this.createDate = const Value.absent(),
    this.updateDate = const Value.absent(),
    this.deleteDate = const Value.absent(),
    required String uuid,
    required String name,
    required String code,
    required String symbol,
    this.decimalDigits = const Value.absent(),
    this.rowid = const Value.absent(),
  })  : uuid = Value(uuid),
        name = Value(name),
        code = Value(code),
        symbol = Value(symbol);
  static Insertable<CurrenciesData> custom({
    Expression<DateTime>? createDate,
    Expression<DateTime>? updateDate,
    Expression<DateTime>? deleteDate,
    Expression<String>? uuid,
    Expression<String>? name,
    Expression<String>? code,
    Expression<String>? symbol,
    Expression<int>? decimalDigits,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (createDate != null) 'create_date': createDate,
      if (updateDate != null) 'update_date': updateDate,
      if (deleteDate != null) 'delete_date': deleteDate,
      if (uuid != null) 'uuid': uuid,
      if (name != null) 'name': name,
      if (code != null) 'code': code,
      if (symbol != null) 'symbol': symbol,
      if (decimalDigits != null) 'decimal_digits': decimalDigits,
      if (rowid != null) 'rowid': rowid,
    });
  }

  CurrenciesCompanion copyWith(
      {Value<DateTime>? createDate,
      Value<DateTime>? updateDate,
      Value<DateTime?>? deleteDate,
      Value<String>? uuid,
      Value<String>? name,
      Value<String>? code,
      Value<String>? symbol,
      Value<int>? decimalDigits,
      Value<int>? rowid}) {
    return CurrenciesCompanion(
      createDate: createDate ?? this.createDate,
      updateDate: updateDate ?? this.updateDate,
      deleteDate: deleteDate ?? this.deleteDate,
      uuid: uuid ?? this.uuid,
      name: name ?? this.name,
      code: code ?? this.code,
      symbol: symbol ?? this.symbol,
      decimalDigits: decimalDigits ?? this.decimalDigits,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (createDate.present) {
      map['create_date'] = Variable<DateTime>(createDate.value);
    }
    if (updateDate.present) {
      map['update_date'] = Variable<DateTime>(updateDate.value);
    }
    if (deleteDate.present) {
      map['delete_date'] = Variable<DateTime>(deleteDate.value);
    }
    if (uuid.present) {
      map['uuid'] = Variable<String>(uuid.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (code.present) {
      map['code'] = Variable<String>(code.value);
    }
    if (symbol.present) {
      map['symbol'] = Variable<String>(symbol.value);
    }
    if (decimalDigits.present) {
      map['decimal_digits'] = Variable<int>(decimalDigits.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('CurrenciesCompanion(')
          ..write('createDate: $createDate, ')
          ..write('updateDate: $updateDate, ')
          ..write('deleteDate: $deleteDate, ')
          ..write('uuid: $uuid, ')
          ..write('name: $name, ')
          ..write('code: $code, ')
          ..write('symbol: $symbol, ')
          ..write('decimalDigits: $decimalDigits, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class Devices extends Table with TableInfo<Devices, DevicesData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  Devices(this.attachedDatabase, [this._alias]);
  late final GeneratedColumn<DateTime> createDate = GeneratedColumn<DateTime>(
      'create_date', aliasedName, false,
      type: DriftSqlType.dateTime,
      requiredDuringInsert: false,
      defaultValue: currentDateAndTime);
  late final GeneratedColumn<DateTime> updateDate = GeneratedColumn<DateTime>(
      'update_date', aliasedName, false,
      type: DriftSqlType.dateTime,
      requiredDuringInsert: false,
      defaultValue: currentDateAndTime);
  late final GeneratedColumn<DateTime> deleteDate = GeneratedColumn<DateTime>(
      'delete_date', aliasedName, true,
      type: DriftSqlType.dateTime, requiredDuringInsert: false);
  late final GeneratedColumn<String> uuid = GeneratedColumn<String>(
      'uuid', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  late final GeneratedColumn<String> name =
      GeneratedColumn<String>('name', aliasedName, false,
          additionalChecks: GeneratedColumn.checkTextLength(
            minTextLength: 1,
          ),
          type: DriftSqlType.string,
          requiredDuringInsert: true);
  late final GeneratedColumn<DateTime> syncDate = GeneratedColumn<DateTime>(
      'sync_date', aliasedName, true,
      type: DriftSqlType.dateTime, requiredDuringInsert: false);
  late final GeneratedColumn<bool> thisDevice = GeneratedColumn<bool>(
      'this_device', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('CHECK ("this_device" IN (0, 1))'),
      defaultValue: const Constant(false));
  @override
  List<GeneratedColumn> get $columns =>
      [createDate, updateDate, deleteDate, uuid, name, syncDate, thisDevice];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'devices';
  @override
  Set<GeneratedColumn> get $primaryKey => {uuid};
  @override
  DevicesData map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return DevicesData(
      createDate: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}create_date'])!,
      updateDate: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}update_date'])!,
      deleteDate: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}delete_date']),
      uuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}uuid'])!,
      name: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}name'])!,
      syncDate: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}sync_date']),
      thisDevice: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}this_device'])!,
    );
  }

  @override
  Devices createAlias(String alias) {
    return Devices(attachedDatabase, alias);
  }
}

class DevicesData extends DataClass implements Insertable<DevicesData> {
  final DateTime createDate;
  final DateTime updateDate;
  final DateTime? deleteDate;
  final String uuid;
  final String name;
  final DateTime? syncDate;
  final bool thisDevice;
  const DevicesData(
      {required this.createDate,
      required this.updateDate,
      this.deleteDate,
      required this.uuid,
      required this.name,
      this.syncDate,
      required this.thisDevice});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['create_date'] = Variable<DateTime>(createDate);
    map['update_date'] = Variable<DateTime>(updateDate);
    if (!nullToAbsent || deleteDate != null) {
      map['delete_date'] = Variable<DateTime>(deleteDate);
    }
    map['uuid'] = Variable<String>(uuid);
    map['name'] = Variable<String>(name);
    if (!nullToAbsent || syncDate != null) {
      map['sync_date'] = Variable<DateTime>(syncDate);
    }
    map['this_device'] = Variable<bool>(thisDevice);
    return map;
  }

  DevicesCompanion toCompanion(bool nullToAbsent) {
    return DevicesCompanion(
      createDate: Value(createDate),
      updateDate: Value(updateDate),
      deleteDate: deleteDate == null && nullToAbsent
          ? const Value.absent()
          : Value(deleteDate),
      uuid: Value(uuid),
      name: Value(name),
      syncDate: syncDate == null && nullToAbsent
          ? const Value.absent()
          : Value(syncDate),
      thisDevice: Value(thisDevice),
    );
  }

  factory DevicesData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return DevicesData(
      createDate: serializer.fromJson<DateTime>(json['createDate']),
      updateDate: serializer.fromJson<DateTime>(json['updateDate']),
      deleteDate: serializer.fromJson<DateTime?>(json['deleteDate']),
      uuid: serializer.fromJson<String>(json['uuid']),
      name: serializer.fromJson<String>(json['name']),
      syncDate: serializer.fromJson<DateTime?>(json['syncDate']),
      thisDevice: serializer.fromJson<bool>(json['thisDevice']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'createDate': serializer.toJson<DateTime>(createDate),
      'updateDate': serializer.toJson<DateTime>(updateDate),
      'deleteDate': serializer.toJson<DateTime?>(deleteDate),
      'uuid': serializer.toJson<String>(uuid),
      'name': serializer.toJson<String>(name),
      'syncDate': serializer.toJson<DateTime?>(syncDate),
      'thisDevice': serializer.toJson<bool>(thisDevice),
    };
  }

  DevicesData copyWith(
          {DateTime? createDate,
          DateTime? updateDate,
          Value<DateTime?> deleteDate = const Value.absent(),
          String? uuid,
          String? name,
          Value<DateTime?> syncDate = const Value.absent(),
          bool? thisDevice}) =>
      DevicesData(
        createDate: createDate ?? this.createDate,
        updateDate: updateDate ?? this.updateDate,
        deleteDate: deleteDate.present ? deleteDate.value : this.deleteDate,
        uuid: uuid ?? this.uuid,
        name: name ?? this.name,
        syncDate: syncDate.present ? syncDate.value : this.syncDate,
        thisDevice: thisDevice ?? this.thisDevice,
      );
  DevicesData copyWithCompanion(DevicesCompanion data) {
    return DevicesData(
      createDate:
          data.createDate.present ? data.createDate.value : this.createDate,
      updateDate:
          data.updateDate.present ? data.updateDate.value : this.updateDate,
      deleteDate:
          data.deleteDate.present ? data.deleteDate.value : this.deleteDate,
      uuid: data.uuid.present ? data.uuid.value : this.uuid,
      name: data.name.present ? data.name.value : this.name,
      syncDate: data.syncDate.present ? data.syncDate.value : this.syncDate,
      thisDevice:
          data.thisDevice.present ? data.thisDevice.value : this.thisDevice,
    );
  }

  @override
  String toString() {
    return (StringBuffer('DevicesData(')
          ..write('createDate: $createDate, ')
          ..write('updateDate: $updateDate, ')
          ..write('deleteDate: $deleteDate, ')
          ..write('uuid: $uuid, ')
          ..write('name: $name, ')
          ..write('syncDate: $syncDate, ')
          ..write('thisDevice: $thisDevice')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(
      createDate, updateDate, deleteDate, uuid, name, syncDate, thisDevice);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is DevicesData &&
          other.createDate == this.createDate &&
          other.updateDate == this.updateDate &&
          other.deleteDate == this.deleteDate &&
          other.uuid == this.uuid &&
          other.name == this.name &&
          other.syncDate == this.syncDate &&
          other.thisDevice == this.thisDevice);
}

class DevicesCompanion extends UpdateCompanion<DevicesData> {
  final Value<DateTime> createDate;
  final Value<DateTime> updateDate;
  final Value<DateTime?> deleteDate;
  final Value<String> uuid;
  final Value<String> name;
  final Value<DateTime?> syncDate;
  final Value<bool> thisDevice;
  final Value<int> rowid;
  const DevicesCompanion({
    this.createDate = const Value.absent(),
    this.updateDate = const Value.absent(),
    this.deleteDate = const Value.absent(),
    this.uuid = const Value.absent(),
    this.name = const Value.absent(),
    this.syncDate = const Value.absent(),
    this.thisDevice = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  DevicesCompanion.insert({
    this.createDate = const Value.absent(),
    this.updateDate = const Value.absent(),
    this.deleteDate = const Value.absent(),
    required String uuid,
    required String name,
    this.syncDate = const Value.absent(),
    this.thisDevice = const Value.absent(),
    this.rowid = const Value.absent(),
  })  : uuid = Value(uuid),
        name = Value(name);
  static Insertable<DevicesData> custom({
    Expression<DateTime>? createDate,
    Expression<DateTime>? updateDate,
    Expression<DateTime>? deleteDate,
    Expression<String>? uuid,
    Expression<String>? name,
    Expression<DateTime>? syncDate,
    Expression<bool>? thisDevice,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (createDate != null) 'create_date': createDate,
      if (updateDate != null) 'update_date': updateDate,
      if (deleteDate != null) 'delete_date': deleteDate,
      if (uuid != null) 'uuid': uuid,
      if (name != null) 'name': name,
      if (syncDate != null) 'sync_date': syncDate,
      if (thisDevice != null) 'this_device': thisDevice,
      if (rowid != null) 'rowid': rowid,
    });
  }

  DevicesCompanion copyWith(
      {Value<DateTime>? createDate,
      Value<DateTime>? updateDate,
      Value<DateTime?>? deleteDate,
      Value<String>? uuid,
      Value<String>? name,
      Value<DateTime?>? syncDate,
      Value<bool>? thisDevice,
      Value<int>? rowid}) {
    return DevicesCompanion(
      createDate: createDate ?? this.createDate,
      updateDate: updateDate ?? this.updateDate,
      deleteDate: deleteDate ?? this.deleteDate,
      uuid: uuid ?? this.uuid,
      name: name ?? this.name,
      syncDate: syncDate ?? this.syncDate,
      thisDevice: thisDevice ?? this.thisDevice,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (createDate.present) {
      map['create_date'] = Variable<DateTime>(createDate.value);
    }
    if (updateDate.present) {
      map['update_date'] = Variable<DateTime>(updateDate.value);
    }
    if (deleteDate.present) {
      map['delete_date'] = Variable<DateTime>(deleteDate.value);
    }
    if (uuid.present) {
      map['uuid'] = Variable<String>(uuid.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (syncDate.present) {
      map['sync_date'] = Variable<DateTime>(syncDate.value);
    }
    if (thisDevice.present) {
      map['this_device'] = Variable<bool>(thisDevice.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('DevicesCompanion(')
          ..write('createDate: $createDate, ')
          ..write('updateDate: $updateDate, ')
          ..write('deleteDate: $deleteDate, ')
          ..write('uuid: $uuid, ')
          ..write('name: $name, ')
          ..write('syncDate: $syncDate, ')
          ..write('thisDevice: $thisDevice, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class Locations extends Table with TableInfo<Locations, LocationsData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  Locations(this.attachedDatabase, [this._alias]);
  late final GeneratedColumn<String> name =
      GeneratedColumn<String>('name', aliasedName, false,
          additionalChecks: GeneratedColumn.checkTextLength(
            minTextLength: 1,
          ),
          type: DriftSqlType.string,
          requiredDuringInsert: true);
  late final GeneratedColumn<String> address = GeneratedColumn<String>(
      'address', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  late final GeneratedColumn<String> city = GeneratedColumn<String>(
      'city', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  late final GeneratedColumn<String> region = GeneratedColumn<String>(
      'region', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  late final GeneratedColumn<String> zip = GeneratedColumn<String>(
      'zip', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  late final GeneratedColumn<String> uuid = GeneratedColumn<String>(
      'uuid', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  late final GeneratedColumn<double> latitude = GeneratedColumn<double>(
      'latitude', aliasedName, true,
      type: DriftSqlType.double, requiredDuringInsert: false);
  late final GeneratedColumn<double> longitude = GeneratedColumn<double>(
      'longitude', aliasedName, true,
      type: DriftSqlType.double, requiredDuringInsert: false);
  @override
  List<GeneratedColumn> get $columns =>
      [name, address, city, region, zip, uuid, latitude, longitude];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'locations';
  @override
  Set<GeneratedColumn> get $primaryKey => {uuid};
  @override
  LocationsData map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return LocationsData(
      name: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}name'])!,
      address: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}address'])!,
      city: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}city'])!,
      region: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}region'])!,
      zip: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}zip'])!,
      uuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}uuid'])!,
      latitude: attachedDatabase.typeMapping
          .read(DriftSqlType.double, data['${effectivePrefix}latitude']),
      longitude: attachedDatabase.typeMapping
          .read(DriftSqlType.double, data['${effectivePrefix}longitude']),
    );
  }

  @override
  Locations createAlias(String alias) {
    return Locations(attachedDatabase, alias);
  }
}

class LocationsData extends DataClass implements Insertable<LocationsData> {
  final String name;
  final String address;
  final String city;
  final String region;
  final String zip;
  final String uuid;
  final double? latitude;
  final double? longitude;
  const LocationsData(
      {required this.name,
      required this.address,
      required this.city,
      required this.region,
      required this.zip,
      required this.uuid,
      this.latitude,
      this.longitude});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['name'] = Variable<String>(name);
    map['address'] = Variable<String>(address);
    map['city'] = Variable<String>(city);
    map['region'] = Variable<String>(region);
    map['zip'] = Variable<String>(zip);
    map['uuid'] = Variable<String>(uuid);
    if (!nullToAbsent || latitude != null) {
      map['latitude'] = Variable<double>(latitude);
    }
    if (!nullToAbsent || longitude != null) {
      map['longitude'] = Variable<double>(longitude);
    }
    return map;
  }

  LocationsCompanion toCompanion(bool nullToAbsent) {
    return LocationsCompanion(
      name: Value(name),
      address: Value(address),
      city: Value(city),
      region: Value(region),
      zip: Value(zip),
      uuid: Value(uuid),
      latitude: latitude == null && nullToAbsent
          ? const Value.absent()
          : Value(latitude),
      longitude: longitude == null && nullToAbsent
          ? const Value.absent()
          : Value(longitude),
    );
  }

  factory LocationsData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return LocationsData(
      name: serializer.fromJson<String>(json['name']),
      address: serializer.fromJson<String>(json['address']),
      city: serializer.fromJson<String>(json['city']),
      region: serializer.fromJson<String>(json['region']),
      zip: serializer.fromJson<String>(json['zip']),
      uuid: serializer.fromJson<String>(json['uuid']),
      latitude: serializer.fromJson<double?>(json['latitude']),
      longitude: serializer.fromJson<double?>(json['longitude']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'name': serializer.toJson<String>(name),
      'address': serializer.toJson<String>(address),
      'city': serializer.toJson<String>(city),
      'region': serializer.toJson<String>(region),
      'zip': serializer.toJson<String>(zip),
      'uuid': serializer.toJson<String>(uuid),
      'latitude': serializer.toJson<double?>(latitude),
      'longitude': serializer.toJson<double?>(longitude),
    };
  }

  LocationsData copyWith(
          {String? name,
          String? address,
          String? city,
          String? region,
          String? zip,
          String? uuid,
          Value<double?> latitude = const Value.absent(),
          Value<double?> longitude = const Value.absent()}) =>
      LocationsData(
        name: name ?? this.name,
        address: address ?? this.address,
        city: city ?? this.city,
        region: region ?? this.region,
        zip: zip ?? this.zip,
        uuid: uuid ?? this.uuid,
        latitude: latitude.present ? latitude.value : this.latitude,
        longitude: longitude.present ? longitude.value : this.longitude,
      );
  LocationsData copyWithCompanion(LocationsCompanion data) {
    return LocationsData(
      name: data.name.present ? data.name.value : this.name,
      address: data.address.present ? data.address.value : this.address,
      city: data.city.present ? data.city.value : this.city,
      region: data.region.present ? data.region.value : this.region,
      zip: data.zip.present ? data.zip.value : this.zip,
      uuid: data.uuid.present ? data.uuid.value : this.uuid,
      latitude: data.latitude.present ? data.latitude.value : this.latitude,
      longitude: data.longitude.present ? data.longitude.value : this.longitude,
    );
  }

  @override
  String toString() {
    return (StringBuffer('LocationsData(')
          ..write('name: $name, ')
          ..write('address: $address, ')
          ..write('city: $city, ')
          ..write('region: $region, ')
          ..write('zip: $zip, ')
          ..write('uuid: $uuid, ')
          ..write('latitude: $latitude, ')
          ..write('longitude: $longitude')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode =>
      Object.hash(name, address, city, region, zip, uuid, latitude, longitude);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is LocationsData &&
          other.name == this.name &&
          other.address == this.address &&
          other.city == this.city &&
          other.region == this.region &&
          other.zip == this.zip &&
          other.uuid == this.uuid &&
          other.latitude == this.latitude &&
          other.longitude == this.longitude);
}

class LocationsCompanion extends UpdateCompanion<LocationsData> {
  final Value<String> name;
  final Value<String> address;
  final Value<String> city;
  final Value<String> region;
  final Value<String> zip;
  final Value<String> uuid;
  final Value<double?> latitude;
  final Value<double?> longitude;
  final Value<int> rowid;
  const LocationsCompanion({
    this.name = const Value.absent(),
    this.address = const Value.absent(),
    this.city = const Value.absent(),
    this.region = const Value.absent(),
    this.zip = const Value.absent(),
    this.uuid = const Value.absent(),
    this.latitude = const Value.absent(),
    this.longitude = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  LocationsCompanion.insert({
    required String name,
    required String address,
    required String city,
    required String region,
    required String zip,
    required String uuid,
    this.latitude = const Value.absent(),
    this.longitude = const Value.absent(),
    this.rowid = const Value.absent(),
  })  : name = Value(name),
        address = Value(address),
        city = Value(city),
        region = Value(region),
        zip = Value(zip),
        uuid = Value(uuid);
  static Insertable<LocationsData> custom({
    Expression<String>? name,
    Expression<String>? address,
    Expression<String>? city,
    Expression<String>? region,
    Expression<String>? zip,
    Expression<String>? uuid,
    Expression<double>? latitude,
    Expression<double>? longitude,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (name != null) 'name': name,
      if (address != null) 'address': address,
      if (city != null) 'city': city,
      if (region != null) 'region': region,
      if (zip != null) 'zip': zip,
      if (uuid != null) 'uuid': uuid,
      if (latitude != null) 'latitude': latitude,
      if (longitude != null) 'longitude': longitude,
      if (rowid != null) 'rowid': rowid,
    });
  }

  LocationsCompanion copyWith(
      {Value<String>? name,
      Value<String>? address,
      Value<String>? city,
      Value<String>? region,
      Value<String>? zip,
      Value<String>? uuid,
      Value<double?>? latitude,
      Value<double?>? longitude,
      Value<int>? rowid}) {
    return LocationsCompanion(
      name: name ?? this.name,
      address: address ?? this.address,
      city: city ?? this.city,
      region: region ?? this.region,
      zip: zip ?? this.zip,
      uuid: uuid ?? this.uuid,
      latitude: latitude ?? this.latitude,
      longitude: longitude ?? this.longitude,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (address.present) {
      map['address'] = Variable<String>(address.value);
    }
    if (city.present) {
      map['city'] = Variable<String>(city.value);
    }
    if (region.present) {
      map['region'] = Variable<String>(region.value);
    }
    if (zip.present) {
      map['zip'] = Variable<String>(zip.value);
    }
    if (uuid.present) {
      map['uuid'] = Variable<String>(uuid.value);
    }
    if (latitude.present) {
      map['latitude'] = Variable<double>(latitude.value);
    }
    if (longitude.present) {
      map['longitude'] = Variable<double>(longitude.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('LocationsCompanion(')
          ..write('name: $name, ')
          ..write('address: $address, ')
          ..write('city: $city, ')
          ..write('region: $region, ')
          ..write('zip: $zip, ')
          ..write('uuid: $uuid, ')
          ..write('latitude: $latitude, ')
          ..write('longitude: $longitude, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class Merchants extends Table with TableInfo<Merchants, MerchantsData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  Merchants(this.attachedDatabase, [this._alias]);
  late final GeneratedColumn<DateTime> createDate = GeneratedColumn<DateTime>(
      'create_date', aliasedName, false,
      type: DriftSqlType.dateTime,
      requiredDuringInsert: false,
      defaultValue: currentDateAndTime);
  late final GeneratedColumn<DateTime> updateDate = GeneratedColumn<DateTime>(
      'update_date', aliasedName, false,
      type: DriftSqlType.dateTime,
      requiredDuringInsert: false,
      defaultValue: currentDateAndTime);
  late final GeneratedColumn<DateTime> deleteDate = GeneratedColumn<DateTime>(
      'delete_date', aliasedName, true,
      type: DriftSqlType.dateTime, requiredDuringInsert: false);
  late final GeneratedColumn<String> uuid = GeneratedColumn<String>(
      'uuid', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  late final GeneratedColumn<String> name =
      GeneratedColumn<String>('name', aliasedName, false,
          additionalChecks: GeneratedColumn.checkTextLength(
            minTextLength: 1,
          ),
          type: DriftSqlType.string,
          requiredDuringInsert: true);
  late final GeneratedColumn<String> syncId = GeneratedColumn<String>(
      'sync_id', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  late final GeneratedColumn<String> locationUuid = GeneratedColumn<String>(
      'location_uuid', aliasedName, true,
      type: DriftSqlType.string,
      requiredDuringInsert: false,
      defaultConstraints: GeneratedColumn.constraintIsAlways(
          'REFERENCES locations (uuid) ON DELETE SET NULL'));
  @override
  List<GeneratedColumn> get $columns =>
      [createDate, updateDate, deleteDate, uuid, name, syncId, locationUuid];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'merchants';
  @override
  Set<GeneratedColumn> get $primaryKey => {uuid};
  @override
  MerchantsData map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return MerchantsData(
      createDate: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}create_date'])!,
      updateDate: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}update_date'])!,
      deleteDate: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}delete_date']),
      uuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}uuid'])!,
      name: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}name'])!,
      syncId: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}sync_id']),
      locationUuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}location_uuid']),
    );
  }

  @override
  Merchants createAlias(String alias) {
    return Merchants(attachedDatabase, alias);
  }
}

class MerchantsData extends DataClass implements Insertable<MerchantsData> {
  final DateTime createDate;
  final DateTime updateDate;
  final DateTime? deleteDate;
  final String uuid;
  final String name;
  final String? syncId;
  final String? locationUuid;
  const MerchantsData(
      {required this.createDate,
      required this.updateDate,
      this.deleteDate,
      required this.uuid,
      required this.name,
      this.syncId,
      this.locationUuid});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['create_date'] = Variable<DateTime>(createDate);
    map['update_date'] = Variable<DateTime>(updateDate);
    if (!nullToAbsent || deleteDate != null) {
      map['delete_date'] = Variable<DateTime>(deleteDate);
    }
    map['uuid'] = Variable<String>(uuid);
    map['name'] = Variable<String>(name);
    if (!nullToAbsent || syncId != null) {
      map['sync_id'] = Variable<String>(syncId);
    }
    if (!nullToAbsent || locationUuid != null) {
      map['location_uuid'] = Variable<String>(locationUuid);
    }
    return map;
  }

  MerchantsCompanion toCompanion(bool nullToAbsent) {
    return MerchantsCompanion(
      createDate: Value(createDate),
      updateDate: Value(updateDate),
      deleteDate: deleteDate == null && nullToAbsent
          ? const Value.absent()
          : Value(deleteDate),
      uuid: Value(uuid),
      name: Value(name),
      syncId:
          syncId == null && nullToAbsent ? const Value.absent() : Value(syncId),
      locationUuid: locationUuid == null && nullToAbsent
          ? const Value.absent()
          : Value(locationUuid),
    );
  }

  factory MerchantsData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return MerchantsData(
      createDate: serializer.fromJson<DateTime>(json['createDate']),
      updateDate: serializer.fromJson<DateTime>(json['updateDate']),
      deleteDate: serializer.fromJson<DateTime?>(json['deleteDate']),
      uuid: serializer.fromJson<String>(json['uuid']),
      name: serializer.fromJson<String>(json['name']),
      syncId: serializer.fromJson<String?>(json['syncId']),
      locationUuid: serializer.fromJson<String?>(json['locationUuid']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'createDate': serializer.toJson<DateTime>(createDate),
      'updateDate': serializer.toJson<DateTime>(updateDate),
      'deleteDate': serializer.toJson<DateTime?>(deleteDate),
      'uuid': serializer.toJson<String>(uuid),
      'name': serializer.toJson<String>(name),
      'syncId': serializer.toJson<String?>(syncId),
      'locationUuid': serializer.toJson<String?>(locationUuid),
    };
  }

  MerchantsData copyWith(
          {DateTime? createDate,
          DateTime? updateDate,
          Value<DateTime?> deleteDate = const Value.absent(),
          String? uuid,
          String? name,
          Value<String?> syncId = const Value.absent(),
          Value<String?> locationUuid = const Value.absent()}) =>
      MerchantsData(
        createDate: createDate ?? this.createDate,
        updateDate: updateDate ?? this.updateDate,
        deleteDate: deleteDate.present ? deleteDate.value : this.deleteDate,
        uuid: uuid ?? this.uuid,
        name: name ?? this.name,
        syncId: syncId.present ? syncId.value : this.syncId,
        locationUuid:
            locationUuid.present ? locationUuid.value : this.locationUuid,
      );
  MerchantsData copyWithCompanion(MerchantsCompanion data) {
    return MerchantsData(
      createDate:
          data.createDate.present ? data.createDate.value : this.createDate,
      updateDate:
          data.updateDate.present ? data.updateDate.value : this.updateDate,
      deleteDate:
          data.deleteDate.present ? data.deleteDate.value : this.deleteDate,
      uuid: data.uuid.present ? data.uuid.value : this.uuid,
      name: data.name.present ? data.name.value : this.name,
      syncId: data.syncId.present ? data.syncId.value : this.syncId,
      locationUuid: data.locationUuid.present
          ? data.locationUuid.value
          : this.locationUuid,
    );
  }

  @override
  String toString() {
    return (StringBuffer('MerchantsData(')
          ..write('createDate: $createDate, ')
          ..write('updateDate: $updateDate, ')
          ..write('deleteDate: $deleteDate, ')
          ..write('uuid: $uuid, ')
          ..write('name: $name, ')
          ..write('syncId: $syncId, ')
          ..write('locationUuid: $locationUuid')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(
      createDate, updateDate, deleteDate, uuid, name, syncId, locationUuid);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is MerchantsData &&
          other.createDate == this.createDate &&
          other.updateDate == this.updateDate &&
          other.deleteDate == this.deleteDate &&
          other.uuid == this.uuid &&
          other.name == this.name &&
          other.syncId == this.syncId &&
          other.locationUuid == this.locationUuid);
}

class MerchantsCompanion extends UpdateCompanion<MerchantsData> {
  final Value<DateTime> createDate;
  final Value<DateTime> updateDate;
  final Value<DateTime?> deleteDate;
  final Value<String> uuid;
  final Value<String> name;
  final Value<String?> syncId;
  final Value<String?> locationUuid;
  final Value<int> rowid;
  const MerchantsCompanion({
    this.createDate = const Value.absent(),
    this.updateDate = const Value.absent(),
    this.deleteDate = const Value.absent(),
    this.uuid = const Value.absent(),
    this.name = const Value.absent(),
    this.syncId = const Value.absent(),
    this.locationUuid = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  MerchantsCompanion.insert({
    this.createDate = const Value.absent(),
    this.updateDate = const Value.absent(),
    this.deleteDate = const Value.absent(),
    required String uuid,
    required String name,
    this.syncId = const Value.absent(),
    this.locationUuid = const Value.absent(),
    this.rowid = const Value.absent(),
  })  : uuid = Value(uuid),
        name = Value(name);
  static Insertable<MerchantsData> custom({
    Expression<DateTime>? createDate,
    Expression<DateTime>? updateDate,
    Expression<DateTime>? deleteDate,
    Expression<String>? uuid,
    Expression<String>? name,
    Expression<String>? syncId,
    Expression<String>? locationUuid,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (createDate != null) 'create_date': createDate,
      if (updateDate != null) 'update_date': updateDate,
      if (deleteDate != null) 'delete_date': deleteDate,
      if (uuid != null) 'uuid': uuid,
      if (name != null) 'name': name,
      if (syncId != null) 'sync_id': syncId,
      if (locationUuid != null) 'location_uuid': locationUuid,
      if (rowid != null) 'rowid': rowid,
    });
  }

  MerchantsCompanion copyWith(
      {Value<DateTime>? createDate,
      Value<DateTime>? updateDate,
      Value<DateTime?>? deleteDate,
      Value<String>? uuid,
      Value<String>? name,
      Value<String?>? syncId,
      Value<String?>? locationUuid,
      Value<int>? rowid}) {
    return MerchantsCompanion(
      createDate: createDate ?? this.createDate,
      updateDate: updateDate ?? this.updateDate,
      deleteDate: deleteDate ?? this.deleteDate,
      uuid: uuid ?? this.uuid,
      name: name ?? this.name,
      syncId: syncId ?? this.syncId,
      locationUuid: locationUuid ?? this.locationUuid,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (createDate.present) {
      map['create_date'] = Variable<DateTime>(createDate.value);
    }
    if (updateDate.present) {
      map['update_date'] = Variable<DateTime>(updateDate.value);
    }
    if (deleteDate.present) {
      map['delete_date'] = Variable<DateTime>(deleteDate.value);
    }
    if (uuid.present) {
      map['uuid'] = Variable<String>(uuid.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (syncId.present) {
      map['sync_id'] = Variable<String>(syncId.value);
    }
    if (locationUuid.present) {
      map['location_uuid'] = Variable<String>(locationUuid.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('MerchantsCompanion(')
          ..write('createDate: $createDate, ')
          ..write('updateDate: $updateDate, ')
          ..write('deleteDate: $deleteDate, ')
          ..write('uuid: $uuid, ')
          ..write('name: $name, ')
          ..write('syncId: $syncId, ')
          ..write('locationUuid: $locationUuid, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class ReportGroups extends Table
    with TableInfo<ReportGroups, ReportGroupsData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  ReportGroups(this.attachedDatabase, [this._alias]);
  late final GeneratedColumn<DateTime> createDate = GeneratedColumn<DateTime>(
      'create_date', aliasedName, false,
      type: DriftSqlType.dateTime,
      requiredDuringInsert: false,
      defaultValue: currentDateAndTime);
  late final GeneratedColumn<DateTime> updateDate = GeneratedColumn<DateTime>(
      'update_date', aliasedName, false,
      type: DriftSqlType.dateTime,
      requiredDuringInsert: false,
      defaultValue: currentDateAndTime);
  late final GeneratedColumn<DateTime> deleteDate = GeneratedColumn<DateTime>(
      'delete_date', aliasedName, true,
      type: DriftSqlType.dateTime, requiredDuringInsert: false);
  late final GeneratedColumn<String> uuid = GeneratedColumn<String>(
      'uuid', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  late final GeneratedColumn<String> name =
      GeneratedColumn<String>('name', aliasedName, false,
          additionalChecks: GeneratedColumn.checkTextLength(
            minTextLength: 1,
          ),
          type: DriftSqlType.string,
          requiredDuringInsert: true);
  late final GeneratedColumn<int> cellSize = GeneratedColumn<int>(
      'cell_size', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns =>
      [createDate, updateDate, deleteDate, uuid, name, cellSize];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'report_groups';
  @override
  Set<GeneratedColumn> get $primaryKey => {uuid};
  @override
  ReportGroupsData map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return ReportGroupsData(
      createDate: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}create_date'])!,
      updateDate: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}update_date'])!,
      deleteDate: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}delete_date']),
      uuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}uuid'])!,
      name: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}name'])!,
      cellSize: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}cell_size'])!,
    );
  }

  @override
  ReportGroups createAlias(String alias) {
    return ReportGroups(attachedDatabase, alias);
  }
}

class ReportGroupsData extends DataClass
    implements Insertable<ReportGroupsData> {
  final DateTime createDate;
  final DateTime updateDate;
  final DateTime? deleteDate;
  final String uuid;
  final String name;
  final int cellSize;
  const ReportGroupsData(
      {required this.createDate,
      required this.updateDate,
      this.deleteDate,
      required this.uuid,
      required this.name,
      required this.cellSize});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['create_date'] = Variable<DateTime>(createDate);
    map['update_date'] = Variable<DateTime>(updateDate);
    if (!nullToAbsent || deleteDate != null) {
      map['delete_date'] = Variable<DateTime>(deleteDate);
    }
    map['uuid'] = Variable<String>(uuid);
    map['name'] = Variable<String>(name);
    map['cell_size'] = Variable<int>(cellSize);
    return map;
  }

  ReportGroupsCompanion toCompanion(bool nullToAbsent) {
    return ReportGroupsCompanion(
      createDate: Value(createDate),
      updateDate: Value(updateDate),
      deleteDate: deleteDate == null && nullToAbsent
          ? const Value.absent()
          : Value(deleteDate),
      uuid: Value(uuid),
      name: Value(name),
      cellSize: Value(cellSize),
    );
  }

  factory ReportGroupsData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return ReportGroupsData(
      createDate: serializer.fromJson<DateTime>(json['createDate']),
      updateDate: serializer.fromJson<DateTime>(json['updateDate']),
      deleteDate: serializer.fromJson<DateTime?>(json['deleteDate']),
      uuid: serializer.fromJson<String>(json['uuid']),
      name: serializer.fromJson<String>(json['name']),
      cellSize: serializer.fromJson<int>(json['cellSize']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'createDate': serializer.toJson<DateTime>(createDate),
      'updateDate': serializer.toJson<DateTime>(updateDate),
      'deleteDate': serializer.toJson<DateTime?>(deleteDate),
      'uuid': serializer.toJson<String>(uuid),
      'name': serializer.toJson<String>(name),
      'cellSize': serializer.toJson<int>(cellSize),
    };
  }

  ReportGroupsData copyWith(
          {DateTime? createDate,
          DateTime? updateDate,
          Value<DateTime?> deleteDate = const Value.absent(),
          String? uuid,
          String? name,
          int? cellSize}) =>
      ReportGroupsData(
        createDate: createDate ?? this.createDate,
        updateDate: updateDate ?? this.updateDate,
        deleteDate: deleteDate.present ? deleteDate.value : this.deleteDate,
        uuid: uuid ?? this.uuid,
        name: name ?? this.name,
        cellSize: cellSize ?? this.cellSize,
      );
  ReportGroupsData copyWithCompanion(ReportGroupsCompanion data) {
    return ReportGroupsData(
      createDate:
          data.createDate.present ? data.createDate.value : this.createDate,
      updateDate:
          data.updateDate.present ? data.updateDate.value : this.updateDate,
      deleteDate:
          data.deleteDate.present ? data.deleteDate.value : this.deleteDate,
      uuid: data.uuid.present ? data.uuid.value : this.uuid,
      name: data.name.present ? data.name.value : this.name,
      cellSize: data.cellSize.present ? data.cellSize.value : this.cellSize,
    );
  }

  @override
  String toString() {
    return (StringBuffer('ReportGroupsData(')
          ..write('createDate: $createDate, ')
          ..write('updateDate: $updateDate, ')
          ..write('deleteDate: $deleteDate, ')
          ..write('uuid: $uuid, ')
          ..write('name: $name, ')
          ..write('cellSize: $cellSize')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode =>
      Object.hash(createDate, updateDate, deleteDate, uuid, name, cellSize);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is ReportGroupsData &&
          other.createDate == this.createDate &&
          other.updateDate == this.updateDate &&
          other.deleteDate == this.deleteDate &&
          other.uuid == this.uuid &&
          other.name == this.name &&
          other.cellSize == this.cellSize);
}

class ReportGroupsCompanion extends UpdateCompanion<ReportGroupsData> {
  final Value<DateTime> createDate;
  final Value<DateTime> updateDate;
  final Value<DateTime?> deleteDate;
  final Value<String> uuid;
  final Value<String> name;
  final Value<int> cellSize;
  final Value<int> rowid;
  const ReportGroupsCompanion({
    this.createDate = const Value.absent(),
    this.updateDate = const Value.absent(),
    this.deleteDate = const Value.absent(),
    this.uuid = const Value.absent(),
    this.name = const Value.absent(),
    this.cellSize = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  ReportGroupsCompanion.insert({
    this.createDate = const Value.absent(),
    this.updateDate = const Value.absent(),
    this.deleteDate = const Value.absent(),
    required String uuid,
    required String name,
    required int cellSize,
    this.rowid = const Value.absent(),
  })  : uuid = Value(uuid),
        name = Value(name),
        cellSize = Value(cellSize);
  static Insertable<ReportGroupsData> custom({
    Expression<DateTime>? createDate,
    Expression<DateTime>? updateDate,
    Expression<DateTime>? deleteDate,
    Expression<String>? uuid,
    Expression<String>? name,
    Expression<int>? cellSize,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (createDate != null) 'create_date': createDate,
      if (updateDate != null) 'update_date': updateDate,
      if (deleteDate != null) 'delete_date': deleteDate,
      if (uuid != null) 'uuid': uuid,
      if (name != null) 'name': name,
      if (cellSize != null) 'cell_size': cellSize,
      if (rowid != null) 'rowid': rowid,
    });
  }

  ReportGroupsCompanion copyWith(
      {Value<DateTime>? createDate,
      Value<DateTime>? updateDate,
      Value<DateTime?>? deleteDate,
      Value<String>? uuid,
      Value<String>? name,
      Value<int>? cellSize,
      Value<int>? rowid}) {
    return ReportGroupsCompanion(
      createDate: createDate ?? this.createDate,
      updateDate: updateDate ?? this.updateDate,
      deleteDate: deleteDate ?? this.deleteDate,
      uuid: uuid ?? this.uuid,
      name: name ?? this.name,
      cellSize: cellSize ?? this.cellSize,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (createDate.present) {
      map['create_date'] = Variable<DateTime>(createDate.value);
    }
    if (updateDate.present) {
      map['update_date'] = Variable<DateTime>(updateDate.value);
    }
    if (deleteDate.present) {
      map['delete_date'] = Variable<DateTime>(deleteDate.value);
    }
    if (uuid.present) {
      map['uuid'] = Variable<String>(uuid.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (cellSize.present) {
      map['cell_size'] = Variable<int>(cellSize.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('ReportGroupsCompanion(')
          ..write('createDate: $createDate, ')
          ..write('updateDate: $updateDate, ')
          ..write('deleteDate: $deleteDate, ')
          ..write('uuid: $uuid, ')
          ..write('name: $name, ')
          ..write('cellSize: $cellSize, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class ReportGroupLayouts extends Table
    with TableInfo<ReportGroupLayouts, ReportGroupLayoutsData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  ReportGroupLayouts(this.attachedDatabase, [this._alias]);
  late final GeneratedColumn<String> uuid = GeneratedColumn<String>(
      'uuid', aliasedName, false,
      type: DriftSqlType.string,
      requiredDuringInsert: true,
      defaultConstraints: GeneratedColumn.constraintIsAlways(
          'REFERENCES report_groups (uuid) ON DELETE CASCADE'));
  late final GeneratedColumn<int> order = GeneratedColumn<int>(
      'order', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns => [uuid, order];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'report_group_layouts';
  @override
  Set<GeneratedColumn> get $primaryKey => {uuid, order};
  @override
  ReportGroupLayoutsData map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return ReportGroupLayoutsData(
      uuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}uuid'])!,
      order: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}order'])!,
    );
  }

  @override
  ReportGroupLayouts createAlias(String alias) {
    return ReportGroupLayouts(attachedDatabase, alias);
  }
}

class ReportGroupLayoutsData extends DataClass
    implements Insertable<ReportGroupLayoutsData> {
  final String uuid;
  final int order;
  const ReportGroupLayoutsData({required this.uuid, required this.order});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['uuid'] = Variable<String>(uuid);
    map['order'] = Variable<int>(order);
    return map;
  }

  ReportGroupLayoutsCompanion toCompanion(bool nullToAbsent) {
    return ReportGroupLayoutsCompanion(
      uuid: Value(uuid),
      order: Value(order),
    );
  }

  factory ReportGroupLayoutsData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return ReportGroupLayoutsData(
      uuid: serializer.fromJson<String>(json['uuid']),
      order: serializer.fromJson<int>(json['order']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'uuid': serializer.toJson<String>(uuid),
      'order': serializer.toJson<int>(order),
    };
  }

  ReportGroupLayoutsData copyWith({String? uuid, int? order}) =>
      ReportGroupLayoutsData(
        uuid: uuid ?? this.uuid,
        order: order ?? this.order,
      );
  ReportGroupLayoutsData copyWithCompanion(ReportGroupLayoutsCompanion data) {
    return ReportGroupLayoutsData(
      uuid: data.uuid.present ? data.uuid.value : this.uuid,
      order: data.order.present ? data.order.value : this.order,
    );
  }

  @override
  String toString() {
    return (StringBuffer('ReportGroupLayoutsData(')
          ..write('uuid: $uuid, ')
          ..write('order: $order')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(uuid, order);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is ReportGroupLayoutsData &&
          other.uuid == this.uuid &&
          other.order == this.order);
}

class ReportGroupLayoutsCompanion
    extends UpdateCompanion<ReportGroupLayoutsData> {
  final Value<String> uuid;
  final Value<int> order;
  final Value<int> rowid;
  const ReportGroupLayoutsCompanion({
    this.uuid = const Value.absent(),
    this.order = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  ReportGroupLayoutsCompanion.insert({
    required String uuid,
    required int order,
    this.rowid = const Value.absent(),
  })  : uuid = Value(uuid),
        order = Value(order);
  static Insertable<ReportGroupLayoutsData> custom({
    Expression<String>? uuid,
    Expression<int>? order,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (uuid != null) 'uuid': uuid,
      if (order != null) 'order': order,
      if (rowid != null) 'rowid': rowid,
    });
  }

  ReportGroupLayoutsCompanion copyWith(
      {Value<String>? uuid, Value<int>? order, Value<int>? rowid}) {
    return ReportGroupLayoutsCompanion(
      uuid: uuid ?? this.uuid,
      order: order ?? this.order,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (uuid.present) {
      map['uuid'] = Variable<String>(uuid.value);
    }
    if (order.present) {
      map['order'] = Variable<int>(order.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('ReportGroupLayoutsCompanion(')
          ..write('uuid: $uuid, ')
          ..write('order: $order, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class ReportGroupRows extends Table
    with TableInfo<ReportGroupRows, ReportGroupRowsData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  ReportGroupRows(this.attachedDatabase, [this._alias]);
  late final GeneratedColumn<String> uuid = GeneratedColumn<String>(
      'uuid', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  late final GeneratedColumn<int> layoutOrder = GeneratedColumn<int>(
      'layout_order', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  late final GeneratedColumn<int> order = GeneratedColumn<int>(
      'order', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  late final GeneratedColumn<String> primaryReport = GeneratedColumn<String>(
      'primary_report', aliasedName, true,
      type: DriftSqlType.string,
      requiredDuringInsert: false,
      defaultConstraints: GeneratedColumn.constraintIsAlways(
          'REFERENCES reports (uuid) ON DELETE SET NULL'));
  late final GeneratedColumn<String> secondaryReport = GeneratedColumn<String>(
      'secondary_report', aliasedName, true,
      type: DriftSqlType.string,
      requiredDuringInsert: false,
      defaultConstraints: GeneratedColumn.constraintIsAlways(
          'REFERENCES reports (uuid) ON DELETE SET NULL'));
  late final GeneratedColumn<String> tertiaryReport = GeneratedColumn<String>(
      'tertiary_report', aliasedName, true,
      type: DriftSqlType.string,
      requiredDuringInsert: false,
      defaultConstraints: GeneratedColumn.constraintIsAlways(
          'REFERENCES reports (uuid) ON DELETE SET NULL'));
  @override
  List<GeneratedColumn> get $columns => [
        uuid,
        layoutOrder,
        order,
        primaryReport,
        secondaryReport,
        tertiaryReport
      ];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'report_group_rows';
  @override
  Set<GeneratedColumn> get $primaryKey => {uuid, layoutOrder, order};
  @override
  ReportGroupRowsData map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return ReportGroupRowsData(
      uuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}uuid'])!,
      layoutOrder: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}layout_order'])!,
      order: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}order'])!,
      primaryReport: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}primary_report']),
      secondaryReport: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}secondary_report']),
      tertiaryReport: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}tertiary_report']),
    );
  }

  @override
  ReportGroupRows createAlias(String alias) {
    return ReportGroupRows(attachedDatabase, alias);
  }
}

class ReportGroupRowsData extends DataClass
    implements Insertable<ReportGroupRowsData> {
  final String uuid;
  final int layoutOrder;
  final int order;
  final String? primaryReport;
  final String? secondaryReport;
  final String? tertiaryReport;
  const ReportGroupRowsData(
      {required this.uuid,
      required this.layoutOrder,
      required this.order,
      this.primaryReport,
      this.secondaryReport,
      this.tertiaryReport});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['uuid'] = Variable<String>(uuid);
    map['layout_order'] = Variable<int>(layoutOrder);
    map['order'] = Variable<int>(order);
    if (!nullToAbsent || primaryReport != null) {
      map['primary_report'] = Variable<String>(primaryReport);
    }
    if (!nullToAbsent || secondaryReport != null) {
      map['secondary_report'] = Variable<String>(secondaryReport);
    }
    if (!nullToAbsent || tertiaryReport != null) {
      map['tertiary_report'] = Variable<String>(tertiaryReport);
    }
    return map;
  }

  ReportGroupRowsCompanion toCompanion(bool nullToAbsent) {
    return ReportGroupRowsCompanion(
      uuid: Value(uuid),
      layoutOrder: Value(layoutOrder),
      order: Value(order),
      primaryReport: primaryReport == null && nullToAbsent
          ? const Value.absent()
          : Value(primaryReport),
      secondaryReport: secondaryReport == null && nullToAbsent
          ? const Value.absent()
          : Value(secondaryReport),
      tertiaryReport: tertiaryReport == null && nullToAbsent
          ? const Value.absent()
          : Value(tertiaryReport),
    );
  }

  factory ReportGroupRowsData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return ReportGroupRowsData(
      uuid: serializer.fromJson<String>(json['uuid']),
      layoutOrder: serializer.fromJson<int>(json['layoutOrder']),
      order: serializer.fromJson<int>(json['order']),
      primaryReport: serializer.fromJson<String?>(json['primaryReport']),
      secondaryReport: serializer.fromJson<String?>(json['secondaryReport']),
      tertiaryReport: serializer.fromJson<String?>(json['tertiaryReport']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'uuid': serializer.toJson<String>(uuid),
      'layoutOrder': serializer.toJson<int>(layoutOrder),
      'order': serializer.toJson<int>(order),
      'primaryReport': serializer.toJson<String?>(primaryReport),
      'secondaryReport': serializer.toJson<String?>(secondaryReport),
      'tertiaryReport': serializer.toJson<String?>(tertiaryReport),
    };
  }

  ReportGroupRowsData copyWith(
          {String? uuid,
          int? layoutOrder,
          int? order,
          Value<String?> primaryReport = const Value.absent(),
          Value<String?> secondaryReport = const Value.absent(),
          Value<String?> tertiaryReport = const Value.absent()}) =>
      ReportGroupRowsData(
        uuid: uuid ?? this.uuid,
        layoutOrder: layoutOrder ?? this.layoutOrder,
        order: order ?? this.order,
        primaryReport:
            primaryReport.present ? primaryReport.value : this.primaryReport,
        secondaryReport: secondaryReport.present
            ? secondaryReport.value
            : this.secondaryReport,
        tertiaryReport:
            tertiaryReport.present ? tertiaryReport.value : this.tertiaryReport,
      );
  ReportGroupRowsData copyWithCompanion(ReportGroupRowsCompanion data) {
    return ReportGroupRowsData(
      uuid: data.uuid.present ? data.uuid.value : this.uuid,
      layoutOrder:
          data.layoutOrder.present ? data.layoutOrder.value : this.layoutOrder,
      order: data.order.present ? data.order.value : this.order,
      primaryReport: data.primaryReport.present
          ? data.primaryReport.value
          : this.primaryReport,
      secondaryReport: data.secondaryReport.present
          ? data.secondaryReport.value
          : this.secondaryReport,
      tertiaryReport: data.tertiaryReport.present
          ? data.tertiaryReport.value
          : this.tertiaryReport,
    );
  }

  @override
  String toString() {
    return (StringBuffer('ReportGroupRowsData(')
          ..write('uuid: $uuid, ')
          ..write('layoutOrder: $layoutOrder, ')
          ..write('order: $order, ')
          ..write('primaryReport: $primaryReport, ')
          ..write('secondaryReport: $secondaryReport, ')
          ..write('tertiaryReport: $tertiaryReport')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(
      uuid, layoutOrder, order, primaryReport, secondaryReport, tertiaryReport);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is ReportGroupRowsData &&
          other.uuid == this.uuid &&
          other.layoutOrder == this.layoutOrder &&
          other.order == this.order &&
          other.primaryReport == this.primaryReport &&
          other.secondaryReport == this.secondaryReport &&
          other.tertiaryReport == this.tertiaryReport);
}

class ReportGroupRowsCompanion extends UpdateCompanion<ReportGroupRowsData> {
  final Value<String> uuid;
  final Value<int> layoutOrder;
  final Value<int> order;
  final Value<String?> primaryReport;
  final Value<String?> secondaryReport;
  final Value<String?> tertiaryReport;
  final Value<int> rowid;
  const ReportGroupRowsCompanion({
    this.uuid = const Value.absent(),
    this.layoutOrder = const Value.absent(),
    this.order = const Value.absent(),
    this.primaryReport = const Value.absent(),
    this.secondaryReport = const Value.absent(),
    this.tertiaryReport = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  ReportGroupRowsCompanion.insert({
    required String uuid,
    required int layoutOrder,
    required int order,
    this.primaryReport = const Value.absent(),
    this.secondaryReport = const Value.absent(),
    this.tertiaryReport = const Value.absent(),
    this.rowid = const Value.absent(),
  })  : uuid = Value(uuid),
        layoutOrder = Value(layoutOrder),
        order = Value(order);
  static Insertable<ReportGroupRowsData> custom({
    Expression<String>? uuid,
    Expression<int>? layoutOrder,
    Expression<int>? order,
    Expression<String>? primaryReport,
    Expression<String>? secondaryReport,
    Expression<String>? tertiaryReport,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (uuid != null) 'uuid': uuid,
      if (layoutOrder != null) 'layout_order': layoutOrder,
      if (order != null) 'order': order,
      if (primaryReport != null) 'primary_report': primaryReport,
      if (secondaryReport != null) 'secondary_report': secondaryReport,
      if (tertiaryReport != null) 'tertiary_report': tertiaryReport,
      if (rowid != null) 'rowid': rowid,
    });
  }

  ReportGroupRowsCompanion copyWith(
      {Value<String>? uuid,
      Value<int>? layoutOrder,
      Value<int>? order,
      Value<String?>? primaryReport,
      Value<String?>? secondaryReport,
      Value<String?>? tertiaryReport,
      Value<int>? rowid}) {
    return ReportGroupRowsCompanion(
      uuid: uuid ?? this.uuid,
      layoutOrder: layoutOrder ?? this.layoutOrder,
      order: order ?? this.order,
      primaryReport: primaryReport ?? this.primaryReport,
      secondaryReport: secondaryReport ?? this.secondaryReport,
      tertiaryReport: tertiaryReport ?? this.tertiaryReport,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (uuid.present) {
      map['uuid'] = Variable<String>(uuid.value);
    }
    if (layoutOrder.present) {
      map['layout_order'] = Variable<int>(layoutOrder.value);
    }
    if (order.present) {
      map['order'] = Variable<int>(order.value);
    }
    if (primaryReport.present) {
      map['primary_report'] = Variable<String>(primaryReport.value);
    }
    if (secondaryReport.present) {
      map['secondary_report'] = Variable<String>(secondaryReport.value);
    }
    if (tertiaryReport.present) {
      map['tertiary_report'] = Variable<String>(tertiaryReport.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('ReportGroupRowsCompanion(')
          ..write('uuid: $uuid, ')
          ..write('layoutOrder: $layoutOrder, ')
          ..write('order: $order, ')
          ..write('primaryReport: $primaryReport, ')
          ..write('secondaryReport: $secondaryReport, ')
          ..write('tertiaryReport: $tertiaryReport, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class Transactions extends Table
    with TableInfo<Transactions, TransactionsData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  Transactions(this.attachedDatabase, [this._alias]);
  late final GeneratedColumn<DateTime> createDate = GeneratedColumn<DateTime>(
      'create_date', aliasedName, false,
      type: DriftSqlType.dateTime,
      requiredDuringInsert: false,
      defaultValue: currentDateAndTime);
  late final GeneratedColumn<DateTime> updateDate = GeneratedColumn<DateTime>(
      'update_date', aliasedName, false,
      type: DriftSqlType.dateTime,
      requiredDuringInsert: false,
      defaultValue: currentDateAndTime);
  late final GeneratedColumn<DateTime> deleteDate = GeneratedColumn<DateTime>(
      'delete_date', aliasedName, true,
      type: DriftSqlType.dateTime, requiredDuringInsert: false);
  late final GeneratedColumn<String> uuid = GeneratedColumn<String>(
      'uuid', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  late final GeneratedColumn<String> name =
      GeneratedColumn<String>('name', aliasedName, false,
          additionalChecks: GeneratedColumn.checkTextLength(
            minTextLength: 1,
          ),
          type: DriftSqlType.string,
          requiredDuringInsert: true);
  late final GeneratedColumn<String> syncId = GeneratedColumn<String>(
      'sync_id', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  late final GeneratedColumn<DateTime> date = GeneratedColumn<DateTime>(
      'date', aliasedName, false,
      type: DriftSqlType.dateTime, requiredDuringInsert: true);
  late final GeneratedColumn<int> amount = GeneratedColumn<int>(
      'amount', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  late final GeneratedColumn<String> accountUuid = GeneratedColumn<String>(
      'account_uuid', aliasedName, false,
      type: DriftSqlType.string,
      requiredDuringInsert: true,
      defaultConstraints: GeneratedColumn.constraintIsAlways(
          'REFERENCES accounts (uuid) ON DELETE CASCADE'));
  late final GeneratedColumn<String> currencyCode = GeneratedColumn<String>(
      'currency_code', aliasedName, false,
      additionalChecks:
          GeneratedColumn.checkTextLength(minTextLength: 3, maxTextLength: 3),
      type: DriftSqlType.string,
      requiredDuringInsert: true);
  late final GeneratedColumn<String> merchantUuid = GeneratedColumn<String>(
      'merchant_uuid', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  late final GeneratedColumn<String> merchantName =
      GeneratedColumn<String>('merchant_name', aliasedName, true,
          additionalChecks: GeneratedColumn.checkTextLength(
            minTextLength: 1,
          ),
          type: DriftSqlType.string,
          requiredDuringInsert: false);
  late final GeneratedColumn<String> locationUuid = GeneratedColumn<String>(
      'location_uuid', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  @override
  List<GeneratedColumn> get $columns => [
        createDate,
        updateDate,
        deleteDate,
        uuid,
        name,
        syncId,
        date,
        amount,
        accountUuid,
        currencyCode,
        merchantUuid,
        merchantName,
        locationUuid
      ];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'transactions';
  @override
  Set<GeneratedColumn> get $primaryKey => {uuid};
  @override
  TransactionsData map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return TransactionsData(
      createDate: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}create_date'])!,
      updateDate: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}update_date'])!,
      deleteDate: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}delete_date']),
      uuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}uuid'])!,
      name: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}name'])!,
      syncId: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}sync_id']),
      date: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}date'])!,
      amount: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}amount'])!,
      accountUuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}account_uuid'])!,
      currencyCode: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}currency_code'])!,
      merchantUuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}merchant_uuid']),
      merchantName: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}merchant_name']),
      locationUuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}location_uuid']),
    );
  }

  @override
  Transactions createAlias(String alias) {
    return Transactions(attachedDatabase, alias);
  }
}

class TransactionsData extends DataClass
    implements Insertable<TransactionsData> {
  final DateTime createDate;
  final DateTime updateDate;
  final DateTime? deleteDate;
  final String uuid;
  final String name;
  final String? syncId;
  final DateTime date;
  final int amount;
  final String accountUuid;
  final String currencyCode;
  final String? merchantUuid;
  final String? merchantName;
  final String? locationUuid;
  const TransactionsData(
      {required this.createDate,
      required this.updateDate,
      this.deleteDate,
      required this.uuid,
      required this.name,
      this.syncId,
      required this.date,
      required this.amount,
      required this.accountUuid,
      required this.currencyCode,
      this.merchantUuid,
      this.merchantName,
      this.locationUuid});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['create_date'] = Variable<DateTime>(createDate);
    map['update_date'] = Variable<DateTime>(updateDate);
    if (!nullToAbsent || deleteDate != null) {
      map['delete_date'] = Variable<DateTime>(deleteDate);
    }
    map['uuid'] = Variable<String>(uuid);
    map['name'] = Variable<String>(name);
    if (!nullToAbsent || syncId != null) {
      map['sync_id'] = Variable<String>(syncId);
    }
    map['date'] = Variable<DateTime>(date);
    map['amount'] = Variable<int>(amount);
    map['account_uuid'] = Variable<String>(accountUuid);
    map['currency_code'] = Variable<String>(currencyCode);
    if (!nullToAbsent || merchantUuid != null) {
      map['merchant_uuid'] = Variable<String>(merchantUuid);
    }
    if (!nullToAbsent || merchantName != null) {
      map['merchant_name'] = Variable<String>(merchantName);
    }
    if (!nullToAbsent || locationUuid != null) {
      map['location_uuid'] = Variable<String>(locationUuid);
    }
    return map;
  }

  TransactionsCompanion toCompanion(bool nullToAbsent) {
    return TransactionsCompanion(
      createDate: Value(createDate),
      updateDate: Value(updateDate),
      deleteDate: deleteDate == null && nullToAbsent
          ? const Value.absent()
          : Value(deleteDate),
      uuid: Value(uuid),
      name: Value(name),
      syncId:
          syncId == null && nullToAbsent ? const Value.absent() : Value(syncId),
      date: Value(date),
      amount: Value(amount),
      accountUuid: Value(accountUuid),
      currencyCode: Value(currencyCode),
      merchantUuid: merchantUuid == null && nullToAbsent
          ? const Value.absent()
          : Value(merchantUuid),
      merchantName: merchantName == null && nullToAbsent
          ? const Value.absent()
          : Value(merchantName),
      locationUuid: locationUuid == null && nullToAbsent
          ? const Value.absent()
          : Value(locationUuid),
    );
  }

  factory TransactionsData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return TransactionsData(
      createDate: serializer.fromJson<DateTime>(json['createDate']),
      updateDate: serializer.fromJson<DateTime>(json['updateDate']),
      deleteDate: serializer.fromJson<DateTime?>(json['deleteDate']),
      uuid: serializer.fromJson<String>(json['uuid']),
      name: serializer.fromJson<String>(json['name']),
      syncId: serializer.fromJson<String?>(json['syncId']),
      date: serializer.fromJson<DateTime>(json['date']),
      amount: serializer.fromJson<int>(json['amount']),
      accountUuid: serializer.fromJson<String>(json['accountUuid']),
      currencyCode: serializer.fromJson<String>(json['currencyCode']),
      merchantUuid: serializer.fromJson<String?>(json['merchantUuid']),
      merchantName: serializer.fromJson<String?>(json['merchantName']),
      locationUuid: serializer.fromJson<String?>(json['locationUuid']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'createDate': serializer.toJson<DateTime>(createDate),
      'updateDate': serializer.toJson<DateTime>(updateDate),
      'deleteDate': serializer.toJson<DateTime?>(deleteDate),
      'uuid': serializer.toJson<String>(uuid),
      'name': serializer.toJson<String>(name),
      'syncId': serializer.toJson<String?>(syncId),
      'date': serializer.toJson<DateTime>(date),
      'amount': serializer.toJson<int>(amount),
      'accountUuid': serializer.toJson<String>(accountUuid),
      'currencyCode': serializer.toJson<String>(currencyCode),
      'merchantUuid': serializer.toJson<String?>(merchantUuid),
      'merchantName': serializer.toJson<String?>(merchantName),
      'locationUuid': serializer.toJson<String?>(locationUuid),
    };
  }

  TransactionsData copyWith(
          {DateTime? createDate,
          DateTime? updateDate,
          Value<DateTime?> deleteDate = const Value.absent(),
          String? uuid,
          String? name,
          Value<String?> syncId = const Value.absent(),
          DateTime? date,
          int? amount,
          String? accountUuid,
          String? currencyCode,
          Value<String?> merchantUuid = const Value.absent(),
          Value<String?> merchantName = const Value.absent(),
          Value<String?> locationUuid = const Value.absent()}) =>
      TransactionsData(
        createDate: createDate ?? this.createDate,
        updateDate: updateDate ?? this.updateDate,
        deleteDate: deleteDate.present ? deleteDate.value : this.deleteDate,
        uuid: uuid ?? this.uuid,
        name: name ?? this.name,
        syncId: syncId.present ? syncId.value : this.syncId,
        date: date ?? this.date,
        amount: amount ?? this.amount,
        accountUuid: accountUuid ?? this.accountUuid,
        currencyCode: currencyCode ?? this.currencyCode,
        merchantUuid:
            merchantUuid.present ? merchantUuid.value : this.merchantUuid,
        merchantName:
            merchantName.present ? merchantName.value : this.merchantName,
        locationUuid:
            locationUuid.present ? locationUuid.value : this.locationUuid,
      );
  TransactionsData copyWithCompanion(TransactionsCompanion data) {
    return TransactionsData(
      createDate:
          data.createDate.present ? data.createDate.value : this.createDate,
      updateDate:
          data.updateDate.present ? data.updateDate.value : this.updateDate,
      deleteDate:
          data.deleteDate.present ? data.deleteDate.value : this.deleteDate,
      uuid: data.uuid.present ? data.uuid.value : this.uuid,
      name: data.name.present ? data.name.value : this.name,
      syncId: data.syncId.present ? data.syncId.value : this.syncId,
      date: data.date.present ? data.date.value : this.date,
      amount: data.amount.present ? data.amount.value : this.amount,
      accountUuid:
          data.accountUuid.present ? data.accountUuid.value : this.accountUuid,
      currencyCode: data.currencyCode.present
          ? data.currencyCode.value
          : this.currencyCode,
      merchantUuid: data.merchantUuid.present
          ? data.merchantUuid.value
          : this.merchantUuid,
      merchantName: data.merchantName.present
          ? data.merchantName.value
          : this.merchantName,
      locationUuid: data.locationUuid.present
          ? data.locationUuid.value
          : this.locationUuid,
    );
  }

  @override
  String toString() {
    return (StringBuffer('TransactionsData(')
          ..write('createDate: $createDate, ')
          ..write('updateDate: $updateDate, ')
          ..write('deleteDate: $deleteDate, ')
          ..write('uuid: $uuid, ')
          ..write('name: $name, ')
          ..write('syncId: $syncId, ')
          ..write('date: $date, ')
          ..write('amount: $amount, ')
          ..write('accountUuid: $accountUuid, ')
          ..write('currencyCode: $currencyCode, ')
          ..write('merchantUuid: $merchantUuid, ')
          ..write('merchantName: $merchantName, ')
          ..write('locationUuid: $locationUuid')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(
      createDate,
      updateDate,
      deleteDate,
      uuid,
      name,
      syncId,
      date,
      amount,
      accountUuid,
      currencyCode,
      merchantUuid,
      merchantName,
      locationUuid);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is TransactionsData &&
          other.createDate == this.createDate &&
          other.updateDate == this.updateDate &&
          other.deleteDate == this.deleteDate &&
          other.uuid == this.uuid &&
          other.name == this.name &&
          other.syncId == this.syncId &&
          other.date == this.date &&
          other.amount == this.amount &&
          other.accountUuid == this.accountUuid &&
          other.currencyCode == this.currencyCode &&
          other.merchantUuid == this.merchantUuid &&
          other.merchantName == this.merchantName &&
          other.locationUuid == this.locationUuid);
}

class TransactionsCompanion extends UpdateCompanion<TransactionsData> {
  final Value<DateTime> createDate;
  final Value<DateTime> updateDate;
  final Value<DateTime?> deleteDate;
  final Value<String> uuid;
  final Value<String> name;
  final Value<String?> syncId;
  final Value<DateTime> date;
  final Value<int> amount;
  final Value<String> accountUuid;
  final Value<String> currencyCode;
  final Value<String?> merchantUuid;
  final Value<String?> merchantName;
  final Value<String?> locationUuid;
  final Value<int> rowid;
  const TransactionsCompanion({
    this.createDate = const Value.absent(),
    this.updateDate = const Value.absent(),
    this.deleteDate = const Value.absent(),
    this.uuid = const Value.absent(),
    this.name = const Value.absent(),
    this.syncId = const Value.absent(),
    this.date = const Value.absent(),
    this.amount = const Value.absent(),
    this.accountUuid = const Value.absent(),
    this.currencyCode = const Value.absent(),
    this.merchantUuid = const Value.absent(),
    this.merchantName = const Value.absent(),
    this.locationUuid = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  TransactionsCompanion.insert({
    this.createDate = const Value.absent(),
    this.updateDate = const Value.absent(),
    this.deleteDate = const Value.absent(),
    required String uuid,
    required String name,
    this.syncId = const Value.absent(),
    required DateTime date,
    required int amount,
    required String accountUuid,
    required String currencyCode,
    this.merchantUuid = const Value.absent(),
    this.merchantName = const Value.absent(),
    this.locationUuid = const Value.absent(),
    this.rowid = const Value.absent(),
  })  : uuid = Value(uuid),
        name = Value(name),
        date = Value(date),
        amount = Value(amount),
        accountUuid = Value(accountUuid),
        currencyCode = Value(currencyCode);
  static Insertable<TransactionsData> custom({
    Expression<DateTime>? createDate,
    Expression<DateTime>? updateDate,
    Expression<DateTime>? deleteDate,
    Expression<String>? uuid,
    Expression<String>? name,
    Expression<String>? syncId,
    Expression<DateTime>? date,
    Expression<int>? amount,
    Expression<String>? accountUuid,
    Expression<String>? currencyCode,
    Expression<String>? merchantUuid,
    Expression<String>? merchantName,
    Expression<String>? locationUuid,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (createDate != null) 'create_date': createDate,
      if (updateDate != null) 'update_date': updateDate,
      if (deleteDate != null) 'delete_date': deleteDate,
      if (uuid != null) 'uuid': uuid,
      if (name != null) 'name': name,
      if (syncId != null) 'sync_id': syncId,
      if (date != null) 'date': date,
      if (amount != null) 'amount': amount,
      if (accountUuid != null) 'account_uuid': accountUuid,
      if (currencyCode != null) 'currency_code': currencyCode,
      if (merchantUuid != null) 'merchant_uuid': merchantUuid,
      if (merchantName != null) 'merchant_name': merchantName,
      if (locationUuid != null) 'location_uuid': locationUuid,
      if (rowid != null) 'rowid': rowid,
    });
  }

  TransactionsCompanion copyWith(
      {Value<DateTime>? createDate,
      Value<DateTime>? updateDate,
      Value<DateTime?>? deleteDate,
      Value<String>? uuid,
      Value<String>? name,
      Value<String?>? syncId,
      Value<DateTime>? date,
      Value<int>? amount,
      Value<String>? accountUuid,
      Value<String>? currencyCode,
      Value<String?>? merchantUuid,
      Value<String?>? merchantName,
      Value<String?>? locationUuid,
      Value<int>? rowid}) {
    return TransactionsCompanion(
      createDate: createDate ?? this.createDate,
      updateDate: updateDate ?? this.updateDate,
      deleteDate: deleteDate ?? this.deleteDate,
      uuid: uuid ?? this.uuid,
      name: name ?? this.name,
      syncId: syncId ?? this.syncId,
      date: date ?? this.date,
      amount: amount ?? this.amount,
      accountUuid: accountUuid ?? this.accountUuid,
      currencyCode: currencyCode ?? this.currencyCode,
      merchantUuid: merchantUuid ?? this.merchantUuid,
      merchantName: merchantName ?? this.merchantName,
      locationUuid: locationUuid ?? this.locationUuid,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (createDate.present) {
      map['create_date'] = Variable<DateTime>(createDate.value);
    }
    if (updateDate.present) {
      map['update_date'] = Variable<DateTime>(updateDate.value);
    }
    if (deleteDate.present) {
      map['delete_date'] = Variable<DateTime>(deleteDate.value);
    }
    if (uuid.present) {
      map['uuid'] = Variable<String>(uuid.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (syncId.present) {
      map['sync_id'] = Variable<String>(syncId.value);
    }
    if (date.present) {
      map['date'] = Variable<DateTime>(date.value);
    }
    if (amount.present) {
      map['amount'] = Variable<int>(amount.value);
    }
    if (accountUuid.present) {
      map['account_uuid'] = Variable<String>(accountUuid.value);
    }
    if (currencyCode.present) {
      map['currency_code'] = Variable<String>(currencyCode.value);
    }
    if (merchantUuid.present) {
      map['merchant_uuid'] = Variable<String>(merchantUuid.value);
    }
    if (merchantName.present) {
      map['merchant_name'] = Variable<String>(merchantName.value);
    }
    if (locationUuid.present) {
      map['location_uuid'] = Variable<String>(locationUuid.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('TransactionsCompanion(')
          ..write('createDate: $createDate, ')
          ..write('updateDate: $updateDate, ')
          ..write('deleteDate: $deleteDate, ')
          ..write('uuid: $uuid, ')
          ..write('name: $name, ')
          ..write('syncId: $syncId, ')
          ..write('date: $date, ')
          ..write('amount: $amount, ')
          ..write('accountUuid: $accountUuid, ')
          ..write('currencyCode: $currencyCode, ')
          ..write('merchantUuid: $merchantUuid, ')
          ..write('merchantName: $merchantName, ')
          ..write('locationUuid: $locationUuid, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class TransactionSplits extends Table
    with TableInfo<TransactionSplits, TransactionSplitsData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  TransactionSplits(this.attachedDatabase, [this._alias]);
  late final GeneratedColumn<String> uuid = GeneratedColumn<String>(
      'uuid', aliasedName, false,
      type: DriftSqlType.string,
      requiredDuringInsert: true,
      defaultConstraints: GeneratedColumn.constraintIsAlways(
          'REFERENCES transactions (uuid) ON DELETE CASCADE'));
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  late final GeneratedColumn<String> name =
      GeneratedColumn<String>('name', aliasedName, false,
          additionalChecks: GeneratedColumn.checkTextLength(
            minTextLength: 1,
          ),
          type: DriftSqlType.string,
          requiredDuringInsert: true);
  late final GeneratedColumn<int> amount = GeneratedColumn<int>(
      'amount', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns => [uuid, id, name, amount];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'transaction_splits';
  @override
  Set<GeneratedColumn> get $primaryKey => {uuid, id};
  @override
  TransactionSplitsData map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return TransactionSplitsData(
      uuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}uuid'])!,
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      name: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}name'])!,
      amount: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}amount'])!,
    );
  }

  @override
  TransactionSplits createAlias(String alias) {
    return TransactionSplits(attachedDatabase, alias);
  }
}

class TransactionSplitsData extends DataClass
    implements Insertable<TransactionSplitsData> {
  final String uuid;
  final int id;
  final String name;
  final int amount;
  const TransactionSplitsData(
      {required this.uuid,
      required this.id,
      required this.name,
      required this.amount});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['uuid'] = Variable<String>(uuid);
    map['id'] = Variable<int>(id);
    map['name'] = Variable<String>(name);
    map['amount'] = Variable<int>(amount);
    return map;
  }

  TransactionSplitsCompanion toCompanion(bool nullToAbsent) {
    return TransactionSplitsCompanion(
      uuid: Value(uuid),
      id: Value(id),
      name: Value(name),
      amount: Value(amount),
    );
  }

  factory TransactionSplitsData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return TransactionSplitsData(
      uuid: serializer.fromJson<String>(json['uuid']),
      id: serializer.fromJson<int>(json['id']),
      name: serializer.fromJson<String>(json['name']),
      amount: serializer.fromJson<int>(json['amount']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'uuid': serializer.toJson<String>(uuid),
      'id': serializer.toJson<int>(id),
      'name': serializer.toJson<String>(name),
      'amount': serializer.toJson<int>(amount),
    };
  }

  TransactionSplitsData copyWith(
          {String? uuid, int? id, String? name, int? amount}) =>
      TransactionSplitsData(
        uuid: uuid ?? this.uuid,
        id: id ?? this.id,
        name: name ?? this.name,
        amount: amount ?? this.amount,
      );
  TransactionSplitsData copyWithCompanion(TransactionSplitsCompanion data) {
    return TransactionSplitsData(
      uuid: data.uuid.present ? data.uuid.value : this.uuid,
      id: data.id.present ? data.id.value : this.id,
      name: data.name.present ? data.name.value : this.name,
      amount: data.amount.present ? data.amount.value : this.amount,
    );
  }

  @override
  String toString() {
    return (StringBuffer('TransactionSplitsData(')
          ..write('uuid: $uuid, ')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('amount: $amount')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(uuid, id, name, amount);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is TransactionSplitsData &&
          other.uuid == this.uuid &&
          other.id == this.id &&
          other.name == this.name &&
          other.amount == this.amount);
}

class TransactionSplitsCompanion
    extends UpdateCompanion<TransactionSplitsData> {
  final Value<String> uuid;
  final Value<int> id;
  final Value<String> name;
  final Value<int> amount;
  final Value<int> rowid;
  const TransactionSplitsCompanion({
    this.uuid = const Value.absent(),
    this.id = const Value.absent(),
    this.name = const Value.absent(),
    this.amount = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  TransactionSplitsCompanion.insert({
    required String uuid,
    required int id,
    required String name,
    required int amount,
    this.rowid = const Value.absent(),
  })  : uuid = Value(uuid),
        id = Value(id),
        name = Value(name),
        amount = Value(amount);
  static Insertable<TransactionSplitsData> custom({
    Expression<String>? uuid,
    Expression<int>? id,
    Expression<String>? name,
    Expression<int>? amount,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (uuid != null) 'uuid': uuid,
      if (id != null) 'id': id,
      if (name != null) 'name': name,
      if (amount != null) 'amount': amount,
      if (rowid != null) 'rowid': rowid,
    });
  }

  TransactionSplitsCompanion copyWith(
      {Value<String>? uuid,
      Value<int>? id,
      Value<String>? name,
      Value<int>? amount,
      Value<int>? rowid}) {
    return TransactionSplitsCompanion(
      uuid: uuid ?? this.uuid,
      id: id ?? this.id,
      name: name ?? this.name,
      amount: amount ?? this.amount,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (uuid.present) {
      map['uuid'] = Variable<String>(uuid.value);
    }
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (amount.present) {
      map['amount'] = Variable<int>(amount.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('TransactionSplitsCompanion(')
          ..write('uuid: $uuid, ')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('amount: $amount, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class TransactionCategories extends Table
    with TableInfo<TransactionCategories, TransactionCategoriesData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  TransactionCategories(this.attachedDatabase, [this._alias]);
  late final GeneratedColumn<String> uuid = GeneratedColumn<String>(
      'uuid', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  late final GeneratedColumn<int> splitId = GeneratedColumn<int>(
      'split_id', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  late final GeneratedColumn<String> categoryUuid = GeneratedColumn<String>(
      'category_uuid', aliasedName, false,
      type: DriftSqlType.string,
      requiredDuringInsert: true,
      defaultConstraints: GeneratedColumn.constraintIsAlways(
          'REFERENCES categories (uuid) ON DELETE CASCADE'));
  @override
  List<GeneratedColumn> get $columns => [uuid, splitId, categoryUuid];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'transaction_categories';
  @override
  Set<GeneratedColumn> get $primaryKey => {uuid, splitId, categoryUuid};
  @override
  TransactionCategoriesData map(Map<String, dynamic> data,
      {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return TransactionCategoriesData(
      uuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}uuid'])!,
      splitId: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}split_id'])!,
      categoryUuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}category_uuid'])!,
    );
  }

  @override
  TransactionCategories createAlias(String alias) {
    return TransactionCategories(attachedDatabase, alias);
  }

  @override
  List<String> get customConstraints => const [
        'FOREIGN KEY (uuid, split_id) REFERENCES transaction_splits(uuid, id) ON DELETE CASCADE ON UPDATE CASCADE'
      ];
}

class TransactionCategoriesData extends DataClass
    implements Insertable<TransactionCategoriesData> {
  final String uuid;
  final int splitId;
  final String categoryUuid;
  const TransactionCategoriesData(
      {required this.uuid, required this.splitId, required this.categoryUuid});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['uuid'] = Variable<String>(uuid);
    map['split_id'] = Variable<int>(splitId);
    map['category_uuid'] = Variable<String>(categoryUuid);
    return map;
  }

  TransactionCategoriesCompanion toCompanion(bool nullToAbsent) {
    return TransactionCategoriesCompanion(
      uuid: Value(uuid),
      splitId: Value(splitId),
      categoryUuid: Value(categoryUuid),
    );
  }

  factory TransactionCategoriesData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return TransactionCategoriesData(
      uuid: serializer.fromJson<String>(json['uuid']),
      splitId: serializer.fromJson<int>(json['splitId']),
      categoryUuid: serializer.fromJson<String>(json['categoryUuid']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'uuid': serializer.toJson<String>(uuid),
      'splitId': serializer.toJson<int>(splitId),
      'categoryUuid': serializer.toJson<String>(categoryUuid),
    };
  }

  TransactionCategoriesData copyWith(
          {String? uuid, int? splitId, String? categoryUuid}) =>
      TransactionCategoriesData(
        uuid: uuid ?? this.uuid,
        splitId: splitId ?? this.splitId,
        categoryUuid: categoryUuid ?? this.categoryUuid,
      );
  TransactionCategoriesData copyWithCompanion(
      TransactionCategoriesCompanion data) {
    return TransactionCategoriesData(
      uuid: data.uuid.present ? data.uuid.value : this.uuid,
      splitId: data.splitId.present ? data.splitId.value : this.splitId,
      categoryUuid: data.categoryUuid.present
          ? data.categoryUuid.value
          : this.categoryUuid,
    );
  }

  @override
  String toString() {
    return (StringBuffer('TransactionCategoriesData(')
          ..write('uuid: $uuid, ')
          ..write('splitId: $splitId, ')
          ..write('categoryUuid: $categoryUuid')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(uuid, splitId, categoryUuid);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is TransactionCategoriesData &&
          other.uuid == this.uuid &&
          other.splitId == this.splitId &&
          other.categoryUuid == this.categoryUuid);
}

class TransactionCategoriesCompanion
    extends UpdateCompanion<TransactionCategoriesData> {
  final Value<String> uuid;
  final Value<int> splitId;
  final Value<String> categoryUuid;
  final Value<int> rowid;
  const TransactionCategoriesCompanion({
    this.uuid = const Value.absent(),
    this.splitId = const Value.absent(),
    this.categoryUuid = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  TransactionCategoriesCompanion.insert({
    required String uuid,
    required int splitId,
    required String categoryUuid,
    this.rowid = const Value.absent(),
  })  : uuid = Value(uuid),
        splitId = Value(splitId),
        categoryUuid = Value(categoryUuid);
  static Insertable<TransactionCategoriesData> custom({
    Expression<String>? uuid,
    Expression<int>? splitId,
    Expression<String>? categoryUuid,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (uuid != null) 'uuid': uuid,
      if (splitId != null) 'split_id': splitId,
      if (categoryUuid != null) 'category_uuid': categoryUuid,
      if (rowid != null) 'rowid': rowid,
    });
  }

  TransactionCategoriesCompanion copyWith(
      {Value<String>? uuid,
      Value<int>? splitId,
      Value<String>? categoryUuid,
      Value<int>? rowid}) {
    return TransactionCategoriesCompanion(
      uuid: uuid ?? this.uuid,
      splitId: splitId ?? this.splitId,
      categoryUuid: categoryUuid ?? this.categoryUuid,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (uuid.present) {
      map['uuid'] = Variable<String>(uuid.value);
    }
    if (splitId.present) {
      map['split_id'] = Variable<int>(splitId.value);
    }
    if (categoryUuid.present) {
      map['category_uuid'] = Variable<String>(categoryUuid.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('TransactionCategoriesCompanion(')
          ..write('uuid: $uuid, ')
          ..write('splitId: $splitId, ')
          ..write('categoryUuid: $categoryUuid, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class TransactionTags extends Table
    with TableInfo<TransactionTags, TransactionTagsData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  TransactionTags(this.attachedDatabase, [this._alias]);
  late final GeneratedColumn<String> uuid = GeneratedColumn<String>(
      'uuid', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  late final GeneratedColumn<int> splitId = GeneratedColumn<int>(
      'split_id', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  late final GeneratedColumn<String> tag =
      GeneratedColumn<String>('tag', aliasedName, false,
          additionalChecks: GeneratedColumn.checkTextLength(
            minTextLength: 1,
          ),
          type: DriftSqlType.string,
          requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns => [uuid, splitId, tag];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'transaction_tags';
  @override
  Set<GeneratedColumn> get $primaryKey => {uuid, splitId, tag};
  @override
  TransactionTagsData map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return TransactionTagsData(
      uuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}uuid'])!,
      splitId: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}split_id'])!,
      tag: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}tag'])!,
    );
  }

  @override
  TransactionTags createAlias(String alias) {
    return TransactionTags(attachedDatabase, alias);
  }

  @override
  List<String> get customConstraints => const [
        'FOREIGN KEY (uuid, split_id) REFERENCES transaction_splits(uuid, id) ON DELETE CASCADE ON UPDATE CASCADE'
      ];
}

class TransactionTagsData extends DataClass
    implements Insertable<TransactionTagsData> {
  final String uuid;
  final int splitId;
  final String tag;
  const TransactionTagsData(
      {required this.uuid, required this.splitId, required this.tag});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['uuid'] = Variable<String>(uuid);
    map['split_id'] = Variable<int>(splitId);
    map['tag'] = Variable<String>(tag);
    return map;
  }

  TransactionTagsCompanion toCompanion(bool nullToAbsent) {
    return TransactionTagsCompanion(
      uuid: Value(uuid),
      splitId: Value(splitId),
      tag: Value(tag),
    );
  }

  factory TransactionTagsData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return TransactionTagsData(
      uuid: serializer.fromJson<String>(json['uuid']),
      splitId: serializer.fromJson<int>(json['splitId']),
      tag: serializer.fromJson<String>(json['tag']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'uuid': serializer.toJson<String>(uuid),
      'splitId': serializer.toJson<int>(splitId),
      'tag': serializer.toJson<String>(tag),
    };
  }

  TransactionTagsData copyWith({String? uuid, int? splitId, String? tag}) =>
      TransactionTagsData(
        uuid: uuid ?? this.uuid,
        splitId: splitId ?? this.splitId,
        tag: tag ?? this.tag,
      );
  TransactionTagsData copyWithCompanion(TransactionTagsCompanion data) {
    return TransactionTagsData(
      uuid: data.uuid.present ? data.uuid.value : this.uuid,
      splitId: data.splitId.present ? data.splitId.value : this.splitId,
      tag: data.tag.present ? data.tag.value : this.tag,
    );
  }

  @override
  String toString() {
    return (StringBuffer('TransactionTagsData(')
          ..write('uuid: $uuid, ')
          ..write('splitId: $splitId, ')
          ..write('tag: $tag')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(uuid, splitId, tag);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is TransactionTagsData &&
          other.uuid == this.uuid &&
          other.splitId == this.splitId &&
          other.tag == this.tag);
}

class TransactionTagsCompanion extends UpdateCompanion<TransactionTagsData> {
  final Value<String> uuid;
  final Value<int> splitId;
  final Value<String> tag;
  final Value<int> rowid;
  const TransactionTagsCompanion({
    this.uuid = const Value.absent(),
    this.splitId = const Value.absent(),
    this.tag = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  TransactionTagsCompanion.insert({
    required String uuid,
    required int splitId,
    required String tag,
    this.rowid = const Value.absent(),
  })  : uuid = Value(uuid),
        splitId = Value(splitId),
        tag = Value(tag);
  static Insertable<TransactionTagsData> custom({
    Expression<String>? uuid,
    Expression<int>? splitId,
    Expression<String>? tag,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (uuid != null) 'uuid': uuid,
      if (splitId != null) 'split_id': splitId,
      if (tag != null) 'tag': tag,
      if (rowid != null) 'rowid': rowid,
    });
  }

  TransactionTagsCompanion copyWith(
      {Value<String>? uuid,
      Value<int>? splitId,
      Value<String>? tag,
      Value<int>? rowid}) {
    return TransactionTagsCompanion(
      uuid: uuid ?? this.uuid,
      splitId: splitId ?? this.splitId,
      tag: tag ?? this.tag,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (uuid.present) {
      map['uuid'] = Variable<String>(uuid.value);
    }
    if (splitId.present) {
      map['split_id'] = Variable<int>(splitId.value);
    }
    if (tag.present) {
      map['tag'] = Variable<String>(tag.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('TransactionTagsCompanion(')
          ..write('uuid: $uuid, ')
          ..write('splitId: $splitId, ')
          ..write('tag: $tag, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class TransactionTransfers extends Table
    with TableInfo<TransactionTransfers, TransactionTransfersData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  TransactionTransfers(this.attachedDatabase, [this._alias]);
  late final GeneratedColumn<String> sourceUuid = GeneratedColumn<String>(
      'source_uuid', aliasedName, false,
      type: DriftSqlType.string,
      requiredDuringInsert: true,
      defaultConstraints: GeneratedColumn.constraintIsAlways(
          'REFERENCES transactions (uuid) ON DELETE CASCADE'));
  late final GeneratedColumn<String> destinationUuid = GeneratedColumn<String>(
      'destination_uuid', aliasedName, false,
      type: DriftSqlType.string,
      requiredDuringInsert: true,
      defaultConstraints: GeneratedColumn.constraintIsAlways(
          'REFERENCES transactions (uuid) ON DELETE CASCADE'));
  @override
  List<GeneratedColumn> get $columns => [sourceUuid, destinationUuid];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'transaction_transfers';
  @override
  Set<GeneratedColumn> get $primaryKey => {sourceUuid, destinationUuid};
  @override
  TransactionTransfersData map(Map<String, dynamic> data,
      {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return TransactionTransfersData(
      sourceUuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}source_uuid'])!,
      destinationUuid: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}destination_uuid'])!,
    );
  }

  @override
  TransactionTransfers createAlias(String alias) {
    return TransactionTransfers(attachedDatabase, alias);
  }
}

class TransactionTransfersData extends DataClass
    implements Insertable<TransactionTransfersData> {
  final String sourceUuid;
  final String destinationUuid;
  const TransactionTransfersData(
      {required this.sourceUuid, required this.destinationUuid});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['source_uuid'] = Variable<String>(sourceUuid);
    map['destination_uuid'] = Variable<String>(destinationUuid);
    return map;
  }

  TransactionTransfersCompanion toCompanion(bool nullToAbsent) {
    return TransactionTransfersCompanion(
      sourceUuid: Value(sourceUuid),
      destinationUuid: Value(destinationUuid),
    );
  }

  factory TransactionTransfersData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return TransactionTransfersData(
      sourceUuid: serializer.fromJson<String>(json['sourceUuid']),
      destinationUuid: serializer.fromJson<String>(json['destinationUuid']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'sourceUuid': serializer.toJson<String>(sourceUuid),
      'destinationUuid': serializer.toJson<String>(destinationUuid),
    };
  }

  TransactionTransfersData copyWith(
          {String? sourceUuid, String? destinationUuid}) =>
      TransactionTransfersData(
        sourceUuid: sourceUuid ?? this.sourceUuid,
        destinationUuid: destinationUuid ?? this.destinationUuid,
      );
  TransactionTransfersData copyWithCompanion(
      TransactionTransfersCompanion data) {
    return TransactionTransfersData(
      sourceUuid:
          data.sourceUuid.present ? data.sourceUuid.value : this.sourceUuid,
      destinationUuid: data.destinationUuid.present
          ? data.destinationUuid.value
          : this.destinationUuid,
    );
  }

  @override
  String toString() {
    return (StringBuffer('TransactionTransfersData(')
          ..write('sourceUuid: $sourceUuid, ')
          ..write('destinationUuid: $destinationUuid')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(sourceUuid, destinationUuid);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is TransactionTransfersData &&
          other.sourceUuid == this.sourceUuid &&
          other.destinationUuid == this.destinationUuid);
}

class TransactionTransfersCompanion
    extends UpdateCompanion<TransactionTransfersData> {
  final Value<String> sourceUuid;
  final Value<String> destinationUuid;
  final Value<int> rowid;
  const TransactionTransfersCompanion({
    this.sourceUuid = const Value.absent(),
    this.destinationUuid = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  TransactionTransfersCompanion.insert({
    required String sourceUuid,
    required String destinationUuid,
    this.rowid = const Value.absent(),
  })  : sourceUuid = Value(sourceUuid),
        destinationUuid = Value(destinationUuid);
  static Insertable<TransactionTransfersData> custom({
    Expression<String>? sourceUuid,
    Expression<String>? destinationUuid,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (sourceUuid != null) 'source_uuid': sourceUuid,
      if (destinationUuid != null) 'destination_uuid': destinationUuid,
      if (rowid != null) 'rowid': rowid,
    });
  }

  TransactionTransfersCompanion copyWith(
      {Value<String>? sourceUuid,
      Value<String>? destinationUuid,
      Value<int>? rowid}) {
    return TransactionTransfersCompanion(
      sourceUuid: sourceUuid ?? this.sourceUuid,
      destinationUuid: destinationUuid ?? this.destinationUuid,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (sourceUuid.present) {
      map['source_uuid'] = Variable<String>(sourceUuid.value);
    }
    if (destinationUuid.present) {
      map['destination_uuid'] = Variable<String>(destinationUuid.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('TransactionTransfersCompanion(')
          ..write('sourceUuid: $sourceUuid, ')
          ..write('destinationUuid: $destinationUuid, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class UserSettings extends Table
    with TableInfo<UserSettings, UserSettingsData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  UserSettings(this.attachedDatabase, [this._alias]);
  late final GeneratedColumn<DateTime> createDate = GeneratedColumn<DateTime>(
      'create_date', aliasedName, false,
      type: DriftSqlType.dateTime,
      requiredDuringInsert: false,
      defaultValue: currentDateAndTime);
  late final GeneratedColumn<DateTime> updateDate = GeneratedColumn<DateTime>(
      'update_date', aliasedName, false,
      type: DriftSqlType.dateTime,
      requiredDuringInsert: false,
      defaultValue: currentDateAndTime);
  late final GeneratedColumn<DateTime> deleteDate = GeneratedColumn<DateTime>(
      'delete_date', aliasedName, true,
      type: DriftSqlType.dateTime, requiredDuringInsert: false);
  late final GeneratedColumn<String> uuid = GeneratedColumn<String>(
      'uuid', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  late final GeneratedColumn<String> navListJson = GeneratedColumn<String>(
      'nav_list_json', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  late final GeneratedColumn<String> defaultCurrencyCode =
      GeneratedColumn<String>('default_currency_code', aliasedName, true,
          additionalChecks: GeneratedColumn.checkTextLength(
              minTextLength: 3, maxTextLength: 3),
          type: DriftSqlType.string,
          requiredDuringInsert: false);
  late final GeneratedColumn<String> transactionReportUuid =
      GeneratedColumn<String>('transaction_report_uuid', aliasedName, true,
          type: DriftSqlType.string,
          requiredDuringInsert: false,
          defaultConstraints: GeneratedColumn.constraintIsAlways(
              'REFERENCES reports (uuid) ON DELETE SET NULL'));
  late final GeneratedColumn<String> accountReportUuid =
      GeneratedColumn<String>('account_report_uuid', aliasedName, true,
          type: DriftSqlType.string,
          requiredDuringInsert: false,
          defaultConstraints: GeneratedColumn.constraintIsAlways(
              'REFERENCES reports (uuid) ON DELETE SET NULL'));
  late final GeneratedColumn<String> merchantReportUuid =
      GeneratedColumn<String>('merchant_report_uuid', aliasedName, true,
          type: DriftSqlType.string,
          requiredDuringInsert: false,
          defaultConstraints: GeneratedColumn.constraintIsAlways(
              'REFERENCES reports (uuid) ON DELETE SET NULL'));
  late final GeneratedColumn<String> tagReportUuid = GeneratedColumn<String>(
      'tag_report_uuid', aliasedName, true,
      type: DriftSqlType.string,
      requiredDuringInsert: false,
      defaultConstraints: GeneratedColumn.constraintIsAlways(
          'REFERENCES reports (uuid) ON DELETE SET NULL'));
  @override
  List<GeneratedColumn> get $columns => [
        createDate,
        updateDate,
        deleteDate,
        uuid,
        navListJson,
        defaultCurrencyCode,
        transactionReportUuid,
        accountReportUuid,
        merchantReportUuid,
        tagReportUuid
      ];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'user_settings';
  @override
  Set<GeneratedColumn> get $primaryKey => {uuid};
  @override
  UserSettingsData map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return UserSettingsData(
      createDate: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}create_date'])!,
      updateDate: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}update_date'])!,
      deleteDate: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}delete_date']),
      uuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}uuid'])!,
      navListJson: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}nav_list_json'])!,
      defaultCurrencyCode: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}default_currency_code']),
      transactionReportUuid: attachedDatabase.typeMapping.read(
          DriftSqlType.string,
          data['${effectivePrefix}transaction_report_uuid']),
      accountReportUuid: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}account_report_uuid']),
      merchantReportUuid: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}merchant_report_uuid']),
      tagReportUuid: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}tag_report_uuid']),
    );
  }

  @override
  UserSettings createAlias(String alias) {
    return UserSettings(attachedDatabase, alias);
  }
}

class UserSettingsData extends DataClass
    implements Insertable<UserSettingsData> {
  final DateTime createDate;
  final DateTime updateDate;
  final DateTime? deleteDate;
  final String uuid;
  final String navListJson;
  final String? defaultCurrencyCode;
  final String? transactionReportUuid;
  final String? accountReportUuid;
  final String? merchantReportUuid;
  final String? tagReportUuid;
  const UserSettingsData(
      {required this.createDate,
      required this.updateDate,
      this.deleteDate,
      required this.uuid,
      required this.navListJson,
      this.defaultCurrencyCode,
      this.transactionReportUuid,
      this.accountReportUuid,
      this.merchantReportUuid,
      this.tagReportUuid});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['create_date'] = Variable<DateTime>(createDate);
    map['update_date'] = Variable<DateTime>(updateDate);
    if (!nullToAbsent || deleteDate != null) {
      map['delete_date'] = Variable<DateTime>(deleteDate);
    }
    map['uuid'] = Variable<String>(uuid);
    map['nav_list_json'] = Variable<String>(navListJson);
    if (!nullToAbsent || defaultCurrencyCode != null) {
      map['default_currency_code'] = Variable<String>(defaultCurrencyCode);
    }
    if (!nullToAbsent || transactionReportUuid != null) {
      map['transaction_report_uuid'] = Variable<String>(transactionReportUuid);
    }
    if (!nullToAbsent || accountReportUuid != null) {
      map['account_report_uuid'] = Variable<String>(accountReportUuid);
    }
    if (!nullToAbsent || merchantReportUuid != null) {
      map['merchant_report_uuid'] = Variable<String>(merchantReportUuid);
    }
    if (!nullToAbsent || tagReportUuid != null) {
      map['tag_report_uuid'] = Variable<String>(tagReportUuid);
    }
    return map;
  }

  UserSettingsCompanion toCompanion(bool nullToAbsent) {
    return UserSettingsCompanion(
      createDate: Value(createDate),
      updateDate: Value(updateDate),
      deleteDate: deleteDate == null && nullToAbsent
          ? const Value.absent()
          : Value(deleteDate),
      uuid: Value(uuid),
      navListJson: Value(navListJson),
      defaultCurrencyCode: defaultCurrencyCode == null && nullToAbsent
          ? const Value.absent()
          : Value(defaultCurrencyCode),
      transactionReportUuid: transactionReportUuid == null && nullToAbsent
          ? const Value.absent()
          : Value(transactionReportUuid),
      accountReportUuid: accountReportUuid == null && nullToAbsent
          ? const Value.absent()
          : Value(accountReportUuid),
      merchantReportUuid: merchantReportUuid == null && nullToAbsent
          ? const Value.absent()
          : Value(merchantReportUuid),
      tagReportUuid: tagReportUuid == null && nullToAbsent
          ? const Value.absent()
          : Value(tagReportUuid),
    );
  }

  factory UserSettingsData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return UserSettingsData(
      createDate: serializer.fromJson<DateTime>(json['createDate']),
      updateDate: serializer.fromJson<DateTime>(json['updateDate']),
      deleteDate: serializer.fromJson<DateTime?>(json['deleteDate']),
      uuid: serializer.fromJson<String>(json['uuid']),
      navListJson: serializer.fromJson<String>(json['navListJson']),
      defaultCurrencyCode:
          serializer.fromJson<String?>(json['defaultCurrencyCode']),
      transactionReportUuid:
          serializer.fromJson<String?>(json['transactionReportUuid']),
      accountReportUuid:
          serializer.fromJson<String?>(json['accountReportUuid']),
      merchantReportUuid:
          serializer.fromJson<String?>(json['merchantReportUuid']),
      tagReportUuid: serializer.fromJson<String?>(json['tagReportUuid']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'createDate': serializer.toJson<DateTime>(createDate),
      'updateDate': serializer.toJson<DateTime>(updateDate),
      'deleteDate': serializer.toJson<DateTime?>(deleteDate),
      'uuid': serializer.toJson<String>(uuid),
      'navListJson': serializer.toJson<String>(navListJson),
      'defaultCurrencyCode': serializer.toJson<String?>(defaultCurrencyCode),
      'transactionReportUuid':
          serializer.toJson<String?>(transactionReportUuid),
      'accountReportUuid': serializer.toJson<String?>(accountReportUuid),
      'merchantReportUuid': serializer.toJson<String?>(merchantReportUuid),
      'tagReportUuid': serializer.toJson<String?>(tagReportUuid),
    };
  }

  UserSettingsData copyWith(
          {DateTime? createDate,
          DateTime? updateDate,
          Value<DateTime?> deleteDate = const Value.absent(),
          String? uuid,
          String? navListJson,
          Value<String?> defaultCurrencyCode = const Value.absent(),
          Value<String?> transactionReportUuid = const Value.absent(),
          Value<String?> accountReportUuid = const Value.absent(),
          Value<String?> merchantReportUuid = const Value.absent(),
          Value<String?> tagReportUuid = const Value.absent()}) =>
      UserSettingsData(
        createDate: createDate ?? this.createDate,
        updateDate: updateDate ?? this.updateDate,
        deleteDate: deleteDate.present ? deleteDate.value : this.deleteDate,
        uuid: uuid ?? this.uuid,
        navListJson: navListJson ?? this.navListJson,
        defaultCurrencyCode: defaultCurrencyCode.present
            ? defaultCurrencyCode.value
            : this.defaultCurrencyCode,
        transactionReportUuid: transactionReportUuid.present
            ? transactionReportUuid.value
            : this.transactionReportUuid,
        accountReportUuid: accountReportUuid.present
            ? accountReportUuid.value
            : this.accountReportUuid,
        merchantReportUuid: merchantReportUuid.present
            ? merchantReportUuid.value
            : this.merchantReportUuid,
        tagReportUuid:
            tagReportUuid.present ? tagReportUuid.value : this.tagReportUuid,
      );
  UserSettingsData copyWithCompanion(UserSettingsCompanion data) {
    return UserSettingsData(
      createDate:
          data.createDate.present ? data.createDate.value : this.createDate,
      updateDate:
          data.updateDate.present ? data.updateDate.value : this.updateDate,
      deleteDate:
          data.deleteDate.present ? data.deleteDate.value : this.deleteDate,
      uuid: data.uuid.present ? data.uuid.value : this.uuid,
      navListJson:
          data.navListJson.present ? data.navListJson.value : this.navListJson,
      defaultCurrencyCode: data.defaultCurrencyCode.present
          ? data.defaultCurrencyCode.value
          : this.defaultCurrencyCode,
      transactionReportUuid: data.transactionReportUuid.present
          ? data.transactionReportUuid.value
          : this.transactionReportUuid,
      accountReportUuid: data.accountReportUuid.present
          ? data.accountReportUuid.value
          : this.accountReportUuid,
      merchantReportUuid: data.merchantReportUuid.present
          ? data.merchantReportUuid.value
          : this.merchantReportUuid,
      tagReportUuid: data.tagReportUuid.present
          ? data.tagReportUuid.value
          : this.tagReportUuid,
    );
  }

  @override
  String toString() {
    return (StringBuffer('UserSettingsData(')
          ..write('createDate: $createDate, ')
          ..write('updateDate: $updateDate, ')
          ..write('deleteDate: $deleteDate, ')
          ..write('uuid: $uuid, ')
          ..write('navListJson: $navListJson, ')
          ..write('defaultCurrencyCode: $defaultCurrencyCode, ')
          ..write('transactionReportUuid: $transactionReportUuid, ')
          ..write('accountReportUuid: $accountReportUuid, ')
          ..write('merchantReportUuid: $merchantReportUuid, ')
          ..write('tagReportUuid: $tagReportUuid')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(
      createDate,
      updateDate,
      deleteDate,
      uuid,
      navListJson,
      defaultCurrencyCode,
      transactionReportUuid,
      accountReportUuid,
      merchantReportUuid,
      tagReportUuid);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is UserSettingsData &&
          other.createDate == this.createDate &&
          other.updateDate == this.updateDate &&
          other.deleteDate == this.deleteDate &&
          other.uuid == this.uuid &&
          other.navListJson == this.navListJson &&
          other.defaultCurrencyCode == this.defaultCurrencyCode &&
          other.transactionReportUuid == this.transactionReportUuid &&
          other.accountReportUuid == this.accountReportUuid &&
          other.merchantReportUuid == this.merchantReportUuid &&
          other.tagReportUuid == this.tagReportUuid);
}

class UserSettingsCompanion extends UpdateCompanion<UserSettingsData> {
  final Value<DateTime> createDate;
  final Value<DateTime> updateDate;
  final Value<DateTime?> deleteDate;
  final Value<String> uuid;
  final Value<String> navListJson;
  final Value<String?> defaultCurrencyCode;
  final Value<String?> transactionReportUuid;
  final Value<String?> accountReportUuid;
  final Value<String?> merchantReportUuid;
  final Value<String?> tagReportUuid;
  final Value<int> rowid;
  const UserSettingsCompanion({
    this.createDate = const Value.absent(),
    this.updateDate = const Value.absent(),
    this.deleteDate = const Value.absent(),
    this.uuid = const Value.absent(),
    this.navListJson = const Value.absent(),
    this.defaultCurrencyCode = const Value.absent(),
    this.transactionReportUuid = const Value.absent(),
    this.accountReportUuid = const Value.absent(),
    this.merchantReportUuid = const Value.absent(),
    this.tagReportUuid = const Value.absent(),
    this.rowid = const Value.absent(),
  });
  UserSettingsCompanion.insert({
    this.createDate = const Value.absent(),
    this.updateDate = const Value.absent(),
    this.deleteDate = const Value.absent(),
    required String uuid,
    required String navListJson,
    this.defaultCurrencyCode = const Value.absent(),
    this.transactionReportUuid = const Value.absent(),
    this.accountReportUuid = const Value.absent(),
    this.merchantReportUuid = const Value.absent(),
    this.tagReportUuid = const Value.absent(),
    this.rowid = const Value.absent(),
  })  : uuid = Value(uuid),
        navListJson = Value(navListJson);
  static Insertable<UserSettingsData> custom({
    Expression<DateTime>? createDate,
    Expression<DateTime>? updateDate,
    Expression<DateTime>? deleteDate,
    Expression<String>? uuid,
    Expression<String>? navListJson,
    Expression<String>? defaultCurrencyCode,
    Expression<String>? transactionReportUuid,
    Expression<String>? accountReportUuid,
    Expression<String>? merchantReportUuid,
    Expression<String>? tagReportUuid,
    Expression<int>? rowid,
  }) {
    return RawValuesInsertable({
      if (createDate != null) 'create_date': createDate,
      if (updateDate != null) 'update_date': updateDate,
      if (deleteDate != null) 'delete_date': deleteDate,
      if (uuid != null) 'uuid': uuid,
      if (navListJson != null) 'nav_list_json': navListJson,
      if (defaultCurrencyCode != null)
        'default_currency_code': defaultCurrencyCode,
      if (transactionReportUuid != null)
        'transaction_report_uuid': transactionReportUuid,
      if (accountReportUuid != null) 'account_report_uuid': accountReportUuid,
      if (merchantReportUuid != null)
        'merchant_report_uuid': merchantReportUuid,
      if (tagReportUuid != null) 'tag_report_uuid': tagReportUuid,
      if (rowid != null) 'rowid': rowid,
    });
  }

  UserSettingsCompanion copyWith(
      {Value<DateTime>? createDate,
      Value<DateTime>? updateDate,
      Value<DateTime?>? deleteDate,
      Value<String>? uuid,
      Value<String>? navListJson,
      Value<String?>? defaultCurrencyCode,
      Value<String?>? transactionReportUuid,
      Value<String?>? accountReportUuid,
      Value<String?>? merchantReportUuid,
      Value<String?>? tagReportUuid,
      Value<int>? rowid}) {
    return UserSettingsCompanion(
      createDate: createDate ?? this.createDate,
      updateDate: updateDate ?? this.updateDate,
      deleteDate: deleteDate ?? this.deleteDate,
      uuid: uuid ?? this.uuid,
      navListJson: navListJson ?? this.navListJson,
      defaultCurrencyCode: defaultCurrencyCode ?? this.defaultCurrencyCode,
      transactionReportUuid:
          transactionReportUuid ?? this.transactionReportUuid,
      accountReportUuid: accountReportUuid ?? this.accountReportUuid,
      merchantReportUuid: merchantReportUuid ?? this.merchantReportUuid,
      tagReportUuid: tagReportUuid ?? this.tagReportUuid,
      rowid: rowid ?? this.rowid,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (createDate.present) {
      map['create_date'] = Variable<DateTime>(createDate.value);
    }
    if (updateDate.present) {
      map['update_date'] = Variable<DateTime>(updateDate.value);
    }
    if (deleteDate.present) {
      map['delete_date'] = Variable<DateTime>(deleteDate.value);
    }
    if (uuid.present) {
      map['uuid'] = Variable<String>(uuid.value);
    }
    if (navListJson.present) {
      map['nav_list_json'] = Variable<String>(navListJson.value);
    }
    if (defaultCurrencyCode.present) {
      map['default_currency_code'] =
          Variable<String>(defaultCurrencyCode.value);
    }
    if (transactionReportUuid.present) {
      map['transaction_report_uuid'] =
          Variable<String>(transactionReportUuid.value);
    }
    if (accountReportUuid.present) {
      map['account_report_uuid'] = Variable<String>(accountReportUuid.value);
    }
    if (merchantReportUuid.present) {
      map['merchant_report_uuid'] = Variable<String>(merchantReportUuid.value);
    }
    if (tagReportUuid.present) {
      map['tag_report_uuid'] = Variable<String>(tagReportUuid.value);
    }
    if (rowid.present) {
      map['rowid'] = Variable<int>(rowid.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('UserSettingsCompanion(')
          ..write('createDate: $createDate, ')
          ..write('updateDate: $updateDate, ')
          ..write('deleteDate: $deleteDate, ')
          ..write('uuid: $uuid, ')
          ..write('navListJson: $navListJson, ')
          ..write('defaultCurrencyCode: $defaultCurrencyCode, ')
          ..write('transactionReportUuid: $transactionReportUuid, ')
          ..write('accountReportUuid: $accountReportUuid, ')
          ..write('merchantReportUuid: $merchantReportUuid, ')
          ..write('tagReportUuid: $tagReportUuid, ')
          ..write('rowid: $rowid')
          ..write(')'))
        .toString();
  }
}

class DatabaseAtV2 extends GeneratedDatabase {
  DatabaseAtV2(QueryExecutor e) : super(e);
  late final Organizations organizations = Organizations(this);
  late final Accounts accounts = Accounts(this);
  late final AccountIdentifiers accountIdentifiers = AccountIdentifiers(this);
  late final AccountAlerts accountAlerts = AccountAlerts(this);
  late final Reports reports = Reports(this);
  late final CategoryGroups categoryGroups = CategoryGroups(this);
  late final Categories categories = Categories(this);
  late final CategoryAlerts categoryAlerts = CategoryAlerts(this);
  late final CategoryLimits categoryLimits = CategoryLimits(this);
  late final CategoryBudgets categoryBudgets = CategoryBudgets(this);
  late final Currencies currencies = Currencies(this);
  late final Devices devices = Devices(this);
  late final Locations locations = Locations(this);
  late final Merchants merchants = Merchants(this);
  late final ReportGroups reportGroups = ReportGroups(this);
  late final ReportGroupLayouts reportGroupLayouts = ReportGroupLayouts(this);
  late final ReportGroupRows reportGroupRows = ReportGroupRows(this);
  late final Transactions transactions = Transactions(this);
  late final TransactionSplits transactionSplits = TransactionSplits(this);
  late final TransactionCategories transactionCategories =
      TransactionCategories(this);
  late final TransactionTags transactionTags = TransactionTags(this);
  late final TransactionTransfers transactionTransfers =
      TransactionTransfers(this);
  late final UserSettings userSettings = UserSettings(this);
  late final Index transactionDate = Index('transaction_date',
      'CREATE INDEX transaction_date ON transactions (date)');
  late final Index transactionMerchant = Index('transaction_merchant',
      'CREATE INDEX transaction_merchant ON transactions (merchant_name)');
  @override
  Iterable<TableInfo<Table, Object?>> get allTables =>
      allSchemaEntities.whereType<TableInfo<Table, Object?>>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [
        organizations,
        accounts,
        accountIdentifiers,
        accountAlerts,
        reports,
        categoryGroups,
        categories,
        categoryAlerts,
        categoryLimits,
        categoryBudgets,
        currencies,
        devices,
        locations,
        merchants,
        reportGroups,
        reportGroupLayouts,
        reportGroupRows,
        transactions,
        transactionSplits,
        transactionCategories,
        transactionTags,
        transactionTransfers,
        userSettings,
        transactionDate,
        transactionMerchant
      ];
  @override
  int get schemaVersion => 2;
}
